<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_1aad15ecda650c0d2416b6f19058bb18fd89ffcbe2d81f201838faa390590703 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_75c998e18bb11ebeac9b734a50117f1579377a25cc52a07850e99f84ffc8fa20 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75c998e18bb11ebeac9b734a50117f1579377a25cc52a07850e99f84ffc8fa20->enter($__internal_75c998e18bb11ebeac9b734a50117f1579377a25cc52a07850e99f84ffc8fa20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        $__internal_052d822352a9f90853ace335771b8527e2f0d0376e5bfb70bed4e475ecb36df7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_052d822352a9f90853ace335771b8527e2f0d0376e5bfb70bed4e475ecb36df7->enter($__internal_052d822352a9f90853ace335771b8527e2f0d0376e5bfb70bed4e475ecb36df7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 2, $this->getSourceContext()); })()), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 2, $this->getSourceContext()); })()), "js", null, true);
        echo "

*/
";
        
        $__internal_75c998e18bb11ebeac9b734a50117f1579377a25cc52a07850e99f84ffc8fa20->leave($__internal_75c998e18bb11ebeac9b734a50117f1579377a25cc52a07850e99f84ffc8fa20_prof);

        
        $__internal_052d822352a9f90853ace335771b8527e2f0d0376e5bfb70bed4e475ecb36df7->leave($__internal_052d822352a9f90853ace335771b8527e2f0d0376e5bfb70bed4e475ecb36df7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.js.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.js.twig");
    }
}
