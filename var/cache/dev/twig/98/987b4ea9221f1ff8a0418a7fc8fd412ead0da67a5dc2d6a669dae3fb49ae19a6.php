<?php

/* SonataAdminBundle:CRUD:history.html.twig */
class __TwigTemplate_a62720a9f115884fae688079298ad9046db4609765dc787c8ddc20819fe00dd8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_history.html.twig", "SonataAdminBundle:CRUD:history.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_history.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed8a604154f75f3e0facd5e339127ace3007977a6492d98a4edbdc8ea8897beb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed8a604154f75f3e0facd5e339127ace3007977a6492d98a4edbdc8ea8897beb->enter($__internal_ed8a604154f75f3e0facd5e339127ace3007977a6492d98a4edbdc8ea8897beb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:history.html.twig"));

        $__internal_ae9ed15f369bdf903cb10a107998ddc455288c3bb91ab1e6ec2a49ac4a3b9c96 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae9ed15f369bdf903cb10a107998ddc455288c3bb91ab1e6ec2a49ac4a3b9c96->enter($__internal_ae9ed15f369bdf903cb10a107998ddc455288c3bb91ab1e6ec2a49ac4a3b9c96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:history.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ed8a604154f75f3e0facd5e339127ace3007977a6492d98a4edbdc8ea8897beb->leave($__internal_ed8a604154f75f3e0facd5e339127ace3007977a6492d98a4edbdc8ea8897beb_prof);

        
        $__internal_ae9ed15f369bdf903cb10a107998ddc455288c3bb91ab1e6ec2a49ac4a3b9c96->leave($__internal_ae9ed15f369bdf903cb10a107998ddc455288c3bb91ab1e6ec2a49ac4a3b9c96_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:history.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_history.html.twig' %}
", "SonataAdminBundle:CRUD:history.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/history.html.twig");
    }
}
