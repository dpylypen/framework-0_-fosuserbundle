<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_421e47e85f692f7dc42c3b61822ac9961cf2d66f9d1a2cc5b3f44276b774692b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1c2c7b3a4eaf3006e7dd644d402aef71f417f920cc482e78a1710b465bec6fcb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c2c7b3a4eaf3006e7dd644d402aef71f417f920cc482e78a1710b465bec6fcb->enter($__internal_1c2c7b3a4eaf3006e7dd644d402aef71f417f920cc482e78a1710b465bec6fcb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $__internal_05d7b0d833fa7fb3e732ca3594c781972b92f7e646b353abe062321835280d59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_05d7b0d833fa7fb3e732ca3594c781972b92f7e646b353abe062321835280d59->enter($__internal_05d7b0d833fa7fb3e732ca3594c781972b92f7e646b353abe062321835280d59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_1c2c7b3a4eaf3006e7dd644d402aef71f417f920cc482e78a1710b465bec6fcb->leave($__internal_1c2c7b3a4eaf3006e7dd644d402aef71f417f920cc482e78a1710b465bec6fcb_prof);

        
        $__internal_05d7b0d833fa7fb3e732ca3594c781972b92f7e646b353abe062321835280d59->leave($__internal_05d7b0d833fa7fb3e732ca3594c781972b92f7e646b353abe062321835280d59_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_b6433e63edf20e7b5bec1824654ca123dbaf49cc09b0eb33c64188b3aaf2dd86 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b6433e63edf20e7b5bec1824654ca123dbaf49cc09b0eb33c64188b3aaf2dd86->enter($__internal_b6433e63edf20e7b5bec1824654ca123dbaf49cc09b0eb33c64188b3aaf2dd86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_e857b66012e996db6691ae63cbe67ce7a2e7671b7ac93e2c44a4332215fc00fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e857b66012e996db6691ae63cbe67ce7a2e7671b7ac93e2c44a4332215fc00fd->enter($__internal_e857b66012e996db6691ae63cbe67ce7a2e7671b7ac93e2c44a4332215fc00fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 4, $this->getSourceContext()); })()), "username", array())), "FOSUserBundle");
        
        $__internal_e857b66012e996db6691ae63cbe67ce7a2e7671b7ac93e2c44a4332215fc00fd->leave($__internal_e857b66012e996db6691ae63cbe67ce7a2e7671b7ac93e2c44a4332215fc00fd_prof);

        
        $__internal_b6433e63edf20e7b5bec1824654ca123dbaf49cc09b0eb33c64188b3aaf2dd86->leave($__internal_b6433e63edf20e7b5bec1824654ca123dbaf49cc09b0eb33c64188b3aaf2dd86_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_dd6c96a97cbf313f0cfbb74008f928b02da8a14b03061691e45f1a2305310e55 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dd6c96a97cbf313f0cfbb74008f928b02da8a14b03061691e45f1a2305310e55->enter($__internal_dd6c96a97cbf313f0cfbb74008f928b02da8a14b03061691e45f1a2305310e55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_f4c6d16b85303cce01bb7d71233db07ffd3b6c8432ec3146d162de5272c908fa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f4c6d16b85303cce01bb7d71233db07ffd3b6c8432ec3146d162de5272c908fa->enter($__internal_f4c6d16b85303cce01bb7d71233db07ffd3b6c8432ec3146d162de5272c908fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 10, $this->getSourceContext()); })()), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) || array_key_exists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new Twig_Error_Runtime('Variable "confirmationUrl" does not exist.', 10, $this->getSourceContext()); })())), "FOSUserBundle");
        echo "
";
        
        $__internal_f4c6d16b85303cce01bb7d71233db07ffd3b6c8432ec3146d162de5272c908fa->leave($__internal_f4c6d16b85303cce01bb7d71233db07ffd3b6c8432ec3146d162de5272c908fa_prof);

        
        $__internal_dd6c96a97cbf313f0cfbb74008f928b02da8a14b03061691e45f1a2305310e55->leave($__internal_dd6c96a97cbf313f0cfbb74008f928b02da8a14b03061691e45f1a2305310e55_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_10c62306a183c58556757269c88d30d7129951483d963674cc7cb9308154efba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_10c62306a183c58556757269c88d30d7129951483d963674cc7cb9308154efba->enter($__internal_10c62306a183c58556757269c88d30d7129951483d963674cc7cb9308154efba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_dbd87deb0db7333eb44e0a7d6e92e42b84036bd39b99afc0dbf07c6942ccb1ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dbd87deb0db7333eb44e0a7d6e92e42b84036bd39b99afc0dbf07c6942ccb1ee->enter($__internal_dbd87deb0db7333eb44e0a7d6e92e42b84036bd39b99afc0dbf07c6942ccb1ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_dbd87deb0db7333eb44e0a7d6e92e42b84036bd39b99afc0dbf07c6942ccb1ee->leave($__internal_dbd87deb0db7333eb44e0a7d6e92e42b84036bd39b99afc0dbf07c6942ccb1ee_prof);

        
        $__internal_10c62306a183c58556757269c88d30d7129951483d963674cc7cb9308154efba->leave($__internal_10c62306a183c58556757269c88d30d7129951483d963674cc7cb9308154efba_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}
