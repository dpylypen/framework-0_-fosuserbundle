<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_af7c61c0478b5df8d320eeb94ce6b963d1a818998d129833287caf5ff2cfe4bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c3d23c5d1b89f9ffdd457c6de197dfc8d1a30d5dca9ddd8823ce39ad2953a70c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3d23c5d1b89f9ffdd457c6de197dfc8d1a30d5dca9ddd8823ce39ad2953a70c->enter($__internal_c3d23c5d1b89f9ffdd457c6de197dfc8d1a30d5dca9ddd8823ce39ad2953a70c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $__internal_31dcb70780516a50ee3bbb5eae51cc20cf002c2ec64db701e6cecca9a5b9b32b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_31dcb70780516a50ee3bbb5eae51cc20cf002c2ec64db701e6cecca9a5b9b32b->enter($__internal_31dcb70780516a50ee3bbb5eae51cc20cf002c2ec64db701e6cecca9a5b9b32b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c3d23c5d1b89f9ffdd457c6de197dfc8d1a30d5dca9ddd8823ce39ad2953a70c->leave($__internal_c3d23c5d1b89f9ffdd457c6de197dfc8d1a30d5dca9ddd8823ce39ad2953a70c_prof);

        
        $__internal_31dcb70780516a50ee3bbb5eae51cc20cf002c2ec64db701e6cecca9a5b9b32b->leave($__internal_31dcb70780516a50ee3bbb5eae51cc20cf002c2ec64db701e6cecca9a5b9b32b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_0ecaf7d33968783994893b1edb7676d71494a590aba23403c8420cdf2e4c4301 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ecaf7d33968783994893b1edb7676d71494a590aba23403c8420cdf2e4c4301->enter($__internal_0ecaf7d33968783994893b1edb7676d71494a590aba23403c8420cdf2e4c4301_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_7e66b35f0f3b40c4b09534be4bf725c2be8f2697b9fb9ab4882f9a5d75e4a61d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e66b35f0f3b40c4b09534be4bf725c2be8f2697b9fb9ab4882f9a5d75e4a61d->enter($__internal_7e66b35f0f3b40c4b09534be4bf725c2be8f2697b9fb9ab4882f9a5d75e4a61d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_7e66b35f0f3b40c4b09534be4bf725c2be8f2697b9fb9ab4882f9a5d75e4a61d->leave($__internal_7e66b35f0f3b40c4b09534be4bf725c2be8f2697b9fb9ab4882f9a5d75e4a61d_prof);

        
        $__internal_0ecaf7d33968783994893b1edb7676d71494a590aba23403c8420cdf2e4c4301->leave($__internal_0ecaf7d33968783994893b1edb7676d71494a590aba23403c8420cdf2e4c4301_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
