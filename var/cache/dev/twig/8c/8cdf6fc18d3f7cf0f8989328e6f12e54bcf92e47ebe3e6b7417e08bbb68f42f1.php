<?php

/* SonataAdminBundle:Core:user_block.html.twig */
class __TwigTemplate_844bfc69ab148783d9fc614d0d5c2d27c26151470fefe6b1bf9eca7dc303f114 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'user_block' => array($this, 'block_user_block'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bbb02c6becda59ad1b661c9bd9b3b59d4378646bf57069831e3da787a041f89e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bbb02c6becda59ad1b661c9bd9b3b59d4378646bf57069831e3da787a041f89e->enter($__internal_bbb02c6becda59ad1b661c9bd9b3b59d4378646bf57069831e3da787a041f89e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Core:user_block.html.twig"));

        $__internal_2d88237b41f3e6eb34d9da4af5ca3c72c502cdf75009ca846872e5d986a18e2e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d88237b41f3e6eb34d9da4af5ca3c72c502cdf75009ca846872e5d986a18e2e->enter($__internal_2d88237b41f3e6eb34d9da4af5ca3c72c502cdf75009ca846872e5d986a18e2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Core:user_block.html.twig"));

        // line 1
        $this->displayBlock('user_block', $context, $blocks);
        
        $__internal_bbb02c6becda59ad1b661c9bd9b3b59d4378646bf57069831e3da787a041f89e->leave($__internal_bbb02c6becda59ad1b661c9bd9b3b59d4378646bf57069831e3da787a041f89e_prof);

        
        $__internal_2d88237b41f3e6eb34d9da4af5ca3c72c502cdf75009ca846872e5d986a18e2e->leave($__internal_2d88237b41f3e6eb34d9da4af5ca3c72c502cdf75009ca846872e5d986a18e2e_prof);

    }

    public function block_user_block($context, array $blocks = array())
    {
        $__internal_d26073851d0ea2d5fd99776c2840d5a0cc55a2bd64cab5eff890424f5afedded = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d26073851d0ea2d5fd99776c2840d5a0cc55a2bd64cab5eff890424f5afedded->enter($__internal_d26073851d0ea2d5fd99776c2840d5a0cc55a2bd64cab5eff890424f5afedded_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_block"));

        $__internal_2062a3f91d62552f0a88bcc0346c8b9ebb67e35ec08547d66b0a005a2257f718 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2062a3f91d62552f0a88bcc0346c8b9ebb67e35ec08547d66b0a005a2257f718->enter($__internal_2062a3f91d62552f0a88bcc0346c8b9ebb67e35ec08547d66b0a005a2257f718_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_block"));

        
        $__internal_2062a3f91d62552f0a88bcc0346c8b9ebb67e35ec08547d66b0a005a2257f718->leave($__internal_2062a3f91d62552f0a88bcc0346c8b9ebb67e35ec08547d66b0a005a2257f718_prof);

        
        $__internal_d26073851d0ea2d5fd99776c2840d5a0cc55a2bd64cab5eff890424f5afedded->leave($__internal_d26073851d0ea2d5fd99776c2840d5a0cc55a2bd64cab5eff890424f5afedded_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Core:user_block.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block user_block %}{# Customize this value #}{% endblock %}
", "SonataAdminBundle:Core:user_block.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/Core/user_block.html.twig");
    }
}
