<?php

/* SonataBlockBundle:Block:block_core_text.html.twig */
class __TwigTemplate_a1388127e5ba5af88c0846e26cfa4b44b365eaaa6034f2ea8660a68e19e86b8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["sonata_block"]) || array_key_exists("sonata_block", $context) ? $context["sonata_block"] : (function () { throw new Twig_Error_Runtime('Variable "sonata_block" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_core_text.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c857471e9b37381818d0ed27ef2045528687a191eab23e8fdd63b2a956eab7c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c857471e9b37381818d0ed27ef2045528687a191eab23e8fdd63b2a956eab7c8->enter($__internal_c857471e9b37381818d0ed27ef2045528687a191eab23e8fdd63b2a956eab7c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_text.html.twig"));

        $__internal_2da55a88a3d08d899bd15ebf286f8734f1ab7473dc01ff3ab8eb1b92fda7839b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2da55a88a3d08d899bd15ebf286f8734f1ab7473dc01ff3ab8eb1b92fda7839b->enter($__internal_2da55a88a3d08d899bd15ebf286f8734f1ab7473dc01ff3ab8eb1b92fda7839b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_text.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c857471e9b37381818d0ed27ef2045528687a191eab23e8fdd63b2a956eab7c8->leave($__internal_c857471e9b37381818d0ed27ef2045528687a191eab23e8fdd63b2a956eab7c8_prof);

        
        $__internal_2da55a88a3d08d899bd15ebf286f8734f1ab7473dc01ff3ab8eb1b92fda7839b->leave($__internal_2da55a88a3d08d899bd15ebf286f8734f1ab7473dc01ff3ab8eb1b92fda7839b_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_435e641163a00489a8a29669b03a93c68bfc4d2a9712a04f4544c5f4b5efd224 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_435e641163a00489a8a29669b03a93c68bfc4d2a9712a04f4544c5f4b5efd224->enter($__internal_435e641163a00489a8a29669b03a93c68bfc4d2a9712a04f4544c5f4b5efd224_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_a404f2a5a2552109390da53c976c86eb75b25ba7fae321abac5454435c2233e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a404f2a5a2552109390da53c976c86eb75b25ba7fae321abac5454435c2233e4->enter($__internal_a404f2a5a2552109390da53c976c86eb75b25ba7fae321abac5454435c2233e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        echo twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new Twig_Error_Runtime('Variable "settings" does not exist.', 15, $this->getSourceContext()); })()), "content", array());
        echo "
";
        
        $__internal_a404f2a5a2552109390da53c976c86eb75b25ba7fae321abac5454435c2233e4->leave($__internal_a404f2a5a2552109390da53c976c86eb75b25ba7fae321abac5454435c2233e4_prof);

        
        $__internal_435e641163a00489a8a29669b03a93c68bfc4d2a9712a04f4544c5f4b5efd224->leave($__internal_435e641163a00489a8a29669b03a93c68bfc4d2a9712a04f4544c5f4b5efd224_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_core_text.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    {{ settings.content|raw }}
{% endblock %}
", "SonataBlockBundle:Block:block_core_text.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/block-bundle/Resources/views/Block/block_core_text.html.twig");
    }
}
