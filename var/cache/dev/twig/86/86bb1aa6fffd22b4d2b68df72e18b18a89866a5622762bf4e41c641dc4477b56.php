<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_839fdeecc4c021775fb895e0a47061bc3174127ebb72c463a60578f8e29f6f8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_87dfd5c6e3d5d56249c363b7411879eb883fae0d6f84d4188a9aeb7eeef0e5e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87dfd5c6e3d5d56249c363b7411879eb883fae0d6f84d4188a9aeb7eeef0e5e9->enter($__internal_87dfd5c6e3d5d56249c363b7411879eb883fae0d6f84d4188a9aeb7eeef0e5e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        $__internal_9f04b0d126f4d7e588a122a026595b9bcd731240e332a6f460fcef346b6d8c83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f04b0d126f4d7e588a122a026595b9bcd731240e332a6f460fcef346b6d8c83->enter($__internal_9f04b0d126f4d7e588a122a026595b9bcd731240e332a6f460fcef346b6d8c83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_87dfd5c6e3d5d56249c363b7411879eb883fae0d6f84d4188a9aeb7eeef0e5e9->leave($__internal_87dfd5c6e3d5d56249c363b7411879eb883fae0d6f84d4188a9aeb7eeef0e5e9_prof);

        
        $__internal_9f04b0d126f4d7e588a122a026595b9bcd731240e332a6f460fcef346b6d8c83->leave($__internal_9f04b0d126f4d7e588a122a026595b9bcd731240e332a6f460fcef346b6d8c83_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.rdf.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
