<?php

/* SonataAdminBundle:CRUD:select_subclass.html.twig */
class __TwigTemplate_96fbe7b6440857093544a85ecd425c4294cb72cb68713ce0c5d0cc422c83e6ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 11
        return $this->loadTemplate((isset($context["base_template"]) || array_key_exists("base_template", $context) ? $context["base_template"] : (function () { throw new Twig_Error_Runtime('Variable "base_template" does not exist.', 11, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:select_subclass.html.twig", 11);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95c63c3cf79a22285cd82e1bdd0308db9c4ce7dcd9284c11548cf992cbcf59cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95c63c3cf79a22285cd82e1bdd0308db9c4ce7dcd9284c11548cf992cbcf59cc->enter($__internal_95c63c3cf79a22285cd82e1bdd0308db9c4ce7dcd9284c11548cf992cbcf59cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:select_subclass.html.twig"));

        $__internal_3035e449417b76d3c8813688551b7041e058a4b07d313a428a272dfc3f44b017 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3035e449417b76d3c8813688551b7041e058a4b07d313a428a272dfc3f44b017->enter($__internal_3035e449417b76d3c8813688551b7041e058a4b07d313a428a272dfc3f44b017_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:select_subclass.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_95c63c3cf79a22285cd82e1bdd0308db9c4ce7dcd9284c11548cf992cbcf59cc->leave($__internal_95c63c3cf79a22285cd82e1bdd0308db9c4ce7dcd9284c11548cf992cbcf59cc_prof);

        
        $__internal_3035e449417b76d3c8813688551b7041e058a4b07d313a428a272dfc3f44b017->leave($__internal_3035e449417b76d3c8813688551b7041e058a4b07d313a428a272dfc3f44b017_prof);

    }

    // line 13
    public function block_title($context, array $blocks = array())
    {
        $__internal_0c415052f58e20565020dd2d87a33281007589e4a5b9b7831f1f322314e9f2fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c415052f58e20565020dd2d87a33281007589e4a5b9b7831f1f322314e9f2fc->enter($__internal_0c415052f58e20565020dd2d87a33281007589e4a5b9b7831f1f322314e9f2fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_c6bbaac41afff6362c110ad54db5c95f7f6fd0b9e43bee1ff7284aaf0fb333dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c6bbaac41afff6362c110ad54db5c95f7f6fd0b9e43bee1ff7284aaf0fb333dc->enter($__internal_c6bbaac41afff6362c110ad54db5c95f7f6fd0b9e43bee1ff7284aaf0fb333dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_select_subclass", array(), "SonataAdminBundle"), "html", null, true);
        
        $__internal_c6bbaac41afff6362c110ad54db5c95f7f6fd0b9e43bee1ff7284aaf0fb333dc->leave($__internal_c6bbaac41afff6362c110ad54db5c95f7f6fd0b9e43bee1ff7284aaf0fb333dc_prof);

        
        $__internal_0c415052f58e20565020dd2d87a33281007589e4a5b9b7831f1f322314e9f2fc->leave($__internal_0c415052f58e20565020dd2d87a33281007589e4a5b9b7831f1f322314e9f2fc_prof);

    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        $__internal_3d26b53e50ef7ff24b06fe3c00239615d4dcf33c8a8da40a8faf253ad8d3021a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d26b53e50ef7ff24b06fe3c00239615d4dcf33c8a8da40a8faf253ad8d3021a->enter($__internal_3d26b53e50ef7ff24b06fe3c00239615d4dcf33c8a8da40a8faf253ad8d3021a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_86a00d3d2dc4bd0b15c16fa98badd0dc8aa64828fff6dc25d9a0f382512e77d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86a00d3d2dc4bd0b15c16fa98badd0dc8aa64828fff6dc25d9a0f382512e77d0->enter($__internal_86a00d3d2dc4bd0b15c16fa98badd0dc8aa64828fff6dc25d9a0f382512e77d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 16
        echo "    <div class=\"box box-success\">
        <div class=\"box-header\">
            <h3 class=\"box-title\">
                ";
        // line 19
        $this->displayBlock("title", $context, $blocks);
        echo "
            </h3>
        </div>
        <div class=\"box-body\">
            ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 23, $this->getSourceContext()); })()), "subclasses", array())));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["subclass"]) {
            // line 24
            echo "                <div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">
                    <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 25, $this->getSourceContext()); })()), "generateUrl", array(0 => (isset($context["action"]) || array_key_exists("action", $context) ? $context["action"] : (function () { throw new Twig_Error_Runtime('Variable "action" does not exist.', 25, $this->getSourceContext()); })()), 1 => array("subclass" => $context["subclass"])), "method"), "html", null, true);
            echo "\"
                       class=\"btn btn-app btn-block\"
                            >
                        <i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i>
                        ";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["subclass"], array(), ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["admin"] ?? null), "translationdomain", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), ($context["admin"] ?? null), "translationdomain", array()), "SonataAdminBundle")) : ("SonataAdminBundle"))), "html", null, true);
            echo "
                    </a>
                </div>
            ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 33
            echo "                <span class=\"alert alert-info\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("no_subclass_available", array(), "SonataAdminBundle"), "html", null, true);
            echo "</span>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subclass'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "            <div class=\"clearfix\"></div>
        </div>
    </div>
";
        
        $__internal_86a00d3d2dc4bd0b15c16fa98badd0dc8aa64828fff6dc25d9a0f382512e77d0->leave($__internal_86a00d3d2dc4bd0b15c16fa98badd0dc8aa64828fff6dc25d9a0f382512e77d0_prof);

        
        $__internal_3d26b53e50ef7ff24b06fe3c00239615d4dcf33c8a8da40a8faf253ad8d3021a->leave($__internal_3d26b53e50ef7ff24b06fe3c00239615d4dcf33c8a8da40a8faf253ad8d3021a_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:select_subclass.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 35,  103 => 33,  94 => 29,  87 => 25,  84 => 24,  79 => 23,  72 => 19,  67 => 16,  58 => 15,  40 => 13,  19 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% extends base_template %}

{% block title %}{{ 'title_select_subclass'|trans({}, 'SonataAdminBundle') }}{% endblock %}

{% block content %}
    <div class=\"box box-success\">
        <div class=\"box-header\">
            <h3 class=\"box-title\">
                {{ block('title') }}
            </h3>
        </div>
        <div class=\"box-body\">
            {% for subclass in admin.subclasses|keys %}
                <div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">
                    <a href=\"{{ admin.generateUrl(action, {'subclass': subclass }) }}\"
                       class=\"btn btn-app btn-block\"
                            >
                        <i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i>
                        {{ subclass|trans({}, admin.translationdomain|default('SonataAdminBundle')) }}
                    </a>
                </div>
            {% else %}
                <span class=\"alert alert-info\">{{ 'no_subclass_available'|trans({}, 'SonataAdminBundle') }}</span>
            {% endfor %}
            <div class=\"clearfix\"></div>
        </div>
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:select_subclass.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/select_subclass.html.twig");
    }
}
