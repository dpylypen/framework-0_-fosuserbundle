<?php

/* SonataAdminBundle:CRUD:show_time.html.twig */
class __TwigTemplate_892a02363d1e705d84a7ecd0b35c839910e6fee92a4e80eb4d0279f5d08c380c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_time.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ee34782b68e4bdcc995265b5578c1984f6fc06dfc5189acc727cd5fb63680700 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee34782b68e4bdcc995265b5578c1984f6fc06dfc5189acc727cd5fb63680700->enter($__internal_ee34782b68e4bdcc995265b5578c1984f6fc06dfc5189acc727cd5fb63680700_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_time.html.twig"));

        $__internal_5dc197a13c1971fc850d37f560110b43d22158064cef2003dd85d8deb1459686 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5dc197a13c1971fc850d37f560110b43d22158064cef2003dd85d8deb1459686->enter($__internal_5dc197a13c1971fc850d37f560110b43d22158064cef2003dd85d8deb1459686_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_time.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ee34782b68e4bdcc995265b5578c1984f6fc06dfc5189acc727cd5fb63680700->leave($__internal_ee34782b68e4bdcc995265b5578c1984f6fc06dfc5189acc727cd5fb63680700_prof);

        
        $__internal_5dc197a13c1971fc850d37f560110b43d22158064cef2003dd85d8deb1459686->leave($__internal_5dc197a13c1971fc850d37f560110b43d22158064cef2003dd85d8deb1459686_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_43399db2ab08c350ab8836722f72cb738fa5e0bcde9f84c4254a9f0e7710ef35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_43399db2ab08c350ab8836722f72cb738fa5e0bcde9f84c4254a9f0e7710ef35->enter($__internal_43399db2ab08c350ab8836722f72cb738fa5e0bcde9f84c4254a9f0e7710ef35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_f0389d98823072cd2ffac0dcd824ab8ade01de3b21ad81232c5c5466ad126998 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0389d98823072cd2ffac0dcd824ab8ade01de3b21ad81232c5c5466ad126998->enter($__internal_f0389d98823072cd2ffac0dcd824ab8ade01de3b21ad81232c5c5466ad126998_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twig_test_empty((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "&nbsp;";
        } else {
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 18, $this->getSourceContext()); })()), "H:i:s"), "html", null, true);
        }
        
        $__internal_f0389d98823072cd2ffac0dcd824ab8ade01de3b21ad81232c5c5466ad126998->leave($__internal_f0389d98823072cd2ffac0dcd824ab8ade01de3b21ad81232c5c5466ad126998_prof);

        
        $__internal_43399db2ab08c350ab8836722f72cb738fa5e0bcde9f84c4254a9f0e7710ef35->leave($__internal_43399db2ab08c350ab8836722f72cb738fa5e0bcde9f84c4254a9f0e7710ef35_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 18,  51 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field %}
    {%- if value is empty -%}
        &nbsp;
    {%- else -%}
        {{ value|date('H:i:s') }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:show_time.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/show_time.html.twig");
    }
}
