<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_78685f087d1bc5d2608179894c53c6d03b76cb6274bb040e8bb384a62ac5c732 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4da9a1bc0929789d788a72c2bb4782de2f4fc29bf908edec7f079a55426e72b3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4da9a1bc0929789d788a72c2bb4782de2f4fc29bf908edec7f079a55426e72b3->enter($__internal_4da9a1bc0929789d788a72c2bb4782de2f4fc29bf908edec7f079a55426e72b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        $__internal_54a3b2cecf7417509038f0173d994c668ff0c625af6fd88a2d89aa8b66a2f151 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_54a3b2cecf7417509038f0173d994c668ff0c625af6fd88a2d89aa8b66a2f151->enter($__internal_54a3b2cecf7417509038f0173d994c668ff0c625af6fd88a2d89aa8b66a2f151_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_4da9a1bc0929789d788a72c2bb4782de2f4fc29bf908edec7f079a55426e72b3->leave($__internal_4da9a1bc0929789d788a72c2bb4782de2f4fc29bf908edec7f079a55426e72b3_prof);

        
        $__internal_54a3b2cecf7417509038f0173d994c668ff0c625af6fd88a2d89aa8b66a2f151->leave($__internal_54a3b2cecf7417509038f0173d994c668ff0c625af6fd88a2d89aa8b66a2f151_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/repeated_row.html.php");
    }
}
