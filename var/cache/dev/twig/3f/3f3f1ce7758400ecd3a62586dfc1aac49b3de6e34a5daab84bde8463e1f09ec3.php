<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_7f95446ce860e4019fbea2c1db22d4c85d0c840627a6d1c85e2674bd5e042cff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6a0c7583df98f0f95a7b96c92af65a60861d03acc8f57680bf420a82ec9b2379 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a0c7583df98f0f95a7b96c92af65a60861d03acc8f57680bf420a82ec9b2379->enter($__internal_6a0c7583df98f0f95a7b96c92af65a60861d03acc8f57680bf420a82ec9b2379_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_e5bf519f3c48a4355bdc6b7f1e235aa09c95d8d96809a32cd444bf6e2f4181f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5bf519f3c48a4355bdc6b7f1e235aa09c95d8d96809a32cd444bf6e2f4181f9->enter($__internal_e5bf519f3c48a4355bdc6b7f1e235aa09c95d8d96809a32cd444bf6e2f4181f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_6a0c7583df98f0f95a7b96c92af65a60861d03acc8f57680bf420a82ec9b2379->leave($__internal_6a0c7583df98f0f95a7b96c92af65a60861d03acc8f57680bf420a82ec9b2379_prof);

        
        $__internal_e5bf519f3c48a4355bdc6b7f1e235aa09c95d8d96809a32cd444bf6e2f4181f9->leave($__internal_e5bf519f3c48a4355bdc6b7f1e235aa09c95d8d96809a32cd444bf6e2f4181f9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
