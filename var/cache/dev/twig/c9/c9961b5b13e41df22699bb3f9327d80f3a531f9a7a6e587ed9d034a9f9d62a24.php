<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_4de5778cac5bdcfa4cbdcd9f1aff2d59cd81ca0b37916b586a653d397669873b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_175690e5caf31a01de074ce8ac1bbdf4dfdecb4a6e56e6d1a5c199cf592e60ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_175690e5caf31a01de074ce8ac1bbdf4dfdecb4a6e56e6d1a5c199cf592e60ba->enter($__internal_175690e5caf31a01de074ce8ac1bbdf4dfdecb4a6e56e6d1a5c199cf592e60ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_3cde423e34e3ef489c2979ab7d244c4a2fe7b1a3ea6f184ce27b88dc20bf1eb6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3cde423e34e3ef489c2979ab7d244c4a2fe7b1a3ea6f184ce27b88dc20bf1eb6->enter($__internal_3cde423e34e3ef489c2979ab7d244c4a2fe7b1a3ea6f184ce27b88dc20bf1eb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_175690e5caf31a01de074ce8ac1bbdf4dfdecb4a6e56e6d1a5c199cf592e60ba->leave($__internal_175690e5caf31a01de074ce8ac1bbdf4dfdecb4a6e56e6d1a5c199cf592e60ba_prof);

        
        $__internal_3cde423e34e3ef489c2979ab7d244c4a2fe7b1a3ea6f184ce27b88dc20bf1eb6->leave($__internal_3cde423e34e3ef489c2979ab7d244c4a2fe7b1a3ea6f184ce27b88dc20bf1eb6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
