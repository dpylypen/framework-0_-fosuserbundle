<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_c5b1b523ca1956b18864ecd56403e158ea04ed4cfecdc42d4d2ecb1f60f667be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_79f0636444287df89b79ea8c07c3e9f91a0a0901d3ba3774e837fdc78a2481b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79f0636444287df89b79ea8c07c3e9f91a0a0901d3ba3774e837fdc78a2481b2->enter($__internal_79f0636444287df89b79ea8c07c3e9f91a0a0901d3ba3774e837fdc78a2481b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $__internal_16570b357e19135d720fb89f7d21dcd71ad2fef3e770ccc4fbbf279f7291f008 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16570b357e19135d720fb89f7d21dcd71ad2fef3e770ccc4fbbf279f7291f008->enter($__internal_16570b357e19135d720fb89f7d21dcd71ad2fef3e770ccc4fbbf279f7291f008_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_79f0636444287df89b79ea8c07c3e9f91a0a0901d3ba3774e837fdc78a2481b2->leave($__internal_79f0636444287df89b79ea8c07c3e9f91a0a0901d3ba3774e837fdc78a2481b2_prof);

        
        $__internal_16570b357e19135d720fb89f7d21dcd71ad2fef3e770ccc4fbbf279f7291f008->leave($__internal_16570b357e19135d720fb89f7d21dcd71ad2fef3e770ccc4fbbf279f7291f008_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_edee6976e25302d43d33d4bd4820677a239ac36779d3cc786e68ff02e656a557 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_edee6976e25302d43d33d4bd4820677a239ac36779d3cc786e68ff02e656a557->enter($__internal_edee6976e25302d43d33d4bd4820677a239ac36779d3cc786e68ff02e656a557_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_28bb3ed923d08ba018fb4a6899a028761396f3d9fe8a42c87816d1246c0568e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28bb3ed923d08ba018fb4a6899a028761396f3d9fe8a42c87816d1246c0568e2->enter($__internal_28bb3ed923d08ba018fb4a6899a028761396f3d9fe8a42c87816d1246c0568e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_28bb3ed923d08ba018fb4a6899a028761396f3d9fe8a42c87816d1246c0568e2->leave($__internal_28bb3ed923d08ba018fb4a6899a028761396f3d9fe8a42c87816d1246c0568e2_prof);

        
        $__internal_edee6976e25302d43d33d4bd4820677a239ac36779d3cc786e68ff02e656a557->leave($__internal_edee6976e25302d43d33d4bd4820677a239ac36779d3cc786e68ff02e656a557_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}
