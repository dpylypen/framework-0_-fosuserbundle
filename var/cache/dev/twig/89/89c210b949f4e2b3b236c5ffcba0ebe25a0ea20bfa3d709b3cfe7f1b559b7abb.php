<?php

/* SonataCoreBundle:Form:datepicker.html.twig */
class __TwigTemplate_c7e2f241f1f87bc092380a457a3917c5d7c052835c4dbd9bf8e8313e4fa401c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sonata_type_date_picker_widget_html' => array($this, 'block_sonata_type_date_picker_widget_html'),
            'sonata_type_date_picker_widget' => array($this, 'block_sonata_type_date_picker_widget'),
            'sonata_type_datetime_picker_widget_html' => array($this, 'block_sonata_type_datetime_picker_widget_html'),
            'sonata_type_datetime_picker_widget' => array($this, 'block_sonata_type_datetime_picker_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c15161681ff829b40e89db77a178c7a116d03dd6f999c62a7360123dd8ffec33 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c15161681ff829b40e89db77a178c7a116d03dd6f999c62a7360123dd8ffec33->enter($__internal_c15161681ff829b40e89db77a178c7a116d03dd6f999c62a7360123dd8ffec33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataCoreBundle:Form:datepicker.html.twig"));

        $__internal_fb051d10fc4084d686cca12fb490938121c97742ca2bd23e428ac5fd89ad865a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fb051d10fc4084d686cca12fb490938121c97742ca2bd23e428ac5fd89ad865a->enter($__internal_fb051d10fc4084d686cca12fb490938121c97742ca2bd23e428ac5fd89ad865a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataCoreBundle:Form:datepicker.html.twig"));

        // line 11
        $this->displayBlock('sonata_type_date_picker_widget_html', $context, $blocks);
        // line 22
        echo "
";
        // line 23
        $this->displayBlock('sonata_type_date_picker_widget', $context, $blocks);
        // line 39
        echo "
";
        // line 40
        $this->displayBlock('sonata_type_datetime_picker_widget_html', $context, $blocks);
        // line 51
        echo "
";
        // line 52
        $this->displayBlock('sonata_type_datetime_picker_widget', $context, $blocks);
        
        $__internal_c15161681ff829b40e89db77a178c7a116d03dd6f999c62a7360123dd8ffec33->leave($__internal_c15161681ff829b40e89db77a178c7a116d03dd6f999c62a7360123dd8ffec33_prof);

        
        $__internal_fb051d10fc4084d686cca12fb490938121c97742ca2bd23e428ac5fd89ad865a->leave($__internal_fb051d10fc4084d686cca12fb490938121c97742ca2bd23e428ac5fd89ad865a_prof);

    }

    // line 11
    public function block_sonata_type_date_picker_widget_html($context, array $blocks = array())
    {
        $__internal_17b67bfd329f9843a40078952ac7e6b7e987bdca03616de49171137475c896a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17b67bfd329f9843a40078952ac7e6b7e987bdca03616de49171137475c896a2->enter($__internal_17b67bfd329f9843a40078952ac7e6b7e987bdca03616de49171137475c896a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_date_picker_widget_html"));

        $__internal_bcba236f15bce77d90154569e4c307858395be2581dd647f516b06cd6e336c50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcba236f15bce77d90154569e4c307858395be2581dd647f516b06cd6e336c50->enter($__internal_bcba236f15bce77d90154569e4c307858395be2581dd647f516b06cd6e336c50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_date_picker_widget_html"));

        // line 12
        echo "    ";
        if ((isset($context["datepicker_use_button"]) || array_key_exists("datepicker_use_button", $context) ? $context["datepicker_use_button"] : (function () { throw new Twig_Error_Runtime('Variable "datepicker_use_button" does not exist.', 12, $this->getSourceContext()); })())) {
            // line 13
            echo "        <div class='input-group date' id='dp_";
            echo twig_escape_filter($this->env, (isset($context["id"]) || array_key_exists("id", $context) ? $context["id"] : (function () { throw new Twig_Error_Runtime('Variable "id" does not exist.', 13, $this->getSourceContext()); })()), "html", null, true);
            echo "'>
    ";
        }
        // line 15
        echo "    ";
        $context["attr"] = twig_array_merge((isset($context["attr"]) || array_key_exists("attr", $context) ? $context["attr"] : (function () { throw new Twig_Error_Runtime('Variable "attr" does not exist.', 15, $this->getSourceContext()); })()), array("data-date-format" => (isset($context["moment_format"]) || array_key_exists("moment_format", $context) ? $context["moment_format"] : (function () { throw new Twig_Error_Runtime('Variable "moment_format" does not exist.', 15, $this->getSourceContext()); })())));
        // line 16
        echo "    ";
        $this->displayBlock("date_widget", $context, $blocks);
        echo "
    ";
        // line 17
        if ((isset($context["datepicker_use_button"]) || array_key_exists("datepicker_use_button", $context) ? $context["datepicker_use_button"] : (function () { throw new Twig_Error_Runtime('Variable "datepicker_use_button" does not exist.', 17, $this->getSourceContext()); })())) {
            // line 18
            echo "            <span class=\"input-group-addon\"><span class=\"fa fa-calendar\"></span></span>
        </div>
    ";
        }
        
        $__internal_bcba236f15bce77d90154569e4c307858395be2581dd647f516b06cd6e336c50->leave($__internal_bcba236f15bce77d90154569e4c307858395be2581dd647f516b06cd6e336c50_prof);

        
        $__internal_17b67bfd329f9843a40078952ac7e6b7e987bdca03616de49171137475c896a2->leave($__internal_17b67bfd329f9843a40078952ac7e6b7e987bdca03616de49171137475c896a2_prof);

    }

    // line 23
    public function block_sonata_type_date_picker_widget($context, array $blocks = array())
    {
        $__internal_a08b490f3b61c6ad1c553299f319b7d32917ee29386ecc09df9f0c09e3d6cedf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a08b490f3b61c6ad1c553299f319b7d32917ee29386ecc09df9f0c09e3d6cedf->enter($__internal_a08b490f3b61c6ad1c553299f319b7d32917ee29386ecc09df9f0c09e3d6cedf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_date_picker_widget"));

        $__internal_4fdf97f182c389ecb5bd5ddebddd78d88f96fc1078ec7c82846d10a15f47ef9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4fdf97f182c389ecb5bd5ddebddd78d88f96fc1078ec7c82846d10a15f47ef9a->enter($__internal_4fdf97f182c389ecb5bd5ddebddd78d88f96fc1078ec7c82846d10a15f47ef9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_date_picker_widget"));

        // line 24
        echo "    ";
        ob_start();
        // line 25
        echo "        ";
        if ((isset($context["wrap_fields_with_addons"]) || array_key_exists("wrap_fields_with_addons", $context) ? $context["wrap_fields_with_addons"] : (function () { throw new Twig_Error_Runtime('Variable "wrap_fields_with_addons" does not exist.', 25, $this->getSourceContext()); })())) {
            // line 26
            echo "            <div class=\"input-group\">
                ";
            // line 27
            $this->displayBlock("sonata_type_date_picker_widget_html", $context, $blocks);
            echo "
            </div>
        ";
        } else {
            // line 30
            echo "            ";
            $this->displayBlock("sonata_type_date_picker_widget_html", $context, $blocks);
            echo "
        ";
        }
        // line 32
        echo "        <script type=\"text/javascript\">
            jQuery(function (\$) {
                \$('#";
        // line 34
        echo (((isset($context["datepicker_use_button"]) || array_key_exists("datepicker_use_button", $context) ? $context["datepicker_use_button"] : (function () { throw new Twig_Error_Runtime('Variable "datepicker_use_button" does not exist.', 34, $this->getSourceContext()); })())) ? ("dp_") : (""));
        echo twig_escape_filter($this->env, (isset($context["id"]) || array_key_exists("id", $context) ? $context["id"] : (function () { throw new Twig_Error_Runtime('Variable "id" does not exist.', 34, $this->getSourceContext()); })()), "html", null, true);
        echo "').datetimepicker(";
        echo json_encode((isset($context["dp_options"]) || array_key_exists("dp_options", $context) ? $context["dp_options"] : (function () { throw new Twig_Error_Runtime('Variable "dp_options" does not exist.', 34, $this->getSourceContext()); })()));
        echo ");
            });
        </script>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_4fdf97f182c389ecb5bd5ddebddd78d88f96fc1078ec7c82846d10a15f47ef9a->leave($__internal_4fdf97f182c389ecb5bd5ddebddd78d88f96fc1078ec7c82846d10a15f47ef9a_prof);

        
        $__internal_a08b490f3b61c6ad1c553299f319b7d32917ee29386ecc09df9f0c09e3d6cedf->leave($__internal_a08b490f3b61c6ad1c553299f319b7d32917ee29386ecc09df9f0c09e3d6cedf_prof);

    }

    // line 40
    public function block_sonata_type_datetime_picker_widget_html($context, array $blocks = array())
    {
        $__internal_5fa31f537e98a1b1808edb2cb9d9f3e774ea675ecdb459256058771625d67036 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5fa31f537e98a1b1808edb2cb9d9f3e774ea675ecdb459256058771625d67036->enter($__internal_5fa31f537e98a1b1808edb2cb9d9f3e774ea675ecdb459256058771625d67036_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_datetime_picker_widget_html"));

        $__internal_65c6d381c33df0052e703a77d76a8120fdd8825d3b089506ebad60d8df304ee0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_65c6d381c33df0052e703a77d76a8120fdd8825d3b089506ebad60d8df304ee0->enter($__internal_65c6d381c33df0052e703a77d76a8120fdd8825d3b089506ebad60d8df304ee0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_datetime_picker_widget_html"));

        // line 41
        echo "    ";
        if ((isset($context["datepicker_use_button"]) || array_key_exists("datepicker_use_button", $context) ? $context["datepicker_use_button"] : (function () { throw new Twig_Error_Runtime('Variable "datepicker_use_button" does not exist.', 41, $this->getSourceContext()); })())) {
            // line 42
            echo "        <div class='input-group date' id='dtp_";
            echo twig_escape_filter($this->env, (isset($context["id"]) || array_key_exists("id", $context) ? $context["id"] : (function () { throw new Twig_Error_Runtime('Variable "id" does not exist.', 42, $this->getSourceContext()); })()), "html", null, true);
            echo "'>
    ";
        }
        // line 44
        echo "    ";
        $context["attr"] = twig_array_merge((isset($context["attr"]) || array_key_exists("attr", $context) ? $context["attr"] : (function () { throw new Twig_Error_Runtime('Variable "attr" does not exist.', 44, $this->getSourceContext()); })()), array("data-date-format" => (isset($context["moment_format"]) || array_key_exists("moment_format", $context) ? $context["moment_format"] : (function () { throw new Twig_Error_Runtime('Variable "moment_format" does not exist.', 44, $this->getSourceContext()); })())));
        // line 45
        echo "    ";
        $this->displayBlock("datetime_widget", $context, $blocks);
        echo "
    ";
        // line 46
        if ((isset($context["datepicker_use_button"]) || array_key_exists("datepicker_use_button", $context) ? $context["datepicker_use_button"] : (function () { throw new Twig_Error_Runtime('Variable "datepicker_use_button" does not exist.', 46, $this->getSourceContext()); })())) {
            // line 47
            echo "          <span class=\"input-group-addon\"><span class=\"fa fa-calendar\"></span></span>
        </div>
    ";
        }
        
        $__internal_65c6d381c33df0052e703a77d76a8120fdd8825d3b089506ebad60d8df304ee0->leave($__internal_65c6d381c33df0052e703a77d76a8120fdd8825d3b089506ebad60d8df304ee0_prof);

        
        $__internal_5fa31f537e98a1b1808edb2cb9d9f3e774ea675ecdb459256058771625d67036->leave($__internal_5fa31f537e98a1b1808edb2cb9d9f3e774ea675ecdb459256058771625d67036_prof);

    }

    // line 52
    public function block_sonata_type_datetime_picker_widget($context, array $blocks = array())
    {
        $__internal_dc5d17b90f35011841741eeb8d1ee3797162c3ee9d0c0a5e486762894a5367de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dc5d17b90f35011841741eeb8d1ee3797162c3ee9d0c0a5e486762894a5367de->enter($__internal_dc5d17b90f35011841741eeb8d1ee3797162c3ee9d0c0a5e486762894a5367de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_datetime_picker_widget"));

        $__internal_0ee46e35bc74d17142d4412ee69ef0d710a783aef0a281e4457e2b68ac2ebc0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0ee46e35bc74d17142d4412ee69ef0d710a783aef0a281e4457e2b68ac2ebc0a->enter($__internal_0ee46e35bc74d17142d4412ee69ef0d710a783aef0a281e4457e2b68ac2ebc0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_datetime_picker_widget"));

        // line 53
        echo "    ";
        ob_start();
        // line 54
        echo "        ";
        if ((isset($context["wrap_fields_with_addons"]) || array_key_exists("wrap_fields_with_addons", $context) ? $context["wrap_fields_with_addons"] : (function () { throw new Twig_Error_Runtime('Variable "wrap_fields_with_addons" does not exist.', 54, $this->getSourceContext()); })())) {
            // line 55
            echo "            <div class=\"input-group\">
                ";
            // line 56
            $this->displayBlock("sonata_type_datetime_picker_widget_html", $context, $blocks);
            echo "
            </div>
        ";
        } else {
            // line 59
            echo "            ";
            $this->displayBlock("sonata_type_datetime_picker_widget_html", $context, $blocks);
            echo "
        ";
        }
        // line 61
        echo "        <script type=\"text/javascript\">
            jQuery(function (\$) {
                \$('#";
        // line 63
        echo (((isset($context["datepicker_use_button"]) || array_key_exists("datepicker_use_button", $context) ? $context["datepicker_use_button"] : (function () { throw new Twig_Error_Runtime('Variable "datepicker_use_button" does not exist.', 63, $this->getSourceContext()); })())) ? ("dtp_") : (""));
        echo twig_escape_filter($this->env, (isset($context["id"]) || array_key_exists("id", $context) ? $context["id"] : (function () { throw new Twig_Error_Runtime('Variable "id" does not exist.', 63, $this->getSourceContext()); })()), "html", null, true);
        echo "').datetimepicker(";
        echo json_encode((isset($context["dp_options"]) || array_key_exists("dp_options", $context) ? $context["dp_options"] : (function () { throw new Twig_Error_Runtime('Variable "dp_options" does not exist.', 63, $this->getSourceContext()); })()));
        echo ");
            });
        </script>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_0ee46e35bc74d17142d4412ee69ef0d710a783aef0a281e4457e2b68ac2ebc0a->leave($__internal_0ee46e35bc74d17142d4412ee69ef0d710a783aef0a281e4457e2b68ac2ebc0a_prof);

        
        $__internal_dc5d17b90f35011841741eeb8d1ee3797162c3ee9d0c0a5e486762894a5367de->leave($__internal_dc5d17b90f35011841741eeb8d1ee3797162c3ee9d0c0a5e486762894a5367de_prof);

    }

    public function getTemplateName()
    {
        return "SonataCoreBundle:Form:datepicker.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  222 => 63,  218 => 61,  212 => 59,  206 => 56,  203 => 55,  200 => 54,  197 => 53,  188 => 52,  175 => 47,  173 => 46,  168 => 45,  165 => 44,  159 => 42,  156 => 41,  147 => 40,  129 => 34,  125 => 32,  119 => 30,  113 => 27,  110 => 26,  107 => 25,  104 => 24,  95 => 23,  82 => 18,  80 => 17,  75 => 16,  72 => 15,  66 => 13,  63 => 12,  54 => 11,  44 => 52,  41 => 51,  39 => 40,  36 => 39,  34 => 23,  31 => 22,  29 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% block sonata_type_date_picker_widget_html %}
    {% if datepicker_use_button %}
        <div class='input-group date' id='dp_{{ id }}'>
    {% endif %}
    {% set attr = attr|merge({'data-date-format': moment_format}) %}
    {{ block('date_widget') }}
    {% if datepicker_use_button %}
            <span class=\"input-group-addon\"><span class=\"fa fa-calendar\"></span></span>
        </div>
    {% endif %}
{% endblock sonata_type_date_picker_widget_html %}

{% block sonata_type_date_picker_widget %}
    {% spaceless %}
        {% if wrap_fields_with_addons %}
            <div class=\"input-group\">
                {{ block('sonata_type_date_picker_widget_html') }}
            </div>
        {% else %}
            {{ block('sonata_type_date_picker_widget_html') }}
        {% endif %}
        <script type=\"text/javascript\">
            jQuery(function (\$) {
                \$('#{{ datepicker_use_button ? 'dp_' : '' }}{{ id }}').datetimepicker({{ dp_options|json_encode|raw }});
            });
        </script>
    {% endspaceless %}
{% endblock sonata_type_date_picker_widget %}

{% block sonata_type_datetime_picker_widget_html %}
    {% if datepicker_use_button %}
        <div class='input-group date' id='dtp_{{ id }}'>
    {% endif %}
    {% set attr = attr|merge({'data-date-format': moment_format}) %}
    {{ block('datetime_widget') }}
    {% if datepicker_use_button %}
          <span class=\"input-group-addon\"><span class=\"fa fa-calendar\"></span></span>
        </div>
    {% endif %}
{% endblock sonata_type_datetime_picker_widget_html %}

{% block sonata_type_datetime_picker_widget %}
    {% spaceless %}
        {% if wrap_fields_with_addons %}
            <div class=\"input-group\">
                {{ block('sonata_type_datetime_picker_widget_html') }}
            </div>
        {% else %}
            {{ block('sonata_type_datetime_picker_widget_html') }}
        {% endif %}
        <script type=\"text/javascript\">
            jQuery(function (\$) {
                \$('#{{ datepicker_use_button ? 'dtp_' : '' }}{{ id }}').datetimepicker({{ dp_options|json_encode|raw }});
            });
        </script>
    {% endspaceless %}
{% endblock sonata_type_datetime_picker_widget %}
", "SonataCoreBundle:Form:datepicker.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/core-bundle/Resources/views/Form/datepicker.html.twig");
    }
}
