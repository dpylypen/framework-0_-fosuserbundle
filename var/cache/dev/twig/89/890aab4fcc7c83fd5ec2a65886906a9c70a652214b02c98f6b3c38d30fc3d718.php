<?php

/* SonataAdminBundle:CRUD:list_array.html.twig */
class __TwigTemplate_e9406a8db32b143bf6f1b8a14bb333ece08106de59e66b1f04adab839e33b8a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 13
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 13, $this->getSourceContext()); })()), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_array.html.twig", 13);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6346d5c96ab45b268067e120653b756f4ba8f53294e26856b0302d24b520d920 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6346d5c96ab45b268067e120653b756f4ba8f53294e26856b0302d24b520d920->enter($__internal_6346d5c96ab45b268067e120653b756f4ba8f53294e26856b0302d24b520d920_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_array.html.twig"));

        $__internal_9e9c6eb3e5b70cda086d2bcaf5cff733bfdf40251db621f0f8f71163b5c76d64 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e9c6eb3e5b70cda086d2bcaf5cff733bfdf40251db621f0f8f71163b5c76d64->enter($__internal_9e9c6eb3e5b70cda086d2bcaf5cff733bfdf40251db621f0f8f71163b5c76d64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_array.html.twig"));

        // line 11
        $context["list"] = $this->loadTemplate("SonataAdminBundle:CRUD:base_array_macro.html.twig", "SonataAdminBundle:CRUD:list_array.html.twig", 11);
        // line 13
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6346d5c96ab45b268067e120653b756f4ba8f53294e26856b0302d24b520d920->leave($__internal_6346d5c96ab45b268067e120653b756f4ba8f53294e26856b0302d24b520d920_prof);

        
        $__internal_9e9c6eb3e5b70cda086d2bcaf5cff733bfdf40251db621f0f8f71163b5c76d64->leave($__internal_9e9c6eb3e5b70cda086d2bcaf5cff733bfdf40251db621f0f8f71163b5c76d64_prof);

    }

    // line 15
    public function block_field($context, array $blocks = array())
    {
        $__internal_8f4e320342d5c857a48b25dac5977fe4f59d6d5a5f8bf270d6a18f422027b85c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f4e320342d5c857a48b25dac5977fe4f59d6d5a5f8bf270d6a18f422027b85c->enter($__internal_8f4e320342d5c857a48b25dac5977fe4f59d6d5a5f8bf270d6a18f422027b85c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_55248788d6a11aad2a31440d11e07c6d2d26de123d928387a393a7888b9824e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55248788d6a11aad2a31440d11e07c6d2d26de123d928387a393a7888b9824e4->enter($__internal_55248788d6a11aad2a31440d11e07c6d2d26de123d928387a393a7888b9824e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 16
        echo "    ";
        echo $context["list"]->macro_render_array((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()), ( !twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "inline", array(), "any", true, true) || twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "inline", array())));
        echo "
";
        
        $__internal_55248788d6a11aad2a31440d11e07c6d2d26de123d928387a393a7888b9824e4->leave($__internal_55248788d6a11aad2a31440d11e07c6d2d26de123d928387a393a7888b9824e4_prof);

        
        $__internal_8f4e320342d5c857a48b25dac5977fe4f59d6d5a5f8bf270d6a18f422027b85c->leave($__internal_8f4e320342d5c857a48b25dac5977fe4f59d6d5a5f8bf270d6a18f422027b85c_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_array.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 16,  42 => 15,  32 => 13,  30 => 11,  18 => 13,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% import 'SonataAdminBundle:CRUD:base_array_macro.html.twig' as list %}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {{ list.render_array(value, field_description.options.inline is not defined or field_description.options.inline) }}
{% endblock %}
", "SonataAdminBundle:CRUD:list_array.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list_array.html.twig");
    }
}
