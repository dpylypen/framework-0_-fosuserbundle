<?php

/* SonataAdminBundle:CRUD:edit_boolean.html.twig */
class __TwigTemplate_3f83819b75cfd6b3aac3bd52afc8c0927ad8ffb9be9e030bf901a615a3e43eaa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'field' => array($this, 'block_field'),
            'label' => array($this, 'block_label'),
            'errors' => array($this, 'block_errors'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec346ef5c4f23cfc953c71aa6551f935f14d685f3d55755b38c6d9c7dab13fcc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec346ef5c4f23cfc953c71aa6551f935f14d685f3d55755b38c6d9c7dab13fcc->enter($__internal_ec346ef5c4f23cfc953c71aa6551f935f14d685f3d55755b38c6d9c7dab13fcc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_boolean.html.twig"));

        $__internal_e5cecd8eefe0b97123c0a5543c92b24527ff8838f2344ed5d3e1dd366877b5d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5cecd8eefe0b97123c0a5543c92b24527ff8838f2344ed5d3e1dd366877b5d7->enter($__internal_e5cecd8eefe0b97123c0a5543c92b24527ff8838f2344ed5d3e1dd366877b5d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_boolean.html.twig"));

        // line 11
        echo "
<div>
    <div class=\"sonata-ba-field ";
        // line 13
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 13, $this->getSourceContext()); })()), "vars", array()), "errors", array())) > 0)) {
            echo "sonata-ba-field-error";
        }
        echo "\">
        ";
        // line 14
        $this->displayBlock('field', $context, $blocks);
        // line 15
        echo "        ";
        $this->displayBlock('label', $context, $blocks);
        // line 23
        echo "
        <div class=\"sonata-ba-field-error-messages\">
            ";
        // line 25
        $this->displayBlock('errors', $context, $blocks);
        // line 26
        echo "        </div>

    </div>
</div>
";
        
        $__internal_ec346ef5c4f23cfc953c71aa6551f935f14d685f3d55755b38c6d9c7dab13fcc->leave($__internal_ec346ef5c4f23cfc953c71aa6551f935f14d685f3d55755b38c6d9c7dab13fcc_prof);

        
        $__internal_e5cecd8eefe0b97123c0a5543c92b24527ff8838f2344ed5d3e1dd366877b5d7->leave($__internal_e5cecd8eefe0b97123c0a5543c92b24527ff8838f2344ed5d3e1dd366877b5d7_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_56c39c7a46088e7d1197fc456aff098fc2c94d51baee109d1d0334e907d642ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56c39c7a46088e7d1197fc456aff098fc2c94d51baee109d1d0334e907d642ed->enter($__internal_56c39c7a46088e7d1197fc456aff098fc2c94d51baee109d1d0334e907d642ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_41402e300662a9df00886017d4ac0c6f0b60b7d949e0611779049e5d56842dc6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41402e300662a9df00886017d4ac0c6f0b60b7d949e0611779049e5d56842dc6->enter($__internal_41402e300662a9df00886017d4ac0c6f0b60b7d949e0611779049e5d56842dc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 14, $this->getSourceContext()); })()), 'widget');
        
        $__internal_41402e300662a9df00886017d4ac0c6f0b60b7d949e0611779049e5d56842dc6->leave($__internal_41402e300662a9df00886017d4ac0c6f0b60b7d949e0611779049e5d56842dc6_prof);

        
        $__internal_56c39c7a46088e7d1197fc456aff098fc2c94d51baee109d1d0334e907d642ed->leave($__internal_56c39c7a46088e7d1197fc456aff098fc2c94d51baee109d1d0334e907d642ed_prof);

    }

    // line 15
    public function block_label($context, array $blocks = array())
    {
        $__internal_5045a9670b8cbb9283388259d68cde3b73c3a94e0321a83c8191f0b2df497e2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5045a9670b8cbb9283388259d68cde3b73c3a94e0321a83c8191f0b2df497e2e->enter($__internal_5045a9670b8cbb9283388259d68cde3b73c3a94e0321a83c8191f0b2df497e2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_3bcff5943cebddbb1f5a8a5626325f9fc714277fbebb13b98f960b06f9dcc495 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3bcff5943cebddbb1f5a8a5626325f9fc714277fbebb13b98f960b06f9dcc495->enter($__internal_3bcff5943cebddbb1f5a8a5626325f9fc714277fbebb13b98f960b06f9dcc495_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        // line 16
        echo "            ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "name", array(), "any", true, true)) {
            // line 17
            echo "                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 17, $this->getSourceContext()); })()), 'label', (twig_test_empty($_label_ = twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 17, $this->getSourceContext()); })()), "options", array()), "name", array())) ? array() : array("label" => $_label_)));
            echo "
            ";
        } else {
            // line 19
            echo "                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 19, $this->getSourceContext()); })()), 'label');
            echo "
            ";
        }
        // line 21
        echo "            <br>
        ";
        
        $__internal_3bcff5943cebddbb1f5a8a5626325f9fc714277fbebb13b98f960b06f9dcc495->leave($__internal_3bcff5943cebddbb1f5a8a5626325f9fc714277fbebb13b98f960b06f9dcc495_prof);

        
        $__internal_5045a9670b8cbb9283388259d68cde3b73c3a94e0321a83c8191f0b2df497e2e->leave($__internal_5045a9670b8cbb9283388259d68cde3b73c3a94e0321a83c8191f0b2df497e2e_prof);

    }

    // line 25
    public function block_errors($context, array $blocks = array())
    {
        $__internal_535602daaaaf9c5ae2f4eb0a9992a1694e1f054b74cc4ac9d813d95b9b2a0882 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_535602daaaaf9c5ae2f4eb0a9992a1694e1f054b74cc4ac9d813d95b9b2a0882->enter($__internal_535602daaaaf9c5ae2f4eb0a9992a1694e1f054b74cc4ac9d813d95b9b2a0882_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "errors"));

        $__internal_9271dadf74ef54c60b02978a8f7b507bcb4a102afba2f6347409f8fae8f7b00f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9271dadf74ef54c60b02978a8f7b507bcb4a102afba2f6347409f8fae8f7b00f->enter($__internal_9271dadf74ef54c60b02978a8f7b507bcb4a102afba2f6347409f8fae8f7b00f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "errors"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 25, $this->getSourceContext()); })()), 'errors');
        
        $__internal_9271dadf74ef54c60b02978a8f7b507bcb4a102afba2f6347409f8fae8f7b00f->leave($__internal_9271dadf74ef54c60b02978a8f7b507bcb4a102afba2f6347409f8fae8f7b00f_prof);

        
        $__internal_535602daaaaf9c5ae2f4eb0a9992a1694e1f054b74cc4ac9d813d95b9b2a0882->leave($__internal_535602daaaaf9c5ae2f4eb0a9992a1694e1f054b74cc4ac9d813d95b9b2a0882_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_boolean.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 25,  105 => 21,  99 => 19,  93 => 17,  90 => 16,  81 => 15,  63 => 14,  49 => 26,  47 => 25,  43 => 23,  40 => 15,  38 => 14,  32 => 13,  28 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<div>
    <div class=\"sonata-ba-field {% if field_element.vars.errors|length > 0 %}sonata-ba-field-error{% endif %}\">
        {% block field %}{{ form_widget(field_element) }}{% endblock %}
        {% block label %}
            {% if field_description.options.name is defined %}
                {{ form_label(field_element, field_description.options.name) }}
            {% else %}
                {{ form_label(field_element) }}
            {% endif %}
            <br>
        {% endblock %}

        <div class=\"sonata-ba-field-error-messages\">
            {% block errors %}{{ form_errors(field_element) }}{% endblock %}
        </div>

    </div>
</div>
", "SonataAdminBundle:CRUD:edit_boolean.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/edit_boolean.html.twig");
    }
}
