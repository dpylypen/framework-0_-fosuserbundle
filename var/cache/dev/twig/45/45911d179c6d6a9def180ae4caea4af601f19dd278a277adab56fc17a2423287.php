<?php

/* SonataAdminBundle:CRUD:base_show_compare.html.twig */
class __TwigTemplate_dcadade28a005269dd20b98e55bb7b1f465040839969985566b0fc08d18173f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show.html.twig", "SonataAdminBundle:CRUD:base_show_compare.html.twig", 12);
        $this->blocks = array(
            'show_field' => array($this, 'block_show_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46eec6118c266568637cda627dc6e99be8906f5e75abdb7a9b76a0c999b965ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_46eec6118c266568637cda627dc6e99be8906f5e75abdb7a9b76a0c999b965ca->enter($__internal_46eec6118c266568637cda627dc6e99be8906f5e75abdb7a9b76a0c999b965ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_show_compare.html.twig"));

        $__internal_86aecc0902cfdba1a06ff8fb14a42b10659792fc6727703d9825741158c5f902 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86aecc0902cfdba1a06ff8fb14a42b10659792fc6727703d9825741158c5f902->enter($__internal_86aecc0902cfdba1a06ff8fb14a42b10659792fc6727703d9825741158c5f902_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_show_compare.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_46eec6118c266568637cda627dc6e99be8906f5e75abdb7a9b76a0c999b965ca->leave($__internal_46eec6118c266568637cda627dc6e99be8906f5e75abdb7a9b76a0c999b965ca_prof);

        
        $__internal_86aecc0902cfdba1a06ff8fb14a42b10659792fc6727703d9825741158c5f902->leave($__internal_86aecc0902cfdba1a06ff8fb14a42b10659792fc6727703d9825741158c5f902_prof);

    }

    // line 14
    public function block_show_field($context, array $blocks = array())
    {
        $__internal_a8e9d83b82ef435141740d9b58b715ed51b80f0ec645839fc05fa6ca0b6a8d45 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8e9d83b82ef435141740d9b58b715ed51b80f0ec645839fc05fa6ca0b6a8d45->enter($__internal_a8e9d83b82ef435141740d9b58b715ed51b80f0ec645839fc05fa6ca0b6a8d45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_field"));

        $__internal_668d13d5adbdd4f8d3d5d98ca404378ba29bee5f5e85f4ce2540f821a70b5454 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_668d13d5adbdd4f8d3d5d98ca404378ba29bee5f5e85f4ce2540f821a70b5454->enter($__internal_668d13d5adbdd4f8d3d5d98ca404378ba29bee5f5e85f4ce2540f821a70b5454_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_field"));

        // line 15
        echo "    <tr class=\"sonata-ba-view-container history-audit-compare\">
        ";
        // line 16
        if (twig_get_attribute($this->env, $this->getSourceContext(), ($context["elements"] ?? null), (isset($context["field_name"]) || array_key_exists("field_name", $context) ? $context["field_name"] : (function () { throw new Twig_Error_Runtime('Variable "field_name" does not exist.', 16, $this->getSourceContext()); })()), array(), "array", true, true)) {
            // line 17
            echo "            ";
            echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderViewElementCompare($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["elements"]) || array_key_exists("elements", $context) ? $context["elements"] : (function () { throw new Twig_Error_Runtime('Variable "elements" does not exist.', 17, $this->getSourceContext()); })()), (isset($context["field_name"]) || array_key_exists("field_name", $context) ? $context["field_name"] : (function () { throw new Twig_Error_Runtime('Variable "field_name" does not exist.', 17, $this->getSourceContext()); })()), array(), "array"), (isset($context["object"]) || array_key_exists("object", $context) ? $context["object"] : (function () { throw new Twig_Error_Runtime('Variable "object" does not exist.', 17, $this->getSourceContext()); })()), (isset($context["object_compare"]) || array_key_exists("object_compare", $context) ? $context["object_compare"] : (function () { throw new Twig_Error_Runtime('Variable "object_compare" does not exist.', 17, $this->getSourceContext()); })()));
            echo "
        ";
        }
        // line 19
        echo "    </tr>
";
        
        $__internal_668d13d5adbdd4f8d3d5d98ca404378ba29bee5f5e85f4ce2540f821a70b5454->leave($__internal_668d13d5adbdd4f8d3d5d98ca404378ba29bee5f5e85f4ce2540f821a70b5454_prof);

        
        $__internal_a8e9d83b82ef435141740d9b58b715ed51b80f0ec645839fc05fa6ca0b6a8d45->leave($__internal_a8e9d83b82ef435141740d9b58b715ed51b80f0ec645839fc05fa6ca0b6a8d45_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_show_compare.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 19,  54 => 17,  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show.html.twig' %}

{% block show_field %}
    <tr class=\"sonata-ba-view-container history-audit-compare\">
        {% if elements[field_name] is defined %}
            {{ elements[field_name]|render_view_element_compare(object, object_compare) }}
        {% endif %}
    </tr>
{% endblock %}
", "SonataAdminBundle:CRUD:base_show_compare.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/base_show_compare.html.twig");
    }
}
