<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_600f63cd281a9e60726933cd853393230768ba6e8e2b342e407497871fbfe6f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_18c5d24a55da381fc02db2ba1d8fd417aa269624048dc019e3506100bef7560b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_18c5d24a55da381fc02db2ba1d8fd417aa269624048dc019e3506100bef7560b->enter($__internal_18c5d24a55da381fc02db2ba1d8fd417aa269624048dc019e3506100bef7560b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_3d62ab668971d1cf5269ab892b46e7900fbcdb59c8a4d25f227e1439a3dac682 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3d62ab668971d1cf5269ab892b46e7900fbcdb59c8a4d25f227e1439a3dac682->enter($__internal_3d62ab668971d1cf5269ab892b46e7900fbcdb59c8a4d25f227e1439a3dac682_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_18c5d24a55da381fc02db2ba1d8fd417aa269624048dc019e3506100bef7560b->leave($__internal_18c5d24a55da381fc02db2ba1d8fd417aa269624048dc019e3506100bef7560b_prof);

        
        $__internal_3d62ab668971d1cf5269ab892b46e7900fbcdb59c8a4d25f227e1439a3dac682->leave($__internal_3d62ab668971d1cf5269ab892b46e7900fbcdb59c8a4d25f227e1439a3dac682_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
