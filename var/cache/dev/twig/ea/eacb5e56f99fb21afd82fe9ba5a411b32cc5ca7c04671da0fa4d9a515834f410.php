<?php

/* SonataBlockBundle:Block:block_core_action.html.twig */
class __TwigTemplate_8a43f9e06577b484c174e9f46d174a37e4ef03b30ec1052f8695c60e39b35005 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["sonata_block"]) || array_key_exists("sonata_block", $context) ? $context["sonata_block"] : (function () { throw new Twig_Error_Runtime('Variable "sonata_block" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_core_action.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0f9d0fa9f8f0a54b130ea1f42084da4cceee7f9543b8a920d91c6025431da0b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0f9d0fa9f8f0a54b130ea1f42084da4cceee7f9543b8a920d91c6025431da0b2->enter($__internal_0f9d0fa9f8f0a54b130ea1f42084da4cceee7f9543b8a920d91c6025431da0b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_action.html.twig"));

        $__internal_1539b4b9e43b73d6ac8116c353d6907ed5fea6e0fc3a93df1472ca3e553e28e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1539b4b9e43b73d6ac8116c353d6907ed5fea6e0fc3a93df1472ca3e553e28e6->enter($__internal_1539b4b9e43b73d6ac8116c353d6907ed5fea6e0fc3a93df1472ca3e553e28e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_action.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0f9d0fa9f8f0a54b130ea1f42084da4cceee7f9543b8a920d91c6025431da0b2->leave($__internal_0f9d0fa9f8f0a54b130ea1f42084da4cceee7f9543b8a920d91c6025431da0b2_prof);

        
        $__internal_1539b4b9e43b73d6ac8116c353d6907ed5fea6e0fc3a93df1472ca3e553e28e6->leave($__internal_1539b4b9e43b73d6ac8116c353d6907ed5fea6e0fc3a93df1472ca3e553e28e6_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_e565a1b0e05c8e8e03e3da872f87dbe9dfa4a348fee57b0e5f1c67359a96cb0f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e565a1b0e05c8e8e03e3da872f87dbe9dfa4a348fee57b0e5f1c67359a96cb0f->enter($__internal_e565a1b0e05c8e8e03e3da872f87dbe9dfa4a348fee57b0e5f1c67359a96cb0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_1a23b30074561ef84f34c441659a7591419299d9efdfae88fd320763ae125fa8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a23b30074561ef84f34c441659a7591419299d9efdfae88fd320763ae125fa8->enter($__internal_1a23b30074561ef84f34c441659a7591419299d9efdfae88fd320763ae125fa8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        echo (isset($context["content"]) || array_key_exists("content", $context) ? $context["content"] : (function () { throw new Twig_Error_Runtime('Variable "content" does not exist.', 15, $this->getSourceContext()); })());
        echo "
";
        
        $__internal_1a23b30074561ef84f34c441659a7591419299d9efdfae88fd320763ae125fa8->leave($__internal_1a23b30074561ef84f34c441659a7591419299d9efdfae88fd320763ae125fa8_prof);

        
        $__internal_e565a1b0e05c8e8e03e3da872f87dbe9dfa4a348fee57b0e5f1c67359a96cb0f->leave($__internal_e565a1b0e05c8e8e03e3da872f87dbe9dfa4a348fee57b0e5f1c67359a96cb0f_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_core_action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    {{ content|raw }}
{% endblock %}
", "SonataBlockBundle:Block:block_core_action.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/block-bundle/Resources/views/Block/block_core_action.html.twig");
    }
}
