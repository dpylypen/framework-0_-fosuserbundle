<?php

/* SonataAdminBundle:CRUD:action.html.twig */
class __TwigTemplate_3c9599740c0eaf0d0524ce32163922e7dca14ddaad69f8ed91826e5a971da7b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'actions' => array($this, 'block_actions'),
            'tab_menu' => array($this, 'block_tab_menu'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) || array_key_exists("base_template", $context) ? $context["base_template"] : (function () { throw new Twig_Error_Runtime('Variable "base_template" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:action.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_759cce7ccca2af9ddfe48b48623bf7679c32e291ab034a214aa2f4b372fac76a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_759cce7ccca2af9ddfe48b48623bf7679c32e291ab034a214aa2f4b372fac76a->enter($__internal_759cce7ccca2af9ddfe48b48623bf7679c32e291ab034a214aa2f4b372fac76a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:action.html.twig"));

        $__internal_eeacfa085649457bd04f88d0e666b87c1455fb649556cd0b16e0d3cd2f85c4df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eeacfa085649457bd04f88d0e666b87c1455fb649556cd0b16e0d3cd2f85c4df->enter($__internal_eeacfa085649457bd04f88d0e666b87c1455fb649556cd0b16e0d3cd2f85c4df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:action.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_759cce7ccca2af9ddfe48b48623bf7679c32e291ab034a214aa2f4b372fac76a->leave($__internal_759cce7ccca2af9ddfe48b48623bf7679c32e291ab034a214aa2f4b372fac76a_prof);

        
        $__internal_eeacfa085649457bd04f88d0e666b87c1455fb649556cd0b16e0d3cd2f85c4df->leave($__internal_eeacfa085649457bd04f88d0e666b87c1455fb649556cd0b16e0d3cd2f85c4df_prof);

    }

    // line 14
    public function block_actions($context, array $blocks = array())
    {
        $__internal_ad18c08e6ffcdc01712df0d64f6b7d974eb8cbe636a4c39a8289025f909e78ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad18c08e6ffcdc01712df0d64f6b7d974eb8cbe636a4c39a8289025f909e78ef->enter($__internal_ad18c08e6ffcdc01712df0d64f6b7d974eb8cbe636a4c39a8289025f909e78ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        $__internal_366b741c49f97aec722b947456cb918e67c4df2d7773f31488a8118cd8396a01 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_366b741c49f97aec722b947456cb918e67c4df2d7773f31488a8118cd8396a01->enter($__internal_366b741c49f97aec722b947456cb918e67c4df2d7773f31488a8118cd8396a01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:action_buttons.html.twig", "SonataAdminBundle:CRUD:action.html.twig", 15)->display($context);
        
        $__internal_366b741c49f97aec722b947456cb918e67c4df2d7773f31488a8118cd8396a01->leave($__internal_366b741c49f97aec722b947456cb918e67c4df2d7773f31488a8118cd8396a01_prof);

        
        $__internal_ad18c08e6ffcdc01712df0d64f6b7d974eb8cbe636a4c39a8289025f909e78ef->leave($__internal_ad18c08e6ffcdc01712df0d64f6b7d974eb8cbe636a4c39a8289025f909e78ef_prof);

    }

    // line 18
    public function block_tab_menu($context, array $blocks = array())
    {
        $__internal_654d3373a84cefdb6f1e51a91a06eb212f66242e9072eeb78e24c9c2b8e521df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_654d3373a84cefdb6f1e51a91a06eb212f66242e9072eeb78e24c9c2b8e521df->enter($__internal_654d3373a84cefdb6f1e51a91a06eb212f66242e9072eeb78e24c9c2b8e521df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        $__internal_3dd1babd16112badff8793a7182935a060751836b5f63488d1b92c4c6acab201 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3dd1babd16112badff8793a7182935a060751836b5f63488d1b92c4c6acab201->enter($__internal_3dd1babd16112badff8793a7182935a060751836b5f63488d1b92c4c6acab201_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        // line 19
        echo "    ";
        if (array_key_exists("action", $context)) {
            // line 20
            echo "        ";
            echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 20, $this->getSourceContext()); })()), "sidemenu", array(0 => (isset($context["action"]) || array_key_exists("action", $context) ? $context["action"] : (function () { throw new Twig_Error_Runtime('Variable "action" does not exist.', 20, $this->getSourceContext()); })())), "method"), array("currentClass" => "active", "template" => twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["sonata_admin"]) || array_key_exists("sonata_admin", $context) ? $context["sonata_admin"] : (function () { throw new Twig_Error_Runtime('Variable "sonata_admin" does not exist.', 20, $this->getSourceContext()); })()), "adminPool", array()), "getTemplate", array(0 => "tab_menu_template"), "method")), "twig");
            echo "
    ";
        }
        
        $__internal_3dd1babd16112badff8793a7182935a060751836b5f63488d1b92c4c6acab201->leave($__internal_3dd1babd16112badff8793a7182935a060751836b5f63488d1b92c4c6acab201_prof);

        
        $__internal_654d3373a84cefdb6f1e51a91a06eb212f66242e9072eeb78e24c9c2b8e521df->leave($__internal_654d3373a84cefdb6f1e51a91a06eb212f66242e9072eeb78e24c9c2b8e521df_prof);

    }

    // line 24
    public function block_content($context, array $blocks = array())
    {
        $__internal_f0a2fb7211d91085857fe573467f1fbc35a3ce8979c84892180ea5f66477b0dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0a2fb7211d91085857fe573467f1fbc35a3ce8979c84892180ea5f66477b0dc->enter($__internal_f0a2fb7211d91085857fe573467f1fbc35a3ce8979c84892180ea5f66477b0dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_07d73927765be3e4d905290e52cc5aa563e31e45ec2532de69d3bb5ac8bca608 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_07d73927765be3e4d905290e52cc5aa563e31e45ec2532de69d3bb5ac8bca608->enter($__internal_07d73927765be3e4d905290e52cc5aa563e31e45ec2532de69d3bb5ac8bca608_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 25
        echo "
    Redefine the content block in your action template

";
        
        $__internal_07d73927765be3e4d905290e52cc5aa563e31e45ec2532de69d3bb5ac8bca608->leave($__internal_07d73927765be3e4d905290e52cc5aa563e31e45ec2532de69d3bb5ac8bca608_prof);

        
        $__internal_f0a2fb7211d91085857fe573467f1fbc35a3ce8979c84892180ea5f66477b0dc->leave($__internal_f0a2fb7211d91085857fe573467f1fbc35a3ce8979c84892180ea5f66477b0dc_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 25,  86 => 24,  72 => 20,  69 => 19,  60 => 18,  50 => 15,  41 => 14,  20 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:action_buttons.html.twig' %}
{%- endblock -%}

{% block tab_menu %}
    {% if action is defined %}
        {{ knp_menu_render(admin.sidemenu(action), {'currentClass' : 'active', 'template': sonata_admin.adminPool.getTemplate('tab_menu_template')}, 'twig') }}
    {% endif %}
{% endblock %}

{% block content %}

    Redefine the content block in your action template

{% endblock %}
", "SonataAdminBundle:CRUD:action.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/action.html.twig");
    }
}
