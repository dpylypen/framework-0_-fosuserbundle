<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_4f675457c98b5dfcf1b01d2dae7eb9d5166f9575405bb7c33c888f64560cc3da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09dbeaf04d51d4f50fa72f5d7b690a32544d173a35ae26230927f7c4297eab70 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09dbeaf04d51d4f50fa72f5d7b690a32544d173a35ae26230927f7c4297eab70->enter($__internal_09dbeaf04d51d4f50fa72f5d7b690a32544d173a35ae26230927f7c4297eab70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_92ef867c2d48bc687a9cca2c3026991ce065517af29ac7b6868c428a397685db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92ef867c2d48bc687a9cca2c3026991ce065517af29ac7b6868c428a397685db->enter($__internal_92ef867c2d48bc687a9cca2c3026991ce065517af29ac7b6868c428a397685db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_09dbeaf04d51d4f50fa72f5d7b690a32544d173a35ae26230927f7c4297eab70->leave($__internal_09dbeaf04d51d4f50fa72f5d7b690a32544d173a35ae26230927f7c4297eab70_prof);

        
        $__internal_92ef867c2d48bc687a9cca2c3026991ce065517af29ac7b6868c428a397685db->leave($__internal_92ef867c2d48bc687a9cca2c3026991ce065517af29ac7b6868c428a397685db_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetime_widget.html.php");
    }
}
