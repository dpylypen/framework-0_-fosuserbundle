<?php

/* SonataAdminBundle:CRUD:show_date.html.twig */
class __TwigTemplate_79cc8f099ffa645108dd76abcc22d95d3809f3b654849f5118c848c1fe309cfd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_date.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_00da7757bf91408eab7fe73a06f24a14374f9a03f2fb952b5cf80541fdb22d9a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_00da7757bf91408eab7fe73a06f24a14374f9a03f2fb952b5cf80541fdb22d9a->enter($__internal_00da7757bf91408eab7fe73a06f24a14374f9a03f2fb952b5cf80541fdb22d9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_date.html.twig"));

        $__internal_cc2adc49629cad810930d1d5ac2c8559b8dc3245f00a4e581ce602ccfeb5157a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cc2adc49629cad810930d1d5ac2c8559b8dc3245f00a4e581ce602ccfeb5157a->enter($__internal_cc2adc49629cad810930d1d5ac2c8559b8dc3245f00a4e581ce602ccfeb5157a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_date.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_00da7757bf91408eab7fe73a06f24a14374f9a03f2fb952b5cf80541fdb22d9a->leave($__internal_00da7757bf91408eab7fe73a06f24a14374f9a03f2fb952b5cf80541fdb22d9a_prof);

        
        $__internal_cc2adc49629cad810930d1d5ac2c8559b8dc3245f00a4e581ce602ccfeb5157a->leave($__internal_cc2adc49629cad810930d1d5ac2c8559b8dc3245f00a4e581ce602ccfeb5157a_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_2d32cd0c0da43e95d9b6624322713e6bca8908651609615d72307d49a6cbac83 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d32cd0c0da43e95d9b6624322713e6bca8908651609615d72307d49a6cbac83->enter($__internal_2d32cd0c0da43e95d9b6624322713e6bca8908651609615d72307d49a6cbac83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_be62f10c1329f23826c1647bbe2c5ae3aff03068ad4819f21948eeab7de27a83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be62f10c1329f23826c1647bbe2c5ae3aff03068ad4819f21948eeab7de27a83->enter($__internal_be62f10c1329f23826c1647bbe2c5ae3aff03068ad4819f21948eeab7de27a83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twig_test_empty((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "&nbsp;";
        } elseif (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(),         // line 17
($context["field_description"] ?? null), "options", array(), "any", false, true), "format", array(), "any", true, true)) {
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 18, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 18, $this->getSourceContext()); })()), "options", array()), "format", array())), "html", null, true);
        } else {
            // line 20
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 20, $this->getSourceContext()); })()), "F j, Y"), "html", null, true);
        }
        
        $__internal_be62f10c1329f23826c1647bbe2c5ae3aff03068ad4819f21948eeab7de27a83->leave($__internal_be62f10c1329f23826c1647bbe2c5ae3aff03068ad4819f21948eeab7de27a83_prof);

        
        $__internal_2d32cd0c0da43e95d9b6624322713e6bca8908651609615d72307d49a6cbac83->leave($__internal_2d32cd0c0da43e95d9b6624322713e6bca8908651609615d72307d49a6cbac83_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_date.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 20,  55 => 18,  53 => 17,  51 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field%}
    {%- if value is empty -%}
        &nbsp;
    {%- elseif field_description.options.format is defined -%}
        {{ value|date(field_description.options.format) }}
    {%- else -%}
        {{ value|date('F j, Y') }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:show_date.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/show_date.html.twig");
    }
}
