<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_531f684bfcae03fbc1b05d8b1f5263e72313fcae81e1b23f903cfb066c8be1d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd231a54233b0834e5d84ccd570d6b04495dbdea85826753e0753285667e597c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dd231a54233b0834e5d84ccd570d6b04495dbdea85826753e0753285667e597c->enter($__internal_dd231a54233b0834e5d84ccd570d6b04495dbdea85826753e0753285667e597c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_d7e0b97ee9e4739035107eeb21391804ebcec61801bac33e78542f382b84b817 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7e0b97ee9e4739035107eeb21391804ebcec61801bac33e78542f382b84b817->enter($__internal_d7e0b97ee9e4739035107eeb21391804ebcec61801bac33e78542f382b84b817_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_dd231a54233b0834e5d84ccd570d6b04495dbdea85826753e0753285667e597c->leave($__internal_dd231a54233b0834e5d84ccd570d6b04495dbdea85826753e0753285667e597c_prof);

        
        $__internal_d7e0b97ee9e4739035107eeb21391804ebcec61801bac33e78542f382b84b817->leave($__internal_d7e0b97ee9e4739035107eeb21391804ebcec61801bac33e78542f382b84b817_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}
