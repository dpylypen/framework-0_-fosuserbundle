<?php

/* SonataAdminBundle:CRUD:base_show_field.html.twig */
class __TwigTemplate_1aa37f75f3e9c08929b2a63fffe33e613393b2534f4a3ad9567499dcd644dfc9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'name' => array($this, 'block_name'),
            'field' => array($this, 'block_field'),
            'field_value' => array($this, 'block_field_value'),
            'field_compare' => array($this, 'block_field_compare'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b41b47a0a58b2ec3e9fe448d8ecd10828578876334a7ff08676e4390a9091fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b41b47a0a58b2ec3e9fe448d8ecd10828578876334a7ff08676e4390a9091fa->enter($__internal_4b41b47a0a58b2ec3e9fe448d8ecd10828578876334a7ff08676e4390a9091fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_show_field.html.twig"));

        $__internal_e97e8ee11655167e13a79893fc5af7a93643e11d468cba4f08bd69023e159ffb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e97e8ee11655167e13a79893fc5af7a93643e11d468cba4f08bd69023e159ffb->enter($__internal_e97e8ee11655167e13a79893fc5af7a93643e11d468cba4f08bd69023e159ffb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_show_field.html.twig"));

        // line 11
        echo "
<th";
        // line 12
        if (((array_key_exists("is_diff", $context)) ? (_twig_default_filter((isset($context["is_diff"]) || array_key_exists("is_diff", $context) ? $context["is_diff"] : (function () { throw new Twig_Error_Runtime('Variable "is_diff" does not exist.', 12, $this->getSourceContext()); })()), false)) : (false))) {
            echo " class=\"diff\"";
        }
        echo ">";
        $this->displayBlock('name', $context, $blocks);
        echo "</th>
<td>";
        // line 14
        $this->displayBlock('field', $context, $blocks);
        // line 31
        echo "</td>

";
        // line 33
        $this->displayBlock('field_compare', $context, $blocks);
        
        $__internal_4b41b47a0a58b2ec3e9fe448d8ecd10828578876334a7ff08676e4390a9091fa->leave($__internal_4b41b47a0a58b2ec3e9fe448d8ecd10828578876334a7ff08676e4390a9091fa_prof);

        
        $__internal_e97e8ee11655167e13a79893fc5af7a93643e11d468cba4f08bd69023e159ffb->leave($__internal_e97e8ee11655167e13a79893fc5af7a93643e11d468cba4f08bd69023e159ffb_prof);

    }

    // line 12
    public function block_name($context, array $blocks = array())
    {
        $__internal_1e8bb629fc7420fc088d88be20fd72c17a3dafd2afb8a893c26f466c2af04aeb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e8bb629fc7420fc088d88be20fd72c17a3dafd2afb8a893c26f466c2af04aeb->enter($__internal_1e8bb629fc7420fc088d88be20fd72c17a3dafd2afb8a893c26f466c2af04aeb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "name"));

        $__internal_228efe3b4d4057c1353b2e04f185e734b8e94fa9cc3e923b61591b0873672f1a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_228efe3b4d4057c1353b2e04f185e734b8e94fa9cc3e923b61591b0873672f1a->enter($__internal_228efe3b4d4057c1353b2e04f185e734b8e94fa9cc3e923b61591b0873672f1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "name"));

        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 12, $this->getSourceContext()); })()), "label", array()), array(), ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 12, $this->getSourceContext()); })()), "translationDomain", array())) ? (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 12, $this->getSourceContext()); })()), "translationDomain", array())) : (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "translationDomain", array())))), "html", null, true);
        
        $__internal_228efe3b4d4057c1353b2e04f185e734b8e94fa9cc3e923b61591b0873672f1a->leave($__internal_228efe3b4d4057c1353b2e04f185e734b8e94fa9cc3e923b61591b0873672f1a_prof);

        
        $__internal_1e8bb629fc7420fc088d88be20fd72c17a3dafd2afb8a893c26f466c2af04aeb->leave($__internal_1e8bb629fc7420fc088d88be20fd72c17a3dafd2afb8a893c26f466c2af04aeb_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_c0d41afd9ce3e767dc59cf9fa6a740f9a7636d1f8805b0800935356d1affa940 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c0d41afd9ce3e767dc59cf9fa6a740f9a7636d1f8805b0800935356d1affa940->enter($__internal_c0d41afd9ce3e767dc59cf9fa6a740f9a7636d1f8805b0800935356d1affa940_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_1f56068d0b88d4d555b5f925bccb835162add7799f33647c17707b0581b3236e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f56068d0b88d4d555b5f925bccb835162add7799f33647c17707b0581b3236e->enter($__internal_1f56068d0b88d4d555b5f925bccb835162add7799f33647c17707b0581b3236e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        ob_start();
        // line 16
        echo "            ";
        $context["collapse"] = ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "collapse", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "collapse", array()), false)) : (false));
        // line 17
        echo "            ";
        if ((isset($context["collapse"]) || array_key_exists("collapse", $context) ? $context["collapse"] : (function () { throw new Twig_Error_Runtime('Variable "collapse" does not exist.', 17, $this->getSourceContext()); })())) {
            // line 18
            echo "                <div class=\"sonata-readmore\"
                      data-readmore-height=\"";
            // line 19
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "height", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "height", array()), 40)) : (40)), "html", null, true);
            echo "\"
                      data-readmore-more=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(((twig_get_attribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "more", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "more", array()), "read_more")) : ("read_more")), array(), "SonataAdminBundle"), "html", null, true);
            echo "\"
                      data-readmore-less=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(((twig_get_attribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "less", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "less", array()), "read_less")) : ("read_less")), array(), "SonataAdminBundle"), "html", null, true);
            echo "\">
                    ";
            // line 22
            $this->displayBlock('field_value', $context, $blocks);
            // line 25
            echo "                </div>
            ";
        } else {
            // line 27
            echo "                ";
            $this->displayBlock("field_value", $context, $blocks);
            echo "
            ";
        }
        // line 29
        echo "        ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_1f56068d0b88d4d555b5f925bccb835162add7799f33647c17707b0581b3236e->leave($__internal_1f56068d0b88d4d555b5f925bccb835162add7799f33647c17707b0581b3236e_prof);

        
        $__internal_c0d41afd9ce3e767dc59cf9fa6a740f9a7636d1f8805b0800935356d1affa940->leave($__internal_c0d41afd9ce3e767dc59cf9fa6a740f9a7636d1f8805b0800935356d1affa940_prof);

    }

    // line 22
    public function block_field_value($context, array $blocks = array())
    {
        $__internal_ec23c9a7170eb4cb15dcba6c74ca9b84a1c3805923aeecd6ca266b4800dfb2ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec23c9a7170eb4cb15dcba6c74ca9b84a1c3805923aeecd6ca266b4800dfb2ed->enter($__internal_ec23c9a7170eb4cb15dcba6c74ca9b84a1c3805923aeecd6ca266b4800dfb2ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_value"));

        $__internal_da5be4594b11676cb0ac77a50b01ad793e3c105a2d1dcf3079b0e52a028b86f1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da5be4594b11676cb0ac77a50b01ad793e3c105a2d1dcf3079b0e52a028b86f1->enter($__internal_da5be4594b11676cb0ac77a50b01ad793e3c105a2d1dcf3079b0e52a028b86f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_value"));

        // line 23
        echo "                        ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 23, $this->getSourceContext()); })()), "options", array()), "safe", array())) {
            echo (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 23, $this->getSourceContext()); })());
        } else {
            echo nl2br(twig_escape_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 23, $this->getSourceContext()); })()), "html", null, true));
        }
        // line 24
        echo "                    ";
        
        $__internal_da5be4594b11676cb0ac77a50b01ad793e3c105a2d1dcf3079b0e52a028b86f1->leave($__internal_da5be4594b11676cb0ac77a50b01ad793e3c105a2d1dcf3079b0e52a028b86f1_prof);

        
        $__internal_ec23c9a7170eb4cb15dcba6c74ca9b84a1c3805923aeecd6ca266b4800dfb2ed->leave($__internal_ec23c9a7170eb4cb15dcba6c74ca9b84a1c3805923aeecd6ca266b4800dfb2ed_prof);

    }

    // line 33
    public function block_field_compare($context, array $blocks = array())
    {
        $__internal_42c552902664816b9da40178ecbb62d4e86844139c1c699ce2481d943b31dfe7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_42c552902664816b9da40178ecbb62d4e86844139c1c699ce2481d943b31dfe7->enter($__internal_42c552902664816b9da40178ecbb62d4e86844139c1c699ce2481d943b31dfe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_compare"));

        $__internal_8ef332a08e79064f471915567b3251d866b444eb1c46d8aeac6b10f75dacd68d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8ef332a08e79064f471915567b3251d866b444eb1c46d8aeac6b10f75dacd68d->enter($__internal_8ef332a08e79064f471915567b3251d866b444eb1c46d8aeac6b10f75dacd68d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_compare"));

        // line 34
        echo "    ";
        if (array_key_exists("value_compare", $context)) {
            // line 35
            echo "        <td>
            ";
            // line 36
            $context["value"] = (isset($context["value_compare"]) || array_key_exists("value_compare", $context) ? $context["value_compare"] : (function () { throw new Twig_Error_Runtime('Variable "value_compare" does not exist.', 36, $this->getSourceContext()); })());
            // line 37
            echo "            ";
            $this->displayBlock("field", $context, $blocks);
            echo "
        </td>
    ";
        }
        
        $__internal_8ef332a08e79064f471915567b3251d866b444eb1c46d8aeac6b10f75dacd68d->leave($__internal_8ef332a08e79064f471915567b3251d866b444eb1c46d8aeac6b10f75dacd68d_prof);

        
        $__internal_42c552902664816b9da40178ecbb62d4e86844139c1c699ce2481d943b31dfe7->leave($__internal_42c552902664816b9da40178ecbb62d4e86844139c1c699ce2481d943b31dfe7_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 37,  170 => 36,  167 => 35,  164 => 34,  155 => 33,  145 => 24,  138 => 23,  129 => 22,  118 => 29,  112 => 27,  108 => 25,  106 => 22,  102 => 21,  98 => 20,  94 => 19,  91 => 18,  88 => 17,  85 => 16,  83 => 15,  74 => 14,  56 => 12,  46 => 33,  42 => 31,  40 => 14,  32 => 12,  29 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<th{% if(is_diff|default(false)) %} class=\"diff\"{% endif %}>{% block name %}{{ field_description.label|trans({}, field_description.translationDomain ?: admin.translationDomain) }}{% endblock %}</th>
<td>
    {%- block field -%}
        {% spaceless %}
            {% set collapse = field_description.options.collapse|default(false) %}
            {% if collapse %}
                <div class=\"sonata-readmore\"
                      data-readmore-height=\"{{ collapse.height|default(40) }}\"
                      data-readmore-more=\"{{ collapse.more|default('read_more')|trans({}, 'SonataAdminBundle') }}\"
                      data-readmore-less=\"{{ collapse.less|default('read_less')|trans({}, 'SonataAdminBundle') }}\">
                    {% block field_value %}
                        {% if field_description.options.safe %}{{ value|raw }}{% else %}{{ value|nl2br }}{% endif %}
                    {% endblock %}
                </div>
            {% else %}
                {{ block('field_value') }}
            {% endif %}
        {% endspaceless %}
    {%- endblock -%}
</td>

{% block field_compare %}
    {% if(value_compare is defined) %}
        <td>
            {% set value = value_compare %}
            {{ block('field') }}
        </td>
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:base_show_field.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/base_show_field.html.twig");
    }
}
