<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_37d39620cd56f677fd089766db290066156822bde67a14a08ee512c7155589af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_12bb3622df094e0714912092dea78c116606cf5a1dfe5b4198bd1d05666ef89d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_12bb3622df094e0714912092dea78c116606cf5a1dfe5b4198bd1d05666ef89d->enter($__internal_12bb3622df094e0714912092dea78c116606cf5a1dfe5b4198bd1d05666ef89d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_5b824cb00c5bd68aaff00d9ac145c4a9389cda8eaa18491c75f1809949d66e52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b824cb00c5bd68aaff00d9ac145c4a9389cda8eaa18491c75f1809949d66e52->enter($__internal_5b824cb00c5bd68aaff00d9ac145c4a9389cda8eaa18491c75f1809949d66e52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_12bb3622df094e0714912092dea78c116606cf5a1dfe5b4198bd1d05666ef89d->leave($__internal_12bb3622df094e0714912092dea78c116606cf5a1dfe5b4198bd1d05666ef89d_prof);

        
        $__internal_5b824cb00c5bd68aaff00d9ac145c4a9389cda8eaa18491c75f1809949d66e52->leave($__internal_5b824cb00c5bd68aaff00d9ac145c4a9389cda8eaa18491c75f1809949d66e52_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
