<?php

/* SonataBlockBundle:Block:block_core_menu.html.twig */
class __TwigTemplate_4c39b7c1991892d4bb768cc1d0e78d228baab8486eb7f9d812d6bc399f9c7925 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["sonata_block"]) || array_key_exists("sonata_block", $context) ? $context["sonata_block"] : (function () { throw new Twig_Error_Runtime('Variable "sonata_block" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_core_menu.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0c63af1d589425baba30d5b449462833f2eee2a4d175ccfe011228714cbf1f0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0c63af1d589425baba30d5b449462833f2eee2a4d175ccfe011228714cbf1f0->enter($__internal_a0c63af1d589425baba30d5b449462833f2eee2a4d175ccfe011228714cbf1f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_menu.html.twig"));

        $__internal_7901809fef2770af6cec22404e43d38491e72f925d195ec32b25fe499e908bfd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7901809fef2770af6cec22404e43d38491e72f925d195ec32b25fe499e908bfd->enter($__internal_7901809fef2770af6cec22404e43d38491e72f925d195ec32b25fe499e908bfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_menu.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a0c63af1d589425baba30d5b449462833f2eee2a4d175ccfe011228714cbf1f0->leave($__internal_a0c63af1d589425baba30d5b449462833f2eee2a4d175ccfe011228714cbf1f0_prof);

        
        $__internal_7901809fef2770af6cec22404e43d38491e72f925d195ec32b25fe499e908bfd->leave($__internal_7901809fef2770af6cec22404e43d38491e72f925d195ec32b25fe499e908bfd_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_f9241ca1743a821b6c628ae665ff3318b7b10c457923d0347dcd8a2891f14ac0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f9241ca1743a821b6c628ae665ff3318b7b10c457923d0347dcd8a2891f14ac0->enter($__internal_f9241ca1743a821b6c628ae665ff3318b7b10c457923d0347dcd8a2891f14ac0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_fd0b90bf5f4a26ed825cf9c88199baedfc0c0ad49e885598808f76f7c010aa6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd0b90bf5f4a26ed825cf9c88199baedfc0c0ad49e885598808f76f7c010aa6b->enter($__internal_fd0b90bf5f4a26ed825cf9c88199baedfc0c0ad49e885598808f76f7c010aa6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render((isset($context["menu"]) || array_key_exists("menu", $context) ? $context["menu"] : (function () { throw new Twig_Error_Runtime('Variable "menu" does not exist.', 15, $this->getSourceContext()); })()), (isset($context["menu_options"]) || array_key_exists("menu_options", $context) ? $context["menu_options"] : (function () { throw new Twig_Error_Runtime('Variable "menu_options" does not exist.', 15, $this->getSourceContext()); })()));
        echo "
";
        
        $__internal_fd0b90bf5f4a26ed825cf9c88199baedfc0c0ad49e885598808f76f7c010aa6b->leave($__internal_fd0b90bf5f4a26ed825cf9c88199baedfc0c0ad49e885598808f76f7c010aa6b_prof);

        
        $__internal_f9241ca1743a821b6c628ae665ff3318b7b10c457923d0347dcd8a2891f14ac0->leave($__internal_f9241ca1743a821b6c628ae665ff3318b7b10c457923d0347dcd8a2891f14ac0_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_core_menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    {{ knp_menu_render(menu, menu_options) }}
{% endblock %}
", "SonataBlockBundle:Block:block_core_menu.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/block-bundle/Resources/views/Block/block_core_menu.html.twig");
    }
}
