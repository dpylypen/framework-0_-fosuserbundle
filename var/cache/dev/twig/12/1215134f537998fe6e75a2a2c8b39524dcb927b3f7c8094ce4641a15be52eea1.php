<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_caffaf3c35df11298a65fa60232241d6c601fd1c6b9a8518579380c68669d499 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c23c419a9147e73cac81494342a5decf79300659c8482de9df6024b4fbf42f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8c23c419a9147e73cac81494342a5decf79300659c8482de9df6024b4fbf42f9->enter($__internal_8c23c419a9147e73cac81494342a5decf79300659c8482de9df6024b4fbf42f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        $__internal_cb2b3a7e88d9219b26fd4047c70a963a17f783c81b7d056da29d422c3558e1b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb2b3a7e88d9219b26fd4047c70a963a17f783c81b7d056da29d422c3558e1b8->enter($__internal_cb2b3a7e88d9219b26fd4047c70a963a17f783c81b7d056da29d422c3558e1b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_8c23c419a9147e73cac81494342a5decf79300659c8482de9df6024b4fbf42f9->leave($__internal_8c23c419a9147e73cac81494342a5decf79300659c8482de9df6024b4fbf42f9_prof);

        
        $__internal_cb2b3a7e88d9219b26fd4047c70a963a17f783c81b7d056da29d422c3558e1b8->leave($__internal_cb2b3a7e88d9219b26fd4047c70a963a17f783c81b7d056da29d422c3558e1b8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choice_attributes.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_attributes.html.php");
    }
}
