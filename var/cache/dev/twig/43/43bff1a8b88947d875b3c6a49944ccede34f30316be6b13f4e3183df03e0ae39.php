<?php

/* SonataAdminBundle:CRUD:delete.html.twig */
class __TwigTemplate_561a453e428fc864ffb44a047ac0b6c7a826013d59174dcb392698fec1deaa15 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'actions' => array($this, 'block_actions'),
            'tab_menu' => array($this, 'block_tab_menu'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) || array_key_exists("base_template", $context) ? $context["base_template"] : (function () { throw new Twig_Error_Runtime('Variable "base_template" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:delete.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_361910efdde279fd6f2183bf0395ee3c3ed5c87e792d9ffb055778d4f9318cdb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_361910efdde279fd6f2183bf0395ee3c3ed5c87e792d9ffb055778d4f9318cdb->enter($__internal_361910efdde279fd6f2183bf0395ee3c3ed5c87e792d9ffb055778d4f9318cdb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:delete.html.twig"));

        $__internal_8ab579a952e3428254a8dc9bd8828fbe9dcca371db88d04653e18dd43d821d85 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8ab579a952e3428254a8dc9bd8828fbe9dcca371db88d04653e18dd43d821d85->enter($__internal_8ab579a952e3428254a8dc9bd8828fbe9dcca371db88d04653e18dd43d821d85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:delete.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_361910efdde279fd6f2183bf0395ee3c3ed5c87e792d9ffb055778d4f9318cdb->leave($__internal_361910efdde279fd6f2183bf0395ee3c3ed5c87e792d9ffb055778d4f9318cdb_prof);

        
        $__internal_8ab579a952e3428254a8dc9bd8828fbe9dcca371db88d04653e18dd43d821d85->leave($__internal_8ab579a952e3428254a8dc9bd8828fbe9dcca371db88d04653e18dd43d821d85_prof);

    }

    // line 14
    public function block_actions($context, array $blocks = array())
    {
        $__internal_1ce9a3a1bf547a9f86d419f4a8f492add6aebf40ff50a0dc70f6474ef7237870 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ce9a3a1bf547a9f86d419f4a8f492add6aebf40ff50a0dc70f6474ef7237870->enter($__internal_1ce9a3a1bf547a9f86d419f4a8f492add6aebf40ff50a0dc70f6474ef7237870_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        $__internal_0f9025a148bbc1d1c6367c370cb9a2dc7ffcd9b924ac702e15592d5f4ce73545 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f9025a148bbc1d1c6367c370cb9a2dc7ffcd9b924ac702e15592d5f4ce73545->enter($__internal_0f9025a148bbc1d1c6367c370cb9a2dc7ffcd9b924ac702e15592d5f4ce73545_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:action_buttons.html.twig", "SonataAdminBundle:CRUD:delete.html.twig", 15)->display($context);
        
        $__internal_0f9025a148bbc1d1c6367c370cb9a2dc7ffcd9b924ac702e15592d5f4ce73545->leave($__internal_0f9025a148bbc1d1c6367c370cb9a2dc7ffcd9b924ac702e15592d5f4ce73545_prof);

        
        $__internal_1ce9a3a1bf547a9f86d419f4a8f492add6aebf40ff50a0dc70f6474ef7237870->leave($__internal_1ce9a3a1bf547a9f86d419f4a8f492add6aebf40ff50a0dc70f6474ef7237870_prof);

    }

    // line 18
    public function block_tab_menu($context, array $blocks = array())
    {
        $__internal_86f4c06fc9e7bfe308ea4500a98a6f081cb4df6250d470a9e11031962dfc3d4d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86f4c06fc9e7bfe308ea4500a98a6f081cb4df6250d470a9e11031962dfc3d4d->enter($__internal_86f4c06fc9e7bfe308ea4500a98a6f081cb4df6250d470a9e11031962dfc3d4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        $__internal_6be6457141070dff946c5c11dfc6638084020dbf7c7982f7c4b5d50ba142e02c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6be6457141070dff946c5c11dfc6638084020dbf7c7982f7c4b5d50ba142e02c->enter($__internal_6be6457141070dff946c5c11dfc6638084020dbf7c7982f7c4b5d50ba142e02c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "sidemenu", array(0 => (isset($context["action"]) || array_key_exists("action", $context) ? $context["action"] : (function () { throw new Twig_Error_Runtime('Variable "action" does not exist.', 18, $this->getSourceContext()); })())), "method"), array("currentClass" => "active", "template" => twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["sonata_admin"]) || array_key_exists("sonata_admin", $context) ? $context["sonata_admin"] : (function () { throw new Twig_Error_Runtime('Variable "sonata_admin" does not exist.', 18, $this->getSourceContext()); })()), "adminPool", array()), "getTemplate", array(0 => "tab_menu_template"), "method")), "twig");
        
        $__internal_6be6457141070dff946c5c11dfc6638084020dbf7c7982f7c4b5d50ba142e02c->leave($__internal_6be6457141070dff946c5c11dfc6638084020dbf7c7982f7c4b5d50ba142e02c_prof);

        
        $__internal_86f4c06fc9e7bfe308ea4500a98a6f081cb4df6250d470a9e11031962dfc3d4d->leave($__internal_86f4c06fc9e7bfe308ea4500a98a6f081cb4df6250d470a9e11031962dfc3d4d_prof);

    }

    // line 20
    public function block_content($context, array $blocks = array())
    {
        $__internal_dc546af17d494ff96e076934c2909a0697867dcc95312dd7ab13252b2ef8886c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dc546af17d494ff96e076934c2909a0697867dcc95312dd7ab13252b2ef8886c->enter($__internal_dc546af17d494ff96e076934c2909a0697867dcc95312dd7ab13252b2ef8886c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_a1b28d5e96a2cf3fc6637a06cf13b96eb209b7b310eb6a3e0c71be36e23d8dbf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1b28d5e96a2cf3fc6637a06cf13b96eb209b7b310eb6a3e0c71be36e23d8dbf->enter($__internal_a1b28d5e96a2cf3fc6637a06cf13b96eb209b7b310eb6a3e0c71be36e23d8dbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 21
        echo "    <div class=\"sonata-ba-delete\">

        <div class=\"box box-danger\">
            <div class=\"box-header\">
                <h3 class=\"box-title\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_delete", array(), "SonataAdminBundle"), "html", null, true);
        echo "</h3>
            </div>
            <div class=\"box-body\">
                ";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("message_delete_confirmation", array("%object%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 28, $this->getSourceContext()); })()), "toString", array(0 => (isset($context["object"]) || array_key_exists("object", $context) ? $context["object"] : (function () { throw new Twig_Error_Runtime('Variable "object" does not exist.', 28, $this->getSourceContext()); })())), "method")), "SonataAdminBundle"), "html", null, true);
        echo "
            </div>
            <div class=\"box-footer clearfix\">
                <form method=\"POST\" action=\"";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 31, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "delete", 1 => (isset($context["object"]) || array_key_exists("object", $context) ? $context["object"] : (function () { throw new Twig_Error_Runtime('Variable "object" does not exist.', 31, $this->getSourceContext()); })())), "method"), "html", null, true);
        echo "\">
                    <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                    <input type=\"hidden\" name=\"_sonata_csrf_token\" value=\"";
        // line 33
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) || array_key_exists("csrf_token", $context) ? $context["csrf_token"] : (function () { throw new Twig_Error_Runtime('Variable "csrf_token" does not exist.', 33, $this->getSourceContext()); })()), "html", null, true);
        echo "\">

                    <button type=\"submit\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i> ";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btn_delete", array(), "SonataAdminBundle"), "html", null, true);
        echo "</button>
                    ";
        // line 36
        if ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 36, $this->getSourceContext()); })()), "hasRoute", array(0 => "edit"), "method") && twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 36, $this->getSourceContext()); })()), "hasAccess", array(0 => "edit", 1 => (isset($context["object"]) || array_key_exists("object", $context) ? $context["object"] : (function () { throw new Twig_Error_Runtime('Variable "object" does not exist.', 36, $this->getSourceContext()); })())), "method"))) {
            // line 37
            echo "                        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("delete_or", array(), "SonataAdminBundle"), "html", null, true);
            echo "

                        <a class=\"btn btn-success\" href=\"";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 39, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "edit", 1 => (isset($context["object"]) || array_key_exists("object", $context) ? $context["object"] : (function () { throw new Twig_Error_Runtime('Variable "object" does not exist.', 39, $this->getSourceContext()); })())), "method"), "html", null, true);
            echo "\">
                            <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>
                            ";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("link_action_edit", array(), "SonataAdminBundle"), "html", null, true);
            echo "</a>
                    ";
        }
        // line 43
        echo "                </form>
            </div>
        </div>
    </div>
";
        
        $__internal_a1b28d5e96a2cf3fc6637a06cf13b96eb209b7b310eb6a3e0c71be36e23d8dbf->leave($__internal_a1b28d5e96a2cf3fc6637a06cf13b96eb209b7b310eb6a3e0c71be36e23d8dbf_prof);

        
        $__internal_dc546af17d494ff96e076934c2909a0697867dcc95312dd7ab13252b2ef8886c->leave($__internal_dc546af17d494ff96e076934c2909a0697867dcc95312dd7ab13252b2ef8886c_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:delete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 43,  132 => 41,  127 => 39,  121 => 37,  119 => 36,  115 => 35,  110 => 33,  105 => 31,  99 => 28,  93 => 25,  87 => 21,  78 => 20,  60 => 18,  50 => 15,  41 => 14,  20 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:action_buttons.html.twig' %}
{%- endblock -%}

{% block tab_menu %}{{ knp_menu_render(admin.sidemenu(action), {'currentClass' : 'active', 'template': sonata_admin.adminPool.getTemplate('tab_menu_template')}, 'twig') }}{% endblock %}

{% block content %}
    <div class=\"sonata-ba-delete\">

        <div class=\"box box-danger\">
            <div class=\"box-header\">
                <h3 class=\"box-title\">{{ 'title_delete'|trans({}, 'SonataAdminBundle') }}</h3>
            </div>
            <div class=\"box-body\">
                {{ 'message_delete_confirmation'|trans({'%object%': admin.toString(object)}, 'SonataAdminBundle') }}
            </div>
            <div class=\"box-footer clearfix\">
                <form method=\"POST\" action=\"{{ admin.generateObjectUrl('delete', object) }}\">
                    <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                    <input type=\"hidden\" name=\"_sonata_csrf_token\" value=\"{{ csrf_token }}\">

                    <button type=\"submit\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i> {{ 'btn_delete'|trans({}, 'SonataAdminBundle') }}</button>
                    {% if admin.hasRoute('edit') and admin.hasAccess('edit', object) %}
                        {{ 'delete_or'|trans({}, 'SonataAdminBundle') }}

                        <a class=\"btn btn-success\" href=\"{{ admin.generateObjectUrl('edit', object) }}\">
                            <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>
                            {{ 'link_action_edit'|trans({}, 'SonataAdminBundle') }}</a>
                    {% endif %}
                </form>
            </div>
        </div>
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:delete.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/delete.html.twig");
    }
}
