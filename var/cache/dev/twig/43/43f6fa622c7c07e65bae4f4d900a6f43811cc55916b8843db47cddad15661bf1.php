<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_e93e3bfe6def0785aa695622816bdc338e0cf3fde61a77d9a2444cd7c4f98824 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_42a1b251a5ab3257b58476eb46db7622882170a943ccc50b3b7cd499ee3f278d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_42a1b251a5ab3257b58476eb46db7622882170a943ccc50b3b7cd499ee3f278d->enter($__internal_42a1b251a5ab3257b58476eb46db7622882170a943ccc50b3b7cd499ee3f278d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $__internal_70feeb6b9e674b38e9d5a4665e248c85367597e7856e9900692e95b40db19dde = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70feeb6b9e674b38e9d5a4665e248c85367597e7856e9900692e95b40db19dde->enter($__internal_70feeb6b9e674b38e9d5a4665e248c85367597e7856e9900692e95b40db19dde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_42a1b251a5ab3257b58476eb46db7622882170a943ccc50b3b7cd499ee3f278d->leave($__internal_42a1b251a5ab3257b58476eb46db7622882170a943ccc50b3b7cd499ee3f278d_prof);

        
        $__internal_70feeb6b9e674b38e9d5a4665e248c85367597e7856e9900692e95b40db19dde->leave($__internal_70feeb6b9e674b38e9d5a4665e248c85367597e7856e9900692e95b40db19dde_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d059f1cf3c20272331d930e26b72c77dffea02f09cd38d97735b0b253525ca4f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d059f1cf3c20272331d930e26b72c77dffea02f09cd38d97735b0b253525ca4f->enter($__internal_d059f1cf3c20272331d930e26b72c77dffea02f09cd38d97735b0b253525ca4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_c997ca6c4c911b813a8a0730aae694bdcc65b8381da4e85c3a0163a10df3989a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c997ca6c4c911b813a8a0730aae694bdcc65b8381da4e85c3a0163a10df3989a->enter($__internal_c997ca6c4c911b813a8a0730aae694bdcc65b8381da4e85c3a0163a10df3989a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_c997ca6c4c911b813a8a0730aae694bdcc65b8381da4e85c3a0163a10df3989a->leave($__internal_c997ca6c4c911b813a8a0730aae694bdcc65b8381da4e85c3a0163a10df3989a_prof);

        
        $__internal_d059f1cf3c20272331d930e26b72c77dffea02f09cd38d97735b0b253525ca4f->leave($__internal_d059f1cf3c20272331d930e26b72c77dffea02f09cd38d97735b0b253525ca4f_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "FOSUserBundle:Security:login.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
