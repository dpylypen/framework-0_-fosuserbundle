<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_c491ad478d07e25ca016ab97fc6ce9a0a1db9cb463b5559c02a9c4d3d0b29e11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c791ff1ed8ce89a31a042d945ca4013418e85fc1757bec2854d7ca63dddb14d6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c791ff1ed8ce89a31a042d945ca4013418e85fc1757bec2854d7ca63dddb14d6->enter($__internal_c791ff1ed8ce89a31a042d945ca4013418e85fc1757bec2854d7ca63dddb14d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_24d5c1ec9f5970ed392b725d068bc0dded8a1648cbbf3e8db25ef8c50fac2a12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24d5c1ec9f5970ed392b725d068bc0dded8a1648cbbf3e8db25ef8c50fac2a12->enter($__internal_24d5c1ec9f5970ed392b725d068bc0dded8a1648cbbf3e8db25ef8c50fac2a12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_c791ff1ed8ce89a31a042d945ca4013418e85fc1757bec2854d7ca63dddb14d6->leave($__internal_c791ff1ed8ce89a31a042d945ca4013418e85fc1757bec2854d7ca63dddb14d6_prof);

        
        $__internal_24d5c1ec9f5970ed392b725d068bc0dded8a1648cbbf3e8db25ef8c50fac2a12->leave($__internal_24d5c1ec9f5970ed392b725d068bc0dded8a1648cbbf3e8db25ef8c50fac2a12_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/chevron-right.svg");
    }
}
