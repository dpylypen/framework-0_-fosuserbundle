<?php

/* WebProfilerBundle:Collector:exception.css.twig */
class __TwigTemplate_f0abdd1a85462b09b91cee530d949b16bc5644939ded2e1be32569709785a0b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d0b79d4eaa3ccf062d97f34288a4f68f5f33e38498bc2a0ca5ba7c19505e57d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0b79d4eaa3ccf062d97f34288a4f68f5f33e38498bc2a0ca5ba7c19505e57d4->enter($__internal_d0b79d4eaa3ccf062d97f34288a4f68f5f33e38498bc2a0ca5ba7c19505e57d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        $__internal_34289134e8e04a026c7bcca235b3e6812e1db7487e9661ea7aa9efcb2c2a4e7a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_34289134e8e04a026c7bcca235b3e6812e1db7487e9661ea7aa9efcb2c2a4e7a->enter($__internal_34289134e8e04a026c7bcca235b3e6812e1db7487e9661ea7aa9efcb2c2a4e7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}
.exception-summary.exception-without-message {
    display: none;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper .container {
    min-height: auto;
}
";
        
        $__internal_d0b79d4eaa3ccf062d97f34288a4f68f5f33e38498bc2a0ca5ba7c19505e57d4->leave($__internal_d0b79d4eaa3ccf062d97f34288a4f68f5f33e38498bc2a0ca5ba7c19505e57d4_prof);

        
        $__internal_34289134e8e04a026c7bcca235b3e6812e1db7487e9661ea7aa9efcb2c2a4e7a->leave($__internal_34289134e8e04a026c7bcca235b3e6812e1db7487e9661ea7aa9efcb2c2a4e7a_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/exception.css.twig') }}

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}
.exception-summary.exception-without-message {
    display: none;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper .container {
    min-height: auto;
}
", "WebProfilerBundle:Collector:exception.css.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.css.twig");
    }
}
