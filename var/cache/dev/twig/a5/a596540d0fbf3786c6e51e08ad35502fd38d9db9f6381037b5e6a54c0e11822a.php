<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_a0d5943e89af59669497d15da8763bc9e7326756bf5c133d6c76d82b9f7e86c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_729f8c8d0d85ebac84561991a9d5713c4aff65a771b5036d4d7f7fdf61f63d51 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_729f8c8d0d85ebac84561991a9d5713c4aff65a771b5036d4d7f7fdf61f63d51->enter($__internal_729f8c8d0d85ebac84561991a9d5713c4aff65a771b5036d4d7f7fdf61f63d51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_5a0464d496ed519836ca1bf5b421b4c4626500535106fed2b49cda0abe2171d8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a0464d496ed519836ca1bf5b421b4c4626500535106fed2b49cda0abe2171d8->enter($__internal_5a0464d496ed519836ca1bf5b421b4c4626500535106fed2b49cda0abe2171d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_729f8c8d0d85ebac84561991a9d5713c4aff65a771b5036d4d7f7fdf61f63d51->leave($__internal_729f8c8d0d85ebac84561991a9d5713c4aff65a771b5036d4d7f7fdf61f63d51_prof);

        
        $__internal_5a0464d496ed519836ca1bf5b421b4c4626500535106fed2b49cda0abe2171d8->leave($__internal_5a0464d496ed519836ca1bf5b421b4c4626500535106fed2b49cda0abe2171d8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
