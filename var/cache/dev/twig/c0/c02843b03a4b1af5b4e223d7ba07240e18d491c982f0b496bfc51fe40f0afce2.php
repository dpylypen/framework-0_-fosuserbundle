<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_f925495ea867336522d4ef0cbab40b7aad9844ab58798750160f49fb16f17818 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_85437f1a0eac08496628467b5d6784343ec6e429e9e10fe7f7b4b728a3c30648 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_85437f1a0eac08496628467b5d6784343ec6e429e9e10fe7f7b4b728a3c30648->enter($__internal_85437f1a0eac08496628467b5d6784343ec6e429e9e10fe7f7b4b728a3c30648_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_967c6809aaa8dd46a4ff6572d85105e17540bbdb2074aa7db5923050f4f3630f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_967c6809aaa8dd46a4ff6572d85105e17540bbdb2074aa7db5923050f4f3630f->enter($__internal_967c6809aaa8dd46a4ff6572d85105e17540bbdb2074aa7db5923050f4f3630f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_85437f1a0eac08496628467b5d6784343ec6e429e9e10fe7f7b4b728a3c30648->leave($__internal_85437f1a0eac08496628467b5d6784343ec6e429e9e10fe7f7b4b728a3c30648_prof);

        
        $__internal_967c6809aaa8dd46a4ff6572d85105e17540bbdb2074aa7db5923050f4f3630f->leave($__internal_967c6809aaa8dd46a4ff6572d85105e17540bbdb2074aa7db5923050f4f3630f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
