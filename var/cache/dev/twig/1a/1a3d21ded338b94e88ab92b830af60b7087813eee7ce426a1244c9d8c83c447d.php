<?php

/* SonataAdminBundle:CRUD:list.html.twig */
class __TwigTemplate_886988979dca5e3d5758ffd0d09df5f6f09abcde8033ee96695bfae978140645 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_list.html.twig", "SonataAdminBundle:CRUD:list.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_list.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_748b30ccebb75ce2d792e04ceaf7525c0b26bd23e2936fbb2e782f7ce77812c5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_748b30ccebb75ce2d792e04ceaf7525c0b26bd23e2936fbb2e782f7ce77812c5->enter($__internal_748b30ccebb75ce2d792e04ceaf7525c0b26bd23e2936fbb2e782f7ce77812c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list.html.twig"));

        $__internal_e27582f6348d65fc3412d59b10ec8db49e373467445b89bf5e640feecbbca53d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e27582f6348d65fc3412d59b10ec8db49e373467445b89bf5e640feecbbca53d->enter($__internal_e27582f6348d65fc3412d59b10ec8db49e373467445b89bf5e640feecbbca53d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_748b30ccebb75ce2d792e04ceaf7525c0b26bd23e2936fbb2e782f7ce77812c5->leave($__internal_748b30ccebb75ce2d792e04ceaf7525c0b26bd23e2936fbb2e782f7ce77812c5_prof);

        
        $__internal_e27582f6348d65fc3412d59b10ec8db49e373467445b89bf5e640feecbbca53d->leave($__internal_e27582f6348d65fc3412d59b10ec8db49e373467445b89bf5e640feecbbca53d_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_list.html.twig' %}
", "SonataAdminBundle:CRUD:list.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list.html.twig");
    }
}
