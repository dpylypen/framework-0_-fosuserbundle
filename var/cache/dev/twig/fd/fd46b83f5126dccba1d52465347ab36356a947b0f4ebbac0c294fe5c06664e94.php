<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_8c79b959145dca7682ea740640a741386190655ab39031816861a58a34ba7b92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_81523990ced2ff7babd49262b57358b1746dbecf1cfc96d9c1e46b8abe43e596 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_81523990ced2ff7babd49262b57358b1746dbecf1cfc96d9c1e46b8abe43e596->enter($__internal_81523990ced2ff7babd49262b57358b1746dbecf1cfc96d9c1e46b8abe43e596_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_8068e964800186ae74940ddc3d91bfe8323eb9a4a3451ea4a9effa87c042acf6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8068e964800186ae74940ddc3d91bfe8323eb9a4a3451ea4a9effa87c042acf6->enter($__internal_8068e964800186ae74940ddc3d91bfe8323eb9a4a3451ea4a9effa87c042acf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_81523990ced2ff7babd49262b57358b1746dbecf1cfc96d9c1e46b8abe43e596->leave($__internal_81523990ced2ff7babd49262b57358b1746dbecf1cfc96d9c1e46b8abe43e596_prof);

        
        $__internal_8068e964800186ae74940ddc3d91bfe8323eb9a4a3451ea4a9effa87c042acf6->leave($__internal_8068e964800186ae74940ddc3d91bfe8323eb9a4a3451ea4a9effa87c042acf6_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_c6c2737a20b04a65ccaf87d6575521faf6c9d85259634790982a051fc4f01f29 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6c2737a20b04a65ccaf87d6575521faf6c9d85259634790982a051fc4f01f29->enter($__internal_c6c2737a20b04a65ccaf87d6575521faf6c9d85259634790982a051fc4f01f29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_82f2d379de541b5140d673ec36212bb635a0c6b319cc026d3832a02b0d53a3e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_82f2d379de541b5140d673ec36212bb635a0c6b319cc026d3832a02b0d53a3e8->enter($__internal_82f2d379de541b5140d673ec36212bb635a0c6b319cc026d3832a02b0d53a3e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_82f2d379de541b5140d673ec36212bb635a0c6b319cc026d3832a02b0d53a3e8->leave($__internal_82f2d379de541b5140d673ec36212bb635a0c6b319cc026d3832a02b0d53a3e8_prof);

        
        $__internal_c6c2737a20b04a65ccaf87d6575521faf6c9d85259634790982a051fc4f01f29->leave($__internal_c6c2737a20b04a65ccaf87d6575521faf6c9d85259634790982a051fc4f01f29_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_1d39509adf6ecd452560d20e92583d20d9956605c0f91dbe16d34859f679838a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d39509adf6ecd452560d20e92583d20d9956605c0f91dbe16d34859f679838a->enter($__internal_1d39509adf6ecd452560d20e92583d20d9956605c0f91dbe16d34859f679838a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_e079e48090d3a528790f66d41db85e22946af6f8c823336eb3fb1893399ca06d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e079e48090d3a528790f66d41db85e22946af6f8c823336eb3fb1893399ca06d->enter($__internal_e079e48090d3a528790f66d41db85e22946af6f8c823336eb3fb1893399ca06d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_e079e48090d3a528790f66d41db85e22946af6f8c823336eb3fb1893399ca06d->leave($__internal_e079e48090d3a528790f66d41db85e22946af6f8c823336eb3fb1893399ca06d_prof);

        
        $__internal_1d39509adf6ecd452560d20e92583d20d9956605c0f91dbe16d34859f679838a->leave($__internal_1d39509adf6ecd452560d20e92583d20d9956605c0f91dbe16d34859f679838a_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_9471b206d62869a04a14ca65391edf1b2819327612214b1ab715f898c762cb61 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9471b206d62869a04a14ca65391edf1b2819327612214b1ab715f898c762cb61->enter($__internal_9471b206d62869a04a14ca65391edf1b2819327612214b1ab715f898c762cb61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b37a534520ccb736bfaea0fde6dee2ce3a0c122cd1a1ab70ab36cb5ca4fa940f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b37a534520ccb736bfaea0fde6dee2ce3a0c122cd1a1ab70ab36cb5ca4fa940f->enter($__internal_b37a534520ccb736bfaea0fde6dee2ce3a0c122cd1a1ab70ab36cb5ca4fa940f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_b37a534520ccb736bfaea0fde6dee2ce3a0c122cd1a1ab70ab36cb5ca4fa940f->leave($__internal_b37a534520ccb736bfaea0fde6dee2ce3a0c122cd1a1ab70ab36cb5ca4fa940f_prof);

        
        $__internal_9471b206d62869a04a14ca65391edf1b2819327612214b1ab715f898c762cb61->leave($__internal_9471b206d62869a04a14ca65391edf1b2819327612214b1ab715f898c762cb61_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
