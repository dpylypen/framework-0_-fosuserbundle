<?php

/* SonataCoreBundle:Form:colorpicker.html.twig */
class __TwigTemplate_628ccd39f0d44c3fc061717d3441e79802152b1cda94a2125810b24df7dcd52e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sonata_type_color_selector_widget' => array($this, 'block_sonata_type_color_selector_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_22b41c0d172c0bf31e60448e07c343ffe20ddaf46a33326e4895ef817b37b8c5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22b41c0d172c0bf31e60448e07c343ffe20ddaf46a33326e4895ef817b37b8c5->enter($__internal_22b41c0d172c0bf31e60448e07c343ffe20ddaf46a33326e4895ef817b37b8c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataCoreBundle:Form:colorpicker.html.twig"));

        $__internal_f379b4c9d0e06242b8533149f9d97287c2e9ba50e97c50bb209e0df456c838dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f379b4c9d0e06242b8533149f9d97287c2e9ba50e97c50bb209e0df456c838dc->enter($__internal_f379b4c9d0e06242b8533149f9d97287c2e9ba50e97c50bb209e0df456c838dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataCoreBundle:Form:colorpicker.html.twig"));

        // line 11
        $this->displayBlock('sonata_type_color_selector_widget', $context, $blocks);
        
        $__internal_22b41c0d172c0bf31e60448e07c343ffe20ddaf46a33326e4895ef817b37b8c5->leave($__internal_22b41c0d172c0bf31e60448e07c343ffe20ddaf46a33326e4895ef817b37b8c5_prof);

        
        $__internal_f379b4c9d0e06242b8533149f9d97287c2e9ba50e97c50bb209e0df456c838dc->leave($__internal_f379b4c9d0e06242b8533149f9d97287c2e9ba50e97c50bb209e0df456c838dc_prof);

    }

    public function block_sonata_type_color_selector_widget($context, array $blocks = array())
    {
        $__internal_b7539ea4a7e5a2bbd4d1a007c7cfe649e61847a007dffc1b7b6cf2b1613efd89 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b7539ea4a7e5a2bbd4d1a007c7cfe649e61847a007dffc1b7b6cf2b1613efd89->enter($__internal_b7539ea4a7e5a2bbd4d1a007c7cfe649e61847a007dffc1b7b6cf2b1613efd89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_color_selector_widget"));

        $__internal_87efdb895d9a1e62373d81b4d18afbd161071e516387dcc589ded241d17a3d57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_87efdb895d9a1e62373d81b4d18afbd161071e516387dcc589ded241d17a3d57->enter($__internal_87efdb895d9a1e62373d81b4d18afbd161071e516387dcc589ded241d17a3d57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_color_selector_widget"));

        // line 12
        echo "    ";
        $this->displayBlock("choice_widget", $context, $blocks);
        echo "
    ";
        // line 13
        ob_start();
        // line 14
        echo "        <script type=\"text/javascript\">
            jQuery(function (\$) {
                var select2FormatColorSelect = function format(state) {
                    if (!state.id || state.disabled) {
                        return state.text;
                    }

                    return ' <i class=\"fa fa-square\" style=\"color: '+ state.id +'\"></i> ' + state.text;
                };

                \$('#";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["id"]) || array_key_exists("id", $context) ? $context["id"] : (function () { throw new Twig_Error_Runtime('Variable "id" does not exist.', 24, $this->getSourceContext()); })()), "html", null, true);
        echo "').select2({
                    formatResult:    select2FormatColorSelect,
                    formatSelection: select2FormatColorSelect,
                    width:           '100%',
                    escapeMarkup:    function(m) { return m; }
                });
            });
        </script>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_87efdb895d9a1e62373d81b4d18afbd161071e516387dcc589ded241d17a3d57->leave($__internal_87efdb895d9a1e62373d81b4d18afbd161071e516387dcc589ded241d17a3d57_prof);

        
        $__internal_b7539ea4a7e5a2bbd4d1a007c7cfe649e61847a007dffc1b7b6cf2b1613efd89->leave($__internal_b7539ea4a7e5a2bbd4d1a007c7cfe649e61847a007dffc1b7b6cf2b1613efd89_prof);

    }

    public function getTemplateName()
    {
        return "SonataCoreBundle:Form:colorpicker.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  63 => 24,  51 => 14,  49 => 13,  44 => 12,  26 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% block sonata_type_color_selector_widget %}
    {{ block('choice_widget') }}
    {% spaceless %}
        <script type=\"text/javascript\">
            jQuery(function (\$) {
                var select2FormatColorSelect = function format(state) {
                    if (!state.id || state.disabled) {
                        return state.text;
                    }

                    return ' <i class=\"fa fa-square\" style=\"color: '+ state.id +'\"></i> ' + state.text;
                };

                \$('#{{ id }}').select2({
                    formatResult:    select2FormatColorSelect,
                    formatSelection: select2FormatColorSelect,
                    width:           '100%',
                    escapeMarkup:    function(m) { return m; }
                });
            });
        </script>
    {% endspaceless %}
{% endblock sonata_type_color_selector_widget %}
", "SonataCoreBundle:Form:colorpicker.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/core-bundle/Resources/views/Form/colorpicker.html.twig");
    }
}
