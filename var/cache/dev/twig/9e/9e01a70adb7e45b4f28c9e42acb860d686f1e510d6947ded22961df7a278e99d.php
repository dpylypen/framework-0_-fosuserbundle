<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_6026ce6a9cb2bf989e7445be81a44888666ab900a39de3f0a69117225e19d826 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7ed160f1b2616f1e315e5976a81b4ea1727207d52aab157c3d5120f39229d1ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ed160f1b2616f1e315e5976a81b4ea1727207d52aab157c3d5120f39229d1ca->enter($__internal_7ed160f1b2616f1e315e5976a81b4ea1727207d52aab157c3d5120f39229d1ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_11a5eb600fb50efbc081eefdc04b320d0471dd1e8ed0bc61e568782292452f2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_11a5eb600fb50efbc081eefdc04b320d0471dd1e8ed0bc61e568782292452f2d->enter($__internal_11a5eb600fb50efbc081eefdc04b320d0471dd1e8ed0bc61e568782292452f2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_7ed160f1b2616f1e315e5976a81b4ea1727207d52aab157c3d5120f39229d1ca->leave($__internal_7ed160f1b2616f1e315e5976a81b4ea1727207d52aab157c3d5120f39229d1ca_prof);

        
        $__internal_11a5eb600fb50efbc081eefdc04b320d0471dd1e8ed0bc61e568782292452f2d->leave($__internal_11a5eb600fb50efbc081eefdc04b320d0471dd1e8ed0bc61e568782292452f2d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_row.html.php");
    }
}
