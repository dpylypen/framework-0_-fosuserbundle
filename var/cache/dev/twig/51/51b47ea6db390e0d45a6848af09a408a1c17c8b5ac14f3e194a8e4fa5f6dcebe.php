<?php

/* SonataAdminBundle:CRUD:show_percent.html.twig */
class __TwigTemplate_0bab609004dff6e3fbf600ce8d70ac43afbc0b476fc3f36c332f6e4c8dc144fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_percent.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3b28d00315c2351d03e9bbf0e9cba5ce76c59629f4684a86e1de71c9e2693da7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3b28d00315c2351d03e9bbf0e9cba5ce76c59629f4684a86e1de71c9e2693da7->enter($__internal_3b28d00315c2351d03e9bbf0e9cba5ce76c59629f4684a86e1de71c9e2693da7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_percent.html.twig"));

        $__internal_e902c1e31dea37b3b9ffcd6de2dc5e5fad8ca2a22e519daa0e31e7de32340c24 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e902c1e31dea37b3b9ffcd6de2dc5e5fad8ca2a22e519daa0e31e7de32340c24->enter($__internal_e902c1e31dea37b3b9ffcd6de2dc5e5fad8ca2a22e519daa0e31e7de32340c24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_percent.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3b28d00315c2351d03e9bbf0e9cba5ce76c59629f4684a86e1de71c9e2693da7->leave($__internal_3b28d00315c2351d03e9bbf0e9cba5ce76c59629f4684a86e1de71c9e2693da7_prof);

        
        $__internal_e902c1e31dea37b3b9ffcd6de2dc5e5fad8ca2a22e519daa0e31e7de32340c24->leave($__internal_e902c1e31dea37b3b9ffcd6de2dc5e5fad8ca2a22e519daa0e31e7de32340c24_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_62009fae49dac33ef6d349899a16b4ca07a4ec057211f94a21174d1bd9db21de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_62009fae49dac33ef6d349899a16b4ca07a4ec057211f94a21174d1bd9db21de->enter($__internal_62009fae49dac33ef6d349899a16b4ca07a4ec057211f94a21174d1bd9db21de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_6597357c293082636425256f3203554067892e8419e941565b28e2bf7bfa08ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6597357c293082636425256f3203554067892e8419e941565b28e2bf7bfa08ec->enter($__internal_6597357c293082636425256f3203554067892e8419e941565b28e2bf7bfa08ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["value"] = ((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()) * 100);
        // line 16
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
        echo " %
";
        
        $__internal_6597357c293082636425256f3203554067892e8419e941565b28e2bf7bfa08ec->leave($__internal_6597357c293082636425256f3203554067892e8419e941565b28e2bf7bfa08ec_prof);

        
        $__internal_62009fae49dac33ef6d349899a16b4ca07a4ec057211f94a21174d1bd9db21de->leave($__internal_62009fae49dac33ef6d349899a16b4ca07a4ec057211f94a21174d1bd9db21de_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_percent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field %}
    {% set value = value * 100 %}
    {{ value }} %
{% endblock %}
", "SonataAdminBundle:CRUD:show_percent.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/show_percent.html.twig");
    }
}
