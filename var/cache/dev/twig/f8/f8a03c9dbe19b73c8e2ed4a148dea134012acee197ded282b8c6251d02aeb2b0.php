<?php

/* SonataAdminBundle:Pager:results.html.twig */
class __TwigTemplate_f3f796eb48c8bad4783e810ff8ffe193dff60072afcaf6027f7e964209b2c4b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:Pager:base_results.html.twig", "SonataAdminBundle:Pager:results.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Pager:base_results.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_06e69867ad77ac1ed5ee1acd239ef769b1cce94407a0ca980f20a0c0aa388e4d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_06e69867ad77ac1ed5ee1acd239ef769b1cce94407a0ca980f20a0c0aa388e4d->enter($__internal_06e69867ad77ac1ed5ee1acd239ef769b1cce94407a0ca980f20a0c0aa388e4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:results.html.twig"));

        $__internal_5fa5b68d276e1b41672dae570dc248f0f925af6d7b7dadc00844428a098329db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5fa5b68d276e1b41672dae570dc248f0f925af6d7b7dadc00844428a098329db->enter($__internal_5fa5b68d276e1b41672dae570dc248f0f925af6d7b7dadc00844428a098329db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:results.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_06e69867ad77ac1ed5ee1acd239ef769b1cce94407a0ca980f20a0c0aa388e4d->leave($__internal_06e69867ad77ac1ed5ee1acd239ef769b1cce94407a0ca980f20a0c0aa388e4d_prof);

        
        $__internal_5fa5b68d276e1b41672dae570dc248f0f925af6d7b7dadc00844428a098329db->leave($__internal_5fa5b68d276e1b41672dae570dc248f0f925af6d7b7dadc00844428a098329db_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Pager:results.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:Pager:base_results.html.twig' %}
", "SonataAdminBundle:Pager:results.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/Pager/results.html.twig");
    }
}
