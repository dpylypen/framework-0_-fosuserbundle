<?php

/* SonataAdminBundle:CRUD:acl.html.twig */
class __TwigTemplate_c80a62b1123ff2ebc835a9f26c9c476916619c976253f0794e805c9709abdf4b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_acl.html.twig", "SonataAdminBundle:CRUD:acl.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_acl.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd2e83dfc22d0873762029ecf814cc00c31b916d7c8a66dfbbac8e0c79048379 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd2e83dfc22d0873762029ecf814cc00c31b916d7c8a66dfbbac8e0c79048379->enter($__internal_fd2e83dfc22d0873762029ecf814cc00c31b916d7c8a66dfbbac8e0c79048379_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:acl.html.twig"));

        $__internal_2ee124f8928dd1af3e742ed7d5e394c6d34c785e0376f63682c39df1c48a4f64 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2ee124f8928dd1af3e742ed7d5e394c6d34c785e0376f63682c39df1c48a4f64->enter($__internal_2ee124f8928dd1af3e742ed7d5e394c6d34c785e0376f63682c39df1c48a4f64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:acl.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fd2e83dfc22d0873762029ecf814cc00c31b916d7c8a66dfbbac8e0c79048379->leave($__internal_fd2e83dfc22d0873762029ecf814cc00c31b916d7c8a66dfbbac8e0c79048379_prof);

        
        $__internal_2ee124f8928dd1af3e742ed7d5e394c6d34c785e0376f63682c39df1c48a4f64->leave($__internal_2ee124f8928dd1af3e742ed7d5e394c6d34c785e0376f63682c39df1c48a4f64_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:acl.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_acl.html.twig' %}
", "SonataAdminBundle:CRUD:acl.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/acl.html.twig");
    }
}
