<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_a88beacbb5926574c1d9a740ea1a76e5bcb7f7b0291289f06a3bdea99791ae86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_233aef261e974669db3997fd8b7141391fd8e61dbcf1366e8d031f44625b2593 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_233aef261e974669db3997fd8b7141391fd8e61dbcf1366e8d031f44625b2593->enter($__internal_233aef261e974669db3997fd8b7141391fd8e61dbcf1366e8d031f44625b2593_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_32d695a251c75096d200a33838bd0fb1084d8ef6471a9f8ba3a778a765c6ddb9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32d695a251c75096d200a33838bd0fb1084d8ef6471a9f8ba3a778a765c6ddb9->enter($__internal_32d695a251c75096d200a33838bd0fb1084d8ef6471a9f8ba3a778a765c6ddb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_233aef261e974669db3997fd8b7141391fd8e61dbcf1366e8d031f44625b2593->leave($__internal_233aef261e974669db3997fd8b7141391fd8e61dbcf1366e8d031f44625b2593_prof);

        
        $__internal_32d695a251c75096d200a33838bd0fb1084d8ef6471a9f8ba3a778a765c6ddb9->leave($__internal_32d695a251c75096d200a33838bd0fb1084d8ef6471a9f8ba3a778a765c6ddb9_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_c8020946e3919c7ecb1aa4d77bc662405f2f6975e4b72a8e863813045ceb06f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8020946e3919c7ecb1aa4d77bc662405f2f6975e4b72a8e863813045ceb06f9->enter($__internal_c8020946e3919c7ecb1aa4d77bc662405f2f6975e4b72a8e863813045ceb06f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_367532588ee36999c5d1ffde50af87f16645b7d3ec47620076cd72c3c3cac9d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_367532588ee36999c5d1ffde50af87f16645b7d3ec47620076cd72c3c3cac9d9->enter($__internal_367532588ee36999c5d1ffde50af87f16645b7d3ec47620076cd72c3c3cac9d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_367532588ee36999c5d1ffde50af87f16645b7d3ec47620076cd72c3c3cac9d9->leave($__internal_367532588ee36999c5d1ffde50af87f16645b7d3ec47620076cd72c3c3cac9d9_prof);

        
        $__internal_c8020946e3919c7ecb1aa4d77bc662405f2f6975e4b72a8e863813045ceb06f9->leave($__internal_c8020946e3919c7ecb1aa4d77bc662405f2f6975e4b72a8e863813045ceb06f9_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_0583e34b027ea14504daa3e493786af1fddc88b69d676b797e140077c017eb20 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0583e34b027ea14504daa3e493786af1fddc88b69d676b797e140077c017eb20->enter($__internal_0583e34b027ea14504daa3e493786af1fddc88b69d676b797e140077c017eb20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_abb5e30ed85bcd6f177e27d2fd4777412a5cfeb7718a3e1fd81b067cc6a709a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_abb5e30ed85bcd6f177e27d2fd4777412a5cfeb7718a3e1fd81b067cc6a709a8->enter($__internal_abb5e30ed85bcd6f177e27d2fd4777412a5cfeb7718a3e1fd81b067cc6a709a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["file"]) || array_key_exists("file", $context) ? $context["file"] : (function () { throw new Twig_Error_Runtime('Variable "file" does not exist.', 11, $this->getSourceContext()); })()), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, (isset($context["line"]) || array_key_exists("line", $context) ? $context["line"] : (function () { throw new Twig_Error_Runtime('Variable "line" does not exist.', 11, $this->getSourceContext()); })()), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) || array_key_exists("filename", $context) ? $context["filename"] : (function () { throw new Twig_Error_Runtime('Variable "filename" does not exist.', 15, $this->getSourceContext()); })()), (isset($context["line"]) || array_key_exists("line", $context) ? $context["line"] : (function () { throw new Twig_Error_Runtime('Variable "line" does not exist.', 15, $this->getSourceContext()); })()),  -1);
        echo "
</div>
";
        
        $__internal_abb5e30ed85bcd6f177e27d2fd4777412a5cfeb7718a3e1fd81b067cc6a709a8->leave($__internal_abb5e30ed85bcd6f177e27d2fd4777412a5cfeb7718a3e1fd81b067cc6a709a8_prof);

        
        $__internal_0583e34b027ea14504daa3e493786af1fddc88b69d676b797e140077c017eb20->leave($__internal_0583e34b027ea14504daa3e493786af1fddc88b69d676b797e140077c017eb20_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
