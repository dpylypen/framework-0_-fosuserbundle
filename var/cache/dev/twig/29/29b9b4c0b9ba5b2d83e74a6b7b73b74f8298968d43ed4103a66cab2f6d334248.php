<?php

/* SonataAdminBundle:Core:create_button.html.twig */
class __TwigTemplate_5868aecaccd09f0186fde1c01f4052eedcbbae404d0ebd3c8770c31b1f32b7a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 15
        $this->parent = $this->loadTemplate("SonataAdminBundle:Button:create_button.html.twig", "SonataAdminBundle:Core:create_button.html.twig", 15);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Button:create_button.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1d3f0970dddd631d6333deacdcf74e39597a1daa208568ccbac9d16ffd83b8f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d3f0970dddd631d6333deacdcf74e39597a1daa208568ccbac9d16ffd83b8f1->enter($__internal_1d3f0970dddd631d6333deacdcf74e39597a1daa208568ccbac9d16ffd83b8f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Core:create_button.html.twig"));

        $__internal_92be38aa6226af1cf4fe1a2b9909a0e8463bf05895b196821f8b41174b0503fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92be38aa6226af1cf4fe1a2b9909a0e8463bf05895b196821f8b41174b0503fe->enter($__internal_92be38aa6226af1cf4fe1a2b9909a0e8463bf05895b196821f8b41174b0503fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Core:create_button.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1d3f0970dddd631d6333deacdcf74e39597a1daa208568ccbac9d16ffd83b8f1->leave($__internal_1d3f0970dddd631d6333deacdcf74e39597a1daa208568ccbac9d16ffd83b8f1_prof);

        
        $__internal_92be38aa6226af1cf4fe1a2b9909a0e8463bf05895b196821f8b41174b0503fe->leave($__internal_92be38aa6226af1cf4fe1a2b9909a0e8463bf05895b196821f8b41174b0503fe_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Core:create_button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 15,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{# DEPRECATED #}
{# This file is kept here for backward compatibility - Rather use SonataAdminBundle:Button:create_button.html.twig #}

{% extends 'SonataAdminBundle:Button:create_button.html.twig' %}
", "SonataAdminBundle:Core:create_button.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/Core/create_button.html.twig");
    }
}
