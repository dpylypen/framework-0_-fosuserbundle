<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_0f1afbce9ba8872bbb50eef726c48847a9d1f3e3c172b87ed877676a9dbc4c89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a24b3b7febbf22a8d13cb32e1367c98ce0c3d10198e14cf796903afb15007f2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a24b3b7febbf22a8d13cb32e1367c98ce0c3d10198e14cf796903afb15007f2e->enter($__internal_a24b3b7febbf22a8d13cb32e1367c98ce0c3d10198e14cf796903afb15007f2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_e653905af720c1038258d7d2b9e79a09c4f1a64cdaf7bfdb0b9c7fc3c3d2fae5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e653905af720c1038258d7d2b9e79a09c4f1a64cdaf7bfdb0b9c7fc3c3d2fae5->enter($__internal_e653905af720c1038258d7d2b9e79a09c4f1a64cdaf7bfdb0b9c7fc3c3d2fae5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_a24b3b7febbf22a8d13cb32e1367c98ce0c3d10198e14cf796903afb15007f2e->leave($__internal_a24b3b7febbf22a8d13cb32e1367c98ce0c3d10198e14cf796903afb15007f2e_prof);

        
        $__internal_e653905af720c1038258d7d2b9e79a09c4f1a64cdaf7bfdb0b9c7fc3c3d2fae5->leave($__internal_e653905af720c1038258d7d2b9e79a09c4f1a64cdaf7bfdb0b9c7fc3c3d2fae5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_widget_compound.html.php");
    }
}
