<?php

/* SonataAdminBundle:CRUD:show_datetime.html.twig */
class __TwigTemplate_dc8c42b25fea1cd3f607b9ca9af8bc7e3471786de53ce5503c41ca46a9cdfdc3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_datetime.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_329bd5cb963beb065804c3b8871692e0454570bf9b26b8d4b5e71ff569900f63 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_329bd5cb963beb065804c3b8871692e0454570bf9b26b8d4b5e71ff569900f63->enter($__internal_329bd5cb963beb065804c3b8871692e0454570bf9b26b8d4b5e71ff569900f63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_datetime.html.twig"));

        $__internal_6171e244f72ad363823d07f3868ca4643c21cc7f7c4cc3ceb5d193440263f175 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6171e244f72ad363823d07f3868ca4643c21cc7f7c4cc3ceb5d193440263f175->enter($__internal_6171e244f72ad363823d07f3868ca4643c21cc7f7c4cc3ceb5d193440263f175_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_datetime.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_329bd5cb963beb065804c3b8871692e0454570bf9b26b8d4b5e71ff569900f63->leave($__internal_329bd5cb963beb065804c3b8871692e0454570bf9b26b8d4b5e71ff569900f63_prof);

        
        $__internal_6171e244f72ad363823d07f3868ca4643c21cc7f7c4cc3ceb5d193440263f175->leave($__internal_6171e244f72ad363823d07f3868ca4643c21cc7f7c4cc3ceb5d193440263f175_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_c606190be9cf99b7fc3b1242c1ed63cf165cbc35fe01017bf27fe12df7ec4d23 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c606190be9cf99b7fc3b1242c1ed63cf165cbc35fe01017bf27fe12df7ec4d23->enter($__internal_c606190be9cf99b7fc3b1242c1ed63cf165cbc35fe01017bf27fe12df7ec4d23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_8895992a226848906848d9ee89281040cf96a7bcdce89bdb26b4b5b1b2bd5784 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8895992a226848906848d9ee89281040cf96a7bcdce89bdb26b4b5b1b2bd5784->enter($__internal_8895992a226848906848d9ee89281040cf96a7bcdce89bdb26b4b5b1b2bd5784_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twig_test_empty((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "&nbsp;";
        } elseif (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(),         // line 17
($context["field_description"] ?? null), "options", array(), "any", false, true), "format", array(), "any", true, true)) {
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 18, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 18, $this->getSourceContext()); })()), "options", array()), "format", array())), "html", null, true);
        } else {
            // line 20
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 20, $this->getSourceContext()); })())), "html", null, true);
        }
        
        $__internal_8895992a226848906848d9ee89281040cf96a7bcdce89bdb26b4b5b1b2bd5784->leave($__internal_8895992a226848906848d9ee89281040cf96a7bcdce89bdb26b4b5b1b2bd5784_prof);

        
        $__internal_c606190be9cf99b7fc3b1242c1ed63cf165cbc35fe01017bf27fe12df7ec4d23->leave($__internal_c606190be9cf99b7fc3b1242c1ed63cf165cbc35fe01017bf27fe12df7ec4d23_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_datetime.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 20,  55 => 18,  53 => 17,  51 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field %}
    {%- if value is empty -%}
        &nbsp;
    {%- elseif field_description.options.format is defined -%}
        {{ value|date(field_description.options.format) }}
    {%- else -%}
        {{ value|date }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:show_datetime.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/show_datetime.html.twig");
    }
}
