<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_016857a7a11f3bf7eba27675c0f8784dd820ae207796899463a2344ccf826ec9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b35abf54849da2515a54fd7a87aff120a568fa9599d744925791b103cb41dd1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b35abf54849da2515a54fd7a87aff120a568fa9599d744925791b103cb41dd1->enter($__internal_6b35abf54849da2515a54fd7a87aff120a568fa9599d744925791b103cb41dd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        $__internal_e027d1750db9a2efc286aaa04c2d4887ca450be0d2ce193988bda5b351f503c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e027d1750db9a2efc286aaa04c2d4887ca450be0d2ce193988bda5b351f503c2->enter($__internal_e027d1750db9a2efc286aaa04c2d4887ca450be0d2ce193988bda5b351f503c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["group"]) || array_key_exists("group", $context) ? $context["group"] : (function () { throw new Twig_Error_Runtime('Variable "group" does not exist.', 4, $this->getSourceContext()); })()), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_6b35abf54849da2515a54fd7a87aff120a568fa9599d744925791b103cb41dd1->leave($__internal_6b35abf54849da2515a54fd7a87aff120a568fa9599d744925791b103cb41dd1_prof);

        
        $__internal_e027d1750db9a2efc286aaa04c2d4887ca450be0d2ce193988bda5b351f503c2->leave($__internal_e027d1750db9a2efc286aaa04c2d4887ca450be0d2ce193988bda5b351f503c2_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_group_show\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "FOSUserBundle:Group:show_content.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show_content.html.twig");
    }
}
