<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_8bfded8c4e88ce1b5324d3c94cdb66cc0fcfb85b039cec6b61991d78e8e9e289 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dcbeaa5599bfb1a98eead96229494ef45614daad035f7809083d30b9b971f31a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dcbeaa5599bfb1a98eead96229494ef45614daad035f7809083d30b9b971f31a->enter($__internal_dcbeaa5599bfb1a98eead96229494ef45614daad035f7809083d30b9b971f31a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        $__internal_20469e25aaadf0398664f5502d456d8fd0480a81ac1c95fa2b8de5bc71cf5ab8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20469e25aaadf0398664f5502d456d8fd0480a81ac1c95fa2b8de5bc71cf5ab8->enter($__internal_20469e25aaadf0398664f5502d456d8fd0480a81ac1c95fa2b8de5bc71cf5ab8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 2, $this->getSourceContext()); })()), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 2, $this->getSourceContext()); })()), "css", null, true);
        echo "

*/
";
        
        $__internal_dcbeaa5599bfb1a98eead96229494ef45614daad035f7809083d30b9b971f31a->leave($__internal_dcbeaa5599bfb1a98eead96229494ef45614daad035f7809083d30b9b971f31a_prof);

        
        $__internal_20469e25aaadf0398664f5502d456d8fd0480a81ac1c95fa2b8de5bc71cf5ab8->leave($__internal_20469e25aaadf0398664f5502d456d8fd0480a81ac1c95fa2b8de5bc71cf5ab8_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.css.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.css.twig");
    }
}
