<?php

/* SonataAdminBundle:CRUD:list__select.html.twig */
class __TwigTemplate_3b97b632045a945bc8979b1e698044ab1525453bed32d88da76bd42f27c6b4b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list__select.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4036b5f854269930af5e24c6ea10416268c4096a148593db23944462429c65ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4036b5f854269930af5e24c6ea10416268c4096a148593db23944462429c65ec->enter($__internal_4036b5f854269930af5e24c6ea10416268c4096a148593db23944462429c65ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list__select.html.twig"));

        $__internal_4241d254473338855641348bc057b55648c0bbf672873a864e1fb1259bd1742c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4241d254473338855641348bc057b55648c0bbf672873a864e1fb1259bd1742c->enter($__internal_4241d254473338855641348bc057b55648c0bbf672873a864e1fb1259bd1742c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list__select.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4036b5f854269930af5e24c6ea10416268c4096a148593db23944462429c65ec->leave($__internal_4036b5f854269930af5e24c6ea10416268c4096a148593db23944462429c65ec_prof);

        
        $__internal_4241d254473338855641348bc057b55648c0bbf672873a864e1fb1259bd1742c->leave($__internal_4241d254473338855641348bc057b55648c0bbf672873a864e1fb1259bd1742c_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_357f8431f7c28351d5864072f0c99c981af662dbc65195290f60e34a56204764 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_357f8431f7c28351d5864072f0c99c981af662dbc65195290f60e34a56204764->enter($__internal_357f8431f7c28351d5864072f0c99c981af662dbc65195290f60e34a56204764_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_02bd822a3ebc56cb11dc9df40024b235d774ecb94834249be935f57f14ca63d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_02bd822a3ebc56cb11dc9df40024b235d774ecb94834249be935f57f14ca63d5->enter($__internal_02bd822a3ebc56cb11dc9df40024b235d774ecb94834249be935f57f14ca63d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <a class=\"btn btn-primary\" href=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "generateUrl", array(0 => "list"), "method"), "html", null, true);
        echo "\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
        ";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("list_select", array(), "SonataAdminBundle"), "html", null, true);
        echo "
    </a>
";
        
        $__internal_02bd822a3ebc56cb11dc9df40024b235d774ecb94834249be935f57f14ca63d5->leave($__internal_02bd822a3ebc56cb11dc9df40024b235d774ecb94834249be935f57f14ca63d5_prof);

        
        $__internal_357f8431f7c28351d5864072f0c99c981af662dbc65195290f60e34a56204764->leave($__internal_357f8431f7c28351d5864072f0c99c981af662dbc65195290f60e34a56204764_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list__select.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 17,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    <a class=\"btn btn-primary\" href=\"{{ admin.generateUrl('list') }}\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
        {{ 'list_select'|trans({}, 'SonataAdminBundle') }}
    </a>
{% endblock %}
", "SonataAdminBundle:CRUD:list__select.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list__select.html.twig");
    }
}
