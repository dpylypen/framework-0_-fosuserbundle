<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_aa2392dbd0dae80ea2da273e677ee751250bc8a392ed6458bb9eacb408b80509 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_91a0ef0b6ef7f0d34ecf6e519b32a1f215573c7de453589a7769571c0ce6eabb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_91a0ef0b6ef7f0d34ecf6e519b32a1f215573c7de453589a7769571c0ce6eabb->enter($__internal_91a0ef0b6ef7f0d34ecf6e519b32a1f215573c7de453589a7769571c0ce6eabb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $__internal_2711e450d64adaec2d6a3eecf4812289fd1a372ef0b503d7897aff5b71e7d232 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2711e450d64adaec2d6a3eecf4812289fd1a372ef0b503d7897aff5b71e7d232->enter($__internal_2711e450d64adaec2d6a3eecf4812289fd1a372ef0b503d7897aff5b71e7d232_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_91a0ef0b6ef7f0d34ecf6e519b32a1f215573c7de453589a7769571c0ce6eabb->leave($__internal_91a0ef0b6ef7f0d34ecf6e519b32a1f215573c7de453589a7769571c0ce6eabb_prof);

        
        $__internal_2711e450d64adaec2d6a3eecf4812289fd1a372ef0b503d7897aff5b71e7d232->leave($__internal_2711e450d64adaec2d6a3eecf4812289fd1a372ef0b503d7897aff5b71e7d232_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_5bfd9f61662dd70d83b9fe89971c0f1fcc8f5b361da02d060c13be2aa3106485 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5bfd9f61662dd70d83b9fe89971c0f1fcc8f5b361da02d060c13be2aa3106485->enter($__internal_5bfd9f61662dd70d83b9fe89971c0f1fcc8f5b361da02d060c13be2aa3106485_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_8b50d9738fd713bdc4feb3b3c98b9525a14d2f1661e72f393723141cd8d225a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b50d9738fd713bdc4feb3b3c98b9525a14d2f1661e72f393723141cd8d225a8->enter($__internal_8b50d9738fd713bdc4feb3b3c98b9525a14d2f1661e72f393723141cd8d225a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 4, $this->getSourceContext()); })()), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) || array_key_exists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new Twig_Error_Runtime('Variable "confirmationUrl" does not exist.', 4, $this->getSourceContext()); })())), "FOSUserBundle");
        
        $__internal_8b50d9738fd713bdc4feb3b3c98b9525a14d2f1661e72f393723141cd8d225a8->leave($__internal_8b50d9738fd713bdc4feb3b3c98b9525a14d2f1661e72f393723141cd8d225a8_prof);

        
        $__internal_5bfd9f61662dd70d83b9fe89971c0f1fcc8f5b361da02d060c13be2aa3106485->leave($__internal_5bfd9f61662dd70d83b9fe89971c0f1fcc8f5b361da02d060c13be2aa3106485_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_faba1ca80588503afee4961cb29a652a69745e88fd66667b1b3ea92129e3763e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_faba1ca80588503afee4961cb29a652a69745e88fd66667b1b3ea92129e3763e->enter($__internal_faba1ca80588503afee4961cb29a652a69745e88fd66667b1b3ea92129e3763e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_a4431e84d29614a4dbf6b9c4b0202b8e7752ecf283d78ce3e78e697d74302338 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4431e84d29614a4dbf6b9c4b0202b8e7752ecf283d78ce3e78e697d74302338->enter($__internal_a4431e84d29614a4dbf6b9c4b0202b8e7752ecf283d78ce3e78e697d74302338_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 10, $this->getSourceContext()); })()), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) || array_key_exists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new Twig_Error_Runtime('Variable "confirmationUrl" does not exist.', 10, $this->getSourceContext()); })())), "FOSUserBundle");
        echo "
";
        
        $__internal_a4431e84d29614a4dbf6b9c4b0202b8e7752ecf283d78ce3e78e697d74302338->leave($__internal_a4431e84d29614a4dbf6b9c4b0202b8e7752ecf283d78ce3e78e697d74302338_prof);

        
        $__internal_faba1ca80588503afee4961cb29a652a69745e88fd66667b1b3ea92129e3763e->leave($__internal_faba1ca80588503afee4961cb29a652a69745e88fd66667b1b3ea92129e3763e_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_1e81efd331794de94650a30dcac3043882e55002c6d14cb76186c0afab3dc1d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e81efd331794de94650a30dcac3043882e55002c6d14cb76186c0afab3dc1d8->enter($__internal_1e81efd331794de94650a30dcac3043882e55002c6d14cb76186c0afab3dc1d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_ab8e6f6776a4f69bc1304fcddf1402fe1fd2f62693009ef553d36b47e6521210 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab8e6f6776a4f69bc1304fcddf1402fe1fd2f62693009ef553d36b47e6521210->enter($__internal_ab8e6f6776a4f69bc1304fcddf1402fe1fd2f62693009ef553d36b47e6521210_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_ab8e6f6776a4f69bc1304fcddf1402fe1fd2f62693009ef553d36b47e6521210->leave($__internal_ab8e6f6776a4f69bc1304fcddf1402fe1fd2f62693009ef553d36b47e6521210_prof);

        
        $__internal_1e81efd331794de94650a30dcac3043882e55002c6d14cb76186c0afab3dc1d8->leave($__internal_1e81efd331794de94650a30dcac3043882e55002c6d14cb76186c0afab3dc1d8_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/email.txt.twig");
    }
}
