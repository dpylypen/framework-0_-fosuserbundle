<?php

/* SonataAdminBundle:CRUD:history_revision_timestamp.html.twig */
class __TwigTemplate_108bacb335b5898a2e8a708b0399a9cc0a7ffbec9f609ee5b1a0dee622cc9349 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a8b53dd1738fc8d1005162f56d52fdf27dbcb2b55f986d3dc385dbde25c3c1db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8b53dd1738fc8d1005162f56d52fdf27dbcb2b55f986d3dc385dbde25c3c1db->enter($__internal_a8b53dd1738fc8d1005162f56d52fdf27dbcb2b55f986d3dc385dbde25c3c1db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:history_revision_timestamp.html.twig"));

        $__internal_33046f9933d829e8eb1b6a18d5149bc3c89f6516124a00eec4f414c9443f0aa6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33046f9933d829e8eb1b6a18d5149bc3c89f6516124a00eec4f414c9443f0aa6->enter($__internal_33046f9933d829e8eb1b6a18d5149bc3c89f6516124a00eec4f414c9443f0aa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:history_revision_timestamp.html.twig"));

        // line 11
        echo "
";
        // line 12
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["revision"]) || array_key_exists("revision", $context) ? $context["revision"] : (function () { throw new Twig_Error_Runtime('Variable "revision" does not exist.', 12, $this->getSourceContext()); })()), "timestamp", array())), "html", null, true);
        echo "
";
        
        $__internal_a8b53dd1738fc8d1005162f56d52fdf27dbcb2b55f986d3dc385dbde25c3c1db->leave($__internal_a8b53dd1738fc8d1005162f56d52fdf27dbcb2b55f986d3dc385dbde25c3c1db_prof);

        
        $__internal_33046f9933d829e8eb1b6a18d5149bc3c89f6516124a00eec4f414c9443f0aa6->leave($__internal_33046f9933d829e8eb1b6a18d5149bc3c89f6516124a00eec4f414c9443f0aa6_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:history_revision_timestamp.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{{ revision.timestamp|date }}
", "SonataAdminBundle:CRUD:history_revision_timestamp.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/history_revision_timestamp.html.twig");
    }
}
