<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_66938d0136d624f93f92d03ed4a8f8cd0aa466721f59bbed1885fd7a51b7b02b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1596017c38fecfaae99f0d510ffab22d00ca472246459a2c3fdb921e1a42f79d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1596017c38fecfaae99f0d510ffab22d00ca472246459a2c3fdb921e1a42f79d->enter($__internal_1596017c38fecfaae99f0d510ffab22d00ca472246459a2c3fdb921e1a42f79d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_0e6769f52bb6c6a5d2ece3925136351402f451f08dd90554c2422e035e8fca8d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e6769f52bb6c6a5d2ece3925136351402f451f08dd90554c2422e035e8fca8d->enter($__internal_0e6769f52bb6c6a5d2ece3925136351402f451f08dd90554c2422e035e8fca8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_1596017c38fecfaae99f0d510ffab22d00ca472246459a2c3fdb921e1a42f79d->leave($__internal_1596017c38fecfaae99f0d510ffab22d00ca472246459a2c3fdb921e1a42f79d_prof);

        
        $__internal_0e6769f52bb6c6a5d2ece3925136351402f451f08dd90554c2422e035e8fca8d->leave($__internal_0e6769f52bb6c6a5d2ece3925136351402f451f08dd90554c2422e035e8fca8d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
