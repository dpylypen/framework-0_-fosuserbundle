<?php

/* SonataAdminBundle:CRUD:edit_text.html.twig */
class __TwigTemplate_5db8dd5de1117cd39b5439450bc48123e59b1893479f5ff10d0ffa8788b4440d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) || array_key_exists("base_template", $context) ? $context["base_template"] : (function () { throw new Twig_Error_Runtime('Variable "base_template" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:edit_text.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3f9f2558a0017d6d85fc463af576ad40e0d6e7625d107545d081f227b86b73ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f9f2558a0017d6d85fc463af576ad40e0d6e7625d107545d081f227b86b73ef->enter($__internal_3f9f2558a0017d6d85fc463af576ad40e0d6e7625d107545d081f227b86b73ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_text.html.twig"));

        $__internal_dc72b4cca74c78183526d8301ec94fa891ab56b795b4bca8689041f01a9a10eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dc72b4cca74c78183526d8301ec94fa891ab56b795b4bca8689041f01a9a10eb->enter($__internal_dc72b4cca74c78183526d8301ec94fa891ab56b795b4bca8689041f01a9a10eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_text.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3f9f2558a0017d6d85fc463af576ad40e0d6e7625d107545d081f227b86b73ef->leave($__internal_3f9f2558a0017d6d85fc463af576ad40e0d6e7625d107545d081f227b86b73ef_prof);

        
        $__internal_dc72b4cca74c78183526d8301ec94fa891ab56b795b4bca8689041f01a9a10eb->leave($__internal_dc72b4cca74c78183526d8301ec94fa891ab56b795b4bca8689041f01a9a10eb_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_4319d17656665f0c96096bbceb433bd126747a0a07c01cb61d7c0333fe90bd4c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4319d17656665f0c96096bbceb433bd126747a0a07c01cb61d7c0333fe90bd4c->enter($__internal_4319d17656665f0c96096bbceb433bd126747a0a07c01cb61d7c0333fe90bd4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_9ffd90485ec78c2c7bee70cd4009c7306f00242ceb05e9cb02dc72116d2d2519 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ffd90485ec78c2c7bee70cd4009c7306f00242ceb05e9cb02dc72116d2d2519->enter($__internal_9ffd90485ec78c2c7bee70cd4009c7306f00242ceb05e9cb02dc72116d2d2519_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 14, $this->getSourceContext()); })()), 'widget', array("attr" => array("class" => "title")));
        
        $__internal_9ffd90485ec78c2c7bee70cd4009c7306f00242ceb05e9cb02dc72116d2d2519->leave($__internal_9ffd90485ec78c2c7bee70cd4009c7306f00242ceb05e9cb02dc72116d2d2519_prof);

        
        $__internal_4319d17656665f0c96096bbceb433bd126747a0a07c01cb61d7c0333fe90bd4c->leave($__internal_4319d17656665f0c96096bbceb433bd126747a0a07c01cb61d7c0333fe90bd4c_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_text.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block field %}{{ form_widget(field_element, {'attr': {'class' : 'title'}}) }}{% endblock %}
", "SonataAdminBundle:CRUD:edit_text.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/edit_text.html.twig");
    }
}
