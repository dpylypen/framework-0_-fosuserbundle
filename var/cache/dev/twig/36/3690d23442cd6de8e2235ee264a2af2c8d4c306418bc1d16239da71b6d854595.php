<?php

/* @Framework/Form/widget_container_attributes.html.php */
class __TwigTemplate_b229c603cf4e343e86e9278341d50129b0adcfc9c6c3daa4dea16f7b4ee602f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_44f46cd7b03e7123035246e6e30109bec9bb5b9dbcbf0e27696f327fc3c168b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44f46cd7b03e7123035246e6e30109bec9bb5b9dbcbf0e27696f327fc3c168b8->enter($__internal_44f46cd7b03e7123035246e6e30109bec9bb5b9dbcbf0e27696f327fc3c168b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        $__internal_d8e0449530076f3af7f65ada67de1f5eed24e641f1d35cffc9cf21a69d42dd8a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8e0449530076f3af7f65ada67de1f5eed24e641f1d35cffc9cf21a69d42dd8a->enter($__internal_d8e0449530076f3af7f65ada67de1f5eed24e641f1d35cffc9cf21a69d42dd8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        // line 1
        echo "<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_44f46cd7b03e7123035246e6e30109bec9bb5b9dbcbf0e27696f327fc3c168b8->leave($__internal_44f46cd7b03e7123035246e6e30109bec9bb5b9dbcbf0e27696f327fc3c168b8_prof);

        
        $__internal_d8e0449530076f3af7f65ada67de1f5eed24e641f1d35cffc9cf21a69d42dd8a->leave($__internal_d8e0449530076f3af7f65ada67de1f5eed24e641f1d35cffc9cf21a69d42dd8a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_container_attributes.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_container_attributes.html.php");
    }
}
