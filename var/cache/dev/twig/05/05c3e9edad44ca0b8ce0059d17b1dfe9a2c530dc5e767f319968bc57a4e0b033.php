<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_32cf5ad9b72f1ddf972093048152648dd591e038e75a8e9f23773fcfa32e481d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_13cea0bd6bc7ce10be26a979edbd25fc47ab38a8675cfa1055d64128bb35208c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13cea0bd6bc7ce10be26a979edbd25fc47ab38a8675cfa1055d64128bb35208c->enter($__internal_13cea0bd6bc7ce10be26a979edbd25fc47ab38a8675cfa1055d64128bb35208c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_a158b858a4974cab0a5b808875232605b77a47459ccdc8a951c9fa7801a089c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a158b858a4974cab0a5b808875232605b77a47459ccdc8a951c9fa7801a089c0->enter($__internal_a158b858a4974cab0a5b808875232605b77a47459ccdc8a951c9fa7801a089c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_13cea0bd6bc7ce10be26a979edbd25fc47ab38a8675cfa1055d64128bb35208c->leave($__internal_13cea0bd6bc7ce10be26a979edbd25fc47ab38a8675cfa1055d64128bb35208c_prof);

        
        $__internal_a158b858a4974cab0a5b808875232605b77a47459ccdc8a951c9fa7801a089c0->leave($__internal_a158b858a4974cab0a5b808875232605b77a47459ccdc8a951c9fa7801a089c0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
