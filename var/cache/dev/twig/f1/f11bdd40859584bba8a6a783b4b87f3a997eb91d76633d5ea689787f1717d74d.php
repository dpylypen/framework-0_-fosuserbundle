<?php

/* @Framework/Form/form_start.html.php */
class __TwigTemplate_e899943aad4efb8462de9cfea342d74b2cac1994baf01b2d5bfb5b91b46e1246 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_20217f52932d3037a6b4e7cfc1b384d6ac3918614e1f2216f7eb7ccb5768b2cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_20217f52932d3037a6b4e7cfc1b384d6ac3918614e1f2216f7eb7ccb5768b2cb->enter($__internal_20217f52932d3037a6b4e7cfc1b384d6ac3918614e1f2216f7eb7ccb5768b2cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_start.html.php"));

        $__internal_a2494fcc7f51e19126e0570542fdd78378e4c72c889c5583e55fbf2afb35f2c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2494fcc7f51e19126e0570542fdd78378e4c72c889c5583e55fbf2afb35f2c6->enter($__internal_a2494fcc7f51e19126e0570542fdd78378e4c72c889c5583e55fbf2afb35f2c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_start.html.php"));

        // line 1
        echo "<?php \$method = strtoupper(\$method) ?>
<?php \$form_method = \$method === 'GET' || \$method === 'POST' ? \$method : 'POST' ?>
<form name=\"<?php echo \$name ?>\" method=\"<?php echo strtolower(\$form_method) ?>\"<?php if (\$action !== ''): ?> action=\"<?php echo \$action ?>\"<?php endif ?><?php foreach (\$attr as \$k => \$v) { printf(' %s=\"%s\"', \$view->escape(\$k), \$view->escape(\$v)); } ?><?php if (\$multipart): ?> enctype=\"multipart/form-data\"<?php endif ?>>
<?php if (\$form_method !== \$method): ?>
    <input type=\"hidden\" name=\"_method\" value=\"<?php echo \$method ?>\" />
<?php endif ?>
";
        
        $__internal_20217f52932d3037a6b4e7cfc1b384d6ac3918614e1f2216f7eb7ccb5768b2cb->leave($__internal_20217f52932d3037a6b4e7cfc1b384d6ac3918614e1f2216f7eb7ccb5768b2cb_prof);

        
        $__internal_a2494fcc7f51e19126e0570542fdd78378e4c72c889c5583e55fbf2afb35f2c6->leave($__internal_a2494fcc7f51e19126e0570542fdd78378e4c72c889c5583e55fbf2afb35f2c6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_start.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php \$method = strtoupper(\$method) ?>
<?php \$form_method = \$method === 'GET' || \$method === 'POST' ? \$method : 'POST' ?>
<form name=\"<?php echo \$name ?>\" method=\"<?php echo strtolower(\$form_method) ?>\"<?php if (\$action !== ''): ?> action=\"<?php echo \$action ?>\"<?php endif ?><?php foreach (\$attr as \$k => \$v) { printf(' %s=\"%s\"', \$view->escape(\$k), \$view->escape(\$v)); } ?><?php if (\$multipart): ?> enctype=\"multipart/form-data\"<?php endif ?>>
<?php if (\$form_method !== \$method): ?>
    <input type=\"hidden\" name=\"_method\" value=\"<?php echo \$method ?>\" />
<?php endif ?>
", "@Framework/Form/form_start.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_start.html.php");
    }
}
