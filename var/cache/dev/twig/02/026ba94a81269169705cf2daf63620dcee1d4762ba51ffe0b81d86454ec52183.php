<?php

/* @Framework/Form/widget_attributes.html.php */
class __TwigTemplate_5cbb379c0bc2fe4d9d012b201d699384e87ee7540d39af4e5358678fc0c24374 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ae85d4c3f525350768f37b684e49195d57b1be380bcc570b15bfd86c348f8cb3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae85d4c3f525350768f37b684e49195d57b1be380bcc570b15bfd86c348f8cb3->enter($__internal_ae85d4c3f525350768f37b684e49195d57b1be380bcc570b15bfd86c348f8cb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        $__internal_52162bda382d75001925a57fbe6209f8288b8976c19115ac9be2d6606b077aef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52162bda382d75001925a57fbe6209f8288b8976c19115ac9be2d6606b077aef->enter($__internal_52162bda382d75001925a57fbe6209f8288b8976c19115ac9be2d6606b077aef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_ae85d4c3f525350768f37b684e49195d57b1be380bcc570b15bfd86c348f8cb3->leave($__internal_ae85d4c3f525350768f37b684e49195d57b1be380bcc570b15bfd86c348f8cb3_prof);

        
        $__internal_52162bda382d75001925a57fbe6209f8288b8976c19115ac9be2d6606b077aef->leave($__internal_52162bda382d75001925a57fbe6209f8288b8976c19115ac9be2d6606b077aef_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_attributes.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_attributes.html.php");
    }
}
