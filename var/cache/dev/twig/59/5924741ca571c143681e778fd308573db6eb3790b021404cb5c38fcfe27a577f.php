<?php

/* SonataAdminBundle:CRUD:show_email.html.twig */
class __TwigTemplate_eb2aabd2fc423be44ba0457e932fcfa4dcf0beb1be9295b1e35962cfda430158 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_email.html.twig", 1);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e3ec1e244424b78c3b6ee38864fd3b425ef8abd0011a9e9e53041b6ca85016b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e3ec1e244424b78c3b6ee38864fd3b425ef8abd0011a9e9e53041b6ca85016b0->enter($__internal_e3ec1e244424b78c3b6ee38864fd3b425ef8abd0011a9e9e53041b6ca85016b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_email.html.twig"));

        $__internal_52909cd6007e6cd21d5b425eed8d3fee9ff96b9b50a7587ff7291a9f834cbfed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52909cd6007e6cd21d5b425eed8d3fee9ff96b9b50a7587ff7291a9f834cbfed->enter($__internal_52909cd6007e6cd21d5b425eed8d3fee9ff96b9b50a7587ff7291a9f834cbfed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e3ec1e244424b78c3b6ee38864fd3b425ef8abd0011a9e9e53041b6ca85016b0->leave($__internal_e3ec1e244424b78c3b6ee38864fd3b425ef8abd0011a9e9e53041b6ca85016b0_prof);

        
        $__internal_52909cd6007e6cd21d5b425eed8d3fee9ff96b9b50a7587ff7291a9f834cbfed->leave($__internal_52909cd6007e6cd21d5b425eed8d3fee9ff96b9b50a7587ff7291a9f834cbfed_prof);

    }

    // line 3
    public function block_field($context, array $blocks = array())
    {
        $__internal_9a5da5aebea9b0877724c33271227e535a64e3eb68ce1271708f4d24bc3f761f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a5da5aebea9b0877724c33271227e535a64e3eb68ce1271708f4d24bc3f761f->enter($__internal_9a5da5aebea9b0877724c33271227e535a64e3eb68ce1271708f4d24bc3f761f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_634b3d76018026fff88de926f6f13832f6e126934bab5b04347a37c33e9e3d7a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_634b3d76018026fff88de926f6f13832f6e126934bab5b04347a37c33e9e3d7a->enter($__internal_634b3d76018026fff88de926f6f13832f6e126934bab5b04347a37c33e9e3d7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 4
        echo "    ";
        $this->loadTemplate("SonataAdminBundle:CRUD:_email_link.html.twig", "SonataAdminBundle:CRUD:show_email.html.twig", 4)->display($context);
        
        $__internal_634b3d76018026fff88de926f6f13832f6e126934bab5b04347a37c33e9e3d7a->leave($__internal_634b3d76018026fff88de926f6f13832f6e126934bab5b04347a37c33e9e3d7a_prof);

        
        $__internal_9a5da5aebea9b0877724c33271227e535a64e3eb68ce1271708f4d24bc3f761f->leave($__internal_9a5da5aebea9b0877724c33271227e535a64e3eb68ce1271708f4d24bc3f761f_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field %}
    {% include 'SonataAdminBundle:CRUD:_email_link.html.twig' %}
{% endblock %}
", "SonataAdminBundle:CRUD:show_email.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/show_email.html.twig");
    }
}
