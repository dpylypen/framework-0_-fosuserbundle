<?php

/* SonataAdminBundle::empty_layout.html.twig */
class __TwigTemplate_0e4435bb111ba8641946e44d464568bec65bd2397eef0df7985d54eeda9cd22b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'sonata_header' => array($this, 'block_sonata_header'),
            'sonata_left_side' => array($this, 'block_sonata_left_side'),
            'sonata_nav' => array($this, 'block_sonata_nav'),
            'sonata_breadcrumb' => array($this, 'block_sonata_breadcrumb'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'sonata_wrapper' => array($this, 'block_sonata_wrapper'),
            'sonata_page_content' => array($this, 'block_sonata_page_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin_pool"]) || array_key_exists("admin_pool", $context) ? $context["admin_pool"] : (function () { throw new Twig_Error_Runtime('Variable "admin_pool" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "layout"), "method"), "SonataAdminBundle::empty_layout.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5f9e5993566fa71a4e73429486a769f3d0147e1cc7f7ddf2ec4c25832d3ff0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a5f9e5993566fa71a4e73429486a769f3d0147e1cc7f7ddf2ec4c25832d3ff0d->enter($__internal_a5f9e5993566fa71a4e73429486a769f3d0147e1cc7f7ddf2ec4c25832d3ff0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle::empty_layout.html.twig"));

        $__internal_2653fa83d0c79df361a279b4da01f13770d394e1e53c703da96e1dd966a8a5ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2653fa83d0c79df361a279b4da01f13770d394e1e53c703da96e1dd966a8a5ad->enter($__internal_2653fa83d0c79df361a279b4da01f13770d394e1e53c703da96e1dd966a8a5ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle::empty_layout.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a5f9e5993566fa71a4e73429486a769f3d0147e1cc7f7ddf2ec4c25832d3ff0d->leave($__internal_a5f9e5993566fa71a4e73429486a769f3d0147e1cc7f7ddf2ec4c25832d3ff0d_prof);

        
        $__internal_2653fa83d0c79df361a279b4da01f13770d394e1e53c703da96e1dd966a8a5ad->leave($__internal_2653fa83d0c79df361a279b4da01f13770d394e1e53c703da96e1dd966a8a5ad_prof);

    }

    // line 14
    public function block_sonata_header($context, array $blocks = array())
    {
        $__internal_eafcd43f0c6968d15e6a1d5a530ebe8c780895980113989aa1d02e49f7754d95 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eafcd43f0c6968d15e6a1d5a530ebe8c780895980113989aa1d02e49f7754d95->enter($__internal_eafcd43f0c6968d15e6a1d5a530ebe8c780895980113989aa1d02e49f7754d95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_header"));

        $__internal_249f57154190e45d8a46fb416dca8009b07c6ed74f20a6f812ece46d1e7149a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_249f57154190e45d8a46fb416dca8009b07c6ed74f20a6f812ece46d1e7149a0->enter($__internal_249f57154190e45d8a46fb416dca8009b07c6ed74f20a6f812ece46d1e7149a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_header"));

        
        $__internal_249f57154190e45d8a46fb416dca8009b07c6ed74f20a6f812ece46d1e7149a0->leave($__internal_249f57154190e45d8a46fb416dca8009b07c6ed74f20a6f812ece46d1e7149a0_prof);

        
        $__internal_eafcd43f0c6968d15e6a1d5a530ebe8c780895980113989aa1d02e49f7754d95->leave($__internal_eafcd43f0c6968d15e6a1d5a530ebe8c780895980113989aa1d02e49f7754d95_prof);

    }

    // line 15
    public function block_sonata_left_side($context, array $blocks = array())
    {
        $__internal_90124c7222f9aecf2ac8a724f913aa2e9e3725b97d58f7e0c2c4ad19f7a8a318 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_90124c7222f9aecf2ac8a724f913aa2e9e3725b97d58f7e0c2c4ad19f7a8a318->enter($__internal_90124c7222f9aecf2ac8a724f913aa2e9e3725b97d58f7e0c2c4ad19f7a8a318_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_left_side"));

        $__internal_ab75101efaedc05c6fc9a56e48a960193ebc240b0e4e16e44f7895767b54b28d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab75101efaedc05c6fc9a56e48a960193ebc240b0e4e16e44f7895767b54b28d->enter($__internal_ab75101efaedc05c6fc9a56e48a960193ebc240b0e4e16e44f7895767b54b28d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_left_side"));

        
        $__internal_ab75101efaedc05c6fc9a56e48a960193ebc240b0e4e16e44f7895767b54b28d->leave($__internal_ab75101efaedc05c6fc9a56e48a960193ebc240b0e4e16e44f7895767b54b28d_prof);

        
        $__internal_90124c7222f9aecf2ac8a724f913aa2e9e3725b97d58f7e0c2c4ad19f7a8a318->leave($__internal_90124c7222f9aecf2ac8a724f913aa2e9e3725b97d58f7e0c2c4ad19f7a8a318_prof);

    }

    // line 16
    public function block_sonata_nav($context, array $blocks = array())
    {
        $__internal_708a91ecad3fd8e6034f9965821e1d7e6f8a2b9298dc2a45ca7c27dbe4d8acf8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_708a91ecad3fd8e6034f9965821e1d7e6f8a2b9298dc2a45ca7c27dbe4d8acf8->enter($__internal_708a91ecad3fd8e6034f9965821e1d7e6f8a2b9298dc2a45ca7c27dbe4d8acf8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_nav"));

        $__internal_7b235cb14c1c4b989dcb6950f58c7cc44e7bfeeb8169e8e139baff960e47e9bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b235cb14c1c4b989dcb6950f58c7cc44e7bfeeb8169e8e139baff960e47e9bc->enter($__internal_7b235cb14c1c4b989dcb6950f58c7cc44e7bfeeb8169e8e139baff960e47e9bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_nav"));

        
        $__internal_7b235cb14c1c4b989dcb6950f58c7cc44e7bfeeb8169e8e139baff960e47e9bc->leave($__internal_7b235cb14c1c4b989dcb6950f58c7cc44e7bfeeb8169e8e139baff960e47e9bc_prof);

        
        $__internal_708a91ecad3fd8e6034f9965821e1d7e6f8a2b9298dc2a45ca7c27dbe4d8acf8->leave($__internal_708a91ecad3fd8e6034f9965821e1d7e6f8a2b9298dc2a45ca7c27dbe4d8acf8_prof);

    }

    // line 17
    public function block_sonata_breadcrumb($context, array $blocks = array())
    {
        $__internal_2a0b9848b616f18e4d30219a915834ea7f06dc69a5657f382e916020dad46711 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a0b9848b616f18e4d30219a915834ea7f06dc69a5657f382e916020dad46711->enter($__internal_2a0b9848b616f18e4d30219a915834ea7f06dc69a5657f382e916020dad46711_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_breadcrumb"));

        $__internal_03f8444dac5ea2da94f463ecc9d776227a52379e222700424ad9ffa886574bee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_03f8444dac5ea2da94f463ecc9d776227a52379e222700424ad9ffa886574bee->enter($__internal_03f8444dac5ea2da94f463ecc9d776227a52379e222700424ad9ffa886574bee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_breadcrumb"));

        
        $__internal_03f8444dac5ea2da94f463ecc9d776227a52379e222700424ad9ffa886574bee->leave($__internal_03f8444dac5ea2da94f463ecc9d776227a52379e222700424ad9ffa886574bee_prof);

        
        $__internal_2a0b9848b616f18e4d30219a915834ea7f06dc69a5657f382e916020dad46711->leave($__internal_2a0b9848b616f18e4d30219a915834ea7f06dc69a5657f382e916020dad46711_prof);

    }

    // line 19
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_7984d1b6cde1c436455278cdfb2ff689547f130e06d83c5133ea897bf2167e49 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7984d1b6cde1c436455278cdfb2ff689547f130e06d83c5133ea897bf2167e49->enter($__internal_7984d1b6cde1c436455278cdfb2ff689547f130e06d83c5133ea897bf2167e49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_184409ae2f692d5c3529de685cac2f29c226a232bc6c5eb1771d83a994e7509e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_184409ae2f692d5c3529de685cac2f29c226a232bc6c5eb1771d83a994e7509e->enter($__internal_184409ae2f692d5c3529de685cac2f29c226a232bc6c5eb1771d83a994e7509e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 20
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
        .content {
            margin: 0px;
            padding: 0px;
        }
    </style>
";
        
        $__internal_184409ae2f692d5c3529de685cac2f29c226a232bc6c5eb1771d83a994e7509e->leave($__internal_184409ae2f692d5c3529de685cac2f29c226a232bc6c5eb1771d83a994e7509e_prof);

        
        $__internal_7984d1b6cde1c436455278cdfb2ff689547f130e06d83c5133ea897bf2167e49->leave($__internal_7984d1b6cde1c436455278cdfb2ff689547f130e06d83c5133ea897bf2167e49_prof);

    }

    // line 30
    public function block_sonata_wrapper($context, array $blocks = array())
    {
        $__internal_3f7afdb22a1be478736213c724505c7a1f01d3d44048dc5fdcca5d1ecee0535b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f7afdb22a1be478736213c724505c7a1f01d3d44048dc5fdcca5d1ecee0535b->enter($__internal_3f7afdb22a1be478736213c724505c7a1f01d3d44048dc5fdcca5d1ecee0535b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_wrapper"));

        $__internal_f4c365ce345fd7805566e9489013236fb0de5cfec52f08157695395c456b0fe9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f4c365ce345fd7805566e9489013236fb0de5cfec52f08157695395c456b0fe9->enter($__internal_f4c365ce345fd7805566e9489013236fb0de5cfec52f08157695395c456b0fe9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_wrapper"));

        // line 31
        echo "    ";
        $this->displayBlock('sonata_page_content', $context, $blocks);
        
        $__internal_f4c365ce345fd7805566e9489013236fb0de5cfec52f08157695395c456b0fe9->leave($__internal_f4c365ce345fd7805566e9489013236fb0de5cfec52f08157695395c456b0fe9_prof);

        
        $__internal_3f7afdb22a1be478736213c724505c7a1f01d3d44048dc5fdcca5d1ecee0535b->leave($__internal_3f7afdb22a1be478736213c724505c7a1f01d3d44048dc5fdcca5d1ecee0535b_prof);

    }

    public function block_sonata_page_content($context, array $blocks = array())
    {
        $__internal_eff2e79346eb3c95662ee5091361712af66a66e26b439172d3c8d453a9447fe0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eff2e79346eb3c95662ee5091361712af66a66e26b439172d3c8d453a9447fe0->enter($__internal_eff2e79346eb3c95662ee5091361712af66a66e26b439172d3c8d453a9447fe0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_page_content"));

        $__internal_45d4cc157090a4351c125bab47776db86b7c2386ea11151562fdfc149b1ff94f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_45d4cc157090a4351c125bab47776db86b7c2386ea11151562fdfc149b1ff94f->enter($__internal_45d4cc157090a4351c125bab47776db86b7c2386ea11151562fdfc149b1ff94f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_page_content"));

        // line 32
        echo "        ";
        $this->displayParentBlock("sonata_page_content", $context, $blocks);
        echo "
    ";
        
        $__internal_45d4cc157090a4351c125bab47776db86b7c2386ea11151562fdfc149b1ff94f->leave($__internal_45d4cc157090a4351c125bab47776db86b7c2386ea11151562fdfc149b1ff94f_prof);

        
        $__internal_eff2e79346eb3c95662ee5091361712af66a66e26b439172d3c8d453a9447fe0->leave($__internal_eff2e79346eb3c95662ee5091361712af66a66e26b439172d3c8d453a9447fe0_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle::empty_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 32,  151 => 31,  142 => 30,  122 => 20,  113 => 19,  96 => 17,  79 => 16,  62 => 15,  45 => 14,  24 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin_pool.getTemplate('layout') %}

{% block sonata_header %}{% endblock %}
{% block sonata_left_side %}{% endblock %}
{% block sonata_nav %}{% endblock %}
{% block sonata_breadcrumb %}{% endblock %}

{% block stylesheets %}
    {{ parent() }}

    <style>
        .content {
            margin: 0px;
            padding: 0px;
        }
    </style>
{% endblock %}

{% block sonata_wrapper %}
    {% block sonata_page_content %}
        {{ parent() }}
    {% endblock %}
{% endblock %}
", "SonataAdminBundle::empty_layout.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/empty_layout.html.twig");
    }
}
