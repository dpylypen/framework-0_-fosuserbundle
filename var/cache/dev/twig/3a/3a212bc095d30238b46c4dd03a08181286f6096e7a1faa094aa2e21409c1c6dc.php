<?php

/* SonataAdminBundle:Pager:base_results.html.twig */
class __TwigTemplate_51bb115d4189f67dc423986eb4224d387c998bde43d28d66624a726e6780ffb0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'num_pages' => array($this, 'block_num_pages'),
            'num_results' => array($this, 'block_num_results'),
            'max_per_page' => array($this, 'block_max_per_page'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f385ee4a22e5f214da36fcaa275005f1fc249ca928a200ce144e4632e4583f7c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f385ee4a22e5f214da36fcaa275005f1fc249ca928a200ce144e4632e4583f7c->enter($__internal_f385ee4a22e5f214da36fcaa275005f1fc249ca928a200ce144e4632e4583f7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:base_results.html.twig"));

        $__internal_3ce83f1072789949129536d1a7d188ddcf575d8d265cdf6dc8b4130cd117cc24 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ce83f1072789949129536d1a7d188ddcf575d8d265cdf6dc8b4130cd117cc24->enter($__internal_3ce83f1072789949129536d1a7d188ddcf575d8d265cdf6dc8b4130cd117cc24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:base_results.html.twig"));

        // line 11
        echo "
";
        // line 12
        $this->displayBlock('num_pages', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('num_results', $context, $blocks);
        // line 21
        echo "
";
        // line 22
        $this->displayBlock('max_per_page', $context, $blocks);
        
        $__internal_f385ee4a22e5f214da36fcaa275005f1fc249ca928a200ce144e4632e4583f7c->leave($__internal_f385ee4a22e5f214da36fcaa275005f1fc249ca928a200ce144e4632e4583f7c_prof);

        
        $__internal_3ce83f1072789949129536d1a7d188ddcf575d8d265cdf6dc8b4130cd117cc24->leave($__internal_3ce83f1072789949129536d1a7d188ddcf575d8d265cdf6dc8b4130cd117cc24_prof);

    }

    // line 12
    public function block_num_pages($context, array $blocks = array())
    {
        $__internal_bb653292f40ee4e515859bcc097db41def98ec0e5d8075ccdc766cf0408e2f07 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb653292f40ee4e515859bcc097db41def98ec0e5d8075ccdc766cf0408e2f07->enter($__internal_bb653292f40ee4e515859bcc097db41def98ec0e5d8075ccdc766cf0408e2f07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_pages"));

        $__internal_c3c4894face17fe867f753db0c767a92386bc82c41d5189adf1a70e0abf49762 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c3c4894face17fe867f753db0c767a92386bc82c41d5189adf1a70e0abf49762->enter($__internal_c3c4894face17fe867f753db0c767a92386bc82c41d5189adf1a70e0abf49762_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_pages"));

        // line 13
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 13, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "page", array()), "html", null, true);
        echo " / ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 13, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "lastpage", array()), "html", null, true);
        echo "
    &nbsp;-&nbsp;
";
        
        $__internal_c3c4894face17fe867f753db0c767a92386bc82c41d5189adf1a70e0abf49762->leave($__internal_c3c4894face17fe867f753db0c767a92386bc82c41d5189adf1a70e0abf49762_prof);

        
        $__internal_bb653292f40ee4e515859bcc097db41def98ec0e5d8075ccdc766cf0408e2f07->leave($__internal_bb653292f40ee4e515859bcc097db41def98ec0e5d8075ccdc766cf0408e2f07_prof);

    }

    // line 17
    public function block_num_results($context, array $blocks = array())
    {
        $__internal_084dfe91b713ebe2e8cf01ba4787f3419cafd637ad2b0378976734e1753070f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_084dfe91b713ebe2e8cf01ba4787f3419cafd637ad2b0378976734e1753070f2->enter($__internal_084dfe91b713ebe2e8cf01ba4787f3419cafd637ad2b0378976734e1753070f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_results"));

        $__internal_ec09ae40776091fe06833a96601ed528058fff2209adc6a6061468bbd1041e0b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ec09ae40776091fe06833a96601ed528058fff2209adc6a6061468bbd1041e0b->enter($__internal_ec09ae40776091fe06833a96601ed528058fff2209adc6a6061468bbd1041e0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_results"));

        // line 18
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->transChoice("list_results_count", twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "nbresults", array()), array("%count%" => twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "nbresults", array())), "SonataAdminBundle");
        // line 19
        echo "    &nbsp;-&nbsp;
";
        
        $__internal_ec09ae40776091fe06833a96601ed528058fff2209adc6a6061468bbd1041e0b->leave($__internal_ec09ae40776091fe06833a96601ed528058fff2209adc6a6061468bbd1041e0b_prof);

        
        $__internal_084dfe91b713ebe2e8cf01ba4787f3419cafd637ad2b0378976734e1753070f2->leave($__internal_084dfe91b713ebe2e8cf01ba4787f3419cafd637ad2b0378976734e1753070f2_prof);

    }

    // line 22
    public function block_max_per_page($context, array $blocks = array())
    {
        $__internal_be2617b8fc9020fdb3f87bd5b88005aeea02ca2e381bf40a634d574f84ff4536 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_be2617b8fc9020fdb3f87bd5b88005aeea02ca2e381bf40a634d574f84ff4536->enter($__internal_be2617b8fc9020fdb3f87bd5b88005aeea02ca2e381bf40a634d574f84ff4536_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "max_per_page"));

        $__internal_75e82321d74c12e8c6a73f701a5fd540d7900a7e7ba8648229a33f52928e1cd9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_75e82321d74c12e8c6a73f701a5fd540d7900a7e7ba8648229a33f52928e1cd9->enter($__internal_75e82321d74c12e8c6a73f701a5fd540d7900a7e7ba8648229a33f52928e1cd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "max_per_page"));

        // line 23
        echo "    <label class=\"control-label\" for=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 23, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
        echo "_per_page\">";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("label_per_page", array(), "SonataAdminBundle");
        echo "</label>
    <select class=\"per-page small form-control\" id=\"";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 24, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
        echo "_per_page\" style=\"width: auto\">
        ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 25, $this->getSourceContext()); })()), "getperpageoptions", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["per_page"]) {
            // line 26
            echo "            <option ";
            if (($context["per_page"] == twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 26, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "maxperpage", array()))) {
                echo "selected=\"selected\"";
            }
            echo " value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 26, $this->getSourceContext()); })()), "generateUrl", array(0 => "list", 1 => array("filter" => twig_array_merge(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 26, $this->getSourceContext()); })()), "datagrid", array()), "values", array()), array("_page" => 1, "_per_page" => $context["per_page"])))), "method"), "html", null, true);
            echo "\">";
            // line 27
            echo twig_escape_filter($this->env, $context["per_page"], "html", null, true);
            // line 28
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['per_page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "    </select>
";
        
        $__internal_75e82321d74c12e8c6a73f701a5fd540d7900a7e7ba8648229a33f52928e1cd9->leave($__internal_75e82321d74c12e8c6a73f701a5fd540d7900a7e7ba8648229a33f52928e1cd9_prof);

        
        $__internal_be2617b8fc9020fdb3f87bd5b88005aeea02ca2e381bf40a634d574f84ff4536->leave($__internal_be2617b8fc9020fdb3f87bd5b88005aeea02ca2e381bf40a634d574f84ff4536_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Pager:base_results.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  140 => 30,  133 => 28,  131 => 27,  123 => 26,  119 => 25,  115 => 24,  108 => 23,  99 => 22,  88 => 19,  85 => 18,  76 => 17,  60 => 13,  51 => 12,  41 => 22,  38 => 21,  36 => 17,  33 => 16,  31 => 12,  28 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% block num_pages %}
    {{ admin.datagrid.pager.page }} / {{ admin.datagrid.pager.lastpage }}
    &nbsp;-&nbsp;
{% endblock %}

{% block num_results %}
    {% transchoice admin.datagrid.pager.nbresults with {'%count%': admin.datagrid.pager.nbresults} from 'SonataAdminBundle' %}list_results_count{% endtranschoice %}
    &nbsp;-&nbsp;
{% endblock %}

{% block max_per_page %}
    <label class=\"control-label\" for=\"{{ admin.uniqid }}_per_page\">{% trans from 'SonataAdminBundle' %}label_per_page{% endtrans %}</label>
    <select class=\"per-page small form-control\" id=\"{{ admin.uniqid }}_per_page\" style=\"width: auto\">
        {% for per_page in admin.getperpageoptions %}
            <option {% if per_page == admin.datagrid.pager.maxperpage %}selected=\"selected\"{% endif %} value=\"{{ admin.generateUrl('list', {'filter': admin.datagrid.values|merge({'_page': 1, '_per_page': per_page})}) }}\">
                {{- per_page -}}
            </option>
        {% endfor %}
    </select>
{% endblock %}
", "SonataAdminBundle:Pager:base_results.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/Pager/base_results.html.twig");
    }
}
