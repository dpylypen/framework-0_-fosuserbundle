<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_cee590426c7f2ff582d19ac0fef8384e1b6564323e8a8d7dcf27178d4fe5197c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_36648fdccdb89aeec277ba846ce92f45d29c5ae68232063d0ec1f5536d7ee651 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_36648fdccdb89aeec277ba846ce92f45d29c5ae68232063d0ec1f5536d7ee651->enter($__internal_36648fdccdb89aeec277ba846ce92f45d29c5ae68232063d0ec1f5536d7ee651_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_8a0f91b3fddf12ebdb552045e60acfbe76ed32b3c8d349315eda5db46d939fda = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a0f91b3fddf12ebdb552045e60acfbe76ed32b3c8d349315eda5db46d939fda->enter($__internal_8a0f91b3fddf12ebdb552045e60acfbe76ed32b3c8d349315eda5db46d939fda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_36648fdccdb89aeec277ba846ce92f45d29c5ae68232063d0ec1f5536d7ee651->leave($__internal_36648fdccdb89aeec277ba846ce92f45d29c5ae68232063d0ec1f5536d7ee651_prof);

        
        $__internal_8a0f91b3fddf12ebdb552045e60acfbe76ed32b3c8d349315eda5db46d939fda->leave($__internal_8a0f91b3fddf12ebdb552045e60acfbe76ed32b3c8d349315eda5db46d939fda_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
