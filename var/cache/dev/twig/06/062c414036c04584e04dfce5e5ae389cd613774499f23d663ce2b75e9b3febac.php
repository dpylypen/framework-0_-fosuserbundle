<?php

/* SonataBlockBundle:Block:block_no_page_available.html.twig */
class __TwigTemplate_db9b297d06072e9232835b69cbf47f57813ad74dad8b2c5dd65b390553c45e32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a83915468ee005579d163b16635242b17cf8717fda01855809470e92f9c34e7c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a83915468ee005579d163b16635242b17cf8717fda01855809470e92f9c34e7c->enter($__internal_a83915468ee005579d163b16635242b17cf8717fda01855809470e92f9c34e7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_no_page_available.html.twig"));

        $__internal_c88ef23ce5a40af926528365fef3119fac0317fe5a36b7141b8840f49d5f54ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c88ef23ce5a40af926528365fef3119fac0317fe5a36b7141b8840f49d5f54ba->enter($__internal_c88ef23ce5a40af926528365fef3119fac0317fe5a36b7141b8840f49d5f54ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_no_page_available.html.twig"));

        
        $__internal_a83915468ee005579d163b16635242b17cf8717fda01855809470e92f9c34e7c->leave($__internal_a83915468ee005579d163b16635242b17cf8717fda01855809470e92f9c34e7c_prof);

        
        $__internal_c88ef23ce5a40af926528365fef3119fac0317fe5a36b7141b8840f49d5f54ba->leave($__internal_c88ef23ce5a40af926528365fef3119fac0317fe5a36b7141b8840f49d5f54ba_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_no_page_available.html.twig";
    }

    public function getDebugInfo()
    {
        return array ();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
", "SonataBlockBundle:Block:block_no_page_available.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/block-bundle/Resources/views/Block/block_no_page_available.html.twig");
    }
}
