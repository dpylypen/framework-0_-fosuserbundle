<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_3ff2240333df890f4c427c536a7c8775181ea8fb6a548d200d2aabe9bad48a13 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c3c5a30878ccc5d0829e6fe524f53f813e515e4dd57595e1b0a3ca4bddfb96b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3c5a30878ccc5d0829e6fe524f53f813e515e4dd57595e1b0a3ca4bddfb96b2->enter($__internal_c3c5a30878ccc5d0829e6fe524f53f813e515e4dd57595e1b0a3ca4bddfb96b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $__internal_6d8ef410b4ebf28b9f1eb5cb497f33ac3e09671edbf05dcab3a34474dff20cf6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d8ef410b4ebf28b9f1eb5cb497f33ac3e09671edbf05dcab3a34474dff20cf6->enter($__internal_6d8ef410b4ebf28b9f1eb5cb497f33ac3e09671edbf05dcab3a34474dff20cf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c3c5a30878ccc5d0829e6fe524f53f813e515e4dd57595e1b0a3ca4bddfb96b2->leave($__internal_c3c5a30878ccc5d0829e6fe524f53f813e515e4dd57595e1b0a3ca4bddfb96b2_prof);

        
        $__internal_6d8ef410b4ebf28b9f1eb5cb497f33ac3e09671edbf05dcab3a34474dff20cf6->leave($__internal_6d8ef410b4ebf28b9f1eb5cb497f33ac3e09671edbf05dcab3a34474dff20cf6_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_7b4ce866b1db7002f24538ea2356562493c19c52333a123211617150279a19dd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7b4ce866b1db7002f24538ea2356562493c19c52333a123211617150279a19dd->enter($__internal_7b4ce866b1db7002f24538ea2356562493c19c52333a123211617150279a19dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1ede6c0cd6876d89394f4804b671bf011e24a5ac01d66618c84bcf43447e6015 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ede6c0cd6876d89394f4804b671bf011e24a5ac01d66618c84bcf43447e6015->enter($__internal_1ede6c0cd6876d89394f4804b671bf011e24a5ac01d66618c84bcf43447e6015_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 6, $this->getSourceContext()); })()), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) || array_key_exists("targetUrl", $context) ? $context["targetUrl"] : (function () { throw new Twig_Error_Runtime('Variable "targetUrl" does not exist.', 7, $this->getSourceContext()); })())) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) || array_key_exists("targetUrl", $context) ? $context["targetUrl"] : (function () { throw new Twig_Error_Runtime('Variable "targetUrl" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_1ede6c0cd6876d89394f4804b671bf011e24a5ac01d66618c84bcf43447e6015->leave($__internal_1ede6c0cd6876d89394f4804b671bf011e24a5ac01d66618c84bcf43447e6015_prof);

        
        $__internal_7b4ce866b1db7002f24538ea2356562493c19c52333a123211617150279a19dd->leave($__internal_7b4ce866b1db7002f24538ea2356562493c19c52333a123211617150279a19dd_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:confirmed.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
