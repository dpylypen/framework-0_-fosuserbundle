<?php

/* SonataAdminBundle:Core:search.html.twig */
class __TwigTemplate_0d09c059cec63cb2533753923e245a8fd44695c702d5e36c0def7797c6a9f819 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) || array_key_exists("base_template", $context) ? $context["base_template"] : (function () { throw new Twig_Error_Runtime('Variable "base_template" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:Core:search.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8165bcf3711d7649bc8653974c8efe6fe6497253bcb9a70dafe9ec06effb21d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8165bcf3711d7649bc8653974c8efe6fe6497253bcb9a70dafe9ec06effb21d4->enter($__internal_8165bcf3711d7649bc8653974c8efe6fe6497253bcb9a70dafe9ec06effb21d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Core:search.html.twig"));

        $__internal_65963c30b3bdaefa6de3dae24cbcbe7dd7fd36316d9710f606ec567d81089b5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_65963c30b3bdaefa6de3dae24cbcbe7dd7fd36316d9710f606ec567d81089b5c->enter($__internal_65963c30b3bdaefa6de3dae24cbcbe7dd7fd36316d9710f606ec567d81089b5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Core:search.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8165bcf3711d7649bc8653974c8efe6fe6497253bcb9a70dafe9ec06effb21d4->leave($__internal_8165bcf3711d7649bc8653974c8efe6fe6497253bcb9a70dafe9ec06effb21d4_prof);

        
        $__internal_65963c30b3bdaefa6de3dae24cbcbe7dd7fd36316d9710f606ec567d81089b5c->leave($__internal_65963c30b3bdaefa6de3dae24cbcbe7dd7fd36316d9710f606ec567d81089b5c_prof);

    }

    // line 14
    public function block_title($context, array $blocks = array())
    {
        $__internal_6c035419882532ee3f1fe86a9066bf58bd34214609582935c42c0c764ac2d042 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6c035419882532ee3f1fe86a9066bf58bd34214609582935c42c0c764ac2d042->enter($__internal_6c035419882532ee3f1fe86a9066bf58bd34214609582935c42c0c764ac2d042_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_923d6479bd001d19099f8975351c442471462b43cd8e2de679ff75ca4e627efb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_923d6479bd001d19099f8975351c442471462b43cd8e2de679ff75ca4e627efb->enter($__internal_923d6479bd001d19099f8975351c442471462b43cd8e2de679ff75ca4e627efb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_search_results", array("%query%" => (isset($context["query"]) || array_key_exists("query", $context) ? $context["query"] : (function () { throw new Twig_Error_Runtime('Variable "query" does not exist.', 14, $this->getSourceContext()); })())), "SonataAdminBundle"), "html", null, true);
        
        $__internal_923d6479bd001d19099f8975351c442471462b43cd8e2de679ff75ca4e627efb->leave($__internal_923d6479bd001d19099f8975351c442471462b43cd8e2de679ff75ca4e627efb_prof);

        
        $__internal_6c035419882532ee3f1fe86a9066bf58bd34214609582935c42c0c764ac2d042->leave($__internal_6c035419882532ee3f1fe86a9066bf58bd34214609582935c42c0c764ac2d042_prof);

    }

    // line 15
    public function block_breadcrumb($context, array $blocks = array())
    {
        $__internal_945a99ec774d24fd6947773524a630b183c9f392e5d113376bd7e73aad354cb6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_945a99ec774d24fd6947773524a630b183c9f392e5d113376bd7e73aad354cb6->enter($__internal_945a99ec774d24fd6947773524a630b183c9f392e5d113376bd7e73aad354cb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "breadcrumb"));

        $__internal_3852ef4c3ade2f47b8044a6759e56c9c68c9277b0434da3e0fdc0322acca43b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3852ef4c3ade2f47b8044a6759e56c9c68c9277b0434da3e0fdc0322acca43b8->enter($__internal_3852ef4c3ade2f47b8044a6759e56c9c68c9277b0434da3e0fdc0322acca43b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "breadcrumb"));

        
        $__internal_3852ef4c3ade2f47b8044a6759e56c9c68c9277b0434da3e0fdc0322acca43b8->leave($__internal_3852ef4c3ade2f47b8044a6759e56c9c68c9277b0434da3e0fdc0322acca43b8_prof);

        
        $__internal_945a99ec774d24fd6947773524a630b183c9f392e5d113376bd7e73aad354cb6->leave($__internal_945a99ec774d24fd6947773524a630b183c9f392e5d113376bd7e73aad354cb6_prof);

    }

    // line 16
    public function block_content($context, array $blocks = array())
    {
        $__internal_f18ff003dd3e98cc40103e93839b173f70220671eb3cda3616cee3d127514a2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f18ff003dd3e98cc40103e93839b173f70220671eb3cda3616cee3d127514a2e->enter($__internal_f18ff003dd3e98cc40103e93839b173f70220671eb3cda3616cee3d127514a2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_d9263b9a80be76b0c64bec2b94a356d6e60bf6a7b4dc89e011f890bfab25e02c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9263b9a80be76b0c64bec2b94a356d6e60bf6a7b4dc89e011f890bfab25e02c->enter($__internal_d9263b9a80be76b0c64bec2b94a356d6e60bf6a7b4dc89e011f890bfab25e02c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 17
        echo "    <h2 class=\"page-header\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_search_results", array("%query%" => (isset($context["query"]) || array_key_exists("query", $context) ? $context["query"] : (function () { throw new Twig_Error_Runtime('Variable "query" does not exist.', 17, $this->getSourceContext()); })())), "SonataAdminBundle"), "html", null, true);
        echo "</h2>

    ";
        // line 19
        if ((array_key_exists("query", $context) &&  !((isset($context["query"]) || array_key_exists("query", $context) ? $context["query"] : (function () { throw new Twig_Error_Runtime('Variable "query" does not exist.', 19, $this->getSourceContext()); })()) === false))) {
            // line 20
            echo "        ";
            $context["count"] = 0;
            // line 21
            echo "        <div class=\"row\" data-masonry='{ \"itemSelector\": \".search-box-item\" }'>
        ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["groups"]) || array_key_exists("groups", $context) ? $context["groups"] : (function () { throw new Twig_Error_Runtime('Variable "groups" does not exist.', 22, $this->getSourceContext()); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["group"]) {
                // line 23
                echo "            ";
                $context["display"] = (twig_test_empty(twig_get_attribute($this->env, $this->getSourceContext(), $context["group"], "roles", array())) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_SUPER_ADMIN"));
                // line 24
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["group"], "roles", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                    if ( !(isset($context["display"]) || array_key_exists("display", $context) ? $context["display"] : (function () { throw new Twig_Error_Runtime('Variable "display" does not exist.', 24, $this->getSourceContext()); })())) {
                        // line 25
                        echo "                ";
                        $context["display"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted($context["role"]);
                        // line 26
                        echo "            ";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 27
                echo "
            ";
                // line 28
                if ((isset($context["display"]) || array_key_exists("display", $context) ? $context["display"] : (function () { throw new Twig_Error_Runtime('Variable "display" does not exist.', 28, $this->getSourceContext()); })())) {
                    // line 29
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["group"], "items", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["admin"]) {
                        // line 30
                        echo "                    ";
                        $context["count"] = ((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 30, $this->getSourceContext()); })()) + 1);
                        // line 31
                        echo "                    ";
                        if (((twig_get_attribute($this->env, $this->getSourceContext(), $context["admin"], "hasRoute", array(0 => "create"), "method") && twig_get_attribute($this->env, $this->getSourceContext(), $context["admin"], "hasAccess", array(0 => "create"), "method")) || (twig_get_attribute($this->env, $this->getSourceContext(), $context["admin"], "hasRoute", array(0 => "list"), "method") && twig_get_attribute($this->env, $this->getSourceContext(), $context["admin"], "hasAccess", array(0 => "list"), "method")))) {
                            // line 32
                            echo "                        ";
                            echo call_user_func_array($this->env->getFunction('sonata_block_render')->getCallable(), array(array("type" => "sonata.admin.block.search_result"), array("query" =>                             // line 35
(isset($context["query"]) || array_key_exists("query", $context) ? $context["query"] : (function () { throw new Twig_Error_Runtime('Variable "query" does not exist.', 35, $this->getSourceContext()); })()), "admin_code" => twig_get_attribute($this->env, $this->getSourceContext(),                             // line 36
$context["admin"], "code", array()), "page" => 0, "per_page" => 10, "icon" => twig_get_attribute($this->env, $this->getSourceContext(),                             // line 39
$context["group"], "icon", array()))));
                            // line 40
                            echo "
                    ";
                        }
                        // line 42
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['admin'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 43
                    echo "            ";
                }
                // line 44
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['group'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "        </div>
    ";
        }
        // line 47
        echo "
";
        
        $__internal_d9263b9a80be76b0c64bec2b94a356d6e60bf6a7b4dc89e011f890bfab25e02c->leave($__internal_d9263b9a80be76b0c64bec2b94a356d6e60bf6a7b4dc89e011f890bfab25e02c_prof);

        
        $__internal_f18ff003dd3e98cc40103e93839b173f70220671eb3cda3616cee3d127514a2e->leave($__internal_f18ff003dd3e98cc40103e93839b173f70220671eb3cda3616cee3d127514a2e_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Core:search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 47,  163 => 45,  157 => 44,  154 => 43,  148 => 42,  144 => 40,  142 => 39,  141 => 36,  140 => 35,  138 => 32,  135 => 31,  132 => 30,  127 => 29,  125 => 28,  122 => 27,  115 => 26,  112 => 25,  106 => 24,  103 => 23,  99 => 22,  96 => 21,  93 => 20,  91 => 19,  85 => 17,  76 => 16,  59 => 15,  41 => 14,  20 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block title %}{{ 'title_search_results'|trans({'%query%': query}, 'SonataAdminBundle') }}{% endblock %}
{% block breadcrumb %}{% endblock %}
{% block content %}
    <h2 class=\"page-header\">{{ 'title_search_results'|trans({'%query%': query}, 'SonataAdminBundle') }}</h2>

    {% if query is defined and query is not same as(false) %}
        {% set count = 0 %}
        <div class=\"row\" data-masonry='{ \"itemSelector\": \".search-box-item\" }'>
        {% for group in groups %}
            {% set display = (group.roles is empty or is_granted('ROLE_SUPER_ADMIN') ) %}
            {% for role in group.roles if not display %}
                {% set display = is_granted(role) %}
            {% endfor %}

            {% if display %}
                {% for admin in group.items %}
                    {% set count = count + 1 %}
                    {% if admin.hasRoute('create') and admin.hasAccess('create') or admin.hasRoute('list') and admin.hasAccess('list') %}
                        {{ sonata_block_render({
                            'type': 'sonata.admin.block.search_result'
                        }, {
                            'query': query,
                            'admin_code': admin.code,
                            'page': 0,
                            'per_page': 10,
                            'icon': group.icon
                        }) }}
                    {% endif %}
                {% endfor %}
            {% endif %}
        {% endfor %}
        </div>
    {% endif %}

{% endblock %}
", "SonataAdminBundle:Core:search.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/Core/search.html.twig");
    }
}
