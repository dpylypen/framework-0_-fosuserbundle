<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_79c6e3fc454dc8b4d99b1d5a77ade72880bb46d4e48d3995ff257f10fc6c1f47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c237ce1fe33100a1c07fea4ae1911ad28e9b4f605817893477797e16984d720a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c237ce1fe33100a1c07fea4ae1911ad28e9b4f605817893477797e16984d720a->enter($__internal_c237ce1fe33100a1c07fea4ae1911ad28e9b4f605817893477797e16984d720a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $__internal_a248f423574bbbd6d7e8ce28a7367d80cd40ed3b266f4ee981bc85db693e5888 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a248f423574bbbd6d7e8ce28a7367d80cd40ed3b266f4ee981bc85db693e5888->enter($__internal_a248f423574bbbd6d7e8ce28a7367d80cd40ed3b266f4ee981bc85db693e5888_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c237ce1fe33100a1c07fea4ae1911ad28e9b4f605817893477797e16984d720a->leave($__internal_c237ce1fe33100a1c07fea4ae1911ad28e9b4f605817893477797e16984d720a_prof);

        
        $__internal_a248f423574bbbd6d7e8ce28a7367d80cd40ed3b266f4ee981bc85db693e5888->leave($__internal_a248f423574bbbd6d7e8ce28a7367d80cd40ed3b266f4ee981bc85db693e5888_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a245c35d0810107b61d6b78fe2f4e609cc14cad1f0a1831fea9c1a6defa10196 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a245c35d0810107b61d6b78fe2f4e609cc14cad1f0a1831fea9c1a6defa10196->enter($__internal_a245c35d0810107b61d6b78fe2f4e609cc14cad1f0a1831fea9c1a6defa10196_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_c23003316dd4c7583a5dc32faac15107f5654b1c44312f186c1e0fcb9cfc2b7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c23003316dd4c7583a5dc32faac15107f5654b1c44312f186c1e0fcb9cfc2b7f->enter($__internal_c23003316dd4c7583a5dc32faac15107f5654b1c44312f186c1e0fcb9cfc2b7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_c23003316dd4c7583a5dc32faac15107f5654b1c44312f186c1e0fcb9cfc2b7f->leave($__internal_c23003316dd4c7583a5dc32faac15107f5654b1c44312f186c1e0fcb9cfc2b7f_prof);

        
        $__internal_a245c35d0810107b61d6b78fe2f4e609cc14cad1f0a1831fea9c1a6defa10196->leave($__internal_a245c35d0810107b61d6b78fe2f4e609cc14cad1f0a1831fea9c1a6defa10196_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
