<?php

/* SonataAdminBundle:CRUD:list__action.html.twig */
class __TwigTemplate_547a98fc6e4c0f64f0cf9b638dc10bc5a5bb30b2a0a5990e67c67a7fc58f4d37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list__action.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a717ec1f13ec340498039498e94fc735258b6610038a11d9a2f6a2f388017681 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a717ec1f13ec340498039498e94fc735258b6610038a11d9a2f6a2f388017681->enter($__internal_a717ec1f13ec340498039498e94fc735258b6610038a11d9a2f6a2f388017681_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list__action.html.twig"));

        $__internal_8c2610faa1fc26a0e37f5677e746079b4b83826ad08c5f0d17c533435f66f0f5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c2610faa1fc26a0e37f5677e746079b4b83826ad08c5f0d17c533435f66f0f5->enter($__internal_8c2610faa1fc26a0e37f5677e746079b4b83826ad08c5f0d17c533435f66f0f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list__action.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a717ec1f13ec340498039498e94fc735258b6610038a11d9a2f6a2f388017681->leave($__internal_a717ec1f13ec340498039498e94fc735258b6610038a11d9a2f6a2f388017681_prof);

        
        $__internal_8c2610faa1fc26a0e37f5677e746079b4b83826ad08c5f0d17c533435f66f0f5->leave($__internal_8c2610faa1fc26a0e37f5677e746079b4b83826ad08c5f0d17c533435f66f0f5_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_00db92b166118cda305d9a9ddfd268c3c3529f76195db20433b434c2488a6eb1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_00db92b166118cda305d9a9ddfd268c3c3529f76195db20433b434c2488a6eb1->enter($__internal_00db92b166118cda305d9a9ddfd268c3c3529f76195db20433b434c2488a6eb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_e9c1e660fdb29d2ee972ea09e03d335930e59cf8bdd3cc1ad8ccd30da1f840d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e9c1e660fdb29d2ee972ea09e03d335930e59cf8bdd3cc1ad8ccd30da1f840d6->enter($__internal_e9c1e660fdb29d2ee972ea09e03d335930e59cf8bdd3cc1ad8ccd30da1f840d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <div class=\"btn-group\">
        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "actions", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["actions"]) {
            // line 17
            echo "            ";
            $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), $context["actions"], "template", array()), "SonataAdminBundle:CRUD:list__action.html.twig", 17)->display($context);
            // line 18
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['actions'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "    </div>
";
        
        $__internal_e9c1e660fdb29d2ee972ea09e03d335930e59cf8bdd3cc1ad8ccd30da1f840d6->leave($__internal_e9c1e660fdb29d2ee972ea09e03d335930e59cf8bdd3cc1ad8ccd30da1f840d6_prof);

        
        $__internal_00db92b166118cda305d9a9ddfd268c3c3529f76195db20433b434c2488a6eb1->leave($__internal_00db92b166118cda305d9a9ddfd268c3c3529f76195db20433b434c2488a6eb1_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list__action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 19,  71 => 18,  68 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    <div class=\"btn-group\">
        {% for actions in field_description.options.actions %}
            {% include actions.template %}
        {% endfor %}
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:list__action.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list__action.html.twig");
    }
}
