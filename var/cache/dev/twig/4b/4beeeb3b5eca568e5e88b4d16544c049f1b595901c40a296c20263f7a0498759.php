<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_4c16b29cfb0a169616d85135033653ce43b566c45c89d7d660440ad7fccddcd8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e42e5365f867caed8ede6a17920f9378a9ba4797bf8f1ff40cf2456ddcad5c12 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e42e5365f867caed8ede6a17920f9378a9ba4797bf8f1ff40cf2456ddcad5c12->enter($__internal_e42e5365f867caed8ede6a17920f9378a9ba4797bf8f1ff40cf2456ddcad5c12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $__internal_fd452f4a66b8225e29f942c1c19e97666b6bb4f3de3079e1a9c377deeb20043e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd452f4a66b8225e29f942c1c19e97666b6bb4f3de3079e1a9c377deeb20043e->enter($__internal_fd452f4a66b8225e29f942c1c19e97666b6bb4f3de3079e1a9c377deeb20043e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo json_encode(array("error" => array("code" => (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 1, $this->getSourceContext()); })()), "message" => (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 1, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_e42e5365f867caed8ede6a17920f9378a9ba4797bf8f1ff40cf2456ddcad5c12->leave($__internal_e42e5365f867caed8ede6a17920f9378a9ba4797bf8f1ff40cf2456ddcad5c12_prof);

        
        $__internal_fd452f4a66b8225e29f942c1c19e97666b6bb4f3de3079e1a9c377deeb20043e->leave($__internal_fd452f4a66b8225e29f942c1c19e97666b6bb4f3de3079e1a9c377deeb20043e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "TwigBundle:Exception:error.json.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.json.twig");
    }
}
