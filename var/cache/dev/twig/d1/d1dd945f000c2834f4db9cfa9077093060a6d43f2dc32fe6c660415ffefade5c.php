<?php

/* SonataAdminBundle:CRUD:preview.html.twig */
class __TwigTemplate_778a97e6feeab2b9346977620ecd4b99eaee27a61edcb1444d9729bf23eac9d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:edit.html.twig", "SonataAdminBundle:CRUD:preview.html.twig", 12);
        $this->blocks = array(
            'actions' => array($this, 'block_actions'),
            'side_menu' => array($this, 'block_side_menu'),
            'formactions' => array($this, 'block_formactions'),
            'preview' => array($this, 'block_preview'),
            'form' => array($this, 'block_form'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:edit.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4bcd72443838c1e00b23621868cd2d5e9da55f51e94573cdd7f6fb813ee23a82 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4bcd72443838c1e00b23621868cd2d5e9da55f51e94573cdd7f6fb813ee23a82->enter($__internal_4bcd72443838c1e00b23621868cd2d5e9da55f51e94573cdd7f6fb813ee23a82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:preview.html.twig"));

        $__internal_778590c4edc73baeb8316f7ab34542361f6ea54f5a39154209b8c1f1cab8048f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_778590c4edc73baeb8316f7ab34542361f6ea54f5a39154209b8c1f1cab8048f->enter($__internal_778590c4edc73baeb8316f7ab34542361f6ea54f5a39154209b8c1f1cab8048f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:preview.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4bcd72443838c1e00b23621868cd2d5e9da55f51e94573cdd7f6fb813ee23a82->leave($__internal_4bcd72443838c1e00b23621868cd2d5e9da55f51e94573cdd7f6fb813ee23a82_prof);

        
        $__internal_778590c4edc73baeb8316f7ab34542361f6ea54f5a39154209b8c1f1cab8048f->leave($__internal_778590c4edc73baeb8316f7ab34542361f6ea54f5a39154209b8c1f1cab8048f_prof);

    }

    // line 14
    public function block_actions($context, array $blocks = array())
    {
        $__internal_15ebb4a41244fddd879982fd4c69695a964dc781ec1ef0cf4f9fc0fdcb6d7c3c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15ebb4a41244fddd879982fd4c69695a964dc781ec1ef0cf4f9fc0fdcb6d7c3c->enter($__internal_15ebb4a41244fddd879982fd4c69695a964dc781ec1ef0cf4f9fc0fdcb6d7c3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        $__internal_1b98873690d7f89ad3dd8eba5b5229c8a053bd0fb90d24a39237bfcac51b5c0f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b98873690d7f89ad3dd8eba5b5229c8a053bd0fb90d24a39237bfcac51b5c0f->enter($__internal_1b98873690d7f89ad3dd8eba5b5229c8a053bd0fb90d24a39237bfcac51b5c0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        
        $__internal_1b98873690d7f89ad3dd8eba5b5229c8a053bd0fb90d24a39237bfcac51b5c0f->leave($__internal_1b98873690d7f89ad3dd8eba5b5229c8a053bd0fb90d24a39237bfcac51b5c0f_prof);

        
        $__internal_15ebb4a41244fddd879982fd4c69695a964dc781ec1ef0cf4f9fc0fdcb6d7c3c->leave($__internal_15ebb4a41244fddd879982fd4c69695a964dc781ec1ef0cf4f9fc0fdcb6d7c3c_prof);

    }

    // line 17
    public function block_side_menu($context, array $blocks = array())
    {
        $__internal_2e5e40e4926737f5846f592a26eabeb763adddf796d8ab41396087eba77a1475 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e5e40e4926737f5846f592a26eabeb763adddf796d8ab41396087eba77a1475->enter($__internal_2e5e40e4926737f5846f592a26eabeb763adddf796d8ab41396087eba77a1475_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "side_menu"));

        $__internal_e83175358823e89306ac10c77af610bf66f75e8d7df5479705d2ae3db0a0de35 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e83175358823e89306ac10c77af610bf66f75e8d7df5479705d2ae3db0a0de35->enter($__internal_e83175358823e89306ac10c77af610bf66f75e8d7df5479705d2ae3db0a0de35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "side_menu"));

        
        $__internal_e83175358823e89306ac10c77af610bf66f75e8d7df5479705d2ae3db0a0de35->leave($__internal_e83175358823e89306ac10c77af610bf66f75e8d7df5479705d2ae3db0a0de35_prof);

        
        $__internal_2e5e40e4926737f5846f592a26eabeb763adddf796d8ab41396087eba77a1475->leave($__internal_2e5e40e4926737f5846f592a26eabeb763adddf796d8ab41396087eba77a1475_prof);

    }

    // line 20
    public function block_formactions($context, array $blocks = array())
    {
        $__internal_52488190c10f8ec5cc638cb2056d62a91ed6d604ed162a0833459cfca964a997 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_52488190c10f8ec5cc638cb2056d62a91ed6d604ed162a0833459cfca964a997->enter($__internal_52488190c10f8ec5cc638cb2056d62a91ed6d604ed162a0833459cfca964a997_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "formactions"));

        $__internal_efa9dff89ad5e30157a3a1daafbf0e626bea1f3e671d61a28f987051ecfbebf6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efa9dff89ad5e30157a3a1daafbf0e626bea1f3e671d61a28f987051ecfbebf6->enter($__internal_efa9dff89ad5e30157a3a1daafbf0e626bea1f3e671d61a28f987051ecfbebf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "formactions"));

        // line 21
        echo "    <button class=\"btn btn-success\" type=\"submit\" name=\"btn_preview_approve\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
        ";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btn_preview_approve", array(), "SonataAdminBundle"), "html", null, true);
        echo "
    </button>
    <button class=\"btn btn-danger\" type=\"submit\" name=\"btn_preview_decline\">
        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>
        ";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btn_preview_decline", array(), "SonataAdminBundle"), "html", null, true);
        echo "
    </button>
";
        
        $__internal_efa9dff89ad5e30157a3a1daafbf0e626bea1f3e671d61a28f987051ecfbebf6->leave($__internal_efa9dff89ad5e30157a3a1daafbf0e626bea1f3e671d61a28f987051ecfbebf6_prof);

        
        $__internal_52488190c10f8ec5cc638cb2056d62a91ed6d604ed162a0833459cfca964a997->leave($__internal_52488190c10f8ec5cc638cb2056d62a91ed6d604ed162a0833459cfca964a997_prof);

    }

    // line 31
    public function block_preview($context, array $blocks = array())
    {
        $__internal_c421163aadf16d6169d9d5709fda7843d63bbdb169cb84f8c1f77fc7235ca226 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c421163aadf16d6169d9d5709fda7843d63bbdb169cb84f8c1f77fc7235ca226->enter($__internal_c421163aadf16d6169d9d5709fda7843d63bbdb169cb84f8c1f77fc7235ca226_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "preview"));

        $__internal_9e148b2805a213a4d2661014c5b0d4216b316fedc0d05d89eb67049736299fd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e148b2805a213a4d2661014c5b0d4216b316fedc0d05d89eb67049736299fd8->enter($__internal_9e148b2805a213a4d2661014c5b0d4216b316fedc0d05d89eb67049736299fd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "preview"));

        // line 32
        echo "    <div class=\"sonata-ba-view\">
        ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 33, $this->getSourceContext()); })()), "showgroups", array()));
        foreach ($context['_seq'] as $context["name"] => $context["view_group"]) {
            // line 34
            echo "            <table class=\"table table-bordered\">
                ";
            // line 35
            if ($context["name"]) {
                // line 36
                echo "                    <tr class=\"sonata-ba-view-title\">
                        <td colspan=\"2\">
                            ";
                // line 38
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["name"], array(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 38, $this->getSourceContext()); })()), "translationdomain", array())), "html", null, true);
                echo "
                        </td>
                    </tr>
                ";
            }
            // line 42
            echo "
                ";
            // line 43
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["view_group"], "fields", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["field_name"]) {
                // line 44
                echo "                    <tr class=\"sonata-ba-view-container\">
                        ";
                // line 45
                if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["admin"] ?? null), "show", array(), "any", false, true), $context["field_name"], array(), "array", true, true)) {
                    // line 46
                    echo "                            ";
                    echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderViewElement($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 46, $this->getSourceContext()); })()), "show", array()), $context["field_name"], array(), "array"), (isset($context["object"]) || array_key_exists("object", $context) ? $context["object"] : (function () { throw new Twig_Error_Runtime('Variable "object" does not exist.', 46, $this->getSourceContext()); })()));
                    echo "
                        ";
                }
                // line 48
                echo "                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field_name'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo "            </table>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['name'], $context['view_group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "    </div>
";
        
        $__internal_9e148b2805a213a4d2661014c5b0d4216b316fedc0d05d89eb67049736299fd8->leave($__internal_9e148b2805a213a4d2661014c5b0d4216b316fedc0d05d89eb67049736299fd8_prof);

        
        $__internal_c421163aadf16d6169d9d5709fda7843d63bbdb169cb84f8c1f77fc7235ca226->leave($__internal_c421163aadf16d6169d9d5709fda7843d63bbdb169cb84f8c1f77fc7235ca226_prof);

    }

    // line 55
    public function block_form($context, array $blocks = array())
    {
        $__internal_e965c6b59d22f74247416cd2cc0e3d34934c6606c1594ef4d18fed53f538f792 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e965c6b59d22f74247416cd2cc0e3d34934c6606c1594ef4d18fed53f538f792->enter($__internal_e965c6b59d22f74247416cd2cc0e3d34934c6606c1594ef4d18fed53f538f792_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        $__internal_760984525ce873717e0b605d770e8468267b7fa5a05a5e569fc6558d5a1d9210 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_760984525ce873717e0b605d770e8468267b7fa5a05a5e569fc6558d5a1d9210->enter($__internal_760984525ce873717e0b605d770e8468267b7fa5a05a5e569fc6558d5a1d9210_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 56
        echo "    <div class=\"sonata-preview-form-container\">
        ";
        // line 57
        $this->displayParentBlock("form", $context, $blocks);
        echo "
    </div>
";
        
        $__internal_760984525ce873717e0b605d770e8468267b7fa5a05a5e569fc6558d5a1d9210->leave($__internal_760984525ce873717e0b605d770e8468267b7fa5a05a5e569fc6558d5a1d9210_prof);

        
        $__internal_e965c6b59d22f74247416cd2cc0e3d34934c6606c1594ef4d18fed53f538f792->leave($__internal_e965c6b59d22f74247416cd2cc0e3d34934c6606c1594ef4d18fed53f538f792_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 57,  195 => 56,  186 => 55,  175 => 52,  168 => 50,  161 => 48,  155 => 46,  153 => 45,  150 => 44,  146 => 43,  143 => 42,  136 => 38,  132 => 36,  130 => 35,  127 => 34,  123 => 33,  120 => 32,  111 => 31,  98 => 27,  91 => 23,  87 => 21,  78 => 20,  61 => 17,  44 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:edit.html.twig' %}

{% block actions %}
{% endblock %}

{% block side_menu %}
{% endblock %}

{% block formactions %}
    <button class=\"btn btn-success\" type=\"submit\" name=\"btn_preview_approve\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
        {{ 'btn_preview_approve'|trans({}, 'SonataAdminBundle') }}
    </button>
    <button class=\"btn btn-danger\" type=\"submit\" name=\"btn_preview_decline\">
        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>
        {{ 'btn_preview_decline'|trans({}, 'SonataAdminBundle') }}
    </button>
{% endblock %}

{% block preview %}
    <div class=\"sonata-ba-view\">
        {% for name, view_group in admin.showgroups %}
            <table class=\"table table-bordered\">
                {% if name %}
                    <tr class=\"sonata-ba-view-title\">
                        <td colspan=\"2\">
                            {{ name|trans({}, admin.translationdomain) }}
                        </td>
                    </tr>
                {% endif %}

                {% for field_name in view_group.fields %}
                    <tr class=\"sonata-ba-view-container\">
                        {% if admin.show[field_name] is defined %}
                            {{ admin.show[field_name]|render_view_element(object) }}
                        {% endif %}
                    </tr>
                {% endfor %}
            </table>
        {% endfor %}
    </div>
{% endblock %}

{% block form %}
    <div class=\"sonata-preview-form-container\">
        {{ parent() }}
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:preview.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/preview.html.twig");
    }
}
