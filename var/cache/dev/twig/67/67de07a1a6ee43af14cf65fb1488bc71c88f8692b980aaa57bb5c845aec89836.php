<?php

/* SonataAdminBundle:Pager:links.html.twig */
class __TwigTemplate_4904010b3b6bad3227d5488f8b0b9ae3c132251fc72ea65886189665e777a965 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:Pager:base_links.html.twig", "SonataAdminBundle:Pager:links.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Pager:base_links.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c8c7ca904e37cc9dc97f8fa0ccc85f58496771368b6df45671a721bdb323d615 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8c7ca904e37cc9dc97f8fa0ccc85f58496771368b6df45671a721bdb323d615->enter($__internal_c8c7ca904e37cc9dc97f8fa0ccc85f58496771368b6df45671a721bdb323d615_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:links.html.twig"));

        $__internal_2d3c0dd15c3668a029931decd3bfe7a6ce9540ba2c28378c3bbab6008139ef87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d3c0dd15c3668a029931decd3bfe7a6ce9540ba2c28378c3bbab6008139ef87->enter($__internal_2d3c0dd15c3668a029931decd3bfe7a6ce9540ba2c28378c3bbab6008139ef87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:links.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c8c7ca904e37cc9dc97f8fa0ccc85f58496771368b6df45671a721bdb323d615->leave($__internal_c8c7ca904e37cc9dc97f8fa0ccc85f58496771368b6df45671a721bdb323d615_prof);

        
        $__internal_2d3c0dd15c3668a029931decd3bfe7a6ce9540ba2c28378c3bbab6008139ef87->leave($__internal_2d3c0dd15c3668a029931decd3bfe7a6ce9540ba2c28378c3bbab6008139ef87_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Pager:links.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:Pager:base_links.html.twig' %}
", "SonataAdminBundle:Pager:links.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/Pager/links.html.twig");
    }
}
