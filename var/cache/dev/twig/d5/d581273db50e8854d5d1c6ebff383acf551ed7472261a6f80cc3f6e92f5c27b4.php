<?php

/* SonataAdminBundle:Helper:render_form_dismissable_errors.html.twig */
class __TwigTemplate_4f23031b082a10c9c6e24c43f6b947960ca2a36e9ee405789eff7351f6ff0bf6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_149ee1fe4b9b3edc20ab2b74efe5b6280ab3cb27b2ac22af11c67eadf5d4748b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_149ee1fe4b9b3edc20ab2b74efe5b6280ab3cb27b2ac22af11c67eadf5d4748b->enter($__internal_149ee1fe4b9b3edc20ab2b74efe5b6280ab3cb27b2ac22af11c67eadf5d4748b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Helper:render_form_dismissable_errors.html.twig"));

        $__internal_82c1d905e86804a48b08a7c6ada58afe8e8cc5250e9f2dc7658914dc62ec3db1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_82c1d905e86804a48b08a7c6ada58afe8e8cc5250e9f2dc7658914dc62ec3db1->enter($__internal_82c1d905e86804a48b08a7c6ada58afe8e8cc5250e9f2dc7658914dc62ec3db1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Helper:render_form_dismissable_errors.html.twig"));

        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 1, $this->getSourceContext()); })()), "vars", array()), "errors", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
            // line 2
            echo "    <div class=\"alert alert-danger alert-dismissable\">
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
        ";
            // line 4
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["error"], "message", array()), "html", null, true);
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_149ee1fe4b9b3edc20ab2b74efe5b6280ab3cb27b2ac22af11c67eadf5d4748b->leave($__internal_149ee1fe4b9b3edc20ab2b74efe5b6280ab3cb27b2ac22af11c67eadf5d4748b_prof);

        
        $__internal_82c1d905e86804a48b08a7c6ada58afe8e8cc5250e9f2dc7658914dc62ec3db1->leave($__internal_82c1d905e86804a48b08a7c6ada58afe8e8cc5250e9f2dc7658914dc62ec3db1_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Helper:render_form_dismissable_errors.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  29 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% for error in form.vars.errors %}
    <div class=\"alert alert-danger alert-dismissable\">
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
        {{ error.message }}
    </div>
{% endfor %}
", "SonataAdminBundle:Helper:render_form_dismissable_errors.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/Helper/render_form_dismissable_errors.html.twig");
    }
}
