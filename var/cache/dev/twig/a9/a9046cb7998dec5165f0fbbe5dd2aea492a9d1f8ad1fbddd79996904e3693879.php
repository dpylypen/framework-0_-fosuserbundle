<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_db5c2c8fbea1746a96f846b5dba9d7240070bfeac0bf462e5c788f20d81547b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d537140459a21c40ea787aaf80ecfd605ccefebe17be12df24f61c84b7ed3d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d537140459a21c40ea787aaf80ecfd605ccefebe17be12df24f61c84b7ed3d8->enter($__internal_5d537140459a21c40ea787aaf80ecfd605ccefebe17be12df24f61c84b7ed3d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        $__internal_5946ab6ee9f91bf1b6f235b982a041093a3ad4b03c328c643b328d20f3e8f34a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5946ab6ee9f91bf1b6f235b982a041093a3ad4b03c328c643b328d20f3e8f34a->enter($__internal_5946ab6ee9f91bf1b6f235b982a041093a3ad4b03c328c643b328d20f3e8f34a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 4, $this->getSourceContext()); })());
        echo " ";
        echo (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 4, $this->getSourceContext()); })());
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_5d537140459a21c40ea787aaf80ecfd605ccefebe17be12df24f61c84b7ed3d8->leave($__internal_5d537140459a21c40ea787aaf80ecfd605ccefebe17be12df24f61c84b7ed3d8_prof);

        
        $__internal_5946ab6ee9f91bf1b6f235b982a041093a3ad4b03c328c643b328d20f3e8f34a->leave($__internal_5946ab6ee9f91bf1b6f235b982a041093a3ad4b03c328c643b328d20f3e8f34a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("Oops! An Error Occurred
=======================

The server returned a \"{{ status_code }} {{ status_text }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
", "TwigBundle:Exception:error.txt.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.txt.twig");
    }
}
