<?php

/* SonataAdminBundle:CRUD:base_standard_edit_field.html.twig */
class __TwigTemplate_52b2deec8ae7c3bbadcc37cc647ee910bc6f9a8c5b094498db1068228e44f0d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'label' => array($this, 'block_label'),
            'field' => array($this, 'block_field'),
            'help' => array($this, 'block_help'),
            'errors' => array($this, 'block_errors'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6256aad5d396f0f4bac406207aef405780e10174104555503e23393b0476a4cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6256aad5d396f0f4bac406207aef405780e10174104555503e23393b0476a4cb->enter($__internal_6256aad5d396f0f4bac406207aef405780e10174104555503e23393b0476a4cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_standard_edit_field.html.twig"));

        $__internal_557664e83b1d9da2edc6bce90b1676e13ef19aacce6dcf89e360ee157abb6d25 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_557664e83b1d9da2edc6bce90b1676e13ef19aacce6dcf89e360ee157abb6d25->enter($__internal_557664e83b1d9da2edc6bce90b1676e13ef19aacce6dcf89e360ee157abb6d25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_standard_edit_field.html.twig"));

        // line 11
        echo "
<div class=\"form-group";
        // line 12
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 12, $this->getSourceContext()); })()), "var", array()), "errors", array())) > 0)) {
            echo " has-error";
        }
        echo "\" id=\"sonata-ba-field-container-";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 12, $this->getSourceContext()); })()), "vars", array()), "id", array()), "html", null, true);
        echo "\">
    ";
        // line 13
        $this->displayBlock('label', $context, $blocks);
        // line 20
        echo "
    <div class=\"col-sm-10 col-md-5 sonata-ba-field sonata-ba-field-";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new Twig_Error_Runtime('Variable "edit" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, (isset($context["inline"]) || array_key_exists("inline", $context) ? $context["inline"] : (function () { throw new Twig_Error_Runtime('Variable "inline" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 21, $this->getSourceContext()); })()), "vars", array()), "errors", array())) > 0)) {
            echo "sonata-ba-field-error";
        }
        echo "\">

        ";
        // line 23
        $this->displayBlock('field', $context, $blocks);
        // line 24
        echo "
        ";
        // line 25
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 25, $this->getSourceContext()); })()), "help", array())) {
            // line 26
            echo "            <span class=\"help-block\">";
            $this->displayBlock('help', $context, $blocks);
            echo "</span>
        ";
        }
        // line 28
        echo "
        <div class=\"sonata-ba-field-error-messages\">
            ";
        // line 30
        $this->displayBlock('errors', $context, $blocks);
        // line 31
        echo "        </div>

    </div>
</div>
";
        
        $__internal_6256aad5d396f0f4bac406207aef405780e10174104555503e23393b0476a4cb->leave($__internal_6256aad5d396f0f4bac406207aef405780e10174104555503e23393b0476a4cb_prof);

        
        $__internal_557664e83b1d9da2edc6bce90b1676e13ef19aacce6dcf89e360ee157abb6d25->leave($__internal_557664e83b1d9da2edc6bce90b1676e13ef19aacce6dcf89e360ee157abb6d25_prof);

    }

    // line 13
    public function block_label($context, array $blocks = array())
    {
        $__internal_75ebf1ae17aa96d713ded134f28a347fbc5fa96a9c85564f99ff9bc5bf7950f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75ebf1ae17aa96d713ded134f28a347fbc5fa96a9c85564f99ff9bc5bf7950f4->enter($__internal_75ebf1ae17aa96d713ded134f28a347fbc5fa96a9c85564f99ff9bc5bf7950f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_317b5793f0b89012d3364924adb50fbd2bb28454a9533305e6925befba6c98cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_317b5793f0b89012d3364924adb50fbd2bb28454a9533305e6925befba6c98cb->enter($__internal_317b5793f0b89012d3364924adb50fbd2bb28454a9533305e6925befba6c98cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        // line 14
        echo "        ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "name", array(), "any", true, true)) {
            // line 15
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 15, $this->getSourceContext()); })()), 'label', (twig_test_empty($_label_ = twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 15, $this->getSourceContext()); })()), "options", array()), "name", array())) ? array() : array("label" => $_label_)));
            echo "
        ";
        } else {
            // line 17
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 17, $this->getSourceContext()); })()), 'label');
            echo "
        ";
        }
        // line 19
        echo "    ";
        
        $__internal_317b5793f0b89012d3364924adb50fbd2bb28454a9533305e6925befba6c98cb->leave($__internal_317b5793f0b89012d3364924adb50fbd2bb28454a9533305e6925befba6c98cb_prof);

        
        $__internal_75ebf1ae17aa96d713ded134f28a347fbc5fa96a9c85564f99ff9bc5bf7950f4->leave($__internal_75ebf1ae17aa96d713ded134f28a347fbc5fa96a9c85564f99ff9bc5bf7950f4_prof);

    }

    // line 23
    public function block_field($context, array $blocks = array())
    {
        $__internal_5fde8236752d1320d505b208b4fbf8d7193158df33cb492f0827b216ddc29bcf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5fde8236752d1320d505b208b4fbf8d7193158df33cb492f0827b216ddc29bcf->enter($__internal_5fde8236752d1320d505b208b4fbf8d7193158df33cb492f0827b216ddc29bcf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_dd62dee1816406e0c4b921152f8fd5d69eb4a9f796a7ce41d37e04a11e6f2caf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd62dee1816406e0c4b921152f8fd5d69eb4a9f796a7ce41d37e04a11e6f2caf->enter($__internal_dd62dee1816406e0c4b921152f8fd5d69eb4a9f796a7ce41d37e04a11e6f2caf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 23, $this->getSourceContext()); })()), 'widget');
        
        $__internal_dd62dee1816406e0c4b921152f8fd5d69eb4a9f796a7ce41d37e04a11e6f2caf->leave($__internal_dd62dee1816406e0c4b921152f8fd5d69eb4a9f796a7ce41d37e04a11e6f2caf_prof);

        
        $__internal_5fde8236752d1320d505b208b4fbf8d7193158df33cb492f0827b216ddc29bcf->leave($__internal_5fde8236752d1320d505b208b4fbf8d7193158df33cb492f0827b216ddc29bcf_prof);

    }

    // line 26
    public function block_help($context, array $blocks = array())
    {
        $__internal_8730b9c081bfe804cc320e25839b8d54962bc64d028dc383670a8ed3a9a4ddf5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8730b9c081bfe804cc320e25839b8d54962bc64d028dc383670a8ed3a9a4ddf5->enter($__internal_8730b9c081bfe804cc320e25839b8d54962bc64d028dc383670a8ed3a9a4ddf5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "help"));

        $__internal_749a64a2baf1ff1ae67da8193a2c25fbd51bd22c8a699ce042636d13a7524792 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_749a64a2baf1ff1ae67da8193a2c25fbd51bd22c8a699ce042636d13a7524792->enter($__internal_749a64a2baf1ff1ae67da8193a2c25fbd51bd22c8a699ce042636d13a7524792_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "help"));

        echo twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 26, $this->getSourceContext()); })()), "help", array());
        
        $__internal_749a64a2baf1ff1ae67da8193a2c25fbd51bd22c8a699ce042636d13a7524792->leave($__internal_749a64a2baf1ff1ae67da8193a2c25fbd51bd22c8a699ce042636d13a7524792_prof);

        
        $__internal_8730b9c081bfe804cc320e25839b8d54962bc64d028dc383670a8ed3a9a4ddf5->leave($__internal_8730b9c081bfe804cc320e25839b8d54962bc64d028dc383670a8ed3a9a4ddf5_prof);

    }

    // line 30
    public function block_errors($context, array $blocks = array())
    {
        $__internal_56d134b204f78f7e4ff0dc82044fdd114dc1399e1625c561162d6ae2a0a1aecc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56d134b204f78f7e4ff0dc82044fdd114dc1399e1625c561162d6ae2a0a1aecc->enter($__internal_56d134b204f78f7e4ff0dc82044fdd114dc1399e1625c561162d6ae2a0a1aecc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "errors"));

        $__internal_cfacb570965c6411bc43ca79850a63cbc62f7c84b2efc227fb2c37bf707386a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cfacb570965c6411bc43ca79850a63cbc62f7c84b2efc227fb2c37bf707386a1->enter($__internal_cfacb570965c6411bc43ca79850a63cbc62f7c84b2efc227fb2c37bf707386a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "errors"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 30, $this->getSourceContext()); })()), 'errors');
        
        $__internal_cfacb570965c6411bc43ca79850a63cbc62f7c84b2efc227fb2c37bf707386a1->leave($__internal_cfacb570965c6411bc43ca79850a63cbc62f7c84b2efc227fb2c37bf707386a1_prof);

        
        $__internal_56d134b204f78f7e4ff0dc82044fdd114dc1399e1625c561162d6ae2a0a1aecc->leave($__internal_56d134b204f78f7e4ff0dc82044fdd114dc1399e1625c561162d6ae2a0a1aecc_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_standard_edit_field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 30,  141 => 26,  123 => 23,  113 => 19,  107 => 17,  101 => 15,  98 => 14,  89 => 13,  75 => 31,  73 => 30,  69 => 28,  63 => 26,  61 => 25,  58 => 24,  56 => 23,  45 => 21,  42 => 20,  40 => 13,  32 => 12,  29 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<div class=\"form-group{% if field_element.var.errors|length > 0%} has-error{%endif%}\" id=\"sonata-ba-field-container-{{ field_element.vars.id }}\">
    {% block label %}
        {% if field_description.options.name is defined %}
            {{ form_label(field_element, field_description.options.name) }}
        {% else %}
            {{ form_label(field_element) }}
        {% endif %}
    {% endblock %}

    <div class=\"col-sm-10 col-md-5 sonata-ba-field sonata-ba-field-{{ edit }}-{{ inline }} {% if field_element.vars.errors|length > 0 %}sonata-ba-field-error{% endif %}\">

        {% block field %}{{ form_widget(field_element) }}{% endblock %}

        {% if field_description.help %}
            <span class=\"help-block\">{% block help %}{{ field_description.help|raw }}{% endblock %}</span>
        {% endif %}

        <div class=\"sonata-ba-field-error-messages\">
            {% block errors %}{{ form_errors(field_element) }}{% endblock %}
        </div>

    </div>
</div>
", "SonataAdminBundle:CRUD:base_standard_edit_field.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/base_standard_edit_field.html.twig");
    }
}
