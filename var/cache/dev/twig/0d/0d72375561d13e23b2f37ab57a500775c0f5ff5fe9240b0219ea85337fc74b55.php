<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_385f49f04fd1a8aa52535055c20aaf4e7faf1e87f94872fecdee70b5db489f28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_17776b8e053d236a612d6f660e6d11d47ad8349a9b9c19ed11c6718068d3dc6f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17776b8e053d236a612d6f660e6d11d47ad8349a9b9c19ed11c6718068d3dc6f->enter($__internal_17776b8e053d236a612d6f660e6d11d47ad8349a9b9c19ed11c6718068d3dc6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_0b332242011b2f0330c153c1412874fa4a385c3421d5241d93add96fa5c5b88f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b332242011b2f0330c153c1412874fa4a385c3421d5241d93add96fa5c5b88f->enter($__internal_0b332242011b2f0330c153c1412874fa4a385c3421d5241d93add96fa5c5b88f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_17776b8e053d236a612d6f660e6d11d47ad8349a9b9c19ed11c6718068d3dc6f->leave($__internal_17776b8e053d236a612d6f660e6d11d47ad8349a9b9c19ed11c6718068d3dc6f_prof);

        
        $__internal_0b332242011b2f0330c153c1412874fa4a385c3421d5241d93add96fa5c5b88f->leave($__internal_0b332242011b2f0330c153c1412874fa4a385c3421d5241d93add96fa5c5b88f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
