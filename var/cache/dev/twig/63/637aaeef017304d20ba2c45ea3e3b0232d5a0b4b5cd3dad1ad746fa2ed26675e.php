<?php

/* SonataAdminBundle:CRUD:edit_sonata_type_immutable_array.html.twig */
class __TwigTemplate_817dc685761755bf155ee0acab33fde79642955f40f9d6f4dcb96651aece5f48 extends Twig_Template
{
    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) || array_key_exists("base_template", $context) ? $context["base_template"] : (function () { throw new Twig_Error_Runtime('Variable "base_template" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:edit_sonata_type_immutable_array.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d04a4143a82e7b9020082e7244a879957d753b465add19401750de6e657a7b23 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d04a4143a82e7b9020082e7244a879957d753b465add19401750de6e657a7b23->enter($__internal_d04a4143a82e7b9020082e7244a879957d753b465add19401750de6e657a7b23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_sonata_type_immutable_array.html.twig"));

        $__internal_630a112906ad4dafeb045af97a748cc1e74d77f97887ed3bb68dd4c51f5b3c82 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_630a112906ad4dafeb045af97a748cc1e74d77f97887ed3bb68dd4c51f5b3c82->enter($__internal_630a112906ad4dafeb045af97a748cc1e74d77f97887ed3bb68dd4c51f5b3c82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_sonata_type_immutable_array.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d04a4143a82e7b9020082e7244a879957d753b465add19401750de6e657a7b23->leave($__internal_d04a4143a82e7b9020082e7244a879957d753b465add19401750de6e657a7b23_prof);

        
        $__internal_630a112906ad4dafeb045af97a748cc1e74d77f97887ed3bb68dd4c51f5b3c82->leave($__internal_630a112906ad4dafeb045af97a748cc1e74d77f97887ed3bb68dd4c51f5b3c82_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_sonata_type_immutable_array.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  9 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}
", "SonataAdminBundle:CRUD:edit_sonata_type_immutable_array.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/edit_sonata_type_immutable_array.html.twig");
    }
}
