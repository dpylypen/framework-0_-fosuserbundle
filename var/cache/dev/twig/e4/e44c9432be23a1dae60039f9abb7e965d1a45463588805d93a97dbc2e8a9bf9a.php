<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_7427b108560f72aa8b31c309058380b12663792f3ef4f89f41920ff1d40752b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_610336e80f65d90cf49bd0527e817bb89ac59673532fee2380d6e854a7a99dd8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_610336e80f65d90cf49bd0527e817bb89ac59673532fee2380d6e854a7a99dd8->enter($__internal_610336e80f65d90cf49bd0527e817bb89ac59673532fee2380d6e854a7a99dd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_4fca25f1f41255ed7a3c66cff3679fae3c474ae8e553110c6d33a16f710b10c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4fca25f1f41255ed7a3c66cff3679fae3c474ae8e553110c6d33a16f710b10c7->enter($__internal_4fca25f1f41255ed7a3c66cff3679fae3c474ae8e553110c6d33a16f710b10c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_610336e80f65d90cf49bd0527e817bb89ac59673532fee2380d6e854a7a99dd8->leave($__internal_610336e80f65d90cf49bd0527e817bb89ac59673532fee2380d6e854a7a99dd8_prof);

        
        $__internal_4fca25f1f41255ed7a3c66cff3679fae3c474ae8e553110c6d33a16f710b10c7->leave($__internal_4fca25f1f41255ed7a3c66cff3679fae3c474ae8e553110c6d33a16f710b10c7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
