<?php

/* WebProfilerBundle:Profiler:header.html.twig */
class __TwigTemplate_28fc6b36d191a0d4a1a4f35bff7401b59bffbb54a3754b915d6ceafb62cfaa3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f875cf7762b1e3c274f24b8244c1b740cd517fb92135d87b271769e88307fdb1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f875cf7762b1e3c274f24b8244c1b740cd517fb92135d87b271769e88307fdb1->enter($__internal_f875cf7762b1e3c274f24b8244c1b740cd517fb92135d87b271769e88307fdb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        $__internal_ec294ea7fa2b3dfe384f31ff2a30e9c49f896b0e4eeaadc7156ec65dbe07d551 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ec294ea7fa2b3dfe384f31ff2a30e9c49f896b0e4eeaadc7156ec65dbe07d551->enter($__internal_ec294ea7fa2b3dfe384f31ff2a30e9c49f896b0e4eeaadc7156ec65dbe07d551_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_f875cf7762b1e3c274f24b8244c1b740cd517fb92135d87b271769e88307fdb1->leave($__internal_f875cf7762b1e3c274f24b8244c1b740cd517fb92135d87b271769e88307fdb1_prof);

        
        $__internal_ec294ea7fa2b3dfe384f31ff2a30e9c49f896b0e4eeaadc7156ec65dbe07d551->leave($__internal_ec294ea7fa2b3dfe384f31ff2a30e9c49f896b0e4eeaadc7156ec65dbe07d551_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"header\">
    <div class=\"container\">
        <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
", "WebProfilerBundle:Profiler:header.html.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/header.html.twig");
    }
}
