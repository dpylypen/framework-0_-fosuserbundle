<?php

/* SonataAdminBundle:CRUD:list__batch.html.twig */
class __TwigTemplate_a40d21698caac274b078dc4c7ca3040486339008a905570b2f7eb7e13c56773f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list__batch.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a59df0b33a4102ffd6e9edf996d8efa60903cce16c88345410dc4d3df276447 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a59df0b33a4102ffd6e9edf996d8efa60903cce16c88345410dc4d3df276447->enter($__internal_4a59df0b33a4102ffd6e9edf996d8efa60903cce16c88345410dc4d3df276447_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list__batch.html.twig"));

        $__internal_dc75dbcf8a039e5400550662f8c5c19c6fdff564220ea78e3fa620b013b9bb7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dc75dbcf8a039e5400550662f8c5c19c6fdff564220ea78e3fa620b013b9bb7d->enter($__internal_dc75dbcf8a039e5400550662f8c5c19c6fdff564220ea78e3fa620b013b9bb7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list__batch.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4a59df0b33a4102ffd6e9edf996d8efa60903cce16c88345410dc4d3df276447->leave($__internal_4a59df0b33a4102ffd6e9edf996d8efa60903cce16c88345410dc4d3df276447_prof);

        
        $__internal_dc75dbcf8a039e5400550662f8c5c19c6fdff564220ea78e3fa620b013b9bb7d->leave($__internal_dc75dbcf8a039e5400550662f8c5c19c6fdff564220ea78e3fa620b013b9bb7d_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_8b4d0dbcb18afbaa7d0003dce3977d838f1c01956bc7d7b087e9749c79c53d92 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8b4d0dbcb18afbaa7d0003dce3977d838f1c01956bc7d7b087e9749c79c53d92->enter($__internal_8b4d0dbcb18afbaa7d0003dce3977d838f1c01956bc7d7b087e9749c79c53d92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_d1e73a0e54ef176467d6b5fc429dfee0d5443e0204d34a90bc222c27f2bf168c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1e73a0e54ef176467d6b5fc429dfee0d5443e0204d34a90bc222c27f2bf168c->enter($__internal_d1e73a0e54ef176467d6b5fc429dfee0d5443e0204d34a90bc222c27f2bf168c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <input type=\"checkbox\" name=\"idx[]\" value=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || array_key_exists("object", $context) ? $context["object"] : (function () { throw new Twig_Error_Runtime('Variable "object" does not exist.', 15, $this->getSourceContext()); })())), "method"), "html", null, true);
        echo "\">
";
        
        $__internal_d1e73a0e54ef176467d6b5fc429dfee0d5443e0204d34a90bc222c27f2bf168c->leave($__internal_d1e73a0e54ef176467d6b5fc429dfee0d5443e0204d34a90bc222c27f2bf168c_prof);

        
        $__internal_8b4d0dbcb18afbaa7d0003dce3977d838f1c01956bc7d7b087e9749c79c53d92->leave($__internal_8b4d0dbcb18afbaa7d0003dce3977d838f1c01956bc7d7b087e9749c79c53d92_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list__batch.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    <input type=\"checkbox\" name=\"idx[]\" value=\"{{ admin.id(object) }}\">
{% endblock %}
", "SonataAdminBundle:CRUD:list__batch.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list__batch.html.twig");
    }
}
