<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_e71db588431c232e60a49f34fb369731fbfdde9755f02615c9e6eabfaca44506 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7731b3a72c47b27ede860357752f7f39a5e5d9c16b4a6a5235c06a847eae0b98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7731b3a72c47b27ede860357752f7f39a5e5d9c16b4a6a5235c06a847eae0b98->enter($__internal_7731b3a72c47b27ede860357752f7f39a5e5d9c16b4a6a5235c06a847eae0b98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_7629b8589a4276e9eb22640546ef02a0433e604fdda8504dfae68e51c38e34d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7629b8589a4276e9eb22640546ef02a0433e604fdda8504dfae68e51c38e34d9->enter($__internal_7629b8589a4276e9eb22640546ef02a0433e604fdda8504dfae68e51c38e34d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_7731b3a72c47b27ede860357752f7f39a5e5d9c16b4a6a5235c06a847eae0b98->leave($__internal_7731b3a72c47b27ede860357752f7f39a5e5d9c16b4a6a5235c06a847eae0b98_prof);

        
        $__internal_7629b8589a4276e9eb22640546ef02a0433e604fdda8504dfae68e51c38e34d9->leave($__internal_7629b8589a4276e9eb22640546ef02a0433e604fdda8504dfae68e51c38e34d9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
