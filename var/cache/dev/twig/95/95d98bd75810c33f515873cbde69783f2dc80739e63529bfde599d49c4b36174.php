<?php

/* SonataAdminBundle:CRUD:edit_integer.html.twig */
class __TwigTemplate_2c8627ccf530bc8b51a17d7153ba8344c09cd2e726ef1ef7488116d6eed5dee6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) || array_key_exists("base_template", $context) ? $context["base_template"] : (function () { throw new Twig_Error_Runtime('Variable "base_template" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:edit_integer.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7fa6e55707035340863cbf1b9d84c2770bf4fdd7d6b276401570156693c508a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7fa6e55707035340863cbf1b9d84c2770bf4fdd7d6b276401570156693c508a3->enter($__internal_7fa6e55707035340863cbf1b9d84c2770bf4fdd7d6b276401570156693c508a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_integer.html.twig"));

        $__internal_89ce333ccda631dbae3130f63e8729b7bc8e2afea3508139b90710d690be7a41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_89ce333ccda631dbae3130f63e8729b7bc8e2afea3508139b90710d690be7a41->enter($__internal_89ce333ccda631dbae3130f63e8729b7bc8e2afea3508139b90710d690be7a41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_integer.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7fa6e55707035340863cbf1b9d84c2770bf4fdd7d6b276401570156693c508a3->leave($__internal_7fa6e55707035340863cbf1b9d84c2770bf4fdd7d6b276401570156693c508a3_prof);

        
        $__internal_89ce333ccda631dbae3130f63e8729b7bc8e2afea3508139b90710d690be7a41->leave($__internal_89ce333ccda631dbae3130f63e8729b7bc8e2afea3508139b90710d690be7a41_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_69af7fbfe022c816710c00500d511bea11fc86d656edda10fa0a5f5783dfac88 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69af7fbfe022c816710c00500d511bea11fc86d656edda10fa0a5f5783dfac88->enter($__internal_69af7fbfe022c816710c00500d511bea11fc86d656edda10fa0a5f5783dfac88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_b7d3ac03663c4ce44651b61589690f15a2db44d5615416b4eb95966d95f4a38b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b7d3ac03663c4ce44651b61589690f15a2db44d5615416b4eb95966d95f4a38b->enter($__internal_b7d3ac03663c4ce44651b61589690f15a2db44d5615416b4eb95966d95f4a38b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 14, $this->getSourceContext()); })()), 'widget', array("attr" => array("class" => "title")));
        
        $__internal_b7d3ac03663c4ce44651b61589690f15a2db44d5615416b4eb95966d95f4a38b->leave($__internal_b7d3ac03663c4ce44651b61589690f15a2db44d5615416b4eb95966d95f4a38b_prof);

        
        $__internal_69af7fbfe022c816710c00500d511bea11fc86d656edda10fa0a5f5783dfac88->leave($__internal_69af7fbfe022c816710c00500d511bea11fc86d656edda10fa0a5f5783dfac88_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_integer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block field %}{{ form_widget(field_element, {'attr': {'class' : 'title'}}) }}{% endblock %}
", "SonataAdminBundle:CRUD:edit_integer.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/edit_integer.html.twig");
    }
}
