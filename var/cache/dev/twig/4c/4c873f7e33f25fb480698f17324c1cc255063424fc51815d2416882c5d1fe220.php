<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_454591f6f0c023737213e4c1cdcd7891513e3e0b47cc504acb65f5f6827994f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1f46b9e121d004cd6c6f392ec633a90d494a14822cfd94b4da648632a47e5077 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1f46b9e121d004cd6c6f392ec633a90d494a14822cfd94b4da648632a47e5077->enter($__internal_1f46b9e121d004cd6c6f392ec633a90d494a14822cfd94b4da648632a47e5077_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $__internal_34228eef13b7e96e9d63588175559b4fccc0c4d04b469de376b169853ed3880d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_34228eef13b7e96e9d63588175559b4fccc0c4d04b469de376b169853ed3880d->enter($__internal_34228eef13b7e96e9d63588175559b4fccc0c4d04b469de376b169853ed3880d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1f46b9e121d004cd6c6f392ec633a90d494a14822cfd94b4da648632a47e5077->leave($__internal_1f46b9e121d004cd6c6f392ec633a90d494a14822cfd94b4da648632a47e5077_prof);

        
        $__internal_34228eef13b7e96e9d63588175559b4fccc0c4d04b469de376b169853ed3880d->leave($__internal_34228eef13b7e96e9d63588175559b4fccc0c4d04b469de376b169853ed3880d_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_64ae54f2a9cefb526844795bf1dc8e31273300274845f107c0fa5402ff607e78 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64ae54f2a9cefb526844795bf1dc8e31273300274845f107c0fa5402ff607e78->enter($__internal_64ae54f2a9cefb526844795bf1dc8e31273300274845f107c0fa5402ff607e78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_bcfed384cf6f08ce31b2520f9de57b64c951705a0cd5a9fe0ee00f75fa162842 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcfed384cf6f08ce31b2520f9de57b64c951705a0cd5a9fe0ee00f75fa162842->enter($__internal_bcfed384cf6f08ce31b2520f9de57b64c951705a0cd5a9fe0ee00f75fa162842_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 6, $this->getSourceContext()); })()), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_bcfed384cf6f08ce31b2520f9de57b64c951705a0cd5a9fe0ee00f75fa162842->leave($__internal_bcfed384cf6f08ce31b2520f9de57b64c951705a0cd5a9fe0ee00f75fa162842_prof);

        
        $__internal_64ae54f2a9cefb526844795bf1dc8e31273300274845f107c0fa5402ff607e78->leave($__internal_64ae54f2a9cefb526844795bf1dc8e31273300274845f107c0fa5402ff607e78_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:check_email.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}
