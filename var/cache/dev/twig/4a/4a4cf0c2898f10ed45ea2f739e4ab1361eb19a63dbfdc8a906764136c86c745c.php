<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_6afd1e83b3041f1fc0442f0be7d5dcd3000d11e66e0d99c3b308e0d67a27f1a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7eb87cad2732493efd901d97d05951d2b7b776f58f78aaeba25fb8d5bf12935f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7eb87cad2732493efd901d97d05951d2b7b776f58f78aaeba25fb8d5bf12935f->enter($__internal_7eb87cad2732493efd901d97d05951d2b7b776f58f78aaeba25fb8d5bf12935f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_bf8d2d92ca09798234d91a7204f99ce622358608a0d4afc43e663449fcb168ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf8d2d92ca09798234d91a7204f99ce622358608a0d4afc43e663449fcb168ac->enter($__internal_bf8d2d92ca09798234d91a7204f99ce622358608a0d4afc43e663449fcb168ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_7eb87cad2732493efd901d97d05951d2b7b776f58f78aaeba25fb8d5bf12935f->leave($__internal_7eb87cad2732493efd901d97d05951d2b7b776f58f78aaeba25fb8d5bf12935f_prof);

        
        $__internal_bf8d2d92ca09798234d91a7204f99ce622358608a0d4afc43e663449fcb168ac->leave($__internal_bf8d2d92ca09798234d91a7204f99ce622358608a0d4afc43e663449fcb168ac_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
