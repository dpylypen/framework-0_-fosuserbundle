<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_d2262a9a21750216ce5d12bb953547113ee71219ed1ddb580c9ac24955536e00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ec22d72d6c78643a4a8c18d16cc75d632984a52c0d5d636b5563155459e40ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ec22d72d6c78643a4a8c18d16cc75d632984a52c0d5d636b5563155459e40ac->enter($__internal_5ec22d72d6c78643a4a8c18d16cc75d632984a52c0d5d636b5563155459e40ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_1ad955d00389eae9ef81083de258396b9cdc41a4819ea36db84fc254c6dcf09a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ad955d00389eae9ef81083de258396b9cdc41a4819ea36db84fc254c6dcf09a->enter($__internal_1ad955d00389eae9ef81083de258396b9cdc41a4819ea36db84fc254c6dcf09a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_5ec22d72d6c78643a4a8c18d16cc75d632984a52c0d5d636b5563155459e40ac->leave($__internal_5ec22d72d6c78643a4a8c18d16cc75d632984a52c0d5d636b5563155459e40ac_prof);

        
        $__internal_1ad955d00389eae9ef81083de258396b9cdc41a4819ea36db84fc254c6dcf09a->leave($__internal_1ad955d00389eae9ef81083de258396b9cdc41a4819ea36db84fc254c6dcf09a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}
