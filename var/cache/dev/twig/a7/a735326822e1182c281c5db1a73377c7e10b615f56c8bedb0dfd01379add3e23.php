<?php

/* SonataAdminBundle:CRUD:show_currency.html.twig */
class __TwigTemplate_ba395e14034a50da6982c2bc2cfb2a8c49e36968ec73424676557d2540bb3ed3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_currency.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4fd1809232e4aad8cc4cc098d759fb428d667c030ef208f6d188a663bad6872f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4fd1809232e4aad8cc4cc098d759fb428d667c030ef208f6d188a663bad6872f->enter($__internal_4fd1809232e4aad8cc4cc098d759fb428d667c030ef208f6d188a663bad6872f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_currency.html.twig"));

        $__internal_18e262298cb4e8b7f0140966e39c6459c298cc716cb476b5fd7e2bad554c9ad6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18e262298cb4e8b7f0140966e39c6459c298cc716cb476b5fd7e2bad554c9ad6->enter($__internal_18e262298cb4e8b7f0140966e39c6459c298cc716cb476b5fd7e2bad554c9ad6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_currency.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4fd1809232e4aad8cc4cc098d759fb428d667c030ef208f6d188a663bad6872f->leave($__internal_4fd1809232e4aad8cc4cc098d759fb428d667c030ef208f6d188a663bad6872f_prof);

        
        $__internal_18e262298cb4e8b7f0140966e39c6459c298cc716cb476b5fd7e2bad554c9ad6->leave($__internal_18e262298cb4e8b7f0140966e39c6459c298cc716cb476b5fd7e2bad554c9ad6_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_bb07c9418cf37121a3f2e348ef7f8aef984cfd48b1ab2afc62ade234990b9978 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb07c9418cf37121a3f2e348ef7f8aef984cfd48b1ab2afc62ade234990b9978->enter($__internal_bb07c9418cf37121a3f2e348ef7f8aef984cfd48b1ab2afc62ade234990b9978_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_01eec8ee94b2b943d91b0ae07de6e40f118038f52e37c501f45c56f3a8a1fb00 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01eec8ee94b2b943d91b0ae07de6e40f118038f52e37c501f45c56f3a8a1fb00->enter($__internal_01eec8ee94b2b943d91b0ae07de6e40f118038f52e37c501f45c56f3a8a1fb00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        if ( !(null === (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "        ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "currency", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
            echo "
    ";
        }
        
        $__internal_01eec8ee94b2b943d91b0ae07de6e40f118038f52e37c501f45c56f3a8a1fb00->leave($__internal_01eec8ee94b2b943d91b0ae07de6e40f118038f52e37c501f45c56f3a8a1fb00_prof);

        
        $__internal_bb07c9418cf37121a3f2e348ef7f8aef984cfd48b1ab2afc62ade234990b9978->leave($__internal_bb07c9418cf37121a3f2e348ef7f8aef984cfd48b1ab2afc62ade234990b9978_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_currency.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field %}
    {% if value is not null %}
        {{ field_description.options.currency }} {{ value }}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:show_currency.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/show_currency.html.twig");
    }
}
