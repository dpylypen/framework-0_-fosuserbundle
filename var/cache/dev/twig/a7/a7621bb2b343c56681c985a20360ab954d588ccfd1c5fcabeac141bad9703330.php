<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_a589d85734448bd4c782da8cf92f86aa96aec5b83dfabcbb4a9beffbc532c995 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7092b00ea87a8a7677f7c26b04ddd02dfbb7deb4f29e236d2e6772ea26688baf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7092b00ea87a8a7677f7c26b04ddd02dfbb7deb4f29e236d2e6772ea26688baf->enter($__internal_7092b00ea87a8a7677f7c26b04ddd02dfbb7deb4f29e236d2e6772ea26688baf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $__internal_876b46b456724bc1fea7c252c6e961d8cbac9fbe8ec34d57535240cf61ba626f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_876b46b456724bc1fea7c252c6e961d8cbac9fbe8ec34d57535240cf61ba626f->enter($__internal_876b46b456724bc1fea7c252c6e961d8cbac9fbe8ec34d57535240cf61ba626f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7092b00ea87a8a7677f7c26b04ddd02dfbb7deb4f29e236d2e6772ea26688baf->leave($__internal_7092b00ea87a8a7677f7c26b04ddd02dfbb7deb4f29e236d2e6772ea26688baf_prof);

        
        $__internal_876b46b456724bc1fea7c252c6e961d8cbac9fbe8ec34d57535240cf61ba626f->leave($__internal_876b46b456724bc1fea7c252c6e961d8cbac9fbe8ec34d57535240cf61ba626f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_399194352ea40d5991d1ad331866fd48e5758bb42bd9a7b3077d292243453d58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_399194352ea40d5991d1ad331866fd48e5758bb42bd9a7b3077d292243453d58->enter($__internal_399194352ea40d5991d1ad331866fd48e5758bb42bd9a7b3077d292243453d58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_aaf75a75089256d0e744ea77aecc43225e177b67be52f50102b6033a80a63dd1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aaf75a75089256d0e744ea77aecc43225e177b67be52f50102b6033a80a63dd1->enter($__internal_aaf75a75089256d0e744ea77aecc43225e177b67be52f50102b6033a80a63dd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_aaf75a75089256d0e744ea77aecc43225e177b67be52f50102b6033a80a63dd1->leave($__internal_aaf75a75089256d0e744ea77aecc43225e177b67be52f50102b6033a80a63dd1_prof);

        
        $__internal_399194352ea40d5991d1ad331866fd48e5758bb42bd9a7b3077d292243453d58->leave($__internal_399194352ea40d5991d1ad331866fd48e5758bb42bd9a7b3077d292243453d58_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:register.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
