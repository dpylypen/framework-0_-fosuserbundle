<?php

/* SonataAdminBundle:CRUD:base_filter_field.html.twig */
class __TwigTemplate_3ff271ac0040d9a40fd275c70a19b24c571f0fd0e4a70029986fe8504e2d87c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'label' => array($this, 'block_label'),
            'field' => array($this, 'block_field'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e32065f5b65ffd18cb3cced41e69ed20012a961159bc97aaf6b8834602840e0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e32065f5b65ffd18cb3cced41e69ed20012a961159bc97aaf6b8834602840e0d->enter($__internal_e32065f5b65ffd18cb3cced41e69ed20012a961159bc97aaf6b8834602840e0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_filter_field.html.twig"));

        $__internal_3f9737cb590883255d0e16cf4e7ccddcb9cfbc0c1a160ce4fa0f021af3c81470 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f9737cb590883255d0e16cf4e7ccddcb9cfbc0c1a160ce4fa0f021af3c81470->enter($__internal_3f9737cb590883255d0e16cf4e7ccddcb9cfbc0c1a160ce4fa0f021af3c81470_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_filter_field.html.twig"));

        // line 11
        echo "

<div>
    ";
        // line 14
        $this->displayBlock('label', $context, $blocks);
        // line 22
        echo "
    <div class=\"sonata-ba-field";
        // line 23
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["filter_form"]) || array_key_exists("filter_form", $context) ? $context["filter_form"] : (function () { throw new Twig_Error_Runtime('Variable "filter_form" does not exist.', 23, $this->getSourceContext()); })()), "vars", array()), "errors", array())) {
            echo " sonata-ba-field-error";
        }
        echo "\">
        ";
        // line 24
        $this->displayBlock('field', $context, $blocks);
        // line 25
        echo "    </div>
</div>
";
        
        $__internal_e32065f5b65ffd18cb3cced41e69ed20012a961159bc97aaf6b8834602840e0d->leave($__internal_e32065f5b65ffd18cb3cced41e69ed20012a961159bc97aaf6b8834602840e0d_prof);

        
        $__internal_3f9737cb590883255d0e16cf4e7ccddcb9cfbc0c1a160ce4fa0f021af3c81470->leave($__internal_3f9737cb590883255d0e16cf4e7ccddcb9cfbc0c1a160ce4fa0f021af3c81470_prof);

    }

    // line 14
    public function block_label($context, array $blocks = array())
    {
        $__internal_1d128eaf6015cf8972a34b43c2a11b6f463a84b56ff5767e3603c7ba82bf3e35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d128eaf6015cf8972a34b43c2a11b6f463a84b56ff5767e3603c7ba82bf3e35->enter($__internal_1d128eaf6015cf8972a34b43c2a11b6f463a84b56ff5767e3603c7ba82bf3e35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_e92773b47c2239099b48364518f45a2f761d5f3a26462fce170a5f938ab478e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e92773b47c2239099b48364518f45a2f761d5f3a26462fce170a5f938ab478e2->enter($__internal_e92773b47c2239099b48364518f45a2f761d5f3a26462fce170a5f938ab478e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        // line 15
        echo "        ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["filter"] ?? null), "fielddescription", array(), "any", false, true), "options", array(), "any", false, true), "name", array(), "any", true, true)) {
            // line 16
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["filter_form"]) || array_key_exists("filter_form", $context) ? $context["filter_form"] : (function () { throw new Twig_Error_Runtime('Variable "filter_form" does not exist.', 16, $this->getSourceContext()); })()), 'label', (twig_test_empty($_label_ = twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["filter"]) || array_key_exists("filter", $context) ? $context["filter"] : (function () { throw new Twig_Error_Runtime('Variable "filter" does not exist.', 16, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "name", array())) ? array() : array("label" => $_label_)));
            echo "
        ";
        } else {
            // line 18
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["filter_form"]) || array_key_exists("filter_form", $context) ? $context["filter_form"] : (function () { throw new Twig_Error_Runtime('Variable "filter_form" does not exist.', 18, $this->getSourceContext()); })()), 'label');
            echo "
        ";
        }
        // line 20
        echo "        <br>
    ";
        
        $__internal_e92773b47c2239099b48364518f45a2f761d5f3a26462fce170a5f938ab478e2->leave($__internal_e92773b47c2239099b48364518f45a2f761d5f3a26462fce170a5f938ab478e2_prof);

        
        $__internal_1d128eaf6015cf8972a34b43c2a11b6f463a84b56ff5767e3603c7ba82bf3e35->leave($__internal_1d128eaf6015cf8972a34b43c2a11b6f463a84b56ff5767e3603c7ba82bf3e35_prof);

    }

    // line 24
    public function block_field($context, array $blocks = array())
    {
        $__internal_90c4a4da92a91ab0cd509db196a94c4083e02d656ce17603ce9b46a83eb08e81 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_90c4a4da92a91ab0cd509db196a94c4083e02d656ce17603ce9b46a83eb08e81->enter($__internal_90c4a4da92a91ab0cd509db196a94c4083e02d656ce17603ce9b46a83eb08e81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_3163e03b4fa3f381117cfc8d9fdbd93f2dee0f51c28cc3049a40ae6de6887b62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3163e03b4fa3f381117cfc8d9fdbd93f2dee0f51c28cc3049a40ae6de6887b62->enter($__internal_3163e03b4fa3f381117cfc8d9fdbd93f2dee0f51c28cc3049a40ae6de6887b62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["filter_form"]) || array_key_exists("filter_form", $context) ? $context["filter_form"] : (function () { throw new Twig_Error_Runtime('Variable "filter_form" does not exist.', 24, $this->getSourceContext()); })()), 'widget');
        
        $__internal_3163e03b4fa3f381117cfc8d9fdbd93f2dee0f51c28cc3049a40ae6de6887b62->leave($__internal_3163e03b4fa3f381117cfc8d9fdbd93f2dee0f51c28cc3049a40ae6de6887b62_prof);

        
        $__internal_90c4a4da92a91ab0cd509db196a94c4083e02d656ce17603ce9b46a83eb08e81->leave($__internal_90c4a4da92a91ab0cd509db196a94c4083e02d656ce17603ce9b46a83eb08e81_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_filter_field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 24,  81 => 20,  75 => 18,  69 => 16,  66 => 15,  57 => 14,  45 => 25,  43 => 24,  37 => 23,  34 => 22,  32 => 14,  27 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}


<div>
    {% block label %}
        {% if filter.fielddescription.options.name is defined %}
            {{ form_label(filter_form, filter.fielddescription.options.name) }}
        {% else %}
            {{ form_label(filter_form) }}
        {% endif %}
        <br>
    {% endblock %}

    <div class=\"sonata-ba-field{% if filter_form.vars.errors %} sonata-ba-field-error{% endif %}\">
        {% block field %}{{ form_widget(filter_form) }}{% endblock %}
    </div>
</div>
", "SonataAdminBundle:CRUD:base_filter_field.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/base_filter_field.html.twig");
    }
}
