<?php

/* WebProfilerBundle:Collector:ajax.html.twig */
class __TwigTemplate_fcf16ac7cc93cf1080405da07d2834f1525aba0debff166ba7dd1154d2d8d750 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d4bfae27aa7263e1b22d16dab743e0afc1e1dcae205c3727f7e9827dc23a0d68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d4bfae27aa7263e1b22d16dab743e0afc1e1dcae205c3727f7e9827dc23a0d68->enter($__internal_d4bfae27aa7263e1b22d16dab743e0afc1e1dcae205c3727f7e9827dc23a0d68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $__internal_50e0e9d2e74c0ef4b72baba0d86ada6bda0b114d7440c83b7915554b233e2e1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_50e0e9d2e74c0ef4b72baba0d86ada6bda0b114d7440c83b7915554b233e2e1c->enter($__internal_50e0e9d2e74c0ef4b72baba0d86ada6bda0b114d7440c83b7915554b233e2e1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d4bfae27aa7263e1b22d16dab743e0afc1e1dcae205c3727f7e9827dc23a0d68->leave($__internal_d4bfae27aa7263e1b22d16dab743e0afc1e1dcae205c3727f7e9827dc23a0d68_prof);

        
        $__internal_50e0e9d2e74c0ef4b72baba0d86ada6bda0b114d7440c83b7915554b233e2e1c->leave($__internal_50e0e9d2e74c0ef4b72baba0d86ada6bda0b114d7440c83b7915554b233e2e1c_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_19d1f7bc8c951a91b26f5118aa08d8374bb1415388b5afbff3f8bed139618fc0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19d1f7bc8c951a91b26f5118aa08d8374bb1415388b5afbff3f8bed139618fc0->enter($__internal_19d1f7bc8c951a91b26f5118aa08d8374bb1415388b5afbff3f8bed139618fc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_540a5790be5bacc36ca364dae80079d5826ca6b3d6906f38d54b267fd1532491 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_540a5790be5bacc36ca364dae80079d5826ca6b3d6906f38d54b267fd1532491->enter($__internal_540a5790be5bacc36ca364dae80079d5826ca6b3d6906f38d54b267fd1532491_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_540a5790be5bacc36ca364dae80079d5826ca6b3d6906f38d54b267fd1532491->leave($__internal_540a5790be5bacc36ca364dae80079d5826ca6b3d6906f38d54b267fd1532491_prof);

        
        $__internal_19d1f7bc8c951a91b26f5118aa08d8374bb1415388b5afbff3f8bed139618fc0->leave($__internal_19d1f7bc8c951a91b26f5118aa08d8374bb1415388b5afbff3f8bed139618fc0_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "WebProfilerBundle:Collector:ajax.html.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
