<?php

/* SonataBlockBundle:Block:block_exception.html.twig */
class __TwigTemplate_3c154b180fd4efdc92c4de08c53ca2ced141c935e5a145673a4302594c3cc801 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["sonata_block"]) || array_key_exists("sonata_block", $context) ? $context["sonata_block"] : (function () { throw new Twig_Error_Runtime('Variable "sonata_block" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_exception.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f3413f6f5d9bafb0b011a8c63a62562ce0db801f80168f09d1b2e804e0dd9173 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f3413f6f5d9bafb0b011a8c63a62562ce0db801f80168f09d1b2e804e0dd9173->enter($__internal_f3413f6f5d9bafb0b011a8c63a62562ce0db801f80168f09d1b2e804e0dd9173_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_exception.html.twig"));

        $__internal_a14a3925d6e52c071feaf0cbc1fa0abfd062854e93f74478382ae172a7b58a9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a14a3925d6e52c071feaf0cbc1fa0abfd062854e93f74478382ae172a7b58a9f->enter($__internal_a14a3925d6e52c071feaf0cbc1fa0abfd062854e93f74478382ae172a7b58a9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_exception.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f3413f6f5d9bafb0b011a8c63a62562ce0db801f80168f09d1b2e804e0dd9173->leave($__internal_f3413f6f5d9bafb0b011a8c63a62562ce0db801f80168f09d1b2e804e0dd9173_prof);

        
        $__internal_a14a3925d6e52c071feaf0cbc1fa0abfd062854e93f74478382ae172a7b58a9f->leave($__internal_a14a3925d6e52c071feaf0cbc1fa0abfd062854e93f74478382ae172a7b58a9f_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_cc727b11fb5cc327da929cc66854c4653c2b56513fa81fe74eb3db117eacf714 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc727b11fb5cc327da929cc66854c4653c2b56513fa81fe74eb3db117eacf714->enter($__internal_cc727b11fb5cc327da929cc66854c4653c2b56513fa81fe74eb3db117eacf714_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_02ee64761ee53dd955a50b64f72447f7facec6622af3f178eab37a0de5660a11 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_02ee64761ee53dd955a50b64f72447f7facec6622af3f178eab37a0de5660a11->enter($__internal_02ee64761ee53dd955a50b64f72447f7facec6622af3f178eab37a0de5660a11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <div class=\"cms-block-exception\">
        <h2>";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["block"]) || array_key_exists("block", $context) ? $context["block"] : (function () { throw new Twig_Error_Runtime('Variable "block" does not exist.', 16, $this->getSourceContext()); })()), "name", array()), "html", null, true);
        echo "</h2>
        <h3>";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 17, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo "</h3>
    </div>
";
        
        $__internal_02ee64761ee53dd955a50b64f72447f7facec6622af3f178eab37a0de5660a11->leave($__internal_02ee64761ee53dd955a50b64f72447f7facec6622af3f178eab37a0de5660a11_prof);

        
        $__internal_cc727b11fb5cc327da929cc66854c4653c2b56513fa81fe74eb3db117eacf714->leave($__internal_cc727b11fb5cc327da929cc66854c4653c2b56513fa81fe74eb3db117eacf714_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    <div class=\"cms-block-exception\">
        <h2>{{ block.name }}</h2>
        <h3>{{ exception.message }}</h3>
    </div>
{% endblock %}
", "SonataBlockBundle:Block:block_exception.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/block-bundle/Resources/views/Block/block_exception.html.twig");
    }
}
