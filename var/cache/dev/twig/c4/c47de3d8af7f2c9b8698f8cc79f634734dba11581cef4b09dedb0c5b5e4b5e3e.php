<?php

/* :home:home.html.twig */
class __TwigTemplate_07e1873481a19b8c8a2f6069ad9860775f1a006d0a50e82eacfc7c60922c5f32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("default/index.html.twig", ":home:home.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_646ecc8903f3609069977e09ba2f1e585a85cde4f9618206760c142c28011e22 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_646ecc8903f3609069977e09ba2f1e585a85cde4f9618206760c142c28011e22->enter($__internal_646ecc8903f3609069977e09ba2f1e585a85cde4f9618206760c142c28011e22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":home:home.html.twig"));

        $__internal_8046fb946b697a66b7e1dd6de46eccb3f86253f3d4aa6756a8d090b07e25eae0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8046fb946b697a66b7e1dd6de46eccb3f86253f3d4aa6756a8d090b07e25eae0->enter($__internal_8046fb946b697a66b7e1dd6de46eccb3f86253f3d4aa6756a8d090b07e25eae0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":home:home.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_646ecc8903f3609069977e09ba2f1e585a85cde4f9618206760c142c28011e22->leave($__internal_646ecc8903f3609069977e09ba2f1e585a85cde4f9618206760c142c28011e22_prof);

        
        $__internal_8046fb946b697a66b7e1dd6de46eccb3f86253f3d4aa6756a8d090b07e25eae0->leave($__internal_8046fb946b697a66b7e1dd6de46eccb3f86253f3d4aa6756a8d090b07e25eae0_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_e837f8904b8521d0b52902f9929ae8a2d987e2f8925d47c92555be62fc222160 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e837f8904b8521d0b52902f9929ae8a2d987e2f8925d47c92555be62fc222160->enter($__internal_e837f8904b8521d0b52902f9929ae8a2d987e2f8925d47c92555be62fc222160_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_b5caef572609e901ad2c81404be762645f891c768dc1edf8997bbdba3d971143 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5caef572609e901ad2c81404be762645f891c768dc1edf8997bbdba3d971143->enter($__internal_b5caef572609e901ad2c81404be762645f891c768dc1edf8997bbdba3d971143_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    Homepage
";
        
        $__internal_b5caef572609e901ad2c81404be762645f891c768dc1edf8997bbdba3d971143->leave($__internal_b5caef572609e901ad2c81404be762645f891c768dc1edf8997bbdba3d971143_prof);

        
        $__internal_e837f8904b8521d0b52902f9929ae8a2d987e2f8925d47c92555be62fc222160->leave($__internal_e837f8904b8521d0b52902f9929ae8a2d987e2f8925d47c92555be62fc222160_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_7f6dfe7d4609f8bf6feccbd790c01331886efc3e1899dfa9b07f903887bd16e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f6dfe7d4609f8bf6feccbd790c01331886efc3e1899dfa9b07f903887bd16e8->enter($__internal_7f6dfe7d4609f8bf6feccbd790c01331886efc3e1899dfa9b07f903887bd16e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_07afa37865a692aa58469663da1cc6e28fdbc8275d539ee721072bdd45dceb45 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_07afa37865a692aa58469663da1cc6e28fdbc8275d539ee721072bdd45dceb45->enter($__internal_07afa37865a692aa58469663da1cc6e28fdbc8275d539ee721072bdd45dceb45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "    <h1>Homepage</h1>
    <div class=\"homepage_menu\">
        <ul>
            <li><a href=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Home</a></li>
            <li><a href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_show");
        echo "\">Profile</a></li>
            <li><a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
        echo "\">Log in</a></li>
            <li><a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
        echo "\">Registration</a></li>
            <li><a href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_edit");
        echo "\">Edit profile</a></li>
        </ul>
    </div>
";
        
        $__internal_07afa37865a692aa58469663da1cc6e28fdbc8275d539ee721072bdd45dceb45->leave($__internal_07afa37865a692aa58469663da1cc6e28fdbc8275d539ee721072bdd45dceb45_prof);

        
        $__internal_7f6dfe7d4609f8bf6feccbd790c01331886efc3e1899dfa9b07f903887bd16e8->leave($__internal_7f6dfe7d4609f8bf6feccbd790c01331886efc3e1899dfa9b07f903887bd16e8_prof);

    }

    public function getTemplateName()
    {
        return ":home:home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 14,  87 => 13,  83 => 12,  79 => 11,  75 => 10,  70 => 7,  61 => 6,  50 => 4,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'default/index.html.twig' %}

{% block title %}
    Homepage
{% endblock %}
{% block body %}
    <h1>Homepage</h1>
    <div class=\"homepage_menu\">
        <ul>
            <li><a href=\"{{ path('homepage') }}\">Home</a></li>
            <li><a href=\"{{ path('fos_user_profile_show') }}\">Profile</a></li>
            <li><a href=\"{{ path('fos_user_security_login') }}\">Log in</a></li>
            <li><a href=\"{{ path('fos_user_registration_register') }}\">Registration</a></li>
            <li><a href=\"{{ path('fos_user_profile_edit') }}\">Edit profile</a></li>
        </ul>
    </div>
{% endblock %}", ":home:home.html.twig", "/Users/dp/Sites/frame-0/app/Resources/views/home/home.html.twig");
    }
}
