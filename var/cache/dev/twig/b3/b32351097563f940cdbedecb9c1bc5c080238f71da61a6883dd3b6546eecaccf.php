<?php

/* SonataAdminBundle::ajax_layout.html.twig */
class __TwigTemplate_bcdf283ce5bd2933320eff9b5a0f53c78fd1e05c989c725208e6e8a1eaeb559e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'preview' => array($this, 'block_preview'),
            'form' => array($this, 'block_form'),
            'list' => array($this, 'block_list'),
            'show' => array($this, 'block_show'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8a93c20d10b2a5d609ebc306c7985fde0e89cc1b3470daa977f71623da91969 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d8a93c20d10b2a5d609ebc306c7985fde0e89cc1b3470daa977f71623da91969->enter($__internal_d8a93c20d10b2a5d609ebc306c7985fde0e89cc1b3470daa977f71623da91969_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle::ajax_layout.html.twig"));

        $__internal_04fd6f8ec2dc074083fde947e157aaa1514f611804c73a2cc5a726841f6b9332 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04fd6f8ec2dc074083fde947e157aaa1514f611804c73a2cc5a726841f6b9332->enter($__internal_04fd6f8ec2dc074083fde947e157aaa1514f611804c73a2cc5a726841f6b9332_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle::ajax_layout.html.twig"));

        // line 11
        echo "
";
        // line 12
        $this->displayBlock('content', $context, $blocks);
        
        $__internal_d8a93c20d10b2a5d609ebc306c7985fde0e89cc1b3470daa977f71623da91969->leave($__internal_d8a93c20d10b2a5d609ebc306c7985fde0e89cc1b3470daa977f71623da91969_prof);

        
        $__internal_04fd6f8ec2dc074083fde947e157aaa1514f611804c73a2cc5a726841f6b9332->leave($__internal_04fd6f8ec2dc074083fde947e157aaa1514f611804c73a2cc5a726841f6b9332_prof);

    }

    public function block_content($context, array $blocks = array())
    {
        $__internal_3803c25de01f1dc07c46610a07a041442b18bb5a1d398021c647d1a1a119eff4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3803c25de01f1dc07c46610a07a041442b18bb5a1d398021c647d1a1a119eff4->enter($__internal_3803c25de01f1dc07c46610a07a041442b18bb5a1d398021c647d1a1a119eff4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_ed14289e0669520ce51d9421bc9b22447fe3cc8d0b6d1be5a9524f9dcebcb76f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed14289e0669520ce51d9421bc9b22447fe3cc8d0b6d1be5a9524f9dcebcb76f->enter($__internal_ed14289e0669520ce51d9421bc9b22447fe3cc8d0b6d1be5a9524f9dcebcb76f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 13
        echo "    ";
        $context["_list_table"] = ((        $this->hasBlock("list_table", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_table", $context, $blocks))) : (null));
        // line 14
        echo "    ";
        $context["_list_filters"] = ((        $this->hasBlock("list_filters", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_filters", $context, $blocks))) : (null));
        // line 15
        echo "    ";
        $context["_list_filters_actions"] = ((        $this->hasBlock("list_filters_actions", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_filters_actions", $context, $blocks))) : (null));
        // line 16
        echo "
    ";
        // line 17
        $this->displayBlock('preview', $context, $blocks);
        // line 18
        echo "    ";
        $this->displayBlock('form', $context, $blocks);
        // line 19
        echo "    ";
        $this->displayBlock('list', $context, $blocks);
        // line 20
        echo "    ";
        $this->displayBlock('show', $context, $blocks);
        // line 21
        echo "
    ";
        // line 22
        if (( !twig_test_empty((isset($context["_list_table"]) || array_key_exists("_list_table", $context) ? $context["_list_table"] : (function () { throw new Twig_Error_Runtime('Variable "_list_table" does not exist.', 22, $this->getSourceContext()); })())) ||  !twig_test_empty((isset($context["_list_filters"]) || array_key_exists("_list_filters", $context) ? $context["_list_filters"] : (function () { throw new Twig_Error_Runtime('Variable "_list_filters" does not exist.', 22, $this->getSourceContext()); })())))) {
            // line 23
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"navbar navbar-default sonata-list-table\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-collapse\">
                        ";
            // line 28
            if ((((array_key_exists("admin", $context) && array_key_exists("action", $context)) && ((isset($context["action"]) || array_key_exists("action", $context) ? $context["action"] : (function () { throw new Twig_Error_Runtime('Variable "action" does not exist.', 28, $this->getSourceContext()); })()) == "list")) && (twig_length_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 28, $this->getSourceContext()); })()), "listModes", array())) > 1))) {
                // line 29
                echo "                            <div class=\"nav navbar-right btn-group\">
                                ";
                // line 30
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 30, $this->getSourceContext()); })()), "listModes", array()));
                foreach ($context['_seq'] as $context["mode"] => $context["settings"]) {
                    // line 31
                    echo "                                    <a href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 31, $this->getSourceContext()); })()), "generateUrl", array(0 => "list", 1 => twig_array_merge(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 31, $this->getSourceContext()); })()), "request", array()), "query", array()), "all", array()), array("_list_mode" => $context["mode"]))), "method"), "html", null, true);
                    echo "\" class=\"btn btn-default navbar-btn btn-sm";
                    if ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 31, $this->getSourceContext()); })()), "getListMode", array(), "method") == $context["mode"])) {
                        echo " active";
                    }
                    echo "\"><i class=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["settings"], "class", array()), "html", null, true);
                    echo "\"></i></a>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['mode'], $context['settings'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 33
                echo "                            </div>
                        ";
            }
            // line 35
            echo "
                        ";
            // line 36
            if ( !twig_test_empty((isset($context["_list_filters_actions"]) || array_key_exists("_list_filters_actions", $context) ? $context["_list_filters_actions"] : (function () { throw new Twig_Error_Runtime('Variable "_list_filters_actions" does not exist.', 36, $this->getSourceContext()); })()))) {
                // line 37
                echo "                            ";
                echo (isset($context["_list_filters_actions"]) || array_key_exists("_list_filters_actions", $context) ? $context["_list_filters_actions"] : (function () { throw new Twig_Error_Runtime('Variable "_list_filters_actions" does not exist.', 37, $this->getSourceContext()); })());
                echo "
                        ";
            }
            // line 39
            echo "                    </div>
                </div>
            </div>
        </div>

        ";
            // line 44
            if (twig_trim_filter((isset($context["_list_filters"]) || array_key_exists("_list_filters", $context) ? $context["_list_filters"] : (function () { throw new Twig_Error_Runtime('Variable "_list_filters" does not exist.', 44, $this->getSourceContext()); })()))) {
                // line 45
                echo "            <div class=\"row\">
                ";
                // line 46
                echo (isset($context["_list_filters"]) || array_key_exists("_list_filters", $context) ? $context["_list_filters"] : (function () { throw new Twig_Error_Runtime('Variable "_list_filters" does not exist.', 46, $this->getSourceContext()); })());
                echo "
            </div>
        ";
            }
            // line 49
            echo "
        <div class=\"row\">
            ";
            // line 51
            echo (isset($context["_list_table"]) || array_key_exists("_list_table", $context) ? $context["_list_table"] : (function () { throw new Twig_Error_Runtime('Variable "_list_table" does not exist.', 51, $this->getSourceContext()); })());
            echo "
        </div>
    </div>
    ";
        }
        
        $__internal_ed14289e0669520ce51d9421bc9b22447fe3cc8d0b6d1be5a9524f9dcebcb76f->leave($__internal_ed14289e0669520ce51d9421bc9b22447fe3cc8d0b6d1be5a9524f9dcebcb76f_prof);

        
        $__internal_3803c25de01f1dc07c46610a07a041442b18bb5a1d398021c647d1a1a119eff4->leave($__internal_3803c25de01f1dc07c46610a07a041442b18bb5a1d398021c647d1a1a119eff4_prof);

    }

    // line 17
    public function block_preview($context, array $blocks = array())
    {
        $__internal_027338892b1bbb19e7a1c54c6032292e0b33bc0d1f5dcec276e02ed899a278f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_027338892b1bbb19e7a1c54c6032292e0b33bc0d1f5dcec276e02ed899a278f7->enter($__internal_027338892b1bbb19e7a1c54c6032292e0b33bc0d1f5dcec276e02ed899a278f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "preview"));

        $__internal_d92f18f9335d29337307c3b96c39ff09c785ae34e91efc5ba140eb5ae9d26ba9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d92f18f9335d29337307c3b96c39ff09c785ae34e91efc5ba140eb5ae9d26ba9->enter($__internal_d92f18f9335d29337307c3b96c39ff09c785ae34e91efc5ba140eb5ae9d26ba9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "preview"));

        
        $__internal_d92f18f9335d29337307c3b96c39ff09c785ae34e91efc5ba140eb5ae9d26ba9->leave($__internal_d92f18f9335d29337307c3b96c39ff09c785ae34e91efc5ba140eb5ae9d26ba9_prof);

        
        $__internal_027338892b1bbb19e7a1c54c6032292e0b33bc0d1f5dcec276e02ed899a278f7->leave($__internal_027338892b1bbb19e7a1c54c6032292e0b33bc0d1f5dcec276e02ed899a278f7_prof);

    }

    // line 18
    public function block_form($context, array $blocks = array())
    {
        $__internal_5754ffd054975792f758a07b691012f149618a5c6740ac414de43465d663774a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5754ffd054975792f758a07b691012f149618a5c6740ac414de43465d663774a->enter($__internal_5754ffd054975792f758a07b691012f149618a5c6740ac414de43465d663774a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        $__internal_3a5ab5e75837f86409f06cb26d618be13d97f16f9820a02a94a5732c1960bbdd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a5ab5e75837f86409f06cb26d618be13d97f16f9820a02a94a5732c1960bbdd->enter($__internal_3a5ab5e75837f86409f06cb26d618be13d97f16f9820a02a94a5732c1960bbdd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        
        $__internal_3a5ab5e75837f86409f06cb26d618be13d97f16f9820a02a94a5732c1960bbdd->leave($__internal_3a5ab5e75837f86409f06cb26d618be13d97f16f9820a02a94a5732c1960bbdd_prof);

        
        $__internal_5754ffd054975792f758a07b691012f149618a5c6740ac414de43465d663774a->leave($__internal_5754ffd054975792f758a07b691012f149618a5c6740ac414de43465d663774a_prof);

    }

    // line 19
    public function block_list($context, array $blocks = array())
    {
        $__internal_a8890fd0d40df3d60ff92e37f71fd5f739825dd20ed30bcb00cdcd3958b5e263 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8890fd0d40df3d60ff92e37f71fd5f739825dd20ed30bcb00cdcd3958b5e263->enter($__internal_a8890fd0d40df3d60ff92e37f71fd5f739825dd20ed30bcb00cdcd3958b5e263_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "list"));

        $__internal_33b1adbe19e31856bba0dcacc82e66dd992d0573ae460d9c575af63127649e55 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33b1adbe19e31856bba0dcacc82e66dd992d0573ae460d9c575af63127649e55->enter($__internal_33b1adbe19e31856bba0dcacc82e66dd992d0573ae460d9c575af63127649e55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "list"));

        
        $__internal_33b1adbe19e31856bba0dcacc82e66dd992d0573ae460d9c575af63127649e55->leave($__internal_33b1adbe19e31856bba0dcacc82e66dd992d0573ae460d9c575af63127649e55_prof);

        
        $__internal_a8890fd0d40df3d60ff92e37f71fd5f739825dd20ed30bcb00cdcd3958b5e263->leave($__internal_a8890fd0d40df3d60ff92e37f71fd5f739825dd20ed30bcb00cdcd3958b5e263_prof);

    }

    // line 20
    public function block_show($context, array $blocks = array())
    {
        $__internal_858fb29a86bac30654b0fd66a19b5489bfa5e0a009487cb262ff16a606ff37fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_858fb29a86bac30654b0fd66a19b5489bfa5e0a009487cb262ff16a606ff37fa->enter($__internal_858fb29a86bac30654b0fd66a19b5489bfa5e0a009487cb262ff16a606ff37fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show"));

        $__internal_d368253314426838106d0afc2fab9c6fb31463ee4b2a76de6f2152d5a8523230 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d368253314426838106d0afc2fab9c6fb31463ee4b2a76de6f2152d5a8523230->enter($__internal_d368253314426838106d0afc2fab9c6fb31463ee4b2a76de6f2152d5a8523230_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show"));

        
        $__internal_d368253314426838106d0afc2fab9c6fb31463ee4b2a76de6f2152d5a8523230->leave($__internal_d368253314426838106d0afc2fab9c6fb31463ee4b2a76de6f2152d5a8523230_prof);

        
        $__internal_858fb29a86bac30654b0fd66a19b5489bfa5e0a009487cb262ff16a606ff37fa->leave($__internal_858fb29a86bac30654b0fd66a19b5489bfa5e0a009487cb262ff16a606ff37fa_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle::ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  213 => 20,  196 => 19,  179 => 18,  162 => 17,  147 => 51,  143 => 49,  137 => 46,  134 => 45,  132 => 44,  125 => 39,  119 => 37,  117 => 36,  114 => 35,  110 => 33,  95 => 31,  91 => 30,  88 => 29,  86 => 28,  79 => 23,  77 => 22,  74 => 21,  71 => 20,  68 => 19,  65 => 18,  63 => 17,  60 => 16,  57 => 15,  54 => 14,  51 => 13,  33 => 12,  30 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% block content %}
    {% set _list_table = block('list_table') is defined ? block('list_table')|trim : null %}
    {% set _list_filters = block('list_filters') is defined ? block('list_filters')|trim : null %}
    {% set _list_filters_actions = block('list_filters_actions') is defined ? block('list_filters_actions')|trim : null %}

    {% block preview %}{% endblock %}
    {% block form %}{% endblock %}
    {% block list %}{% endblock %}
    {% block show %}{% endblock %}

    {% if _list_table is not empty or _list_filters is not empty %}
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"navbar navbar-default sonata-list-table\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-collapse\">
                        {% if admin is defined and action is defined and action == 'list' and admin.listModes|length > 1 %}
                            <div class=\"nav navbar-right btn-group\">
                                {% for mode, settings in admin.listModes %}
                                    <a href=\"{{ admin.generateUrl('list', app.request.query.all|merge({_list_mode: mode})) }}\" class=\"btn btn-default navbar-btn btn-sm{% if admin.getListMode() == mode %} active{% endif %}\"><i class=\"{{ settings.class }}\"></i></a>
                                {% endfor %}
                            </div>
                        {% endif %}

                        {% if _list_filters_actions is not empty %}
                            {{ _list_filters_actions|raw }}
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>

        {% if _list_filters|trim %}
            <div class=\"row\">
                {{ _list_filters|raw }}
            </div>
        {% endif %}

        <div class=\"row\">
            {{ _list_table|raw }}
        </div>
    </div>
    {% endif %}
{% endblock %}
", "SonataAdminBundle::ajax_layout.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/ajax_layout.html.twig");
    }
}
