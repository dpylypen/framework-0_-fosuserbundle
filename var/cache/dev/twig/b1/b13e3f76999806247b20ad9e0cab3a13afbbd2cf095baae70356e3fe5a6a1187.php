<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_864c822154c1ce7b256f2f1636bace484329500baee06eb4f1a5700483052626 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46ea9df681999e688a3256c210722d33da89cc909cf5c9bef2dfcfefc6d7c058 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_46ea9df681999e688a3256c210722d33da89cc909cf5c9bef2dfcfefc6d7c058->enter($__internal_46ea9df681999e688a3256c210722d33da89cc909cf5c9bef2dfcfefc6d7c058_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $__internal_8e033bb9cea7fdfcb096c66924b6f95dcbb57f1acd16cd20d4886dd860cbc2fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e033bb9cea7fdfcb096c66924b6f95dcbb57f1acd16cd20d4886dd860cbc2fb->enter($__internal_8e033bb9cea7fdfcb096c66924b6f95dcbb57f1acd16cd20d4886dd860cbc2fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })())));
        echo "
";
        
        $__internal_46ea9df681999e688a3256c210722d33da89cc909cf5c9bef2dfcfefc6d7c058->leave($__internal_46ea9df681999e688a3256c210722d33da89cc909cf5c9bef2dfcfefc6d7c058_prof);

        
        $__internal_8e033bb9cea7fdfcb096c66924b6f95dcbb57f1acd16cd20d4886dd860cbc2fb->leave($__internal_8e033bb9cea7fdfcb096c66924b6f95dcbb57f1acd16cd20d4886dd860cbc2fb_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.atom.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}
