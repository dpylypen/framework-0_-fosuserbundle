<?php

/* SonataAdminBundle:CRUD:edit_string.html.twig */
class __TwigTemplate_740be910978a1ec4aeb03c3b67424f06dd3734b94c3ff0370c4e12ba5fac090a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) || array_key_exists("base_template", $context) ? $context["base_template"] : (function () { throw new Twig_Error_Runtime('Variable "base_template" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:edit_string.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_484b7186df8f0b3b5635289770a05220818752d76caa4cbeb0daf6ee11417eca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_484b7186df8f0b3b5635289770a05220818752d76caa4cbeb0daf6ee11417eca->enter($__internal_484b7186df8f0b3b5635289770a05220818752d76caa4cbeb0daf6ee11417eca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_string.html.twig"));

        $__internal_b17c276a14c4b6ccef0763114fdb5a0c86f0d031b04c4ac36f9a85cab0e14195 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b17c276a14c4b6ccef0763114fdb5a0c86f0d031b04c4ac36f9a85cab0e14195->enter($__internal_b17c276a14c4b6ccef0763114fdb5a0c86f0d031b04c4ac36f9a85cab0e14195_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_string.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_484b7186df8f0b3b5635289770a05220818752d76caa4cbeb0daf6ee11417eca->leave($__internal_484b7186df8f0b3b5635289770a05220818752d76caa4cbeb0daf6ee11417eca_prof);

        
        $__internal_b17c276a14c4b6ccef0763114fdb5a0c86f0d031b04c4ac36f9a85cab0e14195->leave($__internal_b17c276a14c4b6ccef0763114fdb5a0c86f0d031b04c4ac36f9a85cab0e14195_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_0a48dc7c391364e0ad9710faf90c74410ab324f9f55c8c574b483fb74c5c89c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a48dc7c391364e0ad9710faf90c74410ab324f9f55c8c574b483fb74c5c89c7->enter($__internal_0a48dc7c391364e0ad9710faf90c74410ab324f9f55c8c574b483fb74c5c89c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_fe6b900a73e081543e77066c933e90c83110ed0b0acf79a93dfecb5d8af05138 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe6b900a73e081543e77066c933e90c83110ed0b0acf79a93dfecb5d8af05138->enter($__internal_fe6b900a73e081543e77066c933e90c83110ed0b0acf79a93dfecb5d8af05138_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 14, $this->getSourceContext()); })()), 'widget', array("attr" => array("class" => "title")));
        
        $__internal_fe6b900a73e081543e77066c933e90c83110ed0b0acf79a93dfecb5d8af05138->leave($__internal_fe6b900a73e081543e77066c933e90c83110ed0b0acf79a93dfecb5d8af05138_prof);

        
        $__internal_0a48dc7c391364e0ad9710faf90c74410ab324f9f55c8c574b483fb74c5c89c7->leave($__internal_0a48dc7c391364e0ad9710faf90c74410ab324f9f55c8c574b483fb74c5c89c7_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_string.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block field %}{{ form_widget(field_element, {'attr': {'class' : 'title'}}) }}{% endblock %}
", "SonataAdminBundle:CRUD:edit_string.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/edit_string.html.twig");
    }
}
