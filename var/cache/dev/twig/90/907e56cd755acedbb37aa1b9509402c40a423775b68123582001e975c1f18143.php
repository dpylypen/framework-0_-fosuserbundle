<?php

/* SonataAdminBundle:CRUD:edit_file.html.twig */
class __TwigTemplate_9b8d047346a1e42447ef007a72d6f6aae473b466748d869ec43aa6c383d1cc0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) || array_key_exists("base_template", $context) ? $context["base_template"] : (function () { throw new Twig_Error_Runtime('Variable "base_template" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:edit_file.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e7ba0b5dc83929c1d546ccbaf37c95a17858941170fafa89e743bd61ede35b09 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e7ba0b5dc83929c1d546ccbaf37c95a17858941170fafa89e743bd61ede35b09->enter($__internal_e7ba0b5dc83929c1d546ccbaf37c95a17858941170fafa89e743bd61ede35b09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_file.html.twig"));

        $__internal_f73d1fad4fb4cd65169dcd1e96e12f4f97e35b610de6a1024bd306a936947784 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f73d1fad4fb4cd65169dcd1e96e12f4f97e35b610de6a1024bd306a936947784->enter($__internal_f73d1fad4fb4cd65169dcd1e96e12f4f97e35b610de6a1024bd306a936947784_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_file.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e7ba0b5dc83929c1d546ccbaf37c95a17858941170fafa89e743bd61ede35b09->leave($__internal_e7ba0b5dc83929c1d546ccbaf37c95a17858941170fafa89e743bd61ede35b09_prof);

        
        $__internal_f73d1fad4fb4cd65169dcd1e96e12f4f97e35b610de6a1024bd306a936947784->leave($__internal_f73d1fad4fb4cd65169dcd1e96e12f4f97e35b610de6a1024bd306a936947784_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_4f1e6b437c991572c52cf56a09e069d8879393f9941fcc4b1437fb0873ee9949 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f1e6b437c991572c52cf56a09e069d8879393f9941fcc4b1437fb0873ee9949->enter($__internal_4f1e6b437c991572c52cf56a09e069d8879393f9941fcc4b1437fb0873ee9949_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_b4695b3501c277f628d79aa96c2ea07d78e5a0eda00a6c568c917ada95fd4ad6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4695b3501c277f628d79aa96c2ea07d78e5a0eda00a6c568c917ada95fd4ad6->enter($__internal_b4695b3501c277f628d79aa96c2ea07d78e5a0eda00a6c568c917ada95fd4ad6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 14, $this->getSourceContext()); })()), 'widget', array("attr" => array("class" => "title")));
        
        $__internal_b4695b3501c277f628d79aa96c2ea07d78e5a0eda00a6c568c917ada95fd4ad6->leave($__internal_b4695b3501c277f628d79aa96c2ea07d78e5a0eda00a6c568c917ada95fd4ad6_prof);

        
        $__internal_4f1e6b437c991572c52cf56a09e069d8879393f9941fcc4b1437fb0873ee9949->leave($__internal_4f1e6b437c991572c52cf56a09e069d8879393f9941fcc4b1437fb0873ee9949_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_file.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block field %}{{ form_widget(field_element, {'attr': {'class' : 'title'}}) }}{% endblock %}
", "SonataAdminBundle:CRUD:edit_file.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/edit_file.html.twig");
    }
}
