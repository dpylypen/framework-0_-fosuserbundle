<?php

/* @SonataBlock/Profiler/icon.svg */
class __TwigTemplate_988c43e1a52f57989ed570e2c8f001d62484a8e0426cd0a56d6f11b09795e5c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3370fac53d011f70a2bf61b105082c9f6d19f77e9ba85c71df8be9192c469e44 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3370fac53d011f70a2bf61b105082c9f6d19f77e9ba85c71df8be9192c469e44->enter($__internal_3370fac53d011f70a2bf61b105082c9f6d19f77e9ba85c71df8be9192c469e44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Profiler/icon.svg"));

        $__internal_cc29f80aa39724fac5ddce52918ade2229698c257de22a5d9098335072eae23c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cc29f80aa39724fac5ddce52918ade2229698c257de22a5d9098335072eae23c->enter($__internal_cc29f80aa39724fac5ddce52918ade2229698c257de22a5d9098335072eae23c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Profiler/icon.svg"));

        // line 1
        echo "<svg height=\"24\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\">
    <path fill=\"#AAAAAA\" d=\"M832 1024v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm0-768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm896 768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm0-768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90z\"/>
</svg>
";
        
        $__internal_3370fac53d011f70a2bf61b105082c9f6d19f77e9ba85c71df8be9192c469e44->leave($__internal_3370fac53d011f70a2bf61b105082c9f6d19f77e9ba85c71df8be9192c469e44_prof);

        
        $__internal_cc29f80aa39724fac5ddce52918ade2229698c257de22a5d9098335072eae23c->leave($__internal_cc29f80aa39724fac5ddce52918ade2229698c257de22a5d9098335072eae23c_prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Profiler/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg height=\"24\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\">
    <path fill=\"#AAAAAA\" d=\"M832 1024v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm0-768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm896 768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm0-768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90z\"/>
</svg>
", "@SonataBlock/Profiler/icon.svg", "/Users/dp/Sites/frame-0/vendor/sonata-project/block-bundle/Resources/views/Profiler/icon.svg");
    }
}
