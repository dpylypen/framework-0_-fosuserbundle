<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_d2f03c9432a9a485739ef22c9a7a3e5a1cf9993b03fb5a970f2b40d3c5f1f0de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0f37801698087ee7cc75636263146cfb4853768db6eacdb3badeca6611171951 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0f37801698087ee7cc75636263146cfb4853768db6eacdb3badeca6611171951->enter($__internal_0f37801698087ee7cc75636263146cfb4853768db6eacdb3badeca6611171951_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_3570cd3dd7803b1717022ad47e1799bcce56b0e521080a4da5778ef94ee0a236 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3570cd3dd7803b1717022ad47e1799bcce56b0e521080a4da5778ef94ee0a236->enter($__internal_3570cd3dd7803b1717022ad47e1799bcce56b0e521080a4da5778ef94ee0a236_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_0f37801698087ee7cc75636263146cfb4853768db6eacdb3badeca6611171951->leave($__internal_0f37801698087ee7cc75636263146cfb4853768db6eacdb3badeca6611171951_prof);

        
        $__internal_3570cd3dd7803b1717022ad47e1799bcce56b0e521080a4da5778ef94ee0a236->leave($__internal_3570cd3dd7803b1717022ad47e1799bcce56b0e521080a4da5778ef94ee0a236_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget_expanded.html.php");
    }
}
