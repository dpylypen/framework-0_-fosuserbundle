<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_fbdc567b3802d1b9ff4759908bf503074d4f6c3279b337b683b3ec39651452e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6e6d01d22fc810a683c0a26dbc2c0799fc3d21ecc2afab3969fca3ce0d89398b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6e6d01d22fc810a683c0a26dbc2c0799fc3d21ecc2afab3969fca3ce0d89398b->enter($__internal_6e6d01d22fc810a683c0a26dbc2c0799fc3d21ecc2afab3969fca3ce0d89398b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_4fe8995ad5a70a7cd65e1ffaca9481c072c700d4a08998c0a754c108bdd29ff0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4fe8995ad5a70a7cd65e1ffaca9481c072c700d4a08998c0a754c108bdd29ff0->enter($__internal_4fe8995ad5a70a7cd65e1ffaca9481c072c700d4a08998c0a754c108bdd29ff0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_6e6d01d22fc810a683c0a26dbc2c0799fc3d21ecc2afab3969fca3ce0d89398b->leave($__internal_6e6d01d22fc810a683c0a26dbc2c0799fc3d21ecc2afab3969fca3ce0d89398b_prof);

        
        $__internal_4fe8995ad5a70a7cd65e1ffaca9481c072c700d4a08998c0a754c108bdd29ff0->leave($__internal_4fe8995ad5a70a7cd65e1ffaca9481c072c700d4a08998c0a754c108bdd29ff0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
