<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_e6dd6e524107f7ff2baddec710dd2ceef33a465c0ac8e9ce51f0f26d7a4b01e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04b3b5ede37562d198c2922b845b97dad950baa0053c397e0d964df1a6e50838 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_04b3b5ede37562d198c2922b845b97dad950baa0053c397e0d964df1a6e50838->enter($__internal_04b3b5ede37562d198c2922b845b97dad950baa0053c397e0d964df1a6e50838_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_e6884aa6622141ecae624adfa4b60f94305e776015c9f57c67e12ada27ca0575 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e6884aa6622141ecae624adfa4b60f94305e776015c9f57c67e12ada27ca0575->enter($__internal_e6884aa6622141ecae624adfa4b60f94305e776015c9f57c67e12ada27ca0575_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) || array_key_exists("widget", $context) ? $context["widget"] : (function () { throw new Twig_Error_Runtime('Variable "widget" does not exist.', 1, $this->getSourceContext()); })()), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_04b3b5ede37562d198c2922b845b97dad950baa0053c397e0d964df1a6e50838->leave($__internal_04b3b5ede37562d198c2922b845b97dad950baa0053c397e0d964df1a6e50838_prof);

        
        $__internal_e6884aa6622141ecae624adfa4b60f94305e776015c9f57c67e12ada27ca0575->leave($__internal_e6884aa6622141ecae624adfa4b60f94305e776015c9f57c67e12ada27ca0575_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
", "@Framework/Form/money_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/money_widget.html.php");
    }
}
