<?php

/* SonataAdminBundle:CRUD:show_boolean.html.twig */
class __TwigTemplate_ce73da30adb7ac7c1d55880167433d288ee419a94703425faae068f38a95ab72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_boolean.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d1ef4bb7dfdefda72f5bdd1b329c76257e68dfbf2e09ced23b9bc35042babc7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d1ef4bb7dfdefda72f5bdd1b329c76257e68dfbf2e09ced23b9bc35042babc7->enter($__internal_3d1ef4bb7dfdefda72f5bdd1b329c76257e68dfbf2e09ced23b9bc35042babc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_boolean.html.twig"));

        $__internal_a6eba7c6a0e6392af07b914d43e5aaa65f22e83bf7028712ef9d6106c950a758 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6eba7c6a0e6392af07b914d43e5aaa65f22e83bf7028712ef9d6106c950a758->enter($__internal_a6eba7c6a0e6392af07b914d43e5aaa65f22e83bf7028712ef9d6106c950a758_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_boolean.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3d1ef4bb7dfdefda72f5bdd1b329c76257e68dfbf2e09ced23b9bc35042babc7->leave($__internal_3d1ef4bb7dfdefda72f5bdd1b329c76257e68dfbf2e09ced23b9bc35042babc7_prof);

        
        $__internal_a6eba7c6a0e6392af07b914d43e5aaa65f22e83bf7028712ef9d6106c950a758->leave($__internal_a6eba7c6a0e6392af07b914d43e5aaa65f22e83bf7028712ef9d6106c950a758_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_42c0d2e012b51938683f4db7b959f2f8319a9e2ea2c5ad6f33298b10b3864a90 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_42c0d2e012b51938683f4db7b959f2f8319a9e2ea2c5ad6f33298b10b3864a90->enter($__internal_42c0d2e012b51938683f4db7b959f2f8319a9e2ea2c5ad6f33298b10b3864a90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_9097beff598e3af2938101f502097e1f44768a95e8ce25353792284daed9dec7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9097beff598e3af2938101f502097e1f44768a95e8ce25353792284daed9dec7->enter($__internal_9097beff598e3af2938101f502097e1f44768a95e8ce25353792284daed9dec7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        ob_start();
        // line 16
        echo "    ";
        if ((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 16, $this->getSourceContext()); })())) {
            // line 17
            echo "        <span class=\"label label-success\">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("label_type_yes", array(), "SonataAdminBundle");
            echo "</span>
    ";
        } else {
            // line 19
            echo "        <span class=\"label label-danger\">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("label_type_no", array(), "SonataAdminBundle");
            echo "</span>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_9097beff598e3af2938101f502097e1f44768a95e8ce25353792284daed9dec7->leave($__internal_9097beff598e3af2938101f502097e1f44768a95e8ce25353792284daed9dec7_prof);

        
        $__internal_42c0d2e012b51938683f4db7b959f2f8319a9e2ea2c5ad6f33298b10b3864a90->leave($__internal_42c0d2e012b51938683f4db7b959f2f8319a9e2ea2c5ad6f33298b10b3864a90_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_boolean.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 19,  54 => 17,  51 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field %}
{% spaceless %}
    {% if value %}
        <span class=\"label label-success\">{%- trans from 'SonataAdminBundle' %}label_type_yes{% endtrans -%}</span>
    {% else %}
        <span class=\"label label-danger\">{%- trans from 'SonataAdminBundle' %}label_type_no{% endtrans -%}</span>
    {% endif %}
{% endspaceless %}
{% endblock %}
", "SonataAdminBundle:CRUD:show_boolean.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/show_boolean.html.twig");
    }
}
