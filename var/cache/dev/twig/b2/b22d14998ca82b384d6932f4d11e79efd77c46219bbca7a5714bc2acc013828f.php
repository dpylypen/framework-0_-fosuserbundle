<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_0f1d6029143a4832d84ecd074d2450a44e729020628850135f6231c9e2d3cf2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_10aab87adb3bbe82f10d2629af98c80f0a4bb813a871e869574237ce7d8761c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_10aab87adb3bbe82f10d2629af98c80f0a4bb813a871e869574237ce7d8761c6->enter($__internal_10aab87adb3bbe82f10d2629af98c80f0a4bb813a871e869574237ce7d8761c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_28ac48e685b6eb3f6d1b60c35ca9d9f09f3de178986bbb8621372dfa2fd2889b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28ac48e685b6eb3f6d1b60c35ca9d9f09f3de178986bbb8621372dfa2fd2889b->enter($__internal_28ac48e685b6eb3f6d1b60c35ca9d9f09f3de178986bbb8621372dfa2fd2889b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_10aab87adb3bbe82f10d2629af98c80f0a4bb813a871e869574237ce7d8761c6->leave($__internal_10aab87adb3bbe82f10d2629af98c80f0a4bb813a871e869574237ce7d8761c6_prof);

        
        $__internal_28ac48e685b6eb3f6d1b60c35ca9d9f09f3de178986bbb8621372dfa2fd2889b->leave($__internal_28ac48e685b6eb3f6d1b60c35ca9d9f09f3de178986bbb8621372dfa2fd2889b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
