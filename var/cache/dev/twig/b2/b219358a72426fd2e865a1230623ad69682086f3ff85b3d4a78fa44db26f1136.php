<?php

/* SonataAdminBundle:CRUD:base_edit.html.twig */
class __TwigTemplate_2407332988e119cf7da119d7e02629ee8494a9b64e55d4f458293e76ed60773d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $_trait_0 = $this->loadTemplate("SonataAdminBundle:CRUD:base_edit_form.html.twig", "SonataAdminBundle:CRUD:base_edit.html.twig", 32);
        // line 32
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."SonataAdminBundle:CRUD:base_edit_form.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        if (!isset($_trait_0_blocks["form"])) {
            throw new Twig_Error_Runtime(sprintf('Block "form" is not defined in trait "SonataAdminBundle:CRUD:base_edit_form.html.twig".'));
        }

        $_trait_0_blocks["parentForm"] = $_trait_0_blocks["form"]; unset($_trait_0_blocks["form"]);

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'title' => array($this, 'block_title'),
                'navbar_title' => array($this, 'block_navbar_title'),
                'actions' => array($this, 'block_actions'),
                'tab_menu' => array($this, 'block_tab_menu'),
                'form' => array($this, 'block_form'),
            )
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) || array_key_exists("base_template", $context) ? $context["base_template"] : (function () { throw new Twig_Error_Runtime('Variable "base_template" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:base_edit.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_709af89b169b9fa5d81ec03f73191cc55baf4b15211de48f0ed21df5eb3cd63b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_709af89b169b9fa5d81ec03f73191cc55baf4b15211de48f0ed21df5eb3cd63b->enter($__internal_709af89b169b9fa5d81ec03f73191cc55baf4b15211de48f0ed21df5eb3cd63b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_edit.html.twig"));

        $__internal_3e0b02fdff8b828b13397753a056b8daa0f3ba4153daab26b256788a4a138cf3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e0b02fdff8b828b13397753a056b8daa0f3ba4153daab26b256788a4a138cf3->enter($__internal_3e0b02fdff8b828b13397753a056b8daa0f3ba4153daab26b256788a4a138cf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_edit.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_709af89b169b9fa5d81ec03f73191cc55baf4b15211de48f0ed21df5eb3cd63b->leave($__internal_709af89b169b9fa5d81ec03f73191cc55baf4b15211de48f0ed21df5eb3cd63b_prof);

        
        $__internal_3e0b02fdff8b828b13397753a056b8daa0f3ba4153daab26b256788a4a138cf3->leave($__internal_3e0b02fdff8b828b13397753a056b8daa0f3ba4153daab26b256788a4a138cf3_prof);

    }

    // line 14
    public function block_title($context, array $blocks = array())
    {
        $__internal_972e5ae05b5d0ef0c7c9e78301fdd27efb11b4e93e799f7b792b1b3e2243fba4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_972e5ae05b5d0ef0c7c9e78301fdd27efb11b4e93e799f7b792b1b3e2243fba4->enter($__internal_972e5ae05b5d0ef0c7c9e78301fdd27efb11b4e93e799f7b792b1b3e2243fba4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_90e54d5bd380eced4494e5d17124b3ea70be1957b2a10af55d2313c84b00180c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90e54d5bd380eced4494e5d17124b3ea70be1957b2a10af55d2313c84b00180c->enter($__internal_90e54d5bd380eced4494e5d17124b3ea70be1957b2a10af55d2313c84b00180c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 15
        echo "    ";
        if ( !(null === twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || array_key_exists("object", $context) ? $context["object"] : (function () { throw new Twig_Error_Runtime('Variable "object" does not exist.', 15, $this->getSourceContext()); })())), "method"))) {
            // line 16
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_edit", array("%name%" => twig_truncate_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 16, $this->getSourceContext()); })()), "toString", array(0 => (isset($context["object"]) || array_key_exists("object", $context) ? $context["object"] : (function () { throw new Twig_Error_Runtime('Variable "object" does not exist.', 16, $this->getSourceContext()); })())), "method"), 15)), "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        } else {
            // line 18
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_create", array(), "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        }
        
        $__internal_90e54d5bd380eced4494e5d17124b3ea70be1957b2a10af55d2313c84b00180c->leave($__internal_90e54d5bd380eced4494e5d17124b3ea70be1957b2a10af55d2313c84b00180c_prof);

        
        $__internal_972e5ae05b5d0ef0c7c9e78301fdd27efb11b4e93e799f7b792b1b3e2243fba4->leave($__internal_972e5ae05b5d0ef0c7c9e78301fdd27efb11b4e93e799f7b792b1b3e2243fba4_prof);

    }

    // line 22
    public function block_navbar_title($context, array $blocks = array())
    {
        $__internal_8ce8dc9432b4e60c8673a16d35f4f20285a06b5b162c35d3e115800ec5723178 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ce8dc9432b4e60c8673a16d35f4f20285a06b5b162c35d3e115800ec5723178->enter($__internal_8ce8dc9432b4e60c8673a16d35f4f20285a06b5b162c35d3e115800ec5723178_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navbar_title"));

        $__internal_0d7a76351e454957cfe0cca205c6c0abb15d09dba365503cbf7101fd8d1120ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d7a76351e454957cfe0cca205c6c0abb15d09dba365503cbf7101fd8d1120ef->enter($__internal_0d7a76351e454957cfe0cca205c6c0abb15d09dba365503cbf7101fd8d1120ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navbar_title"));

        // line 23
        echo "    ";
        $this->displayBlock("title", $context, $blocks);
        echo "
";
        
        $__internal_0d7a76351e454957cfe0cca205c6c0abb15d09dba365503cbf7101fd8d1120ef->leave($__internal_0d7a76351e454957cfe0cca205c6c0abb15d09dba365503cbf7101fd8d1120ef_prof);

        
        $__internal_8ce8dc9432b4e60c8673a16d35f4f20285a06b5b162c35d3e115800ec5723178->leave($__internal_8ce8dc9432b4e60c8673a16d35f4f20285a06b5b162c35d3e115800ec5723178_prof);

    }

    // line 26
    public function block_actions($context, array $blocks = array())
    {
        $__internal_7dad64c511abc2cb75e1971d8dc604bec7af0ebc17ffa340ab3b39ab47b170a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7dad64c511abc2cb75e1971d8dc604bec7af0ebc17ffa340ab3b39ab47b170a2->enter($__internal_7dad64c511abc2cb75e1971d8dc604bec7af0ebc17ffa340ab3b39ab47b170a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        $__internal_970af9e3be4f3f27b5c86523bfb91e2a52065e1c537152b25a7d500e8715f9e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_970af9e3be4f3f27b5c86523bfb91e2a52065e1c537152b25a7d500e8715f9e0->enter($__internal_970af9e3be4f3f27b5c86523bfb91e2a52065e1c537152b25a7d500e8715f9e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 27
        $this->loadTemplate("SonataAdminBundle:CRUD:action_buttons.html.twig", "SonataAdminBundle:CRUD:base_edit.html.twig", 27)->display($context);
        
        $__internal_970af9e3be4f3f27b5c86523bfb91e2a52065e1c537152b25a7d500e8715f9e0->leave($__internal_970af9e3be4f3f27b5c86523bfb91e2a52065e1c537152b25a7d500e8715f9e0_prof);

        
        $__internal_7dad64c511abc2cb75e1971d8dc604bec7af0ebc17ffa340ab3b39ab47b170a2->leave($__internal_7dad64c511abc2cb75e1971d8dc604bec7af0ebc17ffa340ab3b39ab47b170a2_prof);

    }

    // line 30
    public function block_tab_menu($context, array $blocks = array())
    {
        $__internal_c661a6fb03f282c0b7d7eb1c5c5b4ec50bca0884c07391bfbc013ccb0fee7f69 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c661a6fb03f282c0b7d7eb1c5c5b4ec50bca0884c07391bfbc013ccb0fee7f69->enter($__internal_c661a6fb03f282c0b7d7eb1c5c5b4ec50bca0884c07391bfbc013ccb0fee7f69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        $__internal_312af02abaeb0aaf91f0780622e60cd36ebdbfb0c61cc29f9f4a546c35f8e83c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_312af02abaeb0aaf91f0780622e60cd36ebdbfb0c61cc29f9f4a546c35f8e83c->enter($__internal_312af02abaeb0aaf91f0780622e60cd36ebdbfb0c61cc29f9f4a546c35f8e83c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 30, $this->getSourceContext()); })()), "sidemenu", array(0 => (isset($context["action"]) || array_key_exists("action", $context) ? $context["action"] : (function () { throw new Twig_Error_Runtime('Variable "action" does not exist.', 30, $this->getSourceContext()); })())), "method"), array("currentClass" => "active", "template" => twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["sonata_admin"]) || array_key_exists("sonata_admin", $context) ? $context["sonata_admin"] : (function () { throw new Twig_Error_Runtime('Variable "sonata_admin" does not exist.', 30, $this->getSourceContext()); })()), "adminPool", array()), "getTemplate", array(0 => "tab_menu_template"), "method")), "twig");
        
        $__internal_312af02abaeb0aaf91f0780622e60cd36ebdbfb0c61cc29f9f4a546c35f8e83c->leave($__internal_312af02abaeb0aaf91f0780622e60cd36ebdbfb0c61cc29f9f4a546c35f8e83c_prof);

        
        $__internal_c661a6fb03f282c0b7d7eb1c5c5b4ec50bca0884c07391bfbc013ccb0fee7f69->leave($__internal_c661a6fb03f282c0b7d7eb1c5c5b4ec50bca0884c07391bfbc013ccb0fee7f69_prof);

    }

    // line 34
    public function block_form($context, array $blocks = array())
    {
        $__internal_4a57eac6e3e483fc966d90f14e30fa18d71233c8bd39e1902979e4078252dd9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a57eac6e3e483fc966d90f14e30fa18d71233c8bd39e1902979e4078252dd9f->enter($__internal_4a57eac6e3e483fc966d90f14e30fa18d71233c8bd39e1902979e4078252dd9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        $__internal_eb5e34129dde7840c1deb0296797ecd239c71d6d709863d10e8c8ad98d5517e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb5e34129dde7840c1deb0296797ecd239c71d6d709863d10e8c8ad98d5517e2->enter($__internal_eb5e34129dde7840c1deb0296797ecd239c71d6d709863d10e8c8ad98d5517e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 35
        echo "    ";
        $this->displayBlock("parentForm", $context, $blocks);
        echo "
";
        
        $__internal_eb5e34129dde7840c1deb0296797ecd239c71d6d709863d10e8c8ad98d5517e2->leave($__internal_eb5e34129dde7840c1deb0296797ecd239c71d6d709863d10e8c8ad98d5517e2_prof);

        
        $__internal_4a57eac6e3e483fc966d90f14e30fa18d71233c8bd39e1902979e4078252dd9f->leave($__internal_4a57eac6e3e483fc966d90f14e30fa18d71233c8bd39e1902979e4078252dd9f_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 35,  152 => 34,  134 => 30,  124 => 27,  115 => 26,  102 => 23,  93 => 22,  79 => 18,  73 => 16,  70 => 15,  61 => 14,  40 => 12,  12 => 32,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block title %}
    {% if admin.id(object) is not null %}
        {{ \"title_edit\"|trans({'%name%': admin.toString(object)|truncate(15) }, 'SonataAdminBundle') }}
    {% else %}
        {{ \"title_create\"|trans({}, 'SonataAdminBundle') }}
    {% endif %}
{% endblock %}

{% block navbar_title %}
    {{ block('title') }}
{% endblock %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:action_buttons.html.twig' %}
{%- endblock -%}

{% block tab_menu %}{{ knp_menu_render(admin.sidemenu(action), {'currentClass' : 'active', 'template': sonata_admin.adminPool.getTemplate('tab_menu_template')}, 'twig') }}{% endblock %}

{% use 'SonataAdminBundle:CRUD:base_edit_form.html.twig' with form as parentForm %}

{% block form %}
    {{ block('parentForm') }}
{% endblock %}
", "SonataAdminBundle:CRUD:base_edit.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/base_edit.html.twig");
    }
}
