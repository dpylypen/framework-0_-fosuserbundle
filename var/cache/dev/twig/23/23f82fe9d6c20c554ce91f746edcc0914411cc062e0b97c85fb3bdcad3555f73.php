<?php

/* @Framework/Form/button_attributes.html.php */
class __TwigTemplate_53ad405b0c9a8cfe242be4314805aa56697ae967f1b2c47d8666372b13842334 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_18903a7b9d122e378db44694e1d3541bcdf42ae88c83909273dce5726b7f9b2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_18903a7b9d122e378db44694e1d3541bcdf42ae88c83909273dce5726b7f9b2e->enter($__internal_18903a7b9d122e378db44694e1d3541bcdf42ae88c83909273dce5726b7f9b2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        $__internal_149762b86c389f0aadf18ed0c63686a3c44f23500ba22fcae98ecad5db2373c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_149762b86c389f0aadf18ed0c63686a3c44f23500ba22fcae98ecad5db2373c1->enter($__internal_149762b86c389f0aadf18ed0c63686a3c44f23500ba22fcae98ecad5db2373c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_18903a7b9d122e378db44694e1d3541bcdf42ae88c83909273dce5726b7f9b2e->leave($__internal_18903a7b9d122e378db44694e1d3541bcdf42ae88c83909273dce5726b7f9b2e_prof);

        
        $__internal_149762b86c389f0aadf18ed0c63686a3c44f23500ba22fcae98ecad5db2373c1->leave($__internal_149762b86c389f0aadf18ed0c63686a3c44f23500ba22fcae98ecad5db2373c1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/button_attributes.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_attributes.html.php");
    }
}
