<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_556e23d0a20ce4933757cd0d0ba0655441952e2fc9f700e2dc4c336aec4f623e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_295a3c219a10a2c9f150801eaad271fbb932fd9619b1c21cc0e97740d82484ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_295a3c219a10a2c9f150801eaad271fbb932fd9619b1c21cc0e97740d82484ee->enter($__internal_295a3c219a10a2c9f150801eaad271fbb932fd9619b1c21cc0e97740d82484ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_b66e9d3319e36eda1604c75a7dbe5e2578035257d9c75ab922d52356b541a9e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b66e9d3319e36eda1604c75a7dbe5e2578035257d9c75ab922d52356b541a9e2->enter($__internal_b66e9d3319e36eda1604c75a7dbe5e2578035257d9c75ab922d52356b541a9e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_295a3c219a10a2c9f150801eaad271fbb932fd9619b1c21cc0e97740d82484ee->leave($__internal_295a3c219a10a2c9f150801eaad271fbb932fd9619b1c21cc0e97740d82484ee_prof);

        
        $__internal_b66e9d3319e36eda1604c75a7dbe5e2578035257d9c75ab922d52356b541a9e2->leave($__internal_b66e9d3319e36eda1604c75a7dbe5e2578035257d9c75ab922d52356b541a9e2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_compound.html.php");
    }
}
