<?php

/* SonataAdminBundle:CRUD:show_compare.html.twig */
class __TwigTemplate_d7feced4d13022fc89e6f355a8a72c7a1cdac604b167e3e54afd7ed0e433b8d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_compare.html.twig", "SonataAdminBundle:CRUD:show_compare.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_compare.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0d023c934e4425b1c6f4db6e703946002a1d086c59f76869ba3264ab5f3f21b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d023c934e4425b1c6f4db6e703946002a1d086c59f76869ba3264ab5f3f21b9->enter($__internal_0d023c934e4425b1c6f4db6e703946002a1d086c59f76869ba3264ab5f3f21b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_compare.html.twig"));

        $__internal_c2d43c527eb51e5157e829b1a46a14d6b643886f57f7034d799b09f01df421dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c2d43c527eb51e5157e829b1a46a14d6b643886f57f7034d799b09f01df421dd->enter($__internal_c2d43c527eb51e5157e829b1a46a14d6b643886f57f7034d799b09f01df421dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_compare.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0d023c934e4425b1c6f4db6e703946002a1d086c59f76869ba3264ab5f3f21b9->leave($__internal_0d023c934e4425b1c6f4db6e703946002a1d086c59f76869ba3264ab5f3f21b9_prof);

        
        $__internal_c2d43c527eb51e5157e829b1a46a14d6b643886f57f7034d799b09f01df421dd->leave($__internal_c2d43c527eb51e5157e829b1a46a14d6b643886f57f7034d799b09f01df421dd_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_compare.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_compare.html.twig' %}
", "SonataAdminBundle:CRUD:show_compare.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/show_compare.html.twig");
    }
}
