<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_da9bf3cd0acdae21491bcdd03e13709135cf118ffd543637b2d82b0c0583aeb2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_38a3211ad8fa184f30547078c274a85445faac67b9b734d3d4d007be781cbb9a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_38a3211ad8fa184f30547078c274a85445faac67b9b734d3d4d007be781cbb9a->enter($__internal_38a3211ad8fa184f30547078c274a85445faac67b9b734d3d4d007be781cbb9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_46b26fabf5f00e80f75a4679f02e8e9bff2f82c56814758823a8e3fb8713a819 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46b26fabf5f00e80f75a4679f02e8e9bff2f82c56814758823a8e3fb8713a819->enter($__internal_46b26fabf5f00e80f75a4679f02e8e9bff2f82c56814758823a8e3fb8713a819_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_38a3211ad8fa184f30547078c274a85445faac67b9b734d3d4d007be781cbb9a->leave($__internal_38a3211ad8fa184f30547078c274a85445faac67b9b734d3d4d007be781cbb9a_prof);

        
        $__internal_46b26fabf5f00e80f75a4679f02e8e9bff2f82c56814758823a8e3fb8713a819->leave($__internal_46b26fabf5f00e80f75a4679f02e8e9bff2f82c56814758823a8e3fb8713a819_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
