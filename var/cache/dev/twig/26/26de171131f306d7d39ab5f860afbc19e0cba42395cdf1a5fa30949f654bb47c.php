<?php

/* SonataAdminBundle:CRUD:base_array_macro.html.twig */
class __TwigTemplate_1f400be4062bb22cb019d03c23cefa99a9039447ea2f8578dab3328d158c8c75 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_592bdd5b0ef7adb611cddd2a3fd1b2845794f9bb4518c661b408d5336cb3cca1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_592bdd5b0ef7adb611cddd2a3fd1b2845794f9bb4518c661b408d5336cb3cca1->enter($__internal_592bdd5b0ef7adb611cddd2a3fd1b2845794f9bb4518c661b408d5336cb3cca1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_array_macro.html.twig"));

        $__internal_7abed420d1fe42a5242d0487e74f1357f551a0056284c9ebf65698a378891f3a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7abed420d1fe42a5242d0487e74f1357f551a0056284c9ebf65698a378891f3a->enter($__internal_7abed420d1fe42a5242d0487e74f1357f551a0056284c9ebf65698a378891f3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_array_macro.html.twig"));

        
        $__internal_592bdd5b0ef7adb611cddd2a3fd1b2845794f9bb4518c661b408d5336cb3cca1->leave($__internal_592bdd5b0ef7adb611cddd2a3fd1b2845794f9bb4518c661b408d5336cb3cca1_prof);

        
        $__internal_7abed420d1fe42a5242d0487e74f1357f551a0056284c9ebf65698a378891f3a->leave($__internal_7abed420d1fe42a5242d0487e74f1357f551a0056284c9ebf65698a378891f3a_prof);

    }

    // line 11
    public function macro_render_array($__value__ = null, $__inline__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "value" => $__value__,
            "inline" => $__inline__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            $__internal_886b89beb7c33a116f28c9bede5c88ed96ba0f834eb4335e41f82c2a9fafe1b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $__internal_886b89beb7c33a116f28c9bede5c88ed96ba0f834eb4335e41f82c2a9fafe1b6->enter($__internal_886b89beb7c33a116f28c9bede5c88ed96ba0f834eb4335e41f82c2a9fafe1b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "macro", "render_array"));

            $__internal_9317d6df0aadcc126e39f234c87dece0d18cb1d88aaa505e2b0e38b2e5861bba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $__internal_9317d6df0aadcc126e39f234c87dece0d18cb1d88aaa505e2b0e38b2e5861bba->enter($__internal_9317d6df0aadcc126e39f234c87dece0d18cb1d88aaa505e2b0e38b2e5861bba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "macro", "render_array"));

            // line 12
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 12, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["key"] => $context["val"]) {
                // line 13
                echo "        ";
                if (twig_test_iterable($context["val"])) {
                    // line 14
                    echo "            [";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo " => ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $this->getTemplateName(), "render_array", array(0 => $context["val"], 1 => (isset($context["inline"]) || array_key_exists("inline", $context) ? $context["inline"] : (function () { throw new Twig_Error_Runtime('Variable "inline" does not exist.', 14, $this->getSourceContext()); })())), "method"), "html", null, true);
                    echo "]
        ";
                } else {
                    // line 16
                    echo "            [";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo " => ";
                    echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                    echo "]
        ";
                }
                // line 18
                echo "
        ";
                // line 19
                if (( !twig_get_attribute($this->env, $this->getSourceContext(), $context["loop"], "last", array()) &&  !(isset($context["inline"]) || array_key_exists("inline", $context) ? $context["inline"] : (function () { throw new Twig_Error_Runtime('Variable "inline" does not exist.', 19, $this->getSourceContext()); })()))) {
                    // line 20
                    echo "            <br>
        ";
                }
                // line 22
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['val'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            
            $__internal_9317d6df0aadcc126e39f234c87dece0d18cb1d88aaa505e2b0e38b2e5861bba->leave($__internal_9317d6df0aadcc126e39f234c87dece0d18cb1d88aaa505e2b0e38b2e5861bba_prof);

            
            $__internal_886b89beb7c33a116f28c9bede5c88ed96ba0f834eb4335e41f82c2a9fafe1b6->leave($__internal_886b89beb7c33a116f28c9bede5c88ed96ba0f834eb4335e41f82c2a9fafe1b6_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_array_macro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 22,  94 => 20,  92 => 19,  89 => 18,  81 => 16,  73 => 14,  70 => 13,  52 => 12,  33 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% macro render_array(value, inline) %}
    {% for key, val in value %}
        {% if val is iterable %}
            [{{ key }} => {{ _self.render_array(val, inline) }}]
        {%  else %}
            [{{ key }} => {{ val }}]
        {%  endif %}

        {% if not loop.last and not inline %}
            <br>
        {% endif %}
    {% endfor %}
{% endmacro %}
", "SonataAdminBundle:CRUD:base_array_macro.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/base_array_macro.html.twig");
    }
}
