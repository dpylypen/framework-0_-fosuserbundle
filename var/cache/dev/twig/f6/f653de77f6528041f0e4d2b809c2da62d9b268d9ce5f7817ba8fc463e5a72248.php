<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_69c76c8a99976ebb97f1efcb24b2387a29d75c494583f0ba49fd871f0361503f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9d2b3743f9d93c6ab847012252f757f8a170ca645439c1f304974e18800fdae8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d2b3743f9d93c6ab847012252f757f8a170ca645439c1f304974e18800fdae8->enter($__internal_9d2b3743f9d93c6ab847012252f757f8a170ca645439c1f304974e18800fdae8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $__internal_88d9b13b55fdef7f61c8124472276b405a0fbd797b57388f1af46c2fa216efae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88d9b13b55fdef7f61c8124472276b405a0fbd797b57388f1af46c2fa216efae->enter($__internal_88d9b13b55fdef7f61c8124472276b405a0fbd797b57388f1af46c2fa216efae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9d2b3743f9d93c6ab847012252f757f8a170ca645439c1f304974e18800fdae8->leave($__internal_9d2b3743f9d93c6ab847012252f757f8a170ca645439c1f304974e18800fdae8_prof);

        
        $__internal_88d9b13b55fdef7f61c8124472276b405a0fbd797b57388f1af46c2fa216efae->leave($__internal_88d9b13b55fdef7f61c8124472276b405a0fbd797b57388f1af46c2fa216efae_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_48ee2b5b1eea1f1339edbb4a7e5b0e8126a60881ee50ad0f9c707953c9c72ad4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_48ee2b5b1eea1f1339edbb4a7e5b0e8126a60881ee50ad0f9c707953c9c72ad4->enter($__internal_48ee2b5b1eea1f1339edbb4a7e5b0e8126a60881ee50ad0f9c707953c9c72ad4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_570c4dfefeb626ad4d12c4abe8b172504afe120943ca0407bb7241bd31c96d52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_570c4dfefeb626ad4d12c4abe8b172504afe120943ca0407bb7241bd31c96d52->enter($__internal_570c4dfefeb626ad4d12c4abe8b172504afe120943ca0407bb7241bd31c96d52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_570c4dfefeb626ad4d12c4abe8b172504afe120943ca0407bb7241bd31c96d52->leave($__internal_570c4dfefeb626ad4d12c4abe8b172504afe120943ca0407bb7241bd31c96d52_prof);

        
        $__internal_48ee2b5b1eea1f1339edbb4a7e5b0e8126a60881ee50ad0f9c707953c9c72ad4->leave($__internal_48ee2b5b1eea1f1339edbb4a7e5b0e8126a60881ee50ad0f9c707953c9c72ad4_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}
