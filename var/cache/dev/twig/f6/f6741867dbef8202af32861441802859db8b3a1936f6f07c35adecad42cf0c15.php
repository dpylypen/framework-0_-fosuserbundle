<?php

/* ::base.html.twig */
class __TwigTemplate_fe563e3afbf708e1cb33ee7fd227ad54628c2c1e7460358731135997a5c0c233 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cbf459b325b58dadc178fcd754c1d99970ba4c624bab57e434cfbee4552b5ebe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cbf459b325b58dadc178fcd754c1d99970ba4c624bab57e434cfbee4552b5ebe->enter($__internal_cbf459b325b58dadc178fcd754c1d99970ba4c624bab57e434cfbee4552b5ebe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_a492e87d4cf3c9c6813bb7cdc5868e37d3ab0995b0dd2a01835d35b627aba2f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a492e87d4cf3c9c6813bb7cdc5868e37d3ab0995b0dd2a01835d35b627aba2f3->enter($__internal_a492e87d4cf3c9c6813bb7cdc5868e37d3ab0995b0dd2a01835d35b627aba2f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_cbf459b325b58dadc178fcd754c1d99970ba4c624bab57e434cfbee4552b5ebe->leave($__internal_cbf459b325b58dadc178fcd754c1d99970ba4c624bab57e434cfbee4552b5ebe_prof);

        
        $__internal_a492e87d4cf3c9c6813bb7cdc5868e37d3ab0995b0dd2a01835d35b627aba2f3->leave($__internal_a492e87d4cf3c9c6813bb7cdc5868e37d3ab0995b0dd2a01835d35b627aba2f3_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_067d69c3e3b7e03275820eb25d09a8eecfa4ee74574b110ef0d771366e92bc3c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_067d69c3e3b7e03275820eb25d09a8eecfa4ee74574b110ef0d771366e92bc3c->enter($__internal_067d69c3e3b7e03275820eb25d09a8eecfa4ee74574b110ef0d771366e92bc3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_f4613b322193107264c03e62496df799923b41dabe66e8301b646653e7df579f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f4613b322193107264c03e62496df799923b41dabe66e8301b646653e7df579f->enter($__internal_f4613b322193107264c03e62496df799923b41dabe66e8301b646653e7df579f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_f4613b322193107264c03e62496df799923b41dabe66e8301b646653e7df579f->leave($__internal_f4613b322193107264c03e62496df799923b41dabe66e8301b646653e7df579f_prof);

        
        $__internal_067d69c3e3b7e03275820eb25d09a8eecfa4ee74574b110ef0d771366e92bc3c->leave($__internal_067d69c3e3b7e03275820eb25d09a8eecfa4ee74574b110ef0d771366e92bc3c_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f07313c8c9df9cdb9a772a152d126271c71f1ea45c88bd3f15c4f995fa6d1804 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f07313c8c9df9cdb9a772a152d126271c71f1ea45c88bd3f15c4f995fa6d1804->enter($__internal_f07313c8c9df9cdb9a772a152d126271c71f1ea45c88bd3f15c4f995fa6d1804_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_3476fc7de3187d718b8b69473b11cc95f346e7a95ab33712f513f0bbb2aee7d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3476fc7de3187d718b8b69473b11cc95f346e7a95ab33712f513f0bbb2aee7d4->enter($__internal_3476fc7de3187d718b8b69473b11cc95f346e7a95ab33712f513f0bbb2aee7d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_3476fc7de3187d718b8b69473b11cc95f346e7a95ab33712f513f0bbb2aee7d4->leave($__internal_3476fc7de3187d718b8b69473b11cc95f346e7a95ab33712f513f0bbb2aee7d4_prof);

        
        $__internal_f07313c8c9df9cdb9a772a152d126271c71f1ea45c88bd3f15c4f995fa6d1804->leave($__internal_f07313c8c9df9cdb9a772a152d126271c71f1ea45c88bd3f15c4f995fa6d1804_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_bae483cbe410faa9dafca93430a9f95b746a669d556f40a641730f4984be59b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bae483cbe410faa9dafca93430a9f95b746a669d556f40a641730f4984be59b1->enter($__internal_bae483cbe410faa9dafca93430a9f95b746a669d556f40a641730f4984be59b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_abc9deb62acd7e57d5ca6c389bf785445e1e89b38a1285e307b8436dbabed26b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_abc9deb62acd7e57d5ca6c389bf785445e1e89b38a1285e307b8436dbabed26b->enter($__internal_abc9deb62acd7e57d5ca6c389bf785445e1e89b38a1285e307b8436dbabed26b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_abc9deb62acd7e57d5ca6c389bf785445e1e89b38a1285e307b8436dbabed26b->leave($__internal_abc9deb62acd7e57d5ca6c389bf785445e1e89b38a1285e307b8436dbabed26b_prof);

        
        $__internal_bae483cbe410faa9dafca93430a9f95b746a669d556f40a641730f4984be59b1->leave($__internal_bae483cbe410faa9dafca93430a9f95b746a669d556f40a641730f4984be59b1_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5cb9a9590c3c76e936d5f2968bc57bd939aa49e4b71b8b704d150a50350a858d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5cb9a9590c3c76e936d5f2968bc57bd939aa49e4b71b8b704d150a50350a858d->enter($__internal_5cb9a9590c3c76e936d5f2968bc57bd939aa49e4b71b8b704d150a50350a858d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_b67a0ef1f1fe1cf2990064771ead16b6f8143edc9d0ddb69d4e3d8df0b2f015c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b67a0ef1f1fe1cf2990064771ead16b6f8143edc9d0ddb69d4e3d8df0b2f015c->enter($__internal_b67a0ef1f1fe1cf2990064771ead16b6f8143edc9d0ddb69d4e3d8df0b2f015c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_b67a0ef1f1fe1cf2990064771ead16b6f8143edc9d0ddb69d4e3d8df0b2f015c->leave($__internal_b67a0ef1f1fe1cf2990064771ead16b6f8143edc9d0ddb69d4e3d8df0b2f015c_prof);

        
        $__internal_5cb9a9590c3c76e936d5f2968bc57bd939aa49e4b71b8b704d150a50350a858d->leave($__internal_5cb9a9590c3c76e936d5f2968bc57bd939aa49e4b71b8b704d150a50350a858d_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "/Users/dp/Sites/frame-0/app/Resources/views/base.html.twig");
    }
}
