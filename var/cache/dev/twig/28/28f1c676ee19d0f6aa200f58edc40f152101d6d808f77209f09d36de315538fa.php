<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_85422e3d43b6c27373e66a807053f1005b221eacdb0234ffca6b62bd9abd1b65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d805aa25269f507535c8b226b6d7c0c2bf7b85cec44ea18a19cdfe5fbc3b4b62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d805aa25269f507535c8b226b6d7c0c2bf7b85cec44ea18a19cdfe5fbc3b4b62->enter($__internal_d805aa25269f507535c8b226b6d7c0c2bf7b85cec44ea18a19cdfe5fbc3b4b62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $__internal_bb65ae35b4be003f6f2739c011dbedee550afd365db0fba8f5326d2acfc50ce9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb65ae35b4be003f6f2739c011dbedee550afd365db0fba8f5326d2acfc50ce9->enter($__internal_bb65ae35b4be003f6f2739c011dbedee550afd365db0fba8f5326d2acfc50ce9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d805aa25269f507535c8b226b6d7c0c2bf7b85cec44ea18a19cdfe5fbc3b4b62->leave($__internal_d805aa25269f507535c8b226b6d7c0c2bf7b85cec44ea18a19cdfe5fbc3b4b62_prof);

        
        $__internal_bb65ae35b4be003f6f2739c011dbedee550afd365db0fba8f5326d2acfc50ce9->leave($__internal_bb65ae35b4be003f6f2739c011dbedee550afd365db0fba8f5326d2acfc50ce9_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_e89c02d6bd115ecd204c7a3e240c8d407919c87956fdf01ffa39cf1e01148575 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e89c02d6bd115ecd204c7a3e240c8d407919c87956fdf01ffa39cf1e01148575->enter($__internal_e89c02d6bd115ecd204c7a3e240c8d407919c87956fdf01ffa39cf1e01148575_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_76bdd7eadeaa4a3aef4d4626f678c49fe8981bd309b4c6ed191c306746f5b3de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_76bdd7eadeaa4a3aef4d4626f678c49fe8981bd309b4c6ed191c306746f5b3de->enter($__internal_76bdd7eadeaa4a3aef4d4626f678c49fe8981bd309b4c6ed191c306746f5b3de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_76bdd7eadeaa4a3aef4d4626f678c49fe8981bd309b4c6ed191c306746f5b3de->leave($__internal_76bdd7eadeaa4a3aef4d4626f678c49fe8981bd309b4c6ed191c306746f5b3de_prof);

        
        $__internal_e89c02d6bd115ecd204c7a3e240c8d407919c87956fdf01ffa39cf1e01148575->leave($__internal_e89c02d6bd115ecd204c7a3e240c8d407919c87956fdf01ffa39cf1e01148575_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:show.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show.html.twig");
    }
}
