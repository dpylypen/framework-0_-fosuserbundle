<?php

/* FOSUserBundle:Resetting:request_content.html.twig */
class __TwigTemplate_e3c6502a1cb6842848e0b01097d8311eb78b68976dec5551a89fbe141287968f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9a58a69474a21b8e9104dcb60923bd345341f596b0cc210cc349b313a906704 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9a58a69474a21b8e9104dcb60923bd345341f596b0cc210cc349b313a906704->enter($__internal_c9a58a69474a21b8e9104dcb60923bd345341f596b0cc210cc349b313a906704_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request_content.html.twig"));

        $__internal_9409465fd5d24dda0a8428dc49762dc46dcfffe661617dc7f5d64f691a66fc9e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9409465fd5d24dda0a8428dc49762dc46dcfffe661617dc7f5d64f691a66fc9e->enter($__internal_9409465fd5d24dda0a8428dc49762dc46dcfffe661617dc7f5d64f691a66fc9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request_content.html.twig"));

        // line 2
        echo "
<form action=\"";
        // line 3
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_resetting_send_email");
        echo "\" method=\"POST\" class=\"fos_user_resetting_request\">
    <div>
        <label for=\"username\">";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.request.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
        <input type=\"text\" id=\"username\" name=\"username\" required=\"required\" />
    </div>
    <div>
        <input type=\"submit\" value=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.request.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
</form>
";
        
        $__internal_c9a58a69474a21b8e9104dcb60923bd345341f596b0cc210cc349b313a906704->leave($__internal_c9a58a69474a21b8e9104dcb60923bd345341f596b0cc210cc349b313a906704_prof);

        
        $__internal_9409465fd5d24dda0a8428dc49762dc46dcfffe661617dc7f5d64f691a66fc9e->leave($__internal_9409465fd5d24dda0a8428dc49762dc46dcfffe661617dc7f5d64f691a66fc9e_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 9,  33 => 5,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<form action=\"{{ path('fos_user_resetting_send_email') }}\" method=\"POST\" class=\"fos_user_resetting_request\">
    <div>
        <label for=\"username\">{{ 'resetting.request.username'|trans }}</label>
        <input type=\"text\" id=\"username\" name=\"username\" required=\"required\" />
    </div>
    <div>
        <input type=\"submit\" value=\"{{ 'resetting.request.submit'|trans }}\" />
    </div>
</form>
", "FOSUserBundle:Resetting:request_content.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request_content.html.twig");
    }
}
