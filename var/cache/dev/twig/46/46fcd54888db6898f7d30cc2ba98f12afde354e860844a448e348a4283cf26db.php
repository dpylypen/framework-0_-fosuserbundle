<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_0c4772bca31b91a5490e5486285c90e5ae8d2cbe6e833bd1d1ce7c8060b7c7c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4c3a0a06e6b61fc86cc362e108d5bccd9248796d08f53ee205d4f6ab594e6e21 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c3a0a06e6b61fc86cc362e108d5bccd9248796d08f53ee205d4f6ab594e6e21->enter($__internal_4c3a0a06e6b61fc86cc362e108d5bccd9248796d08f53ee205d4f6ab594e6e21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_29f59bacae06c0fe628fb3e062fa52a9800ea5ac2be05816438b15591634920e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29f59bacae06c0fe628fb3e062fa52a9800ea5ac2be05816438b15591634920e->enter($__internal_29f59bacae06c0fe628fb3e062fa52a9800ea5ac2be05816438b15591634920e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_4c3a0a06e6b61fc86cc362e108d5bccd9248796d08f53ee205d4f6ab594e6e21->leave($__internal_4c3a0a06e6b61fc86cc362e108d5bccd9248796d08f53ee205d4f6ab594e6e21_prof);

        
        $__internal_29f59bacae06c0fe628fb3e062fa52a9800ea5ac2be05816438b15591634920e->leave($__internal_29f59bacae06c0fe628fb3e062fa52a9800ea5ac2be05816438b15591634920e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
