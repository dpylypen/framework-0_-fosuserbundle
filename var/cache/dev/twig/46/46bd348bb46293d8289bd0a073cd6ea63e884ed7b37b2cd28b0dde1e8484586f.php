<?php

/* FOSUserBundle:Group:new_content.html.twig */
class __TwigTemplate_222876e832a2d7fe526d26786ee2c4030955e8d9e5c3472c821c6a06b590a499 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_96cef99a7f57880d4b5cec779ad85ffe7e0dcd7184ea7e3ac0826b6e183c4c18 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96cef99a7f57880d4b5cec779ad85ffe7e0dcd7184ea7e3ac0826b6e183c4c18->enter($__internal_96cef99a7f57880d4b5cec779ad85ffe7e0dcd7184ea7e3ac0826b6e183c4c18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new_content.html.twig"));

        $__internal_00d9b67b00457df2b9b11d6c905a9bf94c499093c88ea013a59cbcd7ca073a34 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_00d9b67b00457df2b9b11d6c905a9bf94c499093c88ea013a59cbcd7ca073a34->enter($__internal_00d9b67b00457df2b9b11d6c905a9bf94c499093c88ea013a59cbcd7ca073a34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new_content.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 3, $this->getSourceContext()); })()), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_group_new"), "attr" => array("class" => "fos_user_group_new")));
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 4, $this->getSourceContext()); })()), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.new.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'form_end');
        echo "
";
        
        $__internal_96cef99a7f57880d4b5cec779ad85ffe7e0dcd7184ea7e3ac0826b6e183c4c18->leave($__internal_96cef99a7f57880d4b5cec779ad85ffe7e0dcd7184ea7e3ac0826b6e183c4c18_prof);

        
        $__internal_00d9b67b00457df2b9b11d6c905a9bf94c499093c88ea013a59cbcd7ca073a34->leave($__internal_00d9b67b00457df2b9b11d6c905a9bf94c499093c88ea013a59cbcd7ca073a34_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

{{ form_start(form, { 'action': path('fos_user_group_new'), 'attr': { 'class': 'fos_user_group_new' } }) }}
    {{ form_widget(form) }}
    <div>
        <input type=\"submit\" value=\"{{ 'group.new.submit'|trans }}\" />
    </div>
{{ form_end(form) }}
", "FOSUserBundle:Group:new_content.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new_content.html.twig");
    }
}
