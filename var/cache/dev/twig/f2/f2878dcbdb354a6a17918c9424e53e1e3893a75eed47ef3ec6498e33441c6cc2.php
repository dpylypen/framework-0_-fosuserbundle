<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_18be9f7a9978c1efc4e2cc35e5f3a89d68bc51c1bc92975f19b2486672d9f722 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b2be4601e621fc8c8343fc2e9a088d6e030fc9f1dc19b85818f858806d5903f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b2be4601e621fc8c8343fc2e9a088d6e030fc9f1dc19b85818f858806d5903f6->enter($__internal_b2be4601e621fc8c8343fc2e9a088d6e030fc9f1dc19b85818f858806d5903f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_f0f40bc5f2daa65a93fede3e131d5dfb70aafb8afb75c32d52fb605ff67d283b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0f40bc5f2daa65a93fede3e131d5dfb70aafb8afb75c32d52fb605ff67d283b->enter($__internal_f0f40bc5f2daa65a93fede3e131d5dfb70aafb8afb75c32d52fb605ff67d283b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_b2be4601e621fc8c8343fc2e9a088d6e030fc9f1dc19b85818f858806d5903f6->leave($__internal_b2be4601e621fc8c8343fc2e9a088d6e030fc9f1dc19b85818f858806d5903f6_prof);

        
        $__internal_f0f40bc5f2daa65a93fede3e131d5dfb70aafb8afb75c32d52fb605ff67d283b->leave($__internal_f0f40bc5f2daa65a93fede3e131d5dfb70aafb8afb75c32d52fb605ff67d283b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}
