<?php

/* @Twig/images/icon-minus-square.svg */
class __TwigTemplate_e4ab01faa4137839e7f58b6a0b51b75848ffdc35b6b1d87732d087d0a08123fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d43b8789158260ec7f7cc4ce31c55a3e5f211e075f4a71c45c4117ab32a43cb5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d43b8789158260ec7f7cc4ce31c55a3e5f211e075f4a71c45c4117ab32a43cb5->enter($__internal_d43b8789158260ec7f7cc4ce31c55a3e5f211e075f4a71c45c4117ab32a43cb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        $__internal_c7d23f41a254430a923945c8825949165a4f3553f3f4e46ad3d6b8c7dae8dedb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c7d23f41a254430a923945c8825949165a4f3553f3f4e46ad3d6b8c7dae8dedb->enter($__internal_c7d23f41a254430a923945c8825949165a4f3553f3f4e46ad3d6b8c7dae8dedb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
";
        
        $__internal_d43b8789158260ec7f7cc4ce31c55a3e5f211e075f4a71c45c4117ab32a43cb5->leave($__internal_d43b8789158260ec7f7cc4ce31c55a3e5f211e075f4a71c45c4117ab32a43cb5_prof);

        
        $__internal_c7d23f41a254430a923945c8825949165a4f3553f3f4e46ad3d6b8c7dae8dedb->leave($__internal_c7d23f41a254430a923945c8825949165a4f3553f3f4e46ad3d6b8c7dae8dedb_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
", "@Twig/images/icon-minus-square.svg", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-minus-square.svg");
    }
}
