<?php

/* SonataAdminBundle:CRUD:list_time.html.twig */
class __TwigTemplate_52743b100c4c9a430cc8ecd22c04e5fb549ed616a55377c452f95a800c79d3d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_time.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5907a086b58d7569096ea634f6d650f8de9a91931e7f5afeaf0e51de6f677cf1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5907a086b58d7569096ea634f6d650f8de9a91931e7f5afeaf0e51de6f677cf1->enter($__internal_5907a086b58d7569096ea634f6d650f8de9a91931e7f5afeaf0e51de6f677cf1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_time.html.twig"));

        $__internal_d4e0a99cedeb35473508b532a917a5ee1eb16dccb68c1939928b5bb2a8eebee2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4e0a99cedeb35473508b532a917a5ee1eb16dccb68c1939928b5bb2a8eebee2->enter($__internal_d4e0a99cedeb35473508b532a917a5ee1eb16dccb68c1939928b5bb2a8eebee2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_time.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5907a086b58d7569096ea634f6d650f8de9a91931e7f5afeaf0e51de6f677cf1->leave($__internal_5907a086b58d7569096ea634f6d650f8de9a91931e7f5afeaf0e51de6f677cf1_prof);

        
        $__internal_d4e0a99cedeb35473508b532a917a5ee1eb16dccb68c1939928b5bb2a8eebee2->leave($__internal_d4e0a99cedeb35473508b532a917a5ee1eb16dccb68c1939928b5bb2a8eebee2_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_e1715f7798547d40bbfdd57b4c54e56ec279aff22e4b0867609c3e0c466051fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e1715f7798547d40bbfdd57b4c54e56ec279aff22e4b0867609c3e0c466051fc->enter($__internal_e1715f7798547d40bbfdd57b4c54e56ec279aff22e4b0867609c3e0c466051fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_b838d37c64b2b2fd5d9cdfd728ba72348289a5b9e18c5f55db5aea60242f15b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b838d37c64b2b2fd5d9cdfd728ba72348289a5b9e18c5f55db5aea60242f15b1->enter($__internal_b838d37c64b2b2fd5d9cdfd728ba72348289a5b9e18c5f55db5aea60242f15b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twig_test_empty((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "&nbsp;";
        } else {
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 18, $this->getSourceContext()); })()), "H:i:s"), "html", null, true);
        }
        
        $__internal_b838d37c64b2b2fd5d9cdfd728ba72348289a5b9e18c5f55db5aea60242f15b1->leave($__internal_b838d37c64b2b2fd5d9cdfd728ba72348289a5b9e18c5f55db5aea60242f15b1_prof);

        
        $__internal_e1715f7798547d40bbfdd57b4c54e56ec279aff22e4b0867609c3e0c466051fc->leave($__internal_e1715f7798547d40bbfdd57b4c54e56ec279aff22e4b0867609c3e0c466051fc_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 18,  50 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {%- if value is empty -%}
        &nbsp;
    {%- else -%}
        {{ value|date('H:i:s') }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:list_time.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list_time.html.twig");
    }
}
