<?php

/* SonataAdminBundle:CRUD:list_currency.html.twig */
class __TwigTemplate_ad2daeaea43572820e562c09add5f78de3168f6daecdd92052879a733802aa6d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_currency.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3403126a392997f60aaac9d4241b11839e271f9491897441fc8c00a63c0eea7a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3403126a392997f60aaac9d4241b11839e271f9491897441fc8c00a63c0eea7a->enter($__internal_3403126a392997f60aaac9d4241b11839e271f9491897441fc8c00a63c0eea7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_currency.html.twig"));

        $__internal_3a7af9b2c26c05f57720c1984eaf28072156deca2eb9eb10b96cd66538085e54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a7af9b2c26c05f57720c1984eaf28072156deca2eb9eb10b96cd66538085e54->enter($__internal_3a7af9b2c26c05f57720c1984eaf28072156deca2eb9eb10b96cd66538085e54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_currency.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3403126a392997f60aaac9d4241b11839e271f9491897441fc8c00a63c0eea7a->leave($__internal_3403126a392997f60aaac9d4241b11839e271f9491897441fc8c00a63c0eea7a_prof);

        
        $__internal_3a7af9b2c26c05f57720c1984eaf28072156deca2eb9eb10b96cd66538085e54->leave($__internal_3a7af9b2c26c05f57720c1984eaf28072156deca2eb9eb10b96cd66538085e54_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_89cdf234d0d9e9c76eef271ef82c17c941a63844bc4cf689b2ec87ac948683b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_89cdf234d0d9e9c76eef271ef82c17c941a63844bc4cf689b2ec87ac948683b5->enter($__internal_89cdf234d0d9e9c76eef271ef82c17c941a63844bc4cf689b2ec87ac948683b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_4ad0a699799a47557ee26ad1af8724bd431a85b4019caec3e52130a090085d69 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ad0a699799a47557ee26ad1af8724bd431a85b4019caec3e52130a090085d69->enter($__internal_4ad0a699799a47557ee26ad1af8724bd431a85b4019caec3e52130a090085d69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        if ( !(null === (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "        ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "currency", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
            echo "
    ";
        }
        
        $__internal_4ad0a699799a47557ee26ad1af8724bd431a85b4019caec3e52130a090085d69->leave($__internal_4ad0a699799a47557ee26ad1af8724bd431a85b4019caec3e52130a090085d69_prof);

        
        $__internal_89cdf234d0d9e9c76eef271ef82c17c941a63844bc4cf689b2ec87ac948683b5->leave($__internal_89cdf234d0d9e9c76eef271ef82c17c941a63844bc4cf689b2ec87ac948683b5_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_currency.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {% if value is not null %}
        {{ field_description.options.currency }} {{ value }}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:list_currency.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list_currency.html.twig");
    }
}
