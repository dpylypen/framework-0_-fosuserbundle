<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_2e5c408a55b84c32b1d5fe125e0c3572d11aa9eb738445015e4de176113b7c0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2721db77d4b3a9e9490f9f5db23fa35784f7cd2d14f35b85c5c51b781fee6b45 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2721db77d4b3a9e9490f9f5db23fa35784f7cd2d14f35b85c5c51b781fee6b45->enter($__internal_2721db77d4b3a9e9490f9f5db23fa35784f7cd2d14f35b85c5c51b781fee6b45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $__internal_238440f7e982134f5d3865832dd1b041fd2c77a299768b91efcbf593f2e3c632 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_238440f7e982134f5d3865832dd1b041fd2c77a299768b91efcbf593f2e3c632->enter($__internal_238440f7e982134f5d3865832dd1b041fd2c77a299768b91efcbf593f2e3c632_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2721db77d4b3a9e9490f9f5db23fa35784f7cd2d14f35b85c5c51b781fee6b45->leave($__internal_2721db77d4b3a9e9490f9f5db23fa35784f7cd2d14f35b85c5c51b781fee6b45_prof);

        
        $__internal_238440f7e982134f5d3865832dd1b041fd2c77a299768b91efcbf593f2e3c632->leave($__internal_238440f7e982134f5d3865832dd1b041fd2c77a299768b91efcbf593f2e3c632_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_eb551c71c3059353197615c1f74b797603a556a8aeda5150b7a32e81d94222f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb551c71c3059353197615c1f74b797603a556a8aeda5150b7a32e81d94222f8->enter($__internal_eb551c71c3059353197615c1f74b797603a556a8aeda5150b7a32e81d94222f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_7674d79d8188029455cbb1f7ac64893dc3126685062adad1825da39049b6313b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7674d79d8188029455cbb1f7ac64893dc3126685062adad1825da39049b6313b->enter($__internal_7674d79d8188029455cbb1f7ac64893dc3126685062adad1825da39049b6313b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_7674d79d8188029455cbb1f7ac64893dc3126685062adad1825da39049b6313b->leave($__internal_7674d79d8188029455cbb1f7ac64893dc3126685062adad1825da39049b6313b_prof);

        
        $__internal_eb551c71c3059353197615c1f74b797603a556a8aeda5150b7a32e81d94222f8->leave($__internal_eb551c71c3059353197615c1f74b797603a556a8aeda5150b7a32e81d94222f8_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request.html.twig");
    }
}
