<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_6abc6594bb747e91d008a2dd87618209e9ad7931b833321c9f40566ce8d4d936 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7ec4e9ac21beca8cf2e9d1d09e181a2b635166db71faaf786e10da74f95ac93 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f7ec4e9ac21beca8cf2e9d1d09e181a2b635166db71faaf786e10da74f95ac93->enter($__internal_f7ec4e9ac21beca8cf2e9d1d09e181a2b635166db71faaf786e10da74f95ac93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        $__internal_9c34263bdbc31015f3890497f8f5b9108438bf0f315f5d509a05428e0a4600c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c34263bdbc31015f3890497f8f5b9108438bf0f315f5d509a05428e0a4600c3->enter($__internal_9c34263bdbc31015f3890497f8f5b9108438bf0f315f5d509a05428e0a4600c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_f7ec4e9ac21beca8cf2e9d1d09e181a2b635166db71faaf786e10da74f95ac93->leave($__internal_f7ec4e9ac21beca8cf2e9d1d09e181a2b635166db71faaf786e10da74f95ac93_prof);

        
        $__internal_9c34263bdbc31015f3890497f8f5b9108438bf0f315f5d509a05428e0a4600c3->leave($__internal_9c34263bdbc31015f3890497f8f5b9108438bf0f315f5d509a05428e0a4600c3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkbox_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/checkbox_widget.html.php");
    }
}
