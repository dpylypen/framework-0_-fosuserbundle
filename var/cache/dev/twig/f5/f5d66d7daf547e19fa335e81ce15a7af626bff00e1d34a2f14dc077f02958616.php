<?php

/* KnpMenuBundle::menu.html.twig */
class __TwigTemplate_f7496512dbb56ee8ea326cc85aca2a0a59189e0de195048de2519657e0aef7c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("knp_menu.html.twig", "KnpMenuBundle::menu.html.twig", 1);
        $this->blocks = array(
            'label' => array($this, 'block_label'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "knp_menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d5c11cb6196380f5219859c4576d9a0bbb881547572429f76436796328df4a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d5c11cb6196380f5219859c4576d9a0bbb881547572429f76436796328df4a7->enter($__internal_4d5c11cb6196380f5219859c4576d9a0bbb881547572429f76436796328df4a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "KnpMenuBundle::menu.html.twig"));

        $__internal_46ba98d8480a0484f0505d3286e893c4c80c511725f599bfe6511b9f56324c18 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46ba98d8480a0484f0505d3286e893c4c80c511725f599bfe6511b9f56324c18->enter($__internal_46ba98d8480a0484f0505d3286e893c4c80c511725f599bfe6511b9f56324c18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "KnpMenuBundle::menu.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4d5c11cb6196380f5219859c4576d9a0bbb881547572429f76436796328df4a7->leave($__internal_4d5c11cb6196380f5219859c4576d9a0bbb881547572429f76436796328df4a7_prof);

        
        $__internal_46ba98d8480a0484f0505d3286e893c4c80c511725f599bfe6511b9f56324c18->leave($__internal_46ba98d8480a0484f0505d3286e893c4c80c511725f599bfe6511b9f56324c18_prof);

    }

    // line 3
    public function block_label($context, array $blocks = array())
    {
        $__internal_38f60309ba398caea37a43ff93a0e6f40f668075bf19fb83ae1430fd3d2c2062 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_38f60309ba398caea37a43ff93a0e6f40f668075bf19fb83ae1430fd3d2c2062->enter($__internal_38f60309ba398caea37a43ff93a0e6f40f668075bf19fb83ae1430fd3d2c2062_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_9d88e234cd06308ff737060f986f7750850e542a74cebbfd83ad8e9f7276ff24 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d88e234cd06308ff737060f986f7750850e542a74cebbfd83ad8e9f7276ff24->enter($__internal_9d88e234cd06308ff737060f986f7750850e542a74cebbfd83ad8e9f7276ff24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        // line 4
        $context["translation_domain"] = twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new Twig_Error_Runtime('Variable "item" does not exist.', 4, $this->getSourceContext()); })()), "extra", array(0 => "translation_domain", 1 => "messages"), "method");
        // line 5
        $context["label"] = twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new Twig_Error_Runtime('Variable "item" does not exist.', 5, $this->getSourceContext()); })()), "label", array());
        // line 6
        if ( !((isset($context["translation_domain"]) || array_key_exists("translation_domain", $context) ? $context["translation_domain"] : (function () { throw new Twig_Error_Runtime('Variable "translation_domain" does not exist.', 6, $this->getSourceContext()); })()) === false)) {
            // line 7
            $context["label"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) || array_key_exists("label", $context) ? $context["label"] : (function () { throw new Twig_Error_Runtime('Variable "label" does not exist.', 7, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new Twig_Error_Runtime('Variable "item" does not exist.', 7, $this->getSourceContext()); })()), "extra", array(0 => "translation_params", 1 => array()), "method"), (isset($context["translation_domain"]) || array_key_exists("translation_domain", $context) ? $context["translation_domain"] : (function () { throw new Twig_Error_Runtime('Variable "translation_domain" does not exist.', 7, $this->getSourceContext()); })()));
        }
        // line 9
        if ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new Twig_Error_Runtime('Variable "options" does not exist.', 9, $this->getSourceContext()); })()), "allow_safe_labels", array()) && twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new Twig_Error_Runtime('Variable "item" does not exist.', 9, $this->getSourceContext()); })()), "extra", array(0 => "safe_label", 1 => false), "method"))) {
            echo (isset($context["label"]) || array_key_exists("label", $context) ? $context["label"] : (function () { throw new Twig_Error_Runtime('Variable "label" does not exist.', 9, $this->getSourceContext()); })());
        } else {
            echo twig_escape_filter($this->env, (isset($context["label"]) || array_key_exists("label", $context) ? $context["label"] : (function () { throw new Twig_Error_Runtime('Variable "label" does not exist.', 9, $this->getSourceContext()); })()), "html", null, true);
        }
        
        $__internal_9d88e234cd06308ff737060f986f7750850e542a74cebbfd83ad8e9f7276ff24->leave($__internal_9d88e234cd06308ff737060f986f7750850e542a74cebbfd83ad8e9f7276ff24_prof);

        
        $__internal_38f60309ba398caea37a43ff93a0e6f40f668075bf19fb83ae1430fd3d2c2062->leave($__internal_38f60309ba398caea37a43ff93a0e6f40f668075bf19fb83ae1430fd3d2c2062_prof);

    }

    public function getTemplateName()
    {
        return "KnpMenuBundle::menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 9,  55 => 7,  53 => 6,  51 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'knp_menu.html.twig' %}

{% block label %}
    {%- set translation_domain = item.extra('translation_domain', 'messages') -%}
    {%- set label = item.label -%}
    {%- if translation_domain is not same as(false) -%}
        {%- set label = label|trans(item.extra('translation_params', {}), translation_domain) -%}
    {%- endif -%}
    {%- if options.allow_safe_labels and item.extra('safe_label', false) %}{{ label|raw }}{% else %}{{ label }}{% endif -%}
{% endblock %}
", "KnpMenuBundle::menu.html.twig", "/Users/dp/Sites/frame-0/vendor/knplabs/knp-menu-bundle/Resources/views/menu.html.twig");
    }
}
