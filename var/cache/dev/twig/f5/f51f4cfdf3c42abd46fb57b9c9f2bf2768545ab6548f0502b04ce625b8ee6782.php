<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_15f387980dc216115357ada5d2eb1ab3fea716a95e86ce279d646754d0e25b29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8186cd80c741953717d482465edf4482ecbb41f69d81ce22d2fa1aff2e670e03 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8186cd80c741953717d482465edf4482ecbb41f69d81ce22d2fa1aff2e670e03->enter($__internal_8186cd80c741953717d482465edf4482ecbb41f69d81ce22d2fa1aff2e670e03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $__internal_53e79abe981100e33abadebfce74447189b83301b7a073a1bfc989179dbaa738 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_53e79abe981100e33abadebfce74447189b83301b7a073a1bfc989179dbaa738->enter($__internal_53e79abe981100e33abadebfce74447189b83301b7a073a1bfc989179dbaa738_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8186cd80c741953717d482465edf4482ecbb41f69d81ce22d2fa1aff2e670e03->leave($__internal_8186cd80c741953717d482465edf4482ecbb41f69d81ce22d2fa1aff2e670e03_prof);

        
        $__internal_53e79abe981100e33abadebfce74447189b83301b7a073a1bfc989179dbaa738->leave($__internal_53e79abe981100e33abadebfce74447189b83301b7a073a1bfc989179dbaa738_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_dea8006b9cdceb28bb13d3d19743b993b615dd5ac67458e9253c3c410962ede9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dea8006b9cdceb28bb13d3d19743b993b615dd5ac67458e9253c3c410962ede9->enter($__internal_dea8006b9cdceb28bb13d3d19743b993b615dd5ac67458e9253c3c410962ede9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_66ef93d5311bea032ec46f293b2ecb8007d408190e609f1564a567b3a7f55c14 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_66ef93d5311bea032ec46f293b2ecb8007d408190e609f1564a567b3a7f55c14->enter($__internal_66ef93d5311bea032ec46f293b2ecb8007d408190e609f1564a567b3a7f55c14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_66ef93d5311bea032ec46f293b2ecb8007d408190e609f1564a567b3a7f55c14->leave($__internal_66ef93d5311bea032ec46f293b2ecb8007d408190e609f1564a567b3a7f55c14_prof);

        
        $__internal_dea8006b9cdceb28bb13d3d19743b993b615dd5ac67458e9253c3c410962ede9->leave($__internal_dea8006b9cdceb28bb13d3d19743b993b615dd5ac67458e9253c3c410962ede9_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:list.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list.html.twig");
    }
}
