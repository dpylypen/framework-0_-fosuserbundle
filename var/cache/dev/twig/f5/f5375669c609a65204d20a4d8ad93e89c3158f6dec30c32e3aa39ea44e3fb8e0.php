<?php

/* SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig */
class __TwigTemplate_dc0a3bb237166f8674f42d024f372f3723c4e746d6e0b2a769e33751e2c28086 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:Form:filter_admin_fields.html.twig", "SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Form:filter_admin_fields.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b44f1f94e92c9ea0940b039e8ad187b5dcef04b9f69d092b80bc0a7ce2ac1f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b44f1f94e92c9ea0940b039e8ad187b5dcef04b9f69d092b80bc0a7ce2ac1f9->enter($__internal_6b44f1f94e92c9ea0940b039e8ad187b5dcef04b9f69d092b80bc0a7ce2ac1f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig"));

        $__internal_fc8d02bc47ce4cc0fad22844f41e32a4f973bc628647169b05c9e65709f966c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc8d02bc47ce4cc0fad22844f41e32a4f973bc628647169b05c9e65709f966c9->enter($__internal_fc8d02bc47ce4cc0fad22844f41e32a4f973bc628647169b05c9e65709f966c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6b44f1f94e92c9ea0940b039e8ad187b5dcef04b9f69d092b80bc0a7ce2ac1f9->leave($__internal_6b44f1f94e92c9ea0940b039e8ad187b5dcef04b9f69d092b80bc0a7ce2ac1f9_prof);

        
        $__internal_fc8d02bc47ce4cc0fad22844f41e32a4f973bc628647169b05c9e65709f966c9->leave($__internal_fc8d02bc47ce4cc0fad22844f41e32a4f973bc628647169b05c9e65709f966c9_prof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:Form:filter_admin_fields.html.twig' %}
", "SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/Form/filter_admin_fields.html.twig");
    }
}
