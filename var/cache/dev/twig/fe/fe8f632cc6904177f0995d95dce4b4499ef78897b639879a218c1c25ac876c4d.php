<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_fc55f5b24e9be32c0de1d3c9739935ad03c0dbbf7ac2093713ef6007b6d682d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_618f6577b9aab0337332832e82883eb32a4294668e08079b0c4cda67d7152639 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_618f6577b9aab0337332832e82883eb32a4294668e08079b0c4cda67d7152639->enter($__internal_618f6577b9aab0337332832e82883eb32a4294668e08079b0c4cda67d7152639_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_bf5a6644fd193d6a17873d1c4b7b2d33c5201b99bdf0906350e38aac7cd9af59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf5a6644fd193d6a17873d1c4b7b2d33c5201b99bdf0906350e38aac7cd9af59->enter($__internal_bf5a6644fd193d6a17873d1c4b7b2d33c5201b99bdf0906350e38aac7cd9af59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_618f6577b9aab0337332832e82883eb32a4294668e08079b0c4cda67d7152639->leave($__internal_618f6577b9aab0337332832e82883eb32a4294668e08079b0c4cda67d7152639_prof);

        
        $__internal_bf5a6644fd193d6a17873d1c4b7b2d33c5201b99bdf0906350e38aac7cd9af59->leave($__internal_bf5a6644fd193d6a17873d1c4b7b2d33c5201b99bdf0906350e38aac7cd9af59_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
