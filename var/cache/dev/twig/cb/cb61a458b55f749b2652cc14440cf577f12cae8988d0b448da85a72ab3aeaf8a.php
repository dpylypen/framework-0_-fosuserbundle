<?php

/* SonataBlockBundle:Block:block_exception_debug.html.twig */
class __TwigTemplate_031553328bb8b27f6538586e7b749d7fcad8abd96887cf26045591761790c807 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["sonata_block"]) || array_key_exists("sonata_block", $context) ? $context["sonata_block"] : (function () { throw new Twig_Error_Runtime('Variable "sonata_block" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_exception_debug.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7401f4ceb2221da008966e7b6490775c71c74d3d488ee36a30de0756709cc73e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7401f4ceb2221da008966e7b6490775c71c74d3d488ee36a30de0756709cc73e->enter($__internal_7401f4ceb2221da008966e7b6490775c71c74d3d488ee36a30de0756709cc73e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_exception_debug.html.twig"));

        $__internal_cce002c3ac025636fb8627aaa5a0c231eeec5d51f02ad558c69db0e2da1f679d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cce002c3ac025636fb8627aaa5a0c231eeec5d51f02ad558c69db0e2da1f679d->enter($__internal_cce002c3ac025636fb8627aaa5a0c231eeec5d51f02ad558c69db0e2da1f679d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_exception_debug.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7401f4ceb2221da008966e7b6490775c71c74d3d488ee36a30de0756709cc73e->leave($__internal_7401f4ceb2221da008966e7b6490775c71c74d3d488ee36a30de0756709cc73e_prof);

        
        $__internal_cce002c3ac025636fb8627aaa5a0c231eeec5d51f02ad558c69db0e2da1f679d->leave($__internal_cce002c3ac025636fb8627aaa5a0c231eeec5d51f02ad558c69db0e2da1f679d_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_bc2cfb8fb2e6b9dff552983ad43fe05dc0679f7539b98a86f302786a99b58e23 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc2cfb8fb2e6b9dff552983ad43fe05dc0679f7539b98a86f302786a99b58e23->enter($__internal_bc2cfb8fb2e6b9dff552983ad43fe05dc0679f7539b98a86f302786a99b58e23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_d5f21fc7ddc239cb88a4c409f27e17845689d7c203fb7aceefea23c7a14c2759 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5f21fc7ddc239cb88a4c409f27e17845689d7c203fb7aceefea23c7a14c2759->enter($__internal_d5f21fc7ddc239cb88a4c409f27e17845689d7c203fb7aceefea23c7a14c2759_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <div class=\"cms-block-exception\" ";
        if ((isset($context["forceStyle"]) || array_key_exists("forceStyle", $context) ? $context["forceStyle"] : (function () { throw new Twig_Error_Runtime('Variable "forceStyle" does not exist.', 15, $this->getSourceContext()); })())) {
            echo "style=\"overflow-y: scroll; min-width: 300px; max-height: 300px;\"";
        }
        echo ">

        ";
        // line 18
        echo "        ";
        if ((isset($context["forceStyle"]) || array_key_exists("forceStyle", $context) ? $context["forceStyle"] : (function () { throw new Twig_Error_Runtime('Variable "forceStyle" does not exist.', 18, $this->getSourceContext()); })())) {
            // line 19
            echo "            <link href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception_layout.css"), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css"), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        ";
        }
        // line 22
        echo "        ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "SonataBlockBundle:Block:block_exception_debug.html.twig", 22)->display($context);
        // line 23
        echo "    </div>
";
        
        $__internal_d5f21fc7ddc239cb88a4c409f27e17845689d7c203fb7aceefea23c7a14c2759->leave($__internal_d5f21fc7ddc239cb88a4c409f27e17845689d7c203fb7aceefea23c7a14c2759_prof);

        
        $__internal_bc2cfb8fb2e6b9dff552983ad43fe05dc0679f7539b98a86f302786a99b58e23->leave($__internal_bc2cfb8fb2e6b9dff552983ad43fe05dc0679f7539b98a86f302786a99b58e23_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_exception_debug.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 23,  69 => 22,  64 => 20,  59 => 19,  56 => 18,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    <div class=\"cms-block-exception\" {% if forceStyle %}style=\"overflow-y: scroll; min-width: 300px; max-height: 300px;\"{% endif %}>

        {# this is dirty but the alternative would require a new block-optimized exception css #}
        {% if forceStyle %}
            <link href=\"{{ asset('bundles/framework/css/exception_layout.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=\"{{ asset('bundles/framework/css/exception.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        {% endif %}
        {% include 'TwigBundle:Exception:exception.html.twig' %}
    </div>
{% endblock %}
", "SonataBlockBundle:Block:block_exception_debug.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/block-bundle/Resources/views/Block/block_exception_debug.html.twig");
    }
}
