<?php

/* SonataAdminBundle:CRUD:base_acl.html.twig */
class __TwigTemplate_6e923e4c8bf04507e62a475e7ba9491cbeb6b966def0a2eb8d25fc6867649dd1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'actions' => array($this, 'block_actions'),
            'form' => array($this, 'block_form'),
            'form_acl_roles' => array($this, 'block_form_acl_roles'),
            'form_acl_users' => array($this, 'block_form_acl_users'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) || array_key_exists("base_template", $context) ? $context["base_template"] : (function () { throw new Twig_Error_Runtime('Variable "base_template" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:base_acl.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2292d087a6863b11f95fbb695445f6b50ea4c0ffecc4d2ac181f7a662ebf1b90 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2292d087a6863b11f95fbb695445f6b50ea4c0ffecc4d2ac181f7a662ebf1b90->enter($__internal_2292d087a6863b11f95fbb695445f6b50ea4c0ffecc4d2ac181f7a662ebf1b90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_acl.html.twig"));

        $__internal_b46ec745d48f1db379b9abb21a0b0b7b8f0d657eb30592c5b5aadf9dfbdf1e73 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b46ec745d48f1db379b9abb21a0b0b7b8f0d657eb30592c5b5aadf9dfbdf1e73->enter($__internal_b46ec745d48f1db379b9abb21a0b0b7b8f0d657eb30592c5b5aadf9dfbdf1e73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_acl.html.twig"));

        // line 18
        $context["acl"] = $this->loadTemplate("SonataAdminBundle:CRUD:base_acl_macro.html.twig", "SonataAdminBundle:CRUD:base_acl.html.twig", 18);
        // line 12
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2292d087a6863b11f95fbb695445f6b50ea4c0ffecc4d2ac181f7a662ebf1b90->leave($__internal_2292d087a6863b11f95fbb695445f6b50ea4c0ffecc4d2ac181f7a662ebf1b90_prof);

        
        $__internal_b46ec745d48f1db379b9abb21a0b0b7b8f0d657eb30592c5b5aadf9dfbdf1e73->leave($__internal_b46ec745d48f1db379b9abb21a0b0b7b8f0d657eb30592c5b5aadf9dfbdf1e73_prof);

    }

    // line 14
    public function block_actions($context, array $blocks = array())
    {
        $__internal_143ffc5bc898393b2bc825210692b7b806112e75d5af5303e22020c38b222248 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_143ffc5bc898393b2bc825210692b7b806112e75d5af5303e22020c38b222248->enter($__internal_143ffc5bc898393b2bc825210692b7b806112e75d5af5303e22020c38b222248_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        $__internal_394a0ce01b0fa4a811277ad85254ce57e43ad314943d234023ab723424c84ce0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_394a0ce01b0fa4a811277ad85254ce57e43ad314943d234023ab723424c84ce0->enter($__internal_394a0ce01b0fa4a811277ad85254ce57e43ad314943d234023ab723424c84ce0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:action_buttons.html.twig", "SonataAdminBundle:CRUD:base_acl.html.twig", 15)->display($context);
        
        $__internal_394a0ce01b0fa4a811277ad85254ce57e43ad314943d234023ab723424c84ce0->leave($__internal_394a0ce01b0fa4a811277ad85254ce57e43ad314943d234023ab723424c84ce0_prof);

        
        $__internal_143ffc5bc898393b2bc825210692b7b806112e75d5af5303e22020c38b222248->leave($__internal_143ffc5bc898393b2bc825210692b7b806112e75d5af5303e22020c38b222248_prof);

    }

    // line 20
    public function block_form($context, array $blocks = array())
    {
        $__internal_d81c42413510b3e0a1d1526eb0e94b0bb7e299ee664ba8f3d8fce4190cb0d68b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d81c42413510b3e0a1d1526eb0e94b0bb7e299ee664ba8f3d8fce4190cb0d68b->enter($__internal_d81c42413510b3e0a1d1526eb0e94b0bb7e299ee664ba8f3d8fce4190cb0d68b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        $__internal_029a21c52d72876c31ffcab32e55cb7a7b26faf360320436a491256344853b51 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_029a21c52d72876c31ffcab32e55cb7a7b26faf360320436a491256344853b51->enter($__internal_029a21c52d72876c31ffcab32e55cb7a7b26faf360320436a491256344853b51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 21
        echo "    ";
        $this->displayBlock('form_acl_roles', $context, $blocks);
        // line 24
        echo "    ";
        $this->displayBlock('form_acl_users', $context, $blocks);
        
        $__internal_029a21c52d72876c31ffcab32e55cb7a7b26faf360320436a491256344853b51->leave($__internal_029a21c52d72876c31ffcab32e55cb7a7b26faf360320436a491256344853b51_prof);

        
        $__internal_d81c42413510b3e0a1d1526eb0e94b0bb7e299ee664ba8f3d8fce4190cb0d68b->leave($__internal_d81c42413510b3e0a1d1526eb0e94b0bb7e299ee664ba8f3d8fce4190cb0d68b_prof);

    }

    // line 21
    public function block_form_acl_roles($context, array $blocks = array())
    {
        $__internal_28c0ba5b7be5230eb75fba14478ba84745746ad03260c9efcec493aaf493209a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_28c0ba5b7be5230eb75fba14478ba84745746ad03260c9efcec493aaf493209a->enter($__internal_28c0ba5b7be5230eb75fba14478ba84745746ad03260c9efcec493aaf493209a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_acl_roles"));

        $__internal_f26210f38e00326928cdcbc74c149e104ddbf57e48c730341539fee1e42c6502 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f26210f38e00326928cdcbc74c149e104ddbf57e48c730341539fee1e42c6502->enter($__internal_f26210f38e00326928cdcbc74c149e104ddbf57e48c730341539fee1e42c6502_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_acl_roles"));

        // line 22
        echo "        ";
        echo $context["acl"]->macro_render_form((isset($context["aclRolesForm"]) || array_key_exists("aclRolesForm", $context) ? $context["aclRolesForm"] : (function () { throw new Twig_Error_Runtime('Variable "aclRolesForm" does not exist.', 22, $this->getSourceContext()); })()), (isset($context["permissions"]) || array_key_exists("permissions", $context) ? $context["permissions"] : (function () { throw new Twig_Error_Runtime('Variable "permissions" does not exist.', 22, $this->getSourceContext()); })()), "td_role", (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 22, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["sonata_admin"]) || array_key_exists("sonata_admin", $context) ? $context["sonata_admin"] : (function () { throw new Twig_Error_Runtime('Variable "sonata_admin" does not exist.', 22, $this->getSourceContext()); })()), "adminPool", array()), (isset($context["object"]) || array_key_exists("object", $context) ? $context["object"] : (function () { throw new Twig_Error_Runtime('Variable "object" does not exist.', 22, $this->getSourceContext()); })()));
        echo "
    ";
        
        $__internal_f26210f38e00326928cdcbc74c149e104ddbf57e48c730341539fee1e42c6502->leave($__internal_f26210f38e00326928cdcbc74c149e104ddbf57e48c730341539fee1e42c6502_prof);

        
        $__internal_28c0ba5b7be5230eb75fba14478ba84745746ad03260c9efcec493aaf493209a->leave($__internal_28c0ba5b7be5230eb75fba14478ba84745746ad03260c9efcec493aaf493209a_prof);

    }

    // line 24
    public function block_form_acl_users($context, array $blocks = array())
    {
        $__internal_059ab53075ac0adb52298ffb486e192ca59d74648834ec0da06a5a6d12f62a1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_059ab53075ac0adb52298ffb486e192ca59d74648834ec0da06a5a6d12f62a1f->enter($__internal_059ab53075ac0adb52298ffb486e192ca59d74648834ec0da06a5a6d12f62a1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_acl_users"));

        $__internal_830e4a5184a685f5b7724f07977c7ebf157b9a15d2fb4010538323392fbb9c37 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_830e4a5184a685f5b7724f07977c7ebf157b9a15d2fb4010538323392fbb9c37->enter($__internal_830e4a5184a685f5b7724f07977c7ebf157b9a15d2fb4010538323392fbb9c37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_acl_users"));

        // line 25
        echo "        ";
        echo $context["acl"]->macro_render_form((isset($context["aclUsersForm"]) || array_key_exists("aclUsersForm", $context) ? $context["aclUsersForm"] : (function () { throw new Twig_Error_Runtime('Variable "aclUsersForm" does not exist.', 25, $this->getSourceContext()); })()), (isset($context["permissions"]) || array_key_exists("permissions", $context) ? $context["permissions"] : (function () { throw new Twig_Error_Runtime('Variable "permissions" does not exist.', 25, $this->getSourceContext()); })()), "td_username", (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 25, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["sonata_admin"]) || array_key_exists("sonata_admin", $context) ? $context["sonata_admin"] : (function () { throw new Twig_Error_Runtime('Variable "sonata_admin" does not exist.', 25, $this->getSourceContext()); })()), "adminPool", array()), (isset($context["object"]) || array_key_exists("object", $context) ? $context["object"] : (function () { throw new Twig_Error_Runtime('Variable "object" does not exist.', 25, $this->getSourceContext()); })()));
        echo "
    ";
        
        $__internal_830e4a5184a685f5b7724f07977c7ebf157b9a15d2fb4010538323392fbb9c37->leave($__internal_830e4a5184a685f5b7724f07977c7ebf157b9a15d2fb4010538323392fbb9c37_prof);

        
        $__internal_059ab53075ac0adb52298ffb486e192ca59d74648834ec0da06a5a6d12f62a1f->leave($__internal_059ab53075ac0adb52298ffb486e192ca59d74648834ec0da06a5a6d12f62a1f_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_acl.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 25,  109 => 24,  96 => 22,  87 => 21,  76 => 24,  73 => 21,  64 => 20,  54 => 15,  45 => 14,  35 => 12,  33 => 18,  21 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:action_buttons.html.twig' %}
{%- endblock -%}

{% import 'SonataAdminBundle:CRUD:base_acl_macro.html.twig' as acl %}

{% block form %}
    {% block form_acl_roles %}
        {{ acl.render_form(aclRolesForm, permissions, 'td_role', admin, sonata_admin.adminPool, object) }}
    {% endblock %}
    {% block form_acl_users %}
        {{ acl.render_form(aclUsersForm, permissions, 'td_username', admin, sonata_admin.adminPool, object) }}
    {% endblock %}
{% endblock %}
", "SonataAdminBundle:CRUD:base_acl.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/base_acl.html.twig");
    }
}
