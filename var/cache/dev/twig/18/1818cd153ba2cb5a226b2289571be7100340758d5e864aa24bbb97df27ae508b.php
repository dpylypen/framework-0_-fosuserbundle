<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_5728caa0324b025ca5265cbcdff6da6077035a6aedbeda57bccfe2b688a1e875 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac0d767ae177fc0595bf5e40a842c94326b698150902e9025d522b9fc2bfe5be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ac0d767ae177fc0595bf5e40a842c94326b698150902e9025d522b9fc2bfe5be->enter($__internal_ac0d767ae177fc0595bf5e40a842c94326b698150902e9025d522b9fc2bfe5be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_5cb57957e9ac8124eca5adac19f3a9e1c6451f581c158ae17a7a0aea31f3dec7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5cb57957e9ac8124eca5adac19f3a9e1c6451f581c158ae17a7a0aea31f3dec7->enter($__internal_5cb57957e9ac8124eca5adac19f3a9e1c6451f581c158ae17a7a0aea31f3dec7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })())));
        echo "
";
        
        $__internal_ac0d767ae177fc0595bf5e40a842c94326b698150902e9025d522b9fc2bfe5be->leave($__internal_ac0d767ae177fc0595bf5e40a842c94326b698150902e9025d522b9fc2bfe5be_prof);

        
        $__internal_5cb57957e9ac8124eca5adac19f3a9e1c6451f581c158ae17a7a0aea31f3dec7->leave($__internal_5cb57957e9ac8124eca5adac19f3a9e1c6451f581c158ae17a7a0aea31f3dec7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.rdf.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
