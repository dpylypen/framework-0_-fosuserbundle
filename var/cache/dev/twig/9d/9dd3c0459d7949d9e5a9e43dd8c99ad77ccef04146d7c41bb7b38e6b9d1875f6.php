<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_38ef70ac9aef682180b743ce0e2c8f4eaafc023303036168e6f1abc26ba40934 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_931af91cf03cba6849beb9947322f8cecfd6611ded5469bb344cd11c0fbe4ef1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_931af91cf03cba6849beb9947322f8cecfd6611ded5469bb344cd11c0fbe4ef1->enter($__internal_931af91cf03cba6849beb9947322f8cecfd6611ded5469bb344cd11c0fbe4ef1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        $__internal_6b95f64ffabbcadd840d7c0f83eec0477ffae923aaa379991e3b88658d5f3819 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b95f64ffabbcadd840d7c0f83eec0477ffae923aaa379991e3b88658d5f3819->enter($__internal_6b95f64ffabbcadd840d7c0f83eec0477ffae923aaa379991e3b88658d5f3819_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo json_encode(array("error" => array("code" => (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 1, $this->getSourceContext()); })()), "message" => (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 1, $this->getSourceContext()); })()), "exception" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })()), "toarray", array()))));
        echo "
";
        
        $__internal_931af91cf03cba6849beb9947322f8cecfd6611ded5469bb344cd11c0fbe4ef1->leave($__internal_931af91cf03cba6849beb9947322f8cecfd6611ded5469bb344cd11c0fbe4ef1_prof);

        
        $__internal_6b95f64ffabbcadd840d7c0f83eec0477ffae923aaa379991e3b88658d5f3819->leave($__internal_6b95f64ffabbcadd840d7c0f83eec0477ffae923aaa379991e3b88658d5f3819_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "TwigBundle:Exception:exception.json.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
