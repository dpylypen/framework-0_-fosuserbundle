<?php

/* knp_menu_base.html.twig */
class __TwigTemplate_12d8247652c42208fb6d4c9ec8b9f2706237db182b7b5ce7a2403dcbe4bac48d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ebc3b5150c0ff3425bd94d047cd044c0c0b4457b7b448d5752874d6d1fe81194 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ebc3b5150c0ff3425bd94d047cd044c0c0b4457b7b448d5752874d6d1fe81194->enter($__internal_ebc3b5150c0ff3425bd94d047cd044c0c0b4457b7b448d5752874d6d1fe81194_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "knp_menu_base.html.twig"));

        $__internal_823cb0e5698004bbbf6a9c145576967ab05021d1fc2ff5fa0363f010b6253f5b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_823cb0e5698004bbbf6a9c145576967ab05021d1fc2ff5fa0363f010b6253f5b->enter($__internal_823cb0e5698004bbbf6a9c145576967ab05021d1fc2ff5fa0363f010b6253f5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "knp_menu_base.html.twig"));

        // line 1
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new Twig_Error_Runtime('Variable "options" does not exist.', 1, $this->getSourceContext()); })()), "compressed", array())) {
            $this->displayBlock("compressed_root", $context, $blocks);
        } else {
            $this->displayBlock("root", $context, $blocks);
        }
        
        $__internal_ebc3b5150c0ff3425bd94d047cd044c0c0b4457b7b448d5752874d6d1fe81194->leave($__internal_ebc3b5150c0ff3425bd94d047cd044c0c0b4457b7b448d5752874d6d1fe81194_prof);

        
        $__internal_823cb0e5698004bbbf6a9c145576967ab05021d1fc2ff5fa0363f010b6253f5b->leave($__internal_823cb0e5698004bbbf6a9c145576967ab05021d1fc2ff5fa0363f010b6253f5b_prof);

    }

    public function getTemplateName()
    {
        return "knp_menu_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if options.compressed %}{{ block('compressed_root') }}{% else %}{{ block('root') }}{% endif %}
", "knp_menu_base.html.twig", "/Users/dp/Sites/frame-0/vendor/knplabs/knp-menu/src/Knp/Menu/Resources/views/knp_menu_base.html.twig");
    }
}
