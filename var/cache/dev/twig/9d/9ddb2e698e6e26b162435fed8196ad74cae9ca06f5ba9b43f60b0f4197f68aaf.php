<?php

/* SonataAdminBundle:CRUD:edit_array.html.twig */
class __TwigTemplate_94c2da762256582aa0c248659b8c7c1cb6be7b8c2bd796452e8c8febba687d92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) || array_key_exists("base_template", $context) ? $context["base_template"] : (function () { throw new Twig_Error_Runtime('Variable "base_template" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:edit_array.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3f6fb0e332dcb4e5d3d405ab0e0c87871fba21638523d28f70f51b47643f6ac4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f6fb0e332dcb4e5d3d405ab0e0c87871fba21638523d28f70f51b47643f6ac4->enter($__internal_3f6fb0e332dcb4e5d3d405ab0e0c87871fba21638523d28f70f51b47643f6ac4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_array.html.twig"));

        $__internal_7dc528b6194441398cfcf4efff218b943a6c709c699c95499942eb16ef0089f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7dc528b6194441398cfcf4efff218b943a6c709c699c95499942eb16ef0089f7->enter($__internal_7dc528b6194441398cfcf4efff218b943a6c709c699c95499942eb16ef0089f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_array.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3f6fb0e332dcb4e5d3d405ab0e0c87871fba21638523d28f70f51b47643f6ac4->leave($__internal_3f6fb0e332dcb4e5d3d405ab0e0c87871fba21638523d28f70f51b47643f6ac4_prof);

        
        $__internal_7dc528b6194441398cfcf4efff218b943a6c709c699c95499942eb16ef0089f7->leave($__internal_7dc528b6194441398cfcf4efff218b943a6c709c699c95499942eb16ef0089f7_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_d6832f3f9ee582969d90bab402a4baca108c8ef7a7a4adad1e737532b6ff4df7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d6832f3f9ee582969d90bab402a4baca108c8ef7a7a4adad1e737532b6ff4df7->enter($__internal_d6832f3f9ee582969d90bab402a4baca108c8ef7a7a4adad1e737532b6ff4df7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_56ed19a4fb03f441f3828ada70892de45bb29e57497adf0981cf63a29cfff315 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56ed19a4fb03f441f3828ada70892de45bb29e57497adf0981cf63a29cfff315->enter($__internal_56ed19a4fb03f441f3828ada70892de45bb29e57497adf0981cf63a29cfff315_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <span class=\"edit\">
        ";
        // line 16
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 16, $this->getSourceContext()); })()), 'widget', array("attr" => array("class" => "title")));
        echo "
    </span>
";
        
        $__internal_56ed19a4fb03f441f3828ada70892de45bb29e57497adf0981cf63a29cfff315->leave($__internal_56ed19a4fb03f441f3828ada70892de45bb29e57497adf0981cf63a29cfff315_prof);

        
        $__internal_d6832f3f9ee582969d90bab402a4baca108c8ef7a7a4adad1e737532b6ff4df7->leave($__internal_d6832f3f9ee582969d90bab402a4baca108c8ef7a7a4adad1e737532b6ff4df7_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_array.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block field %}
    <span class=\"edit\">
        {{ form_widget(field_element, {'attr': {'class' : 'title'}}) }}
    </span>
{% endblock %}
", "SonataAdminBundle:CRUD:edit_array.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/edit_array.html.twig");
    }
}
