<?php

/* SonataBlockBundle:Block:block_container.html.twig */
class __TwigTemplate_abe2a3dce5494f2f3c426512fe9349b0447a493272c6e18706b8e570be7cc09e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block_class' => array($this, 'block_block_class'),
            'block_role' => array($this, 'block_block_role'),
            'block' => array($this, 'block_block'),
            'block_child_render' => array($this, 'block_block_child_render'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["sonata_block"]) || array_key_exists("sonata_block", $context) ? $context["sonata_block"] : (function () { throw new Twig_Error_Runtime('Variable "sonata_block" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_container.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3ecb53f77ef1145790bcbccd5d6a89620ed6a071fe8ec9a5c8236cd158af962f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3ecb53f77ef1145790bcbccd5d6a89620ed6a071fe8ec9a5c8236cd158af962f->enter($__internal_3ecb53f77ef1145790bcbccd5d6a89620ed6a071fe8ec9a5c8236cd158af962f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_container.html.twig"));

        $__internal_0bcfac684061e01ab63c0d1d1c9ee34aeee77868d17287aa0d73e51123aa649d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0bcfac684061e01ab63c0d1d1c9ee34aeee77868d17287aa0d73e51123aa649d->enter($__internal_0bcfac684061e01ab63c0d1d1c9ee34aeee77868d17287aa0d73e51123aa649d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_container.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3ecb53f77ef1145790bcbccd5d6a89620ed6a071fe8ec9a5c8236cd158af962f->leave($__internal_3ecb53f77ef1145790bcbccd5d6a89620ed6a071fe8ec9a5c8236cd158af962f_prof);

        
        $__internal_0bcfac684061e01ab63c0d1d1c9ee34aeee77868d17287aa0d73e51123aa649d->leave($__internal_0bcfac684061e01ab63c0d1d1c9ee34aeee77868d17287aa0d73e51123aa649d_prof);

    }

    // line 15
    public function block_block_class($context, array $blocks = array())
    {
        $__internal_5450ded6a8f273d29375e4de168ac6f4a63eadca2c0c6292e5eb572410eb782a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5450ded6a8f273d29375e4de168ac6f4a63eadca2c0c6292e5eb572410eb782a->enter($__internal_5450ded6a8f273d29375e4de168ac6f4a63eadca2c0c6292e5eb572410eb782a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_class"));

        $__internal_26610e03e11065bd2e88f6d9ff1c39b9dc5c96fb08993c6f92261b5ab5b926f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_26610e03e11065bd2e88f6d9ff1c39b9dc5c96fb08993c6f92261b5ab5b926f6->enter($__internal_26610e03e11065bd2e88f6d9ff1c39b9dc5c96fb08993c6f92261b5ab5b926f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_class"));

        echo " cms-container";
        if ( !twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["block"]) || array_key_exists("block", $context) ? $context["block"] : (function () { throw new Twig_Error_Runtime('Variable "block" does not exist.', 15, $this->getSourceContext()); })()), "hasParent", array(), "method")) {
            echo " cms-container-root";
        }
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new Twig_Error_Runtime('Variable "settings" does not exist.', 15, $this->getSourceContext()); })()), "class", array())) {
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["settings"]) || array_key_exists("settings", $context) ? $context["settings"] : (function () { throw new Twig_Error_Runtime('Variable "settings" does not exist.', 15, $this->getSourceContext()); })()), "class", array()), "html", null, true);
        }
        
        $__internal_26610e03e11065bd2e88f6d9ff1c39b9dc5c96fb08993c6f92261b5ab5b926f6->leave($__internal_26610e03e11065bd2e88f6d9ff1c39b9dc5c96fb08993c6f92261b5ab5b926f6_prof);

        
        $__internal_5450ded6a8f273d29375e4de168ac6f4a63eadca2c0c6292e5eb572410eb782a->leave($__internal_5450ded6a8f273d29375e4de168ac6f4a63eadca2c0c6292e5eb572410eb782a_prof);

    }

    // line 18
    public function block_block_role($context, array $blocks = array())
    {
        $__internal_4d96a26d779aeecb697296247424c22828ce2044a7ab43a509fc8a7a71410ef4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d96a26d779aeecb697296247424c22828ce2044a7ab43a509fc8a7a71410ef4->enter($__internal_4d96a26d779aeecb697296247424c22828ce2044a7ab43a509fc8a7a71410ef4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_role"));

        $__internal_bd586bc5be1ff81be4203d763738f12e417ebe02608b14381164537a3860dc64 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd586bc5be1ff81be4203d763738f12e417ebe02608b14381164537a3860dc64->enter($__internal_bd586bc5be1ff81be4203d763738f12e417ebe02608b14381164537a3860dc64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_role"));

        echo "container";
        
        $__internal_bd586bc5be1ff81be4203d763738f12e417ebe02608b14381164537a3860dc64->leave($__internal_bd586bc5be1ff81be4203d763738f12e417ebe02608b14381164537a3860dc64_prof);

        
        $__internal_4d96a26d779aeecb697296247424c22828ce2044a7ab43a509fc8a7a71410ef4->leave($__internal_4d96a26d779aeecb697296247424c22828ce2044a7ab43a509fc8a7a71410ef4_prof);

    }

    // line 21
    public function block_block($context, array $blocks = array())
    {
        $__internal_0132342490d27dc2c926143774929b2c48ba02b05c8af431491f9bce2d873358 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0132342490d27dc2c926143774929b2c48ba02b05c8af431491f9bce2d873358->enter($__internal_0132342490d27dc2c926143774929b2c48ba02b05c8af431491f9bce2d873358_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_6bb40b991b0a0eeba7a896fbef33fa6cff6887c326604817704098f1c7953554 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6bb40b991b0a0eeba7a896fbef33fa6cff6887c326604817704098f1c7953554->enter($__internal_6bb40b991b0a0eeba7a896fbef33fa6cff6887c326604817704098f1c7953554_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 22
        echo "    ";
        if ((isset($context["decorator"]) || array_key_exists("decorator", $context) ? $context["decorator"] : (function () { throw new Twig_Error_Runtime('Variable "decorator" does not exist.', 22, $this->getSourceContext()); })())) {
            echo twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["decorator"]) || array_key_exists("decorator", $context) ? $context["decorator"] : (function () { throw new Twig_Error_Runtime('Variable "decorator" does not exist.', 22, $this->getSourceContext()); })()), "pre", array());
        }
        // line 23
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["block"]) || array_key_exists("block", $context) ? $context["block"] : (function () { throw new Twig_Error_Runtime('Variable "block" does not exist.', 23, $this->getSourceContext()); })()), "children", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 24
            echo "        ";
            $this->displayBlock('block_child_render', $context, $blocks);
            // line 27
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "    ";
        if ((isset($context["decorator"]) || array_key_exists("decorator", $context) ? $context["decorator"] : (function () { throw new Twig_Error_Runtime('Variable "decorator" does not exist.', 28, $this->getSourceContext()); })())) {
            echo twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["decorator"]) || array_key_exists("decorator", $context) ? $context["decorator"] : (function () { throw new Twig_Error_Runtime('Variable "decorator" does not exist.', 28, $this->getSourceContext()); })()), "post", array());
        }
        
        $__internal_6bb40b991b0a0eeba7a896fbef33fa6cff6887c326604817704098f1c7953554->leave($__internal_6bb40b991b0a0eeba7a896fbef33fa6cff6887c326604817704098f1c7953554_prof);

        
        $__internal_0132342490d27dc2c926143774929b2c48ba02b05c8af431491f9bce2d873358->leave($__internal_0132342490d27dc2c926143774929b2c48ba02b05c8af431491f9bce2d873358_prof);

    }

    // line 24
    public function block_block_child_render($context, array $blocks = array())
    {
        $__internal_2f9a8ca57dc259018492462b6719546e8d1d81b2f6fcfcc6253a6bebda105d4b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f9a8ca57dc259018492462b6719546e8d1d81b2f6fcfcc6253a6bebda105d4b->enter($__internal_2f9a8ca57dc259018492462b6719546e8d1d81b2f6fcfcc6253a6bebda105d4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_child_render"));

        $__internal_5338ea009c900b92e73dcf574d5cffaf82cf1e17362567c42e00a9d4121b5684 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5338ea009c900b92e73dcf574d5cffaf82cf1e17362567c42e00a9d4121b5684->enter($__internal_5338ea009c900b92e73dcf574d5cffaf82cf1e17362567c42e00a9d4121b5684_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_child_render"));

        // line 25
        echo "            ";
        echo call_user_func_array($this->env->getFunction('sonata_block_render')->getCallable(), array((isset($context["child"]) || array_key_exists("child", $context) ? $context["child"] : (function () { throw new Twig_Error_Runtime('Variable "child" does not exist.', 25, $this->getSourceContext()); })())));
        echo "
        ";
        
        $__internal_5338ea009c900b92e73dcf574d5cffaf82cf1e17362567c42e00a9d4121b5684->leave($__internal_5338ea009c900b92e73dcf574d5cffaf82cf1e17362567c42e00a9d4121b5684_prof);

        
        $__internal_2f9a8ca57dc259018492462b6719546e8d1d81b2f6fcfcc6253a6bebda105d4b->leave($__internal_2f9a8ca57dc259018492462b6719546e8d1d81b2f6fcfcc6253a6bebda105d4b_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_container.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 25,  147 => 24,  134 => 28,  120 => 27,  117 => 24,  99 => 23,  94 => 22,  85 => 21,  67 => 18,  42 => 15,  21 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{# block classes are prepended with a container class #}
{% block block_class %} cms-container{% if not block.hasParent() %} cms-container-root{%endif%}{% if settings.class %} {{ settings.class }}{% endif %}{% endblock %}

{# identify a block role used by the page editor #}
{% block block_role %}container{% endblock %}

{# render container block #}
{% block block %}
    {% if decorator %}{{ decorator.pre|raw }}{% endif %}
    {% for child in block.children %}
        {% block block_child_render %}
            {{ sonata_block_render(child) }}
        {% endblock %}
    {% endfor %}
    {% if decorator %}{{ decorator.post|raw }}{% endif %}
{% endblock %}
", "SonataBlockBundle:Block:block_container.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/block-bundle/Resources/views/Block/block_container.html.twig");
    }
}
