<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_e6ea364fd9e2b51093af6d02ef0978eacadb12c62619770c8fd207b74cbead05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aafff5e2735868656f8a4274bacd6f1e0e025b67acb3c9a18c2af5e503db16e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aafff5e2735868656f8a4274bacd6f1e0e025b67acb3c9a18c2af5e503db16e8->enter($__internal_aafff5e2735868656f8a4274bacd6f1e0e025b67acb3c9a18c2af5e503db16e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_03f245d102a08870c00245f2b3e81a15d73c07df235064e1db47fc50f16120e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_03f245d102a08870c00245f2b3e81a15d73c07df235064e1db47fc50f16120e1->enter($__internal_03f245d102a08870c00245f2b3e81a15d73c07df235064e1db47fc50f16120e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_aafff5e2735868656f8a4274bacd6f1e0e025b67acb3c9a18c2af5e503db16e8->leave($__internal_aafff5e2735868656f8a4274bacd6f1e0e025b67acb3c9a18c2af5e503db16e8_prof);

        
        $__internal_03f245d102a08870c00245f2b3e81a15d73c07df235064e1db47fc50f16120e1->leave($__internal_03f245d102a08870c00245f2b3e81a15d73c07df235064e1db47fc50f16120e1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
