<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_14fbc10fd236263cf797fb3b88b4a9051bda298252f4e40e7ce3b77295ba70f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_32352dc2d4759b01d40f0373570540c229f7d572096afd35317b33163e8bb28f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_32352dc2d4759b01d40f0373570540c229f7d572096afd35317b33163e8bb28f->enter($__internal_32352dc2d4759b01d40f0373570540c229f7d572096afd35317b33163e8bb28f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_6903f4420f80175c2810313c95d05a956d00cc04b24f0047a06f93fb927830ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6903f4420f80175c2810313c95d05a956d00cc04b24f0047a06f93fb927830ad->enter($__internal_6903f4420f80175c2810313c95d05a956d00cc04b24f0047a06f93fb927830ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_32352dc2d4759b01d40f0373570540c229f7d572096afd35317b33163e8bb28f->leave($__internal_32352dc2d4759b01d40f0373570540c229f7d572096afd35317b33163e8bb28f_prof);

        
        $__internal_6903f4420f80175c2810313c95d05a956d00cc04b24f0047a06f93fb927830ad->leave($__internal_6903f4420f80175c2810313c95d05a956d00cc04b24f0047a06f93fb927830ad_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
