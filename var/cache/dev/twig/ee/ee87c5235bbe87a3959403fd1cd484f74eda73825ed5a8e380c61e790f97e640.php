<?php

/* @Framework/Form/form_label.html.php */
class __TwigTemplate_89f46101038343584709dae3170c8b03ab2b21d99d66c44808e0f1b8da4fb97e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_81c22c7ca4d8f998a29c668caef9728105a990fb9da0dc46a2c8e67c292a9466 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_81c22c7ca4d8f998a29c668caef9728105a990fb9da0dc46a2c8e67c292a9466->enter($__internal_81c22c7ca4d8f998a29c668caef9728105a990fb9da0dc46a2c8e67c292a9466_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_label.html.php"));

        $__internal_7b496ee446c65b16c75b26f4504b7cc0b99c7e5836485af103f7c70eb7bb6546 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b496ee446c65b16c75b26f4504b7cc0b99c7e5836485af103f7c70eb7bb6546->enter($__internal_7b496ee446c65b16c75b26f4504b7cc0b99c7e5836485af103f7c70eb7bb6546_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_label.html.php"));

        // line 1
        echo "<?php if (false !== \$label): ?>
<?php if (\$required) { \$label_attr['class'] = trim((isset(\$label_attr['class']) ? \$label_attr['class'] : '').' required'); } ?>
<?php if (!\$compound) { \$label_attr['for'] = \$id; } ?>
<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<label<?php if (\$label_attr) { echo ' '.\$view['form']->block(\$form, 'attributes', array('attr' => \$label_attr)); } ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></label>
<?php endif ?>
";
        
        $__internal_81c22c7ca4d8f998a29c668caef9728105a990fb9da0dc46a2c8e67c292a9466->leave($__internal_81c22c7ca4d8f998a29c668caef9728105a990fb9da0dc46a2c8e67c292a9466_prof);

        
        $__internal_7b496ee446c65b16c75b26f4504b7cc0b99c7e5836485af103f7c70eb7bb6546->leave($__internal_7b496ee446c65b16c75b26f4504b7cc0b99c7e5836485af103f7c70eb7bb6546_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_label.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (false !== \$label): ?>
<?php if (\$required) { \$label_attr['class'] = trim((isset(\$label_attr['class']) ? \$label_attr['class'] : '').' required'); } ?>
<?php if (!\$compound) { \$label_attr['for'] = \$id; } ?>
<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<label<?php if (\$label_attr) { echo ' '.\$view['form']->block(\$form, 'attributes', array('attr' => \$label_attr)); } ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></label>
<?php endif ?>
", "@Framework/Form/form_label.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_label.html.php");
    }
}
