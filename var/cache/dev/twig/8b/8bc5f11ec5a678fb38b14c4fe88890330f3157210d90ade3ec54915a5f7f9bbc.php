<?php

/* FOSUserBundle:Profile:show_content.html.twig */
class __TwigTemplate_73a2fb01174ddb04022388b43cab5ffc17bc6d989cded01ee45a670453c6ef2b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4ad51f5124ebf3d7a77fa10eb0b80870c9688ca2085e6d7d95a6867efeb6aed1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ad51f5124ebf3d7a77fa10eb0b80870c9688ca2085e6d7d95a6867efeb6aed1->enter($__internal_4ad51f5124ebf3d7a77fa10eb0b80870c9688ca2085e6d7d95a6867efeb6aed1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        $__internal_a48dc7342dc5cc289fbfc71def96e03a1c0f7bbadaa2e41b6a83db60a3b5257a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a48dc7342dc5cc289fbfc71def96e03a1c0f7bbadaa2e41b6a83db60a3b5257a->enter($__internal_a48dc7342dc5cc289fbfc71def96e03a1c0f7bbadaa2e41b6a83db60a3b5257a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_user_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 4, $this->getSourceContext()); })()), "username", array()), "html", null, true);
        echo "</p>
    <p>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 5, $this->getSourceContext()); })()), "email", array()), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_4ad51f5124ebf3d7a77fa10eb0b80870c9688ca2085e6d7d95a6867efeb6aed1->leave($__internal_4ad51f5124ebf3d7a77fa10eb0b80870c9688ca2085e6d7d95a6867efeb6aed1_prof);

        
        $__internal_a48dc7342dc5cc289fbfc71def96e03a1c0f7bbadaa2e41b6a83db60a3b5257a->leave($__internal_a48dc7342dc5cc289fbfc71def96e03a1c0f7bbadaa2e41b6a83db60a3b5257a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 5,  29 => 4,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_user_show\">
    <p>{{ 'profile.show.username'|trans }}: {{ user.username }}</p>
    <p>{{ 'profile.show.email'|trans }}: {{ user.email }}</p>
</div>
", "FOSUserBundle:Profile:show_content.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show_content.html.twig");
    }
}
