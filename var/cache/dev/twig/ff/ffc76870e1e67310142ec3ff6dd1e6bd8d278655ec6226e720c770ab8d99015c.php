<?php

/* SonataAdminBundle:CRUD:base_inline_edit_field.html.twig */
class __TwigTemplate_2f9f1d26fc4698b18d5ea20bbdbc7a3d7083e563676d0ddf887fb5a840713744 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'label' => array($this, 'block_label'),
            'field' => array($this, 'block_field'),
            'errors' => array($this, 'block_errors'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0be8dca8118df06612f9224bf0a2e3f860453a63b8a0e782d774efec7648f355 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0be8dca8118df06612f9224bf0a2e3f860453a63b8a0e782d774efec7648f355->enter($__internal_0be8dca8118df06612f9224bf0a2e3f860453a63b8a0e782d774efec7648f355_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_inline_edit_field.html.twig"));

        $__internal_0830cb43bbe80b1e83707ad54fff4411e0ccb9f1365921dca9578361863fe6c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0830cb43bbe80b1e83707ad54fff4411e0ccb9f1365921dca9578361863fe6c6->enter($__internal_0830cb43bbe80b1e83707ad54fff4411e0ccb9f1365921dca9578361863fe6c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_inline_edit_field.html.twig"));

        // line 11
        echo "
<div id=\"sonata-ba-field-container-";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 12, $this->getSourceContext()); })()), "vars", array()), "id", array()), "html", null, true);
        echo "\" class=\"sonata-ba-field sonata-ba-field-";
        echo twig_escape_filter($this->env, (isset($context["edit"]) || array_key_exists("edit", $context) ? $context["edit"] : (function () { throw new Twig_Error_Runtime('Variable "edit" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, (isset($context["inline"]) || array_key_exists("inline", $context) ? $context["inline"] : (function () { throw new Twig_Error_Runtime('Variable "inline" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 12, $this->getSourceContext()); })()), "vars", array()), "errors", array()))) {
            echo "sonata-ba-field-error";
        }
        echo "\">

    ";
        // line 14
        $this->displayBlock('label', $context, $blocks);
        // line 24
        echo "
    ";
        // line 25
        $this->displayBlock('field', $context, $blocks);
        // line 26
        echo "
    <div class=\"sonata-ba-field-error-messages\">
        ";
        // line 28
        $this->displayBlock('errors', $context, $blocks);
        // line 29
        echo "    </div>
</div>
";
        
        $__internal_0be8dca8118df06612f9224bf0a2e3f860453a63b8a0e782d774efec7648f355->leave($__internal_0be8dca8118df06612f9224bf0a2e3f860453a63b8a0e782d774efec7648f355_prof);

        
        $__internal_0830cb43bbe80b1e83707ad54fff4411e0ccb9f1365921dca9578361863fe6c6->leave($__internal_0830cb43bbe80b1e83707ad54fff4411e0ccb9f1365921dca9578361863fe6c6_prof);

    }

    // line 14
    public function block_label($context, array $blocks = array())
    {
        $__internal_2393d941a09fa8c39cc0fbe17c3be93cdda12688d579f867df2cfb5a668933f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2393d941a09fa8c39cc0fbe17c3be93cdda12688d579f867df2cfb5a668933f2->enter($__internal_2393d941a09fa8c39cc0fbe17c3be93cdda12688d579f867df2cfb5a668933f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_6da1ce6348a42ace69771b60fe100db9a214d1913d6d40b4fa9e60120e23c627 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6da1ce6348a42ace69771b60fe100db9a214d1913d6d40b4fa9e60120e23c627->enter($__internal_6da1ce6348a42ace69771b60fe100db9a214d1913d6d40b4fa9e60120e23c627_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        // line 15
        echo "        ";
        if (((isset($context["inline"]) || array_key_exists("inline", $context) ? $context["inline"] : (function () { throw new Twig_Error_Runtime('Variable "inline" does not exist.', 15, $this->getSourceContext()); })()) == "natural")) {
            // line 16
            echo "            ";
            if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "name", array(), "any", true, true)) {
                // line 17
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 17, $this->getSourceContext()); })()), 'label', (twig_test_empty($_label_ = twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 17, $this->getSourceContext()); })()), "options", array()), "name", array())) ? array() : array("label" => $_label_)));
                echo "
            ";
            } else {
                // line 19
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 19, $this->getSourceContext()); })()), 'label');
                echo "
            ";
            }
            // line 21
            echo "            <br>
        ";
        }
        // line 23
        echo "    ";
        
        $__internal_6da1ce6348a42ace69771b60fe100db9a214d1913d6d40b4fa9e60120e23c627->leave($__internal_6da1ce6348a42ace69771b60fe100db9a214d1913d6d40b4fa9e60120e23c627_prof);

        
        $__internal_2393d941a09fa8c39cc0fbe17c3be93cdda12688d579f867df2cfb5a668933f2->leave($__internal_2393d941a09fa8c39cc0fbe17c3be93cdda12688d579f867df2cfb5a668933f2_prof);

    }

    // line 25
    public function block_field($context, array $blocks = array())
    {
        $__internal_14a1b1625a4fd445b97bf8b7fbe1d3f4680833cd2a3c5039b27edc5956eb6c9a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_14a1b1625a4fd445b97bf8b7fbe1d3f4680833cd2a3c5039b27edc5956eb6c9a->enter($__internal_14a1b1625a4fd445b97bf8b7fbe1d3f4680833cd2a3c5039b27edc5956eb6c9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_32518a4b40eaa9aa768d97d362a4459ad4751093c9cca9940bcef881bc76ea05 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32518a4b40eaa9aa768d97d362a4459ad4751093c9cca9940bcef881bc76ea05->enter($__internal_32518a4b40eaa9aa768d97d362a4459ad4751093c9cca9940bcef881bc76ea05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 25, $this->getSourceContext()); })()), 'widget');
        
        $__internal_32518a4b40eaa9aa768d97d362a4459ad4751093c9cca9940bcef881bc76ea05->leave($__internal_32518a4b40eaa9aa768d97d362a4459ad4751093c9cca9940bcef881bc76ea05_prof);

        
        $__internal_14a1b1625a4fd445b97bf8b7fbe1d3f4680833cd2a3c5039b27edc5956eb6c9a->leave($__internal_14a1b1625a4fd445b97bf8b7fbe1d3f4680833cd2a3c5039b27edc5956eb6c9a_prof);

    }

    // line 28
    public function block_errors($context, array $blocks = array())
    {
        $__internal_dde1eec15c0b6ec8fc24aa5e162ae1d300b73d3facce2ff39dbaa7cb4222bf50 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dde1eec15c0b6ec8fc24aa5e162ae1d300b73d3facce2ff39dbaa7cb4222bf50->enter($__internal_dde1eec15c0b6ec8fc24aa5e162ae1d300b73d3facce2ff39dbaa7cb4222bf50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "errors"));

        $__internal_51eb6ccad5621b2a30c9d48a1cf398198b2227b1034d38d54987edb5c1518a0f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51eb6ccad5621b2a30c9d48a1cf398198b2227b1034d38d54987edb5c1518a0f->enter($__internal_51eb6ccad5621b2a30c9d48a1cf398198b2227b1034d38d54987edb5c1518a0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "errors"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) || array_key_exists("field_element", $context) ? $context["field_element"] : (function () { throw new Twig_Error_Runtime('Variable "field_element" does not exist.', 28, $this->getSourceContext()); })()), 'errors');
        
        $__internal_51eb6ccad5621b2a30c9d48a1cf398198b2227b1034d38d54987edb5c1518a0f->leave($__internal_51eb6ccad5621b2a30c9d48a1cf398198b2227b1034d38d54987edb5c1518a0f_prof);

        
        $__internal_dde1eec15c0b6ec8fc24aa5e162ae1d300b73d3facce2ff39dbaa7cb4222bf50->leave($__internal_dde1eec15c0b6ec8fc24aa5e162ae1d300b73d3facce2ff39dbaa7cb4222bf50_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_inline_edit_field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 28,  110 => 25,  100 => 23,  96 => 21,  90 => 19,  84 => 17,  81 => 16,  78 => 15,  69 => 14,  57 => 29,  55 => 28,  51 => 26,  49 => 25,  46 => 24,  44 => 14,  31 => 12,  28 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<div id=\"sonata-ba-field-container-{{ field_element.vars.id }}\" class=\"sonata-ba-field sonata-ba-field-{{ edit }}-{{ inline }} {% if field_element.vars.errors|length %}sonata-ba-field-error{% endif %}\">

    {% block label %}
        {% if inline == 'natural' %}
            {% if field_description.options.name is defined %}
                {{ form_label(field_element, field_description.options.name) }}
            {% else %}
                {{ form_label(field_element) }}
            {% endif %}
            <br>
        {% endif %}
    {% endblock %}

    {% block field %}{{ form_widget(field_element) }}{% endblock %}

    <div class=\"sonata-ba-field-error-messages\">
        {% block errors %}{{ form_errors(field_element) }}{% endblock %}
    </div>
</div>
", "SonataAdminBundle:CRUD:base_inline_edit_field.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/base_inline_edit_field.html.twig");
    }
}
