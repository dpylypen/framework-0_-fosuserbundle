<?php

/* SonataAdminBundle:CRUD:list_inner_row.html.twig */
class __TwigTemplate_f7e7cb8a462b2da94c881711e7d48459d2b7250b3cad5c885c9a1ce82ecee5aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_list_inner_row.html.twig", "SonataAdminBundle:CRUD:list_inner_row.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_list_inner_row.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ff6d8bdd6b55ad28b2dae123cc0b4417897ce88ea9e70666381aaf27451434a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff6d8bdd6b55ad28b2dae123cc0b4417897ce88ea9e70666381aaf27451434a2->enter($__internal_ff6d8bdd6b55ad28b2dae123cc0b4417897ce88ea9e70666381aaf27451434a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_inner_row.html.twig"));

        $__internal_f2ceb8b9485144fff268aee1788dcd1c0f1e3b1c3138d99d3a0421d712c03e93 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2ceb8b9485144fff268aee1788dcd1c0f1e3b1c3138d99d3a0421d712c03e93->enter($__internal_f2ceb8b9485144fff268aee1788dcd1c0f1e3b1c3138d99d3a0421d712c03e93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_inner_row.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ff6d8bdd6b55ad28b2dae123cc0b4417897ce88ea9e70666381aaf27451434a2->leave($__internal_ff6d8bdd6b55ad28b2dae123cc0b4417897ce88ea9e70666381aaf27451434a2_prof);

        
        $__internal_f2ceb8b9485144fff268aee1788dcd1c0f1e3b1c3138d99d3a0421d712c03e93->leave($__internal_f2ceb8b9485144fff268aee1788dcd1c0f1e3b1c3138d99d3a0421d712c03e93_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_inner_row.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_list_inner_row.html.twig' %}
", "SonataAdminBundle:CRUD:list_inner_row.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list_inner_row.html.twig");
    }
}
