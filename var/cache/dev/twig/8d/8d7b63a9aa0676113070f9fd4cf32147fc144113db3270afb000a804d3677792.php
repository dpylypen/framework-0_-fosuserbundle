<?php

/* SonataAdminBundle:CRUD:list_trans.html.twig */
class __TwigTemplate_ac3c725896af35c70e2b8c3836392e573e4cbe429fe96da24d8ae55f36dac5e5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_trans.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0d487b11426ba54bf930f9c51c6e53960937a128c7fa7a22484f7ab222f140e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0d487b11426ba54bf930f9c51c6e53960937a128c7fa7a22484f7ab222f140e->enter($__internal_a0d487b11426ba54bf930f9c51c6e53960937a128c7fa7a22484f7ab222f140e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_trans.html.twig"));

        $__internal_4fe9862370dde4d12408909878587dcd889a8cbbe9e6f52b27b5302d723a142a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4fe9862370dde4d12408909878587dcd889a8cbbe9e6f52b27b5302d723a142a->enter($__internal_4fe9862370dde4d12408909878587dcd889a8cbbe9e6f52b27b5302d723a142a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_trans.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a0d487b11426ba54bf930f9c51c6e53960937a128c7fa7a22484f7ab222f140e->leave($__internal_a0d487b11426ba54bf930f9c51c6e53960937a128c7fa7a22484f7ab222f140e_prof);

        
        $__internal_4fe9862370dde4d12408909878587dcd889a8cbbe9e6f52b27b5302d723a142a->leave($__internal_4fe9862370dde4d12408909878587dcd889a8cbbe9e6f52b27b5302d723a142a_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_fa81b5b94b9dc387e91a8fdd9f9b990612f632763bfb824d064d5c0168e9407f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa81b5b94b9dc387e91a8fdd9f9b990612f632763bfb824d064d5c0168e9407f->enter($__internal_fa81b5b94b9dc387e91a8fdd9f9b990612f632763bfb824d064d5c0168e9407f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_b46cd29f48a4bb204fcbcd1c7c5a7b621e5dd38aadfb29d839baddc76a3ca659 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b46cd29f48a4bb204fcbcd1c7c5a7b621e5dd38aadfb29d839baddc76a3ca659->enter($__internal_b46cd29f48a4bb204fcbcd1c7c5a7b621e5dd38aadfb29d839baddc76a3ca659_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["translationDomain"] = ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "catalogue", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "catalogue", array()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "translationDomain", array()))) : (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "translationDomain", array())));
        // line 16
        echo "    ";
        $context["valueFormat"] = ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "format", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "format", array()), "%s")) : ("%s"));
        // line 17
        echo "
    ";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(sprintf((isset($context["valueFormat"]) || array_key_exists("valueFormat", $context) ? $context["valueFormat"] : (function () { throw new Twig_Error_Runtime('Variable "valueFormat" does not exist.', 18, $this->getSourceContext()); })()), (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 18, $this->getSourceContext()); })())), array(), (isset($context["translationDomain"]) || array_key_exists("translationDomain", $context) ? $context["translationDomain"] : (function () { throw new Twig_Error_Runtime('Variable "translationDomain" does not exist.', 18, $this->getSourceContext()); })())), "html", null, true);
        echo "
";
        
        $__internal_b46cd29f48a4bb204fcbcd1c7c5a7b621e5dd38aadfb29d839baddc76a3ca659->leave($__internal_b46cd29f48a4bb204fcbcd1c7c5a7b621e5dd38aadfb29d839baddc76a3ca659_prof);

        
        $__internal_fa81b5b94b9dc387e91a8fdd9f9b990612f632763bfb824d064d5c0168e9407f->leave($__internal_fa81b5b94b9dc387e91a8fdd9f9b990612f632763bfb824d064d5c0168e9407f_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_trans.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 18,  54 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field%}
    {% set translationDomain = field_description.options.catalogue|default(admin.translationDomain) %}
    {% set valueFormat = field_description.options.format|default('%s') %}

    {{valueFormat|format(value)|trans({}, translationDomain)}}
{% endblock %}
", "SonataAdminBundle:CRUD:list_trans.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list_trans.html.twig");
    }
}
