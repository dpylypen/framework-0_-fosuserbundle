<?php

/* SonataAdminBundle:CRUD:show_array.html.twig */
class __TwigTemplate_722d19a7ab81837f91e1e32a6682a74fc768da3b3350542737bf54d8ab226fe2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 13
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_array.html.twig", 13);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c97114e18d2ef77d728a1db4ac8eeb4010aca879582a1b8dd7c3bb1bd6a05ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8c97114e18d2ef77d728a1db4ac8eeb4010aca879582a1b8dd7c3bb1bd6a05ef->enter($__internal_8c97114e18d2ef77d728a1db4ac8eeb4010aca879582a1b8dd7c3bb1bd6a05ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_array.html.twig"));

        $__internal_816d1355256116838a994cba19132a7f04b7641aa9de52f06e073eb952b2eb3a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_816d1355256116838a994cba19132a7f04b7641aa9de52f06e073eb952b2eb3a->enter($__internal_816d1355256116838a994cba19132a7f04b7641aa9de52f06e073eb952b2eb3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_array.html.twig"));

        // line 11
        $context["show"] = $this->loadTemplate("SonataAdminBundle:CRUD:base_array_macro.html.twig", "SonataAdminBundle:CRUD:show_array.html.twig", 11);
        // line 13
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8c97114e18d2ef77d728a1db4ac8eeb4010aca879582a1b8dd7c3bb1bd6a05ef->leave($__internal_8c97114e18d2ef77d728a1db4ac8eeb4010aca879582a1b8dd7c3bb1bd6a05ef_prof);

        
        $__internal_816d1355256116838a994cba19132a7f04b7641aa9de52f06e073eb952b2eb3a->leave($__internal_816d1355256116838a994cba19132a7f04b7641aa9de52f06e073eb952b2eb3a_prof);

    }

    // line 15
    public function block_field($context, array $blocks = array())
    {
        $__internal_deb2e88da29cf664338d8fec8cdd325d8dcb5eb07ffb501c5d51218abed6f0b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_deb2e88da29cf664338d8fec8cdd325d8dcb5eb07ffb501c5d51218abed6f0b6->enter($__internal_deb2e88da29cf664338d8fec8cdd325d8dcb5eb07ffb501c5d51218abed6f0b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_570dec52bd61155d9fa26dbe0881379acb9f231e5c5b06928650caaf3f44ba19 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_570dec52bd61155d9fa26dbe0881379acb9f231e5c5b06928650caaf3f44ba19->enter($__internal_570dec52bd61155d9fa26dbe0881379acb9f231e5c5b06928650caaf3f44ba19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 16
        echo "    ";
        echo $context["show"]->macro_render_array((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()), ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "inline", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "inline", array()), false)) : (false)));
        echo "
";
        
        $__internal_570dec52bd61155d9fa26dbe0881379acb9f231e5c5b06928650caaf3f44ba19->leave($__internal_570dec52bd61155d9fa26dbe0881379acb9f231e5c5b06928650caaf3f44ba19_prof);

        
        $__internal_deb2e88da29cf664338d8fec8cdd325d8dcb5eb07ffb501c5d51218abed6f0b6->leave($__internal_deb2e88da29cf664338d8fec8cdd325d8dcb5eb07ffb501c5d51218abed6f0b6_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_array.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 16,  43 => 15,  33 => 13,  31 => 11,  11 => 13,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% import 'SonataAdminBundle:CRUD:base_array_macro.html.twig' as show %}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field%}
    {{ show.render_array(value, field_description.options.inline|default(false)) }}
{% endblock %}
", "SonataAdminBundle:CRUD:show_array.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/show_array.html.twig");
    }
}
