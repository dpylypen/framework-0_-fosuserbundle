<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_a7ef6d6055e3cbacb6888671280b9151aa065be55e914b12ab4c335cf18c26ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c368c954493d2448f80d5c0b78a99faf20a6e0a34a3f7866deed92731ce40a9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2c368c954493d2448f80d5c0b78a99faf20a6e0a34a3f7866deed92731ce40a9->enter($__internal_2c368c954493d2448f80d5c0b78a99faf20a6e0a34a3f7866deed92731ce40a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        $__internal_12dcd186e5bef831f866c8439a9eecd599931fac30e6670742b435870c011241 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12dcd186e5bef831f866c8439a9eecd599931fac30e6670742b435870c011241->enter($__internal_12dcd186e5bef831f866c8439a9eecd599931fac30e6670742b435870c011241_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php foreach (\$attr as \$k => \$v): ?>
<?php if ('placeholder' === \$k || 'title' === \$k): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$v, array(), \$translation_domain) : \$v)) ?>
<?php elseif (true === \$v): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (false !== \$v): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_2c368c954493d2448f80d5c0b78a99faf20a6e0a34a3f7866deed92731ce40a9->leave($__internal_2c368c954493d2448f80d5c0b78a99faf20a6e0a34a3f7866deed92731ce40a9_prof);

        
        $__internal_12dcd186e5bef831f866c8439a9eecd599931fac30e6670742b435870c011241->leave($__internal_12dcd186e5bef831f866c8439a9eecd599931fac30e6670742b435870c011241_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$attr as \$k => \$v): ?>
<?php if ('placeholder' === \$k || 'title' === \$k): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$v, array(), \$translation_domain) : \$v)) ?>
<?php elseif (true === \$v): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (false !== \$v): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/attributes.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}
