<?php

/* SonataAdminBundle:CRUD:list_string.html.twig */
class __TwigTemplate_92aa22795e35f9915887a6d4333ba97e1914d66a52cebe4bfb70e2fb300f80d4 extends Twig_Template
{
    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_string.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a578637e01dd1c33ca7b51831ec1d4d5996965690993bd7c146ddeb94fa99e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a578637e01dd1c33ca7b51831ec1d4d5996965690993bd7c146ddeb94fa99e3->enter($__internal_1a578637e01dd1c33ca7b51831ec1d4d5996965690993bd7c146ddeb94fa99e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_string.html.twig"));

        $__internal_7075c32893a3a471be3c2c36f7a53df3fdb32db118ac7de03e2b91e0547ca9e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7075c32893a3a471be3c2c36f7a53df3fdb32db118ac7de03e2b91e0547ca9e2->enter($__internal_7075c32893a3a471be3c2c36f7a53df3fdb32db118ac7de03e2b91e0547ca9e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_string.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1a578637e01dd1c33ca7b51831ec1d4d5996965690993bd7c146ddeb94fa99e3->leave($__internal_1a578637e01dd1c33ca7b51831ec1d4d5996965690993bd7c146ddeb94fa99e3_prof);

        
        $__internal_7075c32893a3a471be3c2c36f7a53df3fdb32db118ac7de03e2b91e0547ca9e2->leave($__internal_7075c32893a3a471be3c2c36f7a53df3fdb32db118ac7de03e2b91e0547ca9e2_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_string.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  9 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}
", "SonataAdminBundle:CRUD:list_string.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list_string.html.twig");
    }
}
