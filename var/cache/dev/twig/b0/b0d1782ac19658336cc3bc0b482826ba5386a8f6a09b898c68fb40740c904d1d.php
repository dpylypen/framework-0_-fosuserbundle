<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_0500440f98c37bacdc796647d4e90b115c5544bc3bd6ae51fdc161e14a78e02e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95c6e053390249d94b08dc8aabfdcc41bf37c75222e176f9cdf9f3e4b5e964dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95c6e053390249d94b08dc8aabfdcc41bf37c75222e176f9cdf9f3e4b5e964dc->enter($__internal_95c6e053390249d94b08dc8aabfdcc41bf37c75222e176f9cdf9f3e4b5e964dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_a5b96fa7e8899098286b0b76ef18a857c68d55ebd372c0530c7164ead4144eed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5b96fa7e8899098286b0b76ef18a857c68d55ebd372c0530c7164ead4144eed->enter($__internal_a5b96fa7e8899098286b0b76ef18a857c68d55ebd372c0530c7164ead4144eed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_95c6e053390249d94b08dc8aabfdcc41bf37c75222e176f9cdf9f3e4b5e964dc->leave($__internal_95c6e053390249d94b08dc8aabfdcc41bf37c75222e176f9cdf9f3e4b5e964dc_prof);

        
        $__internal_a5b96fa7e8899098286b0b76ef18a857c68d55ebd372c0530c7164ead4144eed->leave($__internal_a5b96fa7e8899098286b0b76ef18a857c68d55ebd372c0530c7164ead4144eed_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.atom.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
