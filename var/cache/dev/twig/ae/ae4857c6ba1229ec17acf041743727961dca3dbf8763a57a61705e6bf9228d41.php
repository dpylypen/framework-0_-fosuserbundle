<?php

/* @Twig/images/icon-plus-square-o.svg */
class __TwigTemplate_5c33986bd31fba695ad20603e25b19914905d2ca91536a7520d692fd9a6efaca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0ac4639d69f6255b9510df55c7a1a05f9d7046086973f3b6b8baefa58607c7a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0ac4639d69f6255b9510df55c7a1a05f9d7046086973f3b6b8baefa58607c7a->enter($__internal_e0ac4639d69f6255b9510df55c7a1a05f9d7046086973f3b6b8baefa58607c7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square-o.svg"));

        $__internal_f5cefe6e70c2acdd47053f72ecdf9f43267314a65fd3f074645baab8e14b0834 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5cefe6e70c2acdd47053f72ecdf9f43267314a65fd3f074645baab8e14b0834->enter($__internal_f5cefe6e70c2acdd47053f72ecdf9f43267314a65fd3f074645baab8e14b0834_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square-o.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H960v352q0 14-9 23t-23 9h-64q-14 0-23-9t-9-23V896H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h352V416q0-14 9-23t23-9h64q14 0 23 9t9 23v352h352q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
";
        
        $__internal_e0ac4639d69f6255b9510df55c7a1a05f9d7046086973f3b6b8baefa58607c7a->leave($__internal_e0ac4639d69f6255b9510df55c7a1a05f9d7046086973f3b6b8baefa58607c7a_prof);

        
        $__internal_f5cefe6e70c2acdd47053f72ecdf9f43267314a65fd3f074645baab8e14b0834->leave($__internal_f5cefe6e70c2acdd47053f72ecdf9f43267314a65fd3f074645baab8e14b0834_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-plus-square-o.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H960v352q0 14-9 23t-23 9h-64q-14 0-23-9t-9-23V896H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h352V416q0-14 9-23t23-9h64q14 0 23 9t9 23v352h352q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
", "@Twig/images/icon-plus-square-o.svg", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-plus-square-o.svg");
    }
}
