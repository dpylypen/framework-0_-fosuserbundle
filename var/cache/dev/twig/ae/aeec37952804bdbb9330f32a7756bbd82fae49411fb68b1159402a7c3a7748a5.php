<?php

/* SonataBlockBundle:Block:block_template.html.twig */
class __TwigTemplate_e7d353289b37214420b18d87d11d74ccdce53a0638cf1981864186d3a42038fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["sonata_block"]) || array_key_exists("sonata_block", $context) ? $context["sonata_block"] : (function () { throw new Twig_Error_Runtime('Variable "sonata_block" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_template.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b65e3b4d0cc9088cc45c42789fd195714ab2bd5fe9e3f7c2b629f0daf2c7db42 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b65e3b4d0cc9088cc45c42789fd195714ab2bd5fe9e3f7c2b629f0daf2c7db42->enter($__internal_b65e3b4d0cc9088cc45c42789fd195714ab2bd5fe9e3f7c2b629f0daf2c7db42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_template.html.twig"));

        $__internal_44a0522eb34e40b59dd69fa2020b031c3f0c039e2d6c4fb34dc12dbd8e21d8d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_44a0522eb34e40b59dd69fa2020b031c3f0c039e2d6c4fb34dc12dbd8e21d8d0->enter($__internal_44a0522eb34e40b59dd69fa2020b031c3f0c039e2d6c4fb34dc12dbd8e21d8d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_template.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b65e3b4d0cc9088cc45c42789fd195714ab2bd5fe9e3f7c2b629f0daf2c7db42->leave($__internal_b65e3b4d0cc9088cc45c42789fd195714ab2bd5fe9e3f7c2b629f0daf2c7db42_prof);

        
        $__internal_44a0522eb34e40b59dd69fa2020b031c3f0c039e2d6c4fb34dc12dbd8e21d8d0->leave($__internal_44a0522eb34e40b59dd69fa2020b031c3f0c039e2d6c4fb34dc12dbd8e21d8d0_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_276b6d2228c7435a8ba2d0f22e09a35e0a8117270af7fa91a8e4c771090276f3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_276b6d2228c7435a8ba2d0f22e09a35e0a8117270af7fa91a8e4c771090276f3->enter($__internal_276b6d2228c7435a8ba2d0f22e09a35e0a8117270af7fa91a8e4c771090276f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_e1f4603e77950f3a12dc74541be05674e466600f21d30dc324fd58a1dfc43477 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1f4603e77950f3a12dc74541be05674e466600f21d30dc324fd58a1dfc43477->enter($__internal_e1f4603e77950f3a12dc74541be05674e466600f21d30dc324fd58a1dfc43477_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <h3>Sonata Block Template</h3>
    If you want to use the <code>sonata.block.template</code> block type, you need to create a template :

    <pre>";
        // line 33
        echo "{# file: 'MyBundle:Block:my_block_feature_1.html.twig' #}
{% extends sonata_block.templates.block_base %}

{% block block %}
    &lt;h3&gt;The block title&lt;/h3&gt;
    &lt;p&gt;
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel turpis at lacus
        vehicula fringilla at eu lectus. Duis vitae arcu congue, porttitor nisi sit amet,
        mattis metus. Nunc mollis elit ut lectus cursus luctus. Aliquam eu magna sit amet
        massa volutpat auctor.
    &lt;/p&gt;
{% endblock %}";
        echo "</pre>

    And then call it from a template with the <code>sonata_block_render</code> helper:

    <pre>";
        // line 43
        echo "{{ sonata_block_render({ 'type': 'sonata.block.service.template' }, {
    'template': 'MyBundle:Block:my_block_feature_1.html.twig',
}) }}";
        echo "</pre>
";
        
        $__internal_e1f4603e77950f3a12dc74541be05674e466600f21d30dc324fd58a1dfc43477->leave($__internal_e1f4603e77950f3a12dc74541be05674e466600f21d30dc324fd58a1dfc43477_prof);

        
        $__internal_276b6d2228c7435a8ba2d0f22e09a35e0a8117270af7fa91a8e4c771090276f3->leave($__internal_276b6d2228c7435a8ba2d0f22e09a35e0a8117270af7fa91a8e4c771090276f3_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 43,  53 => 33,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    <h3>Sonata Block Template</h3>
    If you want to use the <code>sonata.block.template</code> block type, you need to create a template :

    <pre>
        {%- verbatim -%}
{# file: 'MyBundle:Block:my_block_feature_1.html.twig' #}
{% extends sonata_block.templates.block_base %}

{% block block %}
    &lt;h3&gt;The block title&lt;/h3&gt;
    &lt;p&gt;
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel turpis at lacus
        vehicula fringilla at eu lectus. Duis vitae arcu congue, porttitor nisi sit amet,
        mattis metus. Nunc mollis elit ut lectus cursus luctus. Aliquam eu magna sit amet
        massa volutpat auctor.
    &lt;/p&gt;
{% endblock %}
        {%- endverbatim -%}
    </pre>

    And then call it from a template with the <code>sonata_block_render</code> helper:

    <pre>
{%- verbatim -%}
{{ sonata_block_render({ 'type': 'sonata.block.service.template' }, {
    'template': 'MyBundle:Block:my_block_feature_1.html.twig',
}) }}
{%- endverbatim -%}
    </pre>
{% endblock %}
", "SonataBlockBundle:Block:block_template.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/block-bundle/Resources/views/Block/block_template.html.twig");
    }
}
