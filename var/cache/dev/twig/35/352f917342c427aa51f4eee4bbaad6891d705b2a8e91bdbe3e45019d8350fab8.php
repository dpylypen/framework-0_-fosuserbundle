<?php

/* TwigBundle:Exception:traces.xml.twig */
class __TwigTemplate_bf2f45fd71eb82b47299f77cbf41368a5bffbbbec8565654c1342c8734d698b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_261e5fd6d1e73621cbacea884e4d3c14177b5d8549216beb60bda8829215769b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_261e5fd6d1e73621cbacea884e4d3c14177b5d8549216beb60bda8829215769b->enter($__internal_261e5fd6d1e73621cbacea884e4d3c14177b5d8549216beb60bda8829215769b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        $__internal_f2d5145bd79f78c22d32e5a58893d52bf3d898bf915442c61c431bcb8d1d2957 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2d5145bd79f78c22d32e5a58893d52bf3d898bf915442c61c431bcb8d1d2957->enter($__internal_f2d5145bd79f78c22d32e5a58893d52bf3d898bf915442c61c431bcb8d1d2957_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        // line 1
        echo "        <traces>
";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })()), "trace", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
            // line 3
            echo "            <trace>
";
            // line 4
            echo twig_include($this->env, $context, "@Twig/Exception/trace.txt.twig", array("trace" => $context["trace"]), false);
            echo "

            </trace>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "        </traces>
";
        
        $__internal_261e5fd6d1e73621cbacea884e4d3c14177b5d8549216beb60bda8829215769b->leave($__internal_261e5fd6d1e73621cbacea884e4d3c14177b5d8549216beb60bda8829215769b_prof);

        
        $__internal_f2d5145bd79f78c22d32e5a58893d52bf3d898bf915442c61c431bcb8d1d2957->leave($__internal_f2d5145bd79f78c22d32e5a58893d52bf3d898bf915442c61c431bcb8d1d2957_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 8,  35 => 4,  32 => 3,  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("        <traces>
{% for trace in exception.trace %}
            <trace>
{{ include('@Twig/Exception/trace.txt.twig', { trace: trace }, with_context = false) }}

            </trace>
{% endfor %}
        </traces>
", "TwigBundle:Exception:traces.xml.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.xml.twig");
    }
}
