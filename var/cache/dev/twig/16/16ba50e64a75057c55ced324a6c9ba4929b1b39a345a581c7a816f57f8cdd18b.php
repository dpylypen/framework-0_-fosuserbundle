<?php

/* SonataAdminBundle:Pager:simple_pager_results.html.twig */
class __TwigTemplate_d783315fbb8e7761c51c525aa8b583265fcbe6920d1c5e748c27cb6680fb1163 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:Pager:base_results.html.twig", "SonataAdminBundle:Pager:simple_pager_results.html.twig", 12);
        $this->blocks = array(
            'num_results' => array($this, 'block_num_results'),
            'num_pages' => array($this, 'block_num_pages'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Pager:base_results.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f3db19eb3f6ec3c96b2cb1c24188f0dd16e7fc1fa7a861c748548ff1970373e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f3db19eb3f6ec3c96b2cb1c24188f0dd16e7fc1fa7a861c748548ff1970373e->enter($__internal_7f3db19eb3f6ec3c96b2cb1c24188f0dd16e7fc1fa7a861c748548ff1970373e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:simple_pager_results.html.twig"));

        $__internal_cce2c5ed89186dc35b8552890edfb1f9b29b2d1998215b65107c5c5016d54312 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cce2c5ed89186dc35b8552890edfb1f9b29b2d1998215b65107c5c5016d54312->enter($__internal_cce2c5ed89186dc35b8552890edfb1f9b29b2d1998215b65107c5c5016d54312_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:simple_pager_results.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7f3db19eb3f6ec3c96b2cb1c24188f0dd16e7fc1fa7a861c748548ff1970373e->leave($__internal_7f3db19eb3f6ec3c96b2cb1c24188f0dd16e7fc1fa7a861c748548ff1970373e_prof);

        
        $__internal_cce2c5ed89186dc35b8552890edfb1f9b29b2d1998215b65107c5c5016d54312->leave($__internal_cce2c5ed89186dc35b8552890edfb1f9b29b2d1998215b65107c5c5016d54312_prof);

    }

    // line 14
    public function block_num_results($context, array $blocks = array())
    {
        $__internal_d5960eb45a9383fabf44e00e13203e8da73add95a881d8b52f835710b0e96e47 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d5960eb45a9383fabf44e00e13203e8da73add95a881d8b52f835710b0e96e47->enter($__internal_d5960eb45a9383fabf44e00e13203e8da73add95a881d8b52f835710b0e96e47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_results"));

        $__internal_b28bda57179bd378ce458a434187412c26fc130838e640ac0d48ffca99e55c55 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b28bda57179bd378ce458a434187412c26fc130838e640ac0d48ffca99e55c55->enter($__internal_b28bda57179bd378ce458a434187412c26fc130838e640ac0d48ffca99e55c55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_results"));

        // line 15
        echo "    ";
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "lastPage", array()) != twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "page", array()))) {
            // line 16
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("list_results_count_prefix", array(), "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        }
        // line 18
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->transChoice("list_results_count", twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "nbresults", array()), array("%count%" => twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "nbresults", array())), "SonataAdminBundle");
        // line 19
        echo "    &nbsp;-&nbsp;
";
        
        $__internal_b28bda57179bd378ce458a434187412c26fc130838e640ac0d48ffca99e55c55->leave($__internal_b28bda57179bd378ce458a434187412c26fc130838e640ac0d48ffca99e55c55_prof);

        
        $__internal_d5960eb45a9383fabf44e00e13203e8da73add95a881d8b52f835710b0e96e47->leave($__internal_d5960eb45a9383fabf44e00e13203e8da73add95a881d8b52f835710b0e96e47_prof);

    }

    // line 22
    public function block_num_pages($context, array $blocks = array())
    {
        $__internal_77051eb81d5dfd529c2b44572cfe81dccf77e26e3f8fc2a8c3ae1e3e52b516b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_77051eb81d5dfd529c2b44572cfe81dccf77e26e3f8fc2a8c3ae1e3e52b516b9->enter($__internal_77051eb81d5dfd529c2b44572cfe81dccf77e26e3f8fc2a8c3ae1e3e52b516b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_pages"));

        $__internal_016d89db775fd369a6c94e4255dec8ec5b1980f26ae298b0165ee8a4f29f2612 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_016d89db775fd369a6c94e4255dec8ec5b1980f26ae298b0165ee8a4f29f2612->enter($__internal_016d89db775fd369a6c94e4255dec8ec5b1980f26ae298b0165ee8a4f29f2612_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_pages"));

        // line 23
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 23, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "page", array()), "html", null, true);
        echo "
    /
    ";
        // line 25
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 25, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "lastPage", array()) != twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 25, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "page", array()))) {
            // line 26
            echo "        ?
    ";
        } else {
            // line 28
            echo "        ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 28, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "lastpage", array()), "html", null, true);
            echo "
    ";
        }
        // line 30
        echo "    &nbsp;-&nbsp;
";
        
        $__internal_016d89db775fd369a6c94e4255dec8ec5b1980f26ae298b0165ee8a4f29f2612->leave($__internal_016d89db775fd369a6c94e4255dec8ec5b1980f26ae298b0165ee8a4f29f2612_prof);

        
        $__internal_77051eb81d5dfd529c2b44572cfe81dccf77e26e3f8fc2a8c3ae1e3e52b516b9->leave($__internal_77051eb81d5dfd529c2b44572cfe81dccf77e26e3f8fc2a8c3ae1e3e52b516b9_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Pager:simple_pager_results.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 30,  94 => 28,  90 => 26,  88 => 25,  82 => 23,  73 => 22,  62 => 19,  59 => 18,  53 => 16,  50 => 15,  41 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:Pager:base_results.html.twig' %}

{% block num_results %}
    {% if admin.datagrid.pager.lastPage != admin.datagrid.pager.page %}
        {{ 'list_results_count_prefix'|trans({}, 'SonataAdminBundle') }}
    {% endif %}
    {% transchoice admin.datagrid.pager.nbresults with {'%count%': admin.datagrid.pager.nbresults} from 'SonataAdminBundle' %}list_results_count{% endtranschoice %}
    &nbsp;-&nbsp;
{% endblock %}

{% block num_pages %}
    {{ admin.datagrid.pager.page }}
    /
    {% if admin.datagrid.pager.lastPage != admin.datagrid.pager.page %}
        ?
    {% else %}
        {{ admin.datagrid.pager.lastpage }}
    {% endif %}
    &nbsp;-&nbsp;
{% endblock %}
", "SonataAdminBundle:Pager:simple_pager_results.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/Pager/simple_pager_results.html.twig");
    }
}
