<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_b002aee00ef767166559c3a28b7d145b3241af20996469a5cc5aa77dd3a913c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c6728d6456d9020555aa20e937a9d6c6474c19cd0185be622db64268e12d5e9c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6728d6456d9020555aa20e937a9d6c6474c19cd0185be622db64268e12d5e9c->enter($__internal_c6728d6456d9020555aa20e937a9d6c6474c19cd0185be622db64268e12d5e9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        $__internal_5a249b47c67798235433b56acd19584d4a9f2b1af220f2c6bc28a68c9700f56f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a249b47c67798235433b56acd19584d4a9f2b1af220f2c6bc28a68c9700f56f->enter($__internal_5a249b47c67798235433b56acd19584d4a9f2b1af220f2c6bc28a68c9700f56f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 3, $this->getSourceContext()); })()), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 3, $this->getSourceContext()); })()), "html", null, true);
        echo "\" />
";
        
        $__internal_c6728d6456d9020555aa20e937a9d6c6474c19cd0185be622db64268e12d5e9c->leave($__internal_c6728d6456d9020555aa20e937a9d6c6474c19cd0185be622db64268e12d5e9c_prof);

        
        $__internal_5a249b47c67798235433b56acd19584d4a9f2b1af220f2c6bc28a68c9700f56f->leave($__internal_5a249b47c67798235433b56acd19584d4a9f2b1af220f2c6bc28a68c9700f56f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"{{ _charset }}\" ?>

<error code=\"{{ status_code }}\" message=\"{{ status_text }}\" />
", "TwigBundle:Exception:error.xml.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.xml.twig");
    }
}
