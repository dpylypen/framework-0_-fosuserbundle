<?php

/* SonataAdminBundle:CRUD:show_trans.html.twig */
class __TwigTemplate_3c1ea0d5ef10b8e51f826dcca553e09e3139f33ec2e937e3e466b7657170352b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 11
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_trans.html.twig", 11);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_16c209336254a46acc348c39f42e30cf70ee6daaf70ade311b62b90d9b283abe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16c209336254a46acc348c39f42e30cf70ee6daaf70ade311b62b90d9b283abe->enter($__internal_16c209336254a46acc348c39f42e30cf70ee6daaf70ade311b62b90d9b283abe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_trans.html.twig"));

        $__internal_83d9c518a5dc2a9b21cff6eb59ee1f385e988b49f9a237335ee71589755ec0ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83d9c518a5dc2a9b21cff6eb59ee1f385e988b49f9a237335ee71589755ec0ce->enter($__internal_83d9c518a5dc2a9b21cff6eb59ee1f385e988b49f9a237335ee71589755ec0ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_trans.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_16c209336254a46acc348c39f42e30cf70ee6daaf70ade311b62b90d9b283abe->leave($__internal_16c209336254a46acc348c39f42e30cf70ee6daaf70ade311b62b90d9b283abe_prof);

        
        $__internal_83d9c518a5dc2a9b21cff6eb59ee1f385e988b49f9a237335ee71589755ec0ce->leave($__internal_83d9c518a5dc2a9b21cff6eb59ee1f385e988b49f9a237335ee71589755ec0ce_prof);

    }

    // line 13
    public function block_field($context, array $blocks = array())
    {
        $__internal_92dc6bff9d8d0eb18b686342f54726c94a9f249bceb0f80a6470979029ffcc2b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_92dc6bff9d8d0eb18b686342f54726c94a9f249bceb0f80a6470979029ffcc2b->enter($__internal_92dc6bff9d8d0eb18b686342f54726c94a9f249bceb0f80a6470979029ffcc2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_acdb69a93cfab6c0e5e86594e97d8bb010c94f70ba27b7fe760701158b3b93d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_acdb69a93cfab6c0e5e86594e97d8bb010c94f70ba27b7fe760701158b3b93d7->enter($__internal_acdb69a93cfab6c0e5e86594e97d8bb010c94f70ba27b7fe760701158b3b93d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 14
        echo "    ";
        if ( !twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["field_description"] ?? null), "options", array(), "any", false, true), "catalogue", array(), "any", true, true)) {
            // line 15
            echo "        ";
            $context["value"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()));
            // line 16
            echo "    ";
        } else {
            // line 17
            echo "        ";
            $context["value"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 17, $this->getSourceContext()); })()), array(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 17, $this->getSourceContext()); })()), "options", array()), "catalogue", array()));
            // line 18
            echo "    ";
        }
        // line 19
        echo "
    ";
        // line 20
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 20, $this->getSourceContext()); })()), "options", array()), "safe", array())) {
            // line 21
            echo "        ";
            echo (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 21, $this->getSourceContext()); })());
            echo "
    ";
        } else {
            // line 23
            echo "        ";
            echo twig_escape_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 23, $this->getSourceContext()); })()), "html", null, true);
            echo "
    ";
        }
        
        $__internal_acdb69a93cfab6c0e5e86594e97d8bb010c94f70ba27b7fe760701158b3b93d7->leave($__internal_acdb69a93cfab6c0e5e86594e97d8bb010c94f70ba27b7fe760701158b3b93d7_prof);

        
        $__internal_92dc6bff9d8d0eb18b686342f54726c94a9f249bceb0f80a6470979029ffcc2b->leave($__internal_92dc6bff9d8d0eb18b686342f54726c94a9f249bceb0f80a6470979029ffcc2b_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_trans.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 23,  69 => 21,  67 => 20,  64 => 19,  61 => 18,  58 => 17,  55 => 16,  52 => 15,  49 => 14,  40 => 13,  11 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field%}
    {% if field_description.options.catalogue is not defined %}
        {% set value = value|trans %}
    {% else %}
        {% set value = value|trans({}, field_description.options.catalogue) %}
    {% endif %}

    {% if field_description.options.safe %}
        {{ value|raw }}
    {% else %}
        {{ value }}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:show_trans.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/show_trans.html.twig");
    }
}
