<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_802e37cf39c27cb3ca75a5ebb2fe3583350f4b13b4672e4871ae6443dc231acc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca3c1f85a33cd881255d1e664706a02a1c2e7e5c8d5307d3b8474f5fdd134d6d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ca3c1f85a33cd881255d1e664706a02a1c2e7e5c8d5307d3b8474f5fdd134d6d->enter($__internal_ca3c1f85a33cd881255d1e664706a02a1c2e7e5c8d5307d3b8474f5fdd134d6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_0bfebd6c2bb94ca7cc789086d36ec594f59c52f3c9dc0670c6cb3dd52a199b4e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0bfebd6c2bb94ca7cc789086d36ec594f59c52f3c9dc0670c6cb3dd52a199b4e->enter($__internal_0bfebd6c2bb94ca7cc789086d36ec594f59c52f3c9dc0670c6cb3dd52a199b4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 7, $this->getSourceContext()); })()), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 7, $this->getSourceContext()); })()), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ca3c1f85a33cd881255d1e664706a02a1c2e7e5c8d5307d3b8474f5fdd134d6d->leave($__internal_ca3c1f85a33cd881255d1e664706a02a1c2e7e5c8d5307d3b8474f5fdd134d6d_prof);

        
        $__internal_0bfebd6c2bb94ca7cc789086d36ec594f59c52f3c9dc0670c6cb3dd52a199b4e->leave($__internal_0bfebd6c2bb94ca7cc789086d36ec594f59c52f3c9dc0670c6cb3dd52a199b4e_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_6cf71b7b0f493ddc81c43eb2090e8e7b4ab1ccba6ed98b78e28321d58d914a69 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6cf71b7b0f493ddc81c43eb2090e8e7b4ab1ccba6ed98b78e28321d58d914a69->enter($__internal_6cf71b7b0f493ddc81c43eb2090e8e7b4ab1ccba6ed98b78e28321d58d914a69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_759bc00fa0a5a1013d203c52afcc73be4cd9aa21a4fb921020e672f9baf0cdee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_759bc00fa0a5a1013d203c52afcc73be4cd9aa21a4fb921020e672f9baf0cdee->enter($__internal_759bc00fa0a5a1013d203c52afcc73be4cd9aa21a4fb921020e672f9baf0cdee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 12, $this->getSourceContext()); })()), (isset($context["about"]) || array_key_exists("about", $context) ? $context["about"] : (function () { throw new Twig_Error_Runtime('Variable "about" does not exist.', 12, $this->getSourceContext()); })()), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 14, $this->getSourceContext()); })()), (isset($context["about"]) || array_key_exists("about", $context) ? $context["about"] : (function () { throw new Twig_Error_Runtime('Variable "about" does not exist.', 14, $this->getSourceContext()); })()), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_759bc00fa0a5a1013d203c52afcc73be4cd9aa21a4fb921020e672f9baf0cdee->leave($__internal_759bc00fa0a5a1013d203c52afcc73be4cd9aa21a4fb921020e672f9baf0cdee_prof);

        
        $__internal_6cf71b7b0f493ddc81c43eb2090e8e7b4ab1ccba6ed98b78e28321d58d914a69->leave($__internal_6cf71b7b0f493ddc81c43eb2090e8e7b4ab1ccba6ed98b78e28321d58d914a69_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_fb7cc1cbb2b77063e439c6a211e4d7cfcf3cf8194e2919a854a7440ccfda322b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb7cc1cbb2b77063e439c6a211e4d7cfcf3cf8194e2919a854a7440ccfda322b->enter($__internal_fb7cc1cbb2b77063e439c6a211e4d7cfcf3cf8194e2919a854a7440ccfda322b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_85325f8e2f467ec535c29bd5204a8e84df063093d8b0a1fc76646a2e1c80a3eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_85325f8e2f467ec535c29bd5204a8e84df063093d8b0a1fc76646a2e1c80a3eb->enter($__internal_85325f8e2f467ec535c29bd5204a8e84df063093d8b0a1fc76646a2e1c80a3eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 20, $this->getSourceContext()); })()), (isset($context["about"]) || array_key_exists("about", $context) ? $context["about"] : (function () { throw new Twig_Error_Runtime('Variable "about" does not exist.', 20, $this->getSourceContext()); })()), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 21, $this->getSourceContext()); })()), (isset($context["about"]) || array_key_exists("about", $context) ? $context["about"] : (function () { throw new Twig_Error_Runtime('Variable "about" does not exist.', 21, $this->getSourceContext()); })()), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_85325f8e2f467ec535c29bd5204a8e84df063093d8b0a1fc76646a2e1c80a3eb->leave($__internal_85325f8e2f467ec535c29bd5204a8e84df063093d8b0a1fc76646a2e1c80a3eb_prof);

        
        $__internal_fb7cc1cbb2b77063e439c6a211e4d7cfcf3cf8194e2919a854a7440ccfda322b->leave($__internal_fb7cc1cbb2b77063e439c6a211e4d7cfcf3cf8194e2919a854a7440ccfda322b_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
