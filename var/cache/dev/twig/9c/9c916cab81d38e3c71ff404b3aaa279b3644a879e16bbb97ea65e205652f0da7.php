<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_42d7c3598267bddb92da5faa0bfd660794b0213be3d08c246fe139ec874c4452 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2e8c49cad5349582a28322a43e5a30096f2017444793fd243a8795df520f4596 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e8c49cad5349582a28322a43e5a30096f2017444793fd243a8795df520f4596->enter($__internal_2e8c49cad5349582a28322a43e5a30096f2017444793fd243a8795df520f4596_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_88322173eb1ce3768c0b822acf3f2b2d3db4f605e57983e9ae05caf9ef691897 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88322173eb1ce3768c0b822acf3f2b2d3db4f605e57983e9ae05caf9ef691897->enter($__internal_88322173eb1ce3768c0b822acf3f2b2d3db4f605e57983e9ae05caf9ef691897_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_2e8c49cad5349582a28322a43e5a30096f2017444793fd243a8795df520f4596->leave($__internal_2e8c49cad5349582a28322a43e5a30096f2017444793fd243a8795df520f4596_prof);

        
        $__internal_88322173eb1ce3768c0b822acf3f2b2d3db4f605e57983e9ae05caf9ef691897->leave($__internal_88322173eb1ce3768c0b822acf3f2b2d3db4f605e57983e9ae05caf9ef691897_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_7ac1ac63d0c608e7a2e448a3eb08a62304fdeec0510ca9475ae705d4e7b607b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ac1ac63d0c608e7a2e448a3eb08a62304fdeec0510ca9475ae705d4e7b607b6->enter($__internal_7ac1ac63d0c608e7a2e448a3eb08a62304fdeec0510ca9475ae705d4e7b607b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_24cbb2403a833d550192334255950dc10fb859623bef7fada884a8ce66a8c007 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24cbb2403a833d550192334255950dc10fb859623bef7fada884a8ce66a8c007->enter($__internal_24cbb2403a833d550192334255950dc10fb859623bef7fada884a8ce66a8c007_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_24cbb2403a833d550192334255950dc10fb859623bef7fada884a8ce66a8c007->leave($__internal_24cbb2403a833d550192334255950dc10fb859623bef7fada884a8ce66a8c007_prof);

        
        $__internal_7ac1ac63d0c608e7a2e448a3eb08a62304fdeec0510ca9475ae705d4e7b607b6->leave($__internal_7ac1ac63d0c608e7a2e448a3eb08a62304fdeec0510ca9475ae705d4e7b607b6_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
