<?php

/* SonataAdminBundle:CRUD:list_date.html.twig */
class __TwigTemplate_b33648fe5cbd6804372f013944ee04ed047afc7ecf750a208910fab537a4a9ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_date.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_68bf3e99c562ff0878dec3b54a4db9d66edcd8b6f32e74e8aaf42c9187c3c8c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_68bf3e99c562ff0878dec3b54a4db9d66edcd8b6f32e74e8aaf42c9187c3c8c3->enter($__internal_68bf3e99c562ff0878dec3b54a4db9d66edcd8b6f32e74e8aaf42c9187c3c8c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_date.html.twig"));

        $__internal_991f83ec1bbe6091f820dabb04e8cbcf12b2b21cea3b2561b8ba69eb13f0a227 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_991f83ec1bbe6091f820dabb04e8cbcf12b2b21cea3b2561b8ba69eb13f0a227->enter($__internal_991f83ec1bbe6091f820dabb04e8cbcf12b2b21cea3b2561b8ba69eb13f0a227_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_date.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_68bf3e99c562ff0878dec3b54a4db9d66edcd8b6f32e74e8aaf42c9187c3c8c3->leave($__internal_68bf3e99c562ff0878dec3b54a4db9d66edcd8b6f32e74e8aaf42c9187c3c8c3_prof);

        
        $__internal_991f83ec1bbe6091f820dabb04e8cbcf12b2b21cea3b2561b8ba69eb13f0a227->leave($__internal_991f83ec1bbe6091f820dabb04e8cbcf12b2b21cea3b2561b8ba69eb13f0a227_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_c880956895ba92cea4a448f386d55570b26cac2a3c15c439951504e8c2d91c10 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c880956895ba92cea4a448f386d55570b26cac2a3c15c439951504e8c2d91c10->enter($__internal_c880956895ba92cea4a448f386d55570b26cac2a3c15c439951504e8c2d91c10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_a92431a7557446cd5fa36646c384aa83c7c14cbbb9badad0126a57547be48380 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a92431a7557446cd5fa36646c384aa83c7c14cbbb9badad0126a57547be48380->enter($__internal_a92431a7557446cd5fa36646c384aa83c7c14cbbb9badad0126a57547be48380_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twig_test_empty((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "&nbsp;";
        } elseif (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(),         // line 17
($context["field_description"] ?? null), "options", array(), "any", false, true), "format", array(), "any", true, true)) {
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 18, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_description"]) || array_key_exists("field_description", $context) ? $context["field_description"] : (function () { throw new Twig_Error_Runtime('Variable "field_description" does not exist.', 18, $this->getSourceContext()); })()), "options", array()), "format", array())), "html", null, true);
        } else {
            // line 20
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 20, $this->getSourceContext()); })()), "F j, Y"), "html", null, true);
        }
        
        $__internal_a92431a7557446cd5fa36646c384aa83c7c14cbbb9badad0126a57547be48380->leave($__internal_a92431a7557446cd5fa36646c384aa83c7c14cbbb9badad0126a57547be48380_prof);

        
        $__internal_c880956895ba92cea4a448f386d55570b26cac2a3c15c439951504e8c2d91c10->leave($__internal_c880956895ba92cea4a448f386d55570b26cac2a3c15c439951504e8c2d91c10_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_date.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 20,  54 => 18,  52 => 17,  50 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field%}
    {%- if value is empty -%}
        &nbsp;
    {%- elseif field_description.options.format is defined -%}
        {{ value|date(field_description.options.format) }}
    {%- else -%}
        {{ value|date('F j, Y') }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:list_date.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list_date.html.twig");
    }
}
