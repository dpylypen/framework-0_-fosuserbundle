<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_7e53595704ac37a88e80a6b8183901f5fe6bb0cb1e6de2ea7c0dcd0678de6ef8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed430f707a104d87eed7a5e0bdb4298c841a11fd74551bfa5febe9315cac3410 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed430f707a104d87eed7a5e0bdb4298c841a11fd74551bfa5febe9315cac3410->enter($__internal_ed430f707a104d87eed7a5e0bdb4298c841a11fd74551bfa5febe9315cac3410_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $__internal_6dafae03b79df1b29a4a67f1649999b48843fc5af3bc1a4abfd90d18a5fa3336 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6dafae03b79df1b29a4a67f1649999b48843fc5af3bc1a4abfd90d18a5fa3336->enter($__internal_6dafae03b79df1b29a4a67f1649999b48843fc5af3bc1a4abfd90d18a5fa3336_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ed430f707a104d87eed7a5e0bdb4298c841a11fd74551bfa5febe9315cac3410->leave($__internal_ed430f707a104d87eed7a5e0bdb4298c841a11fd74551bfa5febe9315cac3410_prof);

        
        $__internal_6dafae03b79df1b29a4a67f1649999b48843fc5af3bc1a4abfd90d18a5fa3336->leave($__internal_6dafae03b79df1b29a4a67f1649999b48843fc5af3bc1a4abfd90d18a5fa3336_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_fa3a14cdeca45022ec682702f87269e770e8155d3779aa5f7f2a4c825bcd1430 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa3a14cdeca45022ec682702f87269e770e8155d3779aa5f7f2a4c825bcd1430->enter($__internal_fa3a14cdeca45022ec682702f87269e770e8155d3779aa5f7f2a4c825bcd1430_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_4bc4ade4108398e9e11b22a31d7c79bebb90bceb9c8c3ef809d570cacd103148 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4bc4ade4108398e9e11b22a31d7c79bebb90bceb9c8c3ef809d570cacd103148->enter($__internal_4bc4ade4108398e9e11b22a31d7c79bebb90bceb9c8c3ef809d570cacd103148_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_4bc4ade4108398e9e11b22a31d7c79bebb90bceb9c8c3ef809d570cacd103148->leave($__internal_4bc4ade4108398e9e11b22a31d7c79bebb90bceb9c8c3ef809d570cacd103148_prof);

        
        $__internal_fa3a14cdeca45022ec682702f87269e770e8155d3779aa5f7f2a4c825bcd1430->leave($__internal_fa3a14cdeca45022ec682702f87269e770e8155d3779aa5f7f2a4c825bcd1430_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_e7cc60713eb5767e7714c11d1493a21ac924b913caf943a10ed9290e0fdda655 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e7cc60713eb5767e7714c11d1493a21ac924b913caf943a10ed9290e0fdda655->enter($__internal_e7cc60713eb5767e7714c11d1493a21ac924b913caf943a10ed9290e0fdda655_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_fd5e727f6f8a429c38b6f66f5c52ae1968b55d868ac6ae5074b9f144c5583dd4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd5e727f6f8a429c38b6f66f5c52ae1968b55d868ac6ae5074b9f144c5583dd4->enter($__internal_fd5e727f6f8a429c38b6f66f5c52ae1968b55d868ac6ae5074b9f144c5583dd4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 13, $this->getSourceContext()); })()), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_fd5e727f6f8a429c38b6f66f5c52ae1968b55d868ac6ae5074b9f144c5583dd4->leave($__internal_fd5e727f6f8a429c38b6f66f5c52ae1968b55d868ac6ae5074b9f144c5583dd4_prof);

        
        $__internal_e7cc60713eb5767e7714c11d1493a21ac924b913caf943a10ed9290e0fdda655->leave($__internal_e7cc60713eb5767e7714c11d1493a21ac924b913caf943a10ed9290e0fdda655_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_5395aa8a7b54d0f630dbd925474eecf65d5808ee549aca82a2aa1ebc1fbf44c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5395aa8a7b54d0f630dbd925474eecf65d5808ee549aca82a2aa1ebc1fbf44c8->enter($__internal_5395aa8a7b54d0f630dbd925474eecf65d5808ee549aca82a2aa1ebc1fbf44c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_5f24a1b16ea6b7d35637b0a02586bc0700c21564a0a7bdb1e959cd8e0d487be4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f24a1b16ea6b7d35637b0a02586bc0700c21564a0a7bdb1e959cd8e0d487be4->enter($__internal_5f24a1b16ea6b7d35637b0a02586bc0700c21564a0a7bdb1e959cd8e0d487be4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 27, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 33, $this->getSourceContext()); })()))));
            echo "
        </div>
    ";
        }
        
        $__internal_5f24a1b16ea6b7d35637b0a02586bc0700c21564a0a7bdb1e959cd8e0d487be4->leave($__internal_5f24a1b16ea6b7d35637b0a02586bc0700c21564a0a7bdb1e959cd8e0d487be4_prof);

        
        $__internal_5395aa8a7b54d0f630dbd925474eecf65d5808ee549aca82a2aa1ebc1fbf44c8->leave($__internal_5395aa8a7b54d0f630dbd925474eecf65d5808ee549aca82a2aa1ebc1fbf44c8_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "WebProfilerBundle:Collector:exception.html.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
