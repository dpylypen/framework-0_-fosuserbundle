<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_2e34fa4d32675eebb2482910462f4a21159d3a5d411089426d3b4b855c4bbefb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_92e759e3a423591307b6228d261e06fc800ad1fe625e668b21849dcf49ba3298 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_92e759e3a423591307b6228d261e06fc800ad1fe625e668b21849dcf49ba3298->enter($__internal_92e759e3a423591307b6228d261e06fc800ad1fe625e668b21849dcf49ba3298_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_5c3e7dfdadfebe4f453d76dd40efceb3d91931a161afbb9b1a1f178e80bba05b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c3e7dfdadfebe4f453d76dd40efceb3d91931a161afbb9b1a1f178e80bba05b->enter($__internal_5c3e7dfdadfebe4f453d76dd40efceb3d91931a161afbb9b1a1f178e80bba05b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_92e759e3a423591307b6228d261e06fc800ad1fe625e668b21849dcf49ba3298->leave($__internal_92e759e3a423591307b6228d261e06fc800ad1fe625e668b21849dcf49ba3298_prof);

        
        $__internal_5c3e7dfdadfebe4f453d76dd40efceb3d91931a161afbb9b1a1f178e80bba05b->leave($__internal_5c3e7dfdadfebe4f453d76dd40efceb3d91931a161afbb9b1a1f178e80bba05b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
