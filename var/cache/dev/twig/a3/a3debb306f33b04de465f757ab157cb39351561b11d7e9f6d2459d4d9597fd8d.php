<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_6ded606f9c507e947b997fe4be9e94e75220a65f03da6a9dfe455b8e28db1274 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7541149fc07a336c1f0156b3dac629616e6b959ec20b91336e295842f09039e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7541149fc07a336c1f0156b3dac629616e6b959ec20b91336e295842f09039e->enter($__internal_d7541149fc07a336c1f0156b3dac629616e6b959ec20b91336e295842f09039e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $__internal_ae81a7fe0c29074a0e8fcd61471f1b966a40b00365e0edced5753617b8e8e8dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae81a7fe0c29074a0e8fcd61471f1b966a40b00365e0edced5753617b8e8e8dd->enter($__internal_ae81a7fe0c29074a0e8fcd61471f1b966a40b00365e0edced5753617b8e8e8dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d7541149fc07a336c1f0156b3dac629616e6b959ec20b91336e295842f09039e->leave($__internal_d7541149fc07a336c1f0156b3dac629616e6b959ec20b91336e295842f09039e_prof);

        
        $__internal_ae81a7fe0c29074a0e8fcd61471f1b966a40b00365e0edced5753617b8e8e8dd->leave($__internal_ae81a7fe0c29074a0e8fcd61471f1b966a40b00365e0edced5753617b8e8e8dd_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_3523d83876602b0361093e9d71cbb71cb2ec68d8d95a8d8bb437162c273eff02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3523d83876602b0361093e9d71cbb71cb2ec68d8d95a8d8bb437162c273eff02->enter($__internal_3523d83876602b0361093e9d71cbb71cb2ec68d8d95a8d8bb437162c273eff02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_bd4640bdf0a4ca98e7b34d9fcd40ab334be092fbd31c98c70fcc6e0fb712394a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd4640bdf0a4ca98e7b34d9fcd40ab334be092fbd31c98c70fcc6e0fb712394a->enter($__internal_bd4640bdf0a4ca98e7b34d9fcd40ab334be092fbd31c98c70fcc6e0fb712394a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_bd4640bdf0a4ca98e7b34d9fcd40ab334be092fbd31c98c70fcc6e0fb712394a->leave($__internal_bd4640bdf0a4ca98e7b34d9fcd40ab334be092fbd31c98c70fcc6e0fb712394a_prof);

        
        $__internal_3523d83876602b0361093e9d71cbb71cb2ec68d8d95a8d8bb437162c273eff02->leave($__internal_3523d83876602b0361093e9d71cbb71cb2ec68d8d95a8d8bb437162c273eff02_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_d868a78d2bd42fe78858e4497b0302f0b4830914c117aee4d11cdb1c17b91f2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d868a78d2bd42fe78858e4497b0302f0b4830914c117aee4d11cdb1c17b91f2e->enter($__internal_d868a78d2bd42fe78858e4497b0302f0b4830914c117aee4d11cdb1c17b91f2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_89322836b9bea84b09b2fde130e409d78c9de965e5b76780b8b53c1846a5f2ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_89322836b9bea84b09b2fde130e409d78c9de965e5b76780b8b53c1846a5f2ed->enter($__internal_89322836b9bea84b09b2fde130e409d78c9de965e5b76780b8b53c1846a5f2ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 137, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo ")
";
        
        $__internal_89322836b9bea84b09b2fde130e409d78c9de965e5b76780b8b53c1846a5f2ed->leave($__internal_89322836b9bea84b09b2fde130e409d78c9de965e5b76780b8b53c1846a5f2ed_prof);

        
        $__internal_d868a78d2bd42fe78858e4497b0302f0b4830914c117aee4d11cdb1c17b91f2e->leave($__internal_d868a78d2bd42fe78858e4497b0302f0b4830914c117aee4d11cdb1c17b91f2e_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_2e383179887a214901f0ead62489526b216febeed6a34c8e8e05ad6fda2b7f64 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e383179887a214901f0ead62489526b216febeed6a34c8e8e05ad6fda2b7f64->enter($__internal_2e383179887a214901f0ead62489526b216febeed6a34c8e8e05ad6fda2b7f64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_154b14bf132134a7fd150f353478a58c744938ef8b6c6a56448c6d279b9c850e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_154b14bf132134a7fd150f353478a58c744938ef8b6c6a56448c6d279b9c850e->enter($__internal_154b14bf132134a7fd150f353478a58c744938ef8b6c6a56448c6d279b9c850e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 141)->display($context);
        
        $__internal_154b14bf132134a7fd150f353478a58c744938ef8b6c6a56448c6d279b9c850e->leave($__internal_154b14bf132134a7fd150f353478a58c744938ef8b6c6a56448c6d279b9c850e_prof);

        
        $__internal_2e383179887a214901f0ead62489526b216febeed6a34c8e8e05ad6fda2b7f64->leave($__internal_2e383179887a214901f0ead62489526b216febeed6a34c8e8e05ad6fda2b7f64_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
