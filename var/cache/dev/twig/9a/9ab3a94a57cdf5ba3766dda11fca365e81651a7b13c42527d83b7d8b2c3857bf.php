<?php

/* SonataAdminBundle:CRUD:show.html.twig */
class __TwigTemplate_33c2735b5dfbcd9fabd07fc6f2f78ee328b63e269754b03c1a7100e78c9cda1e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show.html.twig", "SonataAdminBundle:CRUD:show.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_765a3df94b73033d37bb25addfecfe79eeb52ef9143ac1e9650ea6dd27aa82c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_765a3df94b73033d37bb25addfecfe79eeb52ef9143ac1e9650ea6dd27aa82c1->enter($__internal_765a3df94b73033d37bb25addfecfe79eeb52ef9143ac1e9650ea6dd27aa82c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show.html.twig"));

        $__internal_4513b358139d3124e85b28e92119de1c15f938df0f43350691ac052cf8141a0f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4513b358139d3124e85b28e92119de1c15f938df0f43350691ac052cf8141a0f->enter($__internal_4513b358139d3124e85b28e92119de1c15f938df0f43350691ac052cf8141a0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_765a3df94b73033d37bb25addfecfe79eeb52ef9143ac1e9650ea6dd27aa82c1->leave($__internal_765a3df94b73033d37bb25addfecfe79eeb52ef9143ac1e9650ea6dd27aa82c1_prof);

        
        $__internal_4513b358139d3124e85b28e92119de1c15f938df0f43350691ac052cf8141a0f->leave($__internal_4513b358139d3124e85b28e92119de1c15f938df0f43350691ac052cf8141a0f_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show.html.twig' %}
", "SonataAdminBundle:CRUD:show.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/show.html.twig");
    }
}
