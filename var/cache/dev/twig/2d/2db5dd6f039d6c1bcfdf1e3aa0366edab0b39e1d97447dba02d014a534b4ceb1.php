<?php

/* :test:test.html.twig */
class __TwigTemplate_95f05eed540fb0894cf223ebc56fe77cfb103de804a4d49c4af99c1c962f795b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fb8cb9016165255712b5ff5e3ae59b0846ba8037027c0529a33dd8b5d5fd922b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb8cb9016165255712b5ff5e3ae59b0846ba8037027c0529a33dd8b5d5fd922b->enter($__internal_fb8cb9016165255712b5ff5e3ae59b0846ba8037027c0529a33dd8b5d5fd922b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":test:test.html.twig"));

        $__internal_5fba935642dc8efd579058d8a598b4bde646e8a0e35d753e57f5c29fa373286f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5fba935642dc8efd579058d8a598b4bde646e8a0e35d753e57f5c29fa373286f->enter($__internal_5fba935642dc8efd579058d8a598b4bde646e8a0e35d753e57f5c29fa373286f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":test:test.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\" />
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
</head>
<body>
";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 13
        $this->displayBlock('javascripts', $context, $blocks);
        // line 14
        echo "</body>
</html>
";
        
        $__internal_fb8cb9016165255712b5ff5e3ae59b0846ba8037027c0529a33dd8b5d5fd922b->leave($__internal_fb8cb9016165255712b5ff5e3ae59b0846ba8037027c0529a33dd8b5d5fd922b_prof);

        
        $__internal_5fba935642dc8efd579058d8a598b4bde646e8a0e35d753e57f5c29fa373286f->leave($__internal_5fba935642dc8efd579058d8a598b4bde646e8a0e35d753e57f5c29fa373286f_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_bd86d1b9dc5c6695c8c83e59dd68d47e79893ae5237985bbb8e6da5bc82511d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd86d1b9dc5c6695c8c83e59dd68d47e79893ae5237985bbb8e6da5bc82511d4->enter($__internal_bd86d1b9dc5c6695c8c83e59dd68d47e79893ae5237985bbb8e6da5bc82511d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_5bfc42af8599225e4be323ddd34b2b635a4e62ae939ff51e06456f2e3ec4fd3b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5bfc42af8599225e4be323ddd34b2b635a4e62ae939ff51e06456f2e3ec4fd3b->enter($__internal_5bfc42af8599225e4be323ddd34b2b635a4e62ae939ff51e06456f2e3ec4fd3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Test!";
        
        $__internal_5bfc42af8599225e4be323ddd34b2b635a4e62ae939ff51e06456f2e3ec4fd3b->leave($__internal_5bfc42af8599225e4be323ddd34b2b635a4e62ae939ff51e06456f2e3ec4fd3b_prof);

        
        $__internal_bd86d1b9dc5c6695c8c83e59dd68d47e79893ae5237985bbb8e6da5bc82511d4->leave($__internal_bd86d1b9dc5c6695c8c83e59dd68d47e79893ae5237985bbb8e6da5bc82511d4_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_baa1306e5f7a3f95c111b467fda55281e63692cb273894fe35ffba53445023db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_baa1306e5f7a3f95c111b467fda55281e63692cb273894fe35ffba53445023db->enter($__internal_baa1306e5f7a3f95c111b467fda55281e63692cb273894fe35ffba53445023db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_f9a157f1826ce6e5ac288fcc06f676ebac0f4d70bba76db2aadd318a642ad9e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f9a157f1826ce6e5ac288fcc06f676ebac0f4d70bba76db2aadd318a642ad9e8->enter($__internal_f9a157f1826ce6e5ac288fcc06f676ebac0f4d70bba76db2aadd318a642ad9e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_f9a157f1826ce6e5ac288fcc06f676ebac0f4d70bba76db2aadd318a642ad9e8->leave($__internal_f9a157f1826ce6e5ac288fcc06f676ebac0f4d70bba76db2aadd318a642ad9e8_prof);

        
        $__internal_baa1306e5f7a3f95c111b467fda55281e63692cb273894fe35ffba53445023db->leave($__internal_baa1306e5f7a3f95c111b467fda55281e63692cb273894fe35ffba53445023db_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_33a88c25dc38779c60c4d1b7bff43edb764f526e443eec98f0495e01f56a5da1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_33a88c25dc38779c60c4d1b7bff43edb764f526e443eec98f0495e01f56a5da1->enter($__internal_33a88c25dc38779c60c4d1b7bff43edb764f526e443eec98f0495e01f56a5da1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a24c0a05a9621bfff00dbf3ec7e12a49a8f92072fdc30f52532d93dcdbf0a0bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a24c0a05a9621bfff00dbf3ec7e12a49a8f92072fdc30f52532d93dcdbf0a0bf->enter($__internal_a24c0a05a9621bfff00dbf3ec7e12a49a8f92072fdc30f52532d93dcdbf0a0bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 11
        echo "    <p>test1</p>
";
        
        $__internal_a24c0a05a9621bfff00dbf3ec7e12a49a8f92072fdc30f52532d93dcdbf0a0bf->leave($__internal_a24c0a05a9621bfff00dbf3ec7e12a49a8f92072fdc30f52532d93dcdbf0a0bf_prof);

        
        $__internal_33a88c25dc38779c60c4d1b7bff43edb764f526e443eec98f0495e01f56a5da1->leave($__internal_33a88c25dc38779c60c4d1b7bff43edb764f526e443eec98f0495e01f56a5da1_prof);

    }

    // line 13
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_b557ae541fae9d161cbf93553f0ae5d9fd4e72d3ccda3a982e43a1cf2cd45ff3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b557ae541fae9d161cbf93553f0ae5d9fd4e72d3ccda3a982e43a1cf2cd45ff3->enter($__internal_b557ae541fae9d161cbf93553f0ae5d9fd4e72d3ccda3a982e43a1cf2cd45ff3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_53b906e2e6544623b5689628a3e63a5535a504f237e2481c2402e604a516e814 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_53b906e2e6544623b5689628a3e63a5535a504f237e2481c2402e604a516e814->enter($__internal_53b906e2e6544623b5689628a3e63a5535a504f237e2481c2402e604a516e814_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_53b906e2e6544623b5689628a3e63a5535a504f237e2481c2402e604a516e814->leave($__internal_53b906e2e6544623b5689628a3e63a5535a504f237e2481c2402e604a516e814_prof);

        
        $__internal_b557ae541fae9d161cbf93553f0ae5d9fd4e72d3ccda3a982e43a1cf2cd45ff3->leave($__internal_b557ae541fae9d161cbf93553f0ae5d9fd4e72d3ccda3a982e43a1cf2cd45ff3_prof);

    }

    public function getTemplateName()
    {
        return ":test:test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 13,  108 => 11,  99 => 10,  82 => 6,  64 => 5,  52 => 14,  50 => 13,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\" />
    <title>{% block title %}Test!{% endblock %}</title>
    {% block stylesheets %}{% endblock %}
    <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
</head>
<body>
{% block body %}
    <p>test1</p>
{% endblock %}
{% block javascripts %}{% endblock %}
</body>
</html>
", ":test:test.html.twig", "/Users/dp/Sites/frame-0/app/Resources/views/test/test.html.twig");
    }
}
