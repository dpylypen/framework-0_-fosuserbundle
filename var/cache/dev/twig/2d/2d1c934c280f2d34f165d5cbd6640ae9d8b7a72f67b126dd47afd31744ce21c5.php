<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_b38b7fe69436a3d4eedd790c8a1478b097064322f1a5f9429f044fe17af7ec1a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c5efa971ad81909f4598ef52d24308240b59a086a4fcf6a727c6793c3263588e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c5efa971ad81909f4598ef52d24308240b59a086a4fcf6a727c6793c3263588e->enter($__internal_c5efa971ad81909f4598ef52d24308240b59a086a4fcf6a727c6793c3263588e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $__internal_0b310992dee5cc6ee2a89d14f71758858c5fa3577da0a761d9838a7aa5b943bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b310992dee5cc6ee2a89d14f71758858c5fa3577da0a761d9838a7aa5b943bf->enter($__internal_0b310992dee5cc6ee2a89d14f71758858c5fa3577da0a761d9838a7aa5b943bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c5efa971ad81909f4598ef52d24308240b59a086a4fcf6a727c6793c3263588e->leave($__internal_c5efa971ad81909f4598ef52d24308240b59a086a4fcf6a727c6793c3263588e_prof);

        
        $__internal_0b310992dee5cc6ee2a89d14f71758858c5fa3577da0a761d9838a7aa5b943bf->leave($__internal_0b310992dee5cc6ee2a89d14f71758858c5fa3577da0a761d9838a7aa5b943bf_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_99c92a7596ad0e4f8da81bc0afece175725a4ace8bdf3e4d617df659c93b8337 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_99c92a7596ad0e4f8da81bc0afece175725a4ace8bdf3e4d617df659c93b8337->enter($__internal_99c92a7596ad0e4f8da81bc0afece175725a4ace8bdf3e4d617df659c93b8337_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_bb27c8ba467580b17cd7a7fbb4a26448c17aee24d7eada11dea66e0d12fdccca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb27c8ba467580b17cd7a7fbb4a26448c17aee24d7eada11dea66e0d12fdccca->enter($__internal_bb27c8ba467580b17cd7a7fbb4a26448c17aee24d7eada11dea66e0d12fdccca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_bb27c8ba467580b17cd7a7fbb4a26448c17aee24d7eada11dea66e0d12fdccca->leave($__internal_bb27c8ba467580b17cd7a7fbb4a26448c17aee24d7eada11dea66e0d12fdccca_prof);

        
        $__internal_99c92a7596ad0e4f8da81bc0afece175725a4ace8bdf3e4d617df659c93b8337->leave($__internal_99c92a7596ad0e4f8da81bc0afece175725a4ace8bdf3e4d617df659c93b8337_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}
