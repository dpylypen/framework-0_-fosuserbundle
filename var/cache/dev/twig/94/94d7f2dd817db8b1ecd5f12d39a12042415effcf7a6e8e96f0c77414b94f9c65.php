<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_bf3fd542c254903cdaebf9daaa7f720757acd58ee7740329601b11c2098ac28e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3702070b4d8eedc4f9292799332f94781bd3e12be263ece907f7c90a05fe308c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3702070b4d8eedc4f9292799332f94781bd3e12be263ece907f7c90a05fe308c->enter($__internal_3702070b4d8eedc4f9292799332f94781bd3e12be263ece907f7c90a05fe308c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $__internal_b92fe6be0eed59a1c797ce74d32b09c6091cf7b45a7d10a2f8b133a582f70ca7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b92fe6be0eed59a1c797ce74d32b09c6091cf7b45a7d10a2f8b133a582f70ca7->enter($__internal_b92fe6be0eed59a1c797ce74d32b09c6091cf7b45a7d10a2f8b133a582f70ca7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3702070b4d8eedc4f9292799332f94781bd3e12be263ece907f7c90a05fe308c->leave($__internal_3702070b4d8eedc4f9292799332f94781bd3e12be263ece907f7c90a05fe308c_prof);

        
        $__internal_b92fe6be0eed59a1c797ce74d32b09c6091cf7b45a7d10a2f8b133a582f70ca7->leave($__internal_b92fe6be0eed59a1c797ce74d32b09c6091cf7b45a7d10a2f8b133a582f70ca7_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_bbcabeb223df55a119420b119ee43cb135f92f9e105fa6d944abb696d5912f9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bbcabeb223df55a119420b119ee43cb135f92f9e105fa6d944abb696d5912f9e->enter($__internal_bbcabeb223df55a119420b119ee43cb135f92f9e105fa6d944abb696d5912f9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_d859e19185a4b30e8c4f2d12e5d649094ca8e10c240355a662740a39bd12b071 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d859e19185a4b30e8c4f2d12e5d649094ca8e10c240355a662740a39bd12b071->enter($__internal_d859e19185a4b30e8c4f2d12e5d649094ca8e10c240355a662740a39bd12b071_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => (isset($context["tokenLifetime"]) || array_key_exists("tokenLifetime", $context) ? $context["tokenLifetime"] : (function () { throw new Twig_Error_Runtime('Variable "tokenLifetime" does not exist.', 7, $this->getSourceContext()); })())), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_d859e19185a4b30e8c4f2d12e5d649094ca8e10c240355a662740a39bd12b071->leave($__internal_d859e19185a4b30e8c4f2d12e5d649094ca8e10c240355a662740a39bd12b071_prof);

        
        $__internal_bbcabeb223df55a119420b119ee43cb135f92f9e105fa6d944abb696d5912f9e->leave($__internal_bbcabeb223df55a119420b119ee43cb135f92f9e105fa6d944abb696d5912f9e_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:check_email.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}
