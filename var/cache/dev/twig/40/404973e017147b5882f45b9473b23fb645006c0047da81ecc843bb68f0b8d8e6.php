<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_8654e792d9d9e0f54474310d9e923f767c18f716cbcd7137e57e8dd1a1e456df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_df3e0e1c15ca0dfed1805edfbfebeecd1856fae40cbece9e89e5a65cefb4efa9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df3e0e1c15ca0dfed1805edfbfebeecd1856fae40cbece9e89e5a65cefb4efa9->enter($__internal_df3e0e1c15ca0dfed1805edfbfebeecd1856fae40cbece9e89e5a65cefb4efa9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        $__internal_f97b4e5e57d898d712d20da8c40ee34be022d9de4f2e8688d77e1dedf315192a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f97b4e5e57d898d712d20da8c40ee34be022d9de4f2e8688d77e1dedf315192a->enter($__internal_f97b4e5e57d898d712d20da8c40ee34be022d9de4f2e8688d77e1dedf315192a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_df3e0e1c15ca0dfed1805edfbfebeecd1856fae40cbece9e89e5a65cefb4efa9->leave($__internal_df3e0e1c15ca0dfed1805edfbfebeecd1856fae40cbece9e89e5a65cefb4efa9_prof);

        
        $__internal_f97b4e5e57d898d712d20da8c40ee34be022d9de4f2e8688d77e1dedf315192a->leave($__internal_f97b4e5e57d898d712d20da8c40ee34be022d9de4f2e8688d77e1dedf315192a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
", "@Framework/Form/form_widget_simple.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_simple.html.php");
    }
}
