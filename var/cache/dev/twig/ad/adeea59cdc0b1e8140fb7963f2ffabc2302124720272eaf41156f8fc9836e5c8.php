<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_4b6ec600105fdbf016705120b9c0ead55027215b220837b5c2586a4c745b12a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2767ae661697dc9230a7f5537152cd1cfa60bb5f0e4d08a52dc6df96b1ac5bc1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2767ae661697dc9230a7f5537152cd1cfa60bb5f0e4d08a52dc6df96b1ac5bc1->enter($__internal_2767ae661697dc9230a7f5537152cd1cfa60bb5f0e4d08a52dc6df96b1ac5bc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_b54c85b27b22dd5eb8751aef86fb3b37f0ab1f992ad072bda69d857dfc6b957d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b54c85b27b22dd5eb8751aef86fb3b37f0ab1f992ad072bda69d857dfc6b957d->enter($__internal_b54c85b27b22dd5eb8751aef86fb3b37f0ab1f992ad072bda69d857dfc6b957d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })())));
        echo "
*/
";
        
        $__internal_2767ae661697dc9230a7f5537152cd1cfa60bb5f0e4d08a52dc6df96b1ac5bc1->leave($__internal_2767ae661697dc9230a7f5537152cd1cfa60bb5f0e4d08a52dc6df96b1ac5bc1_prof);

        
        $__internal_b54c85b27b22dd5eb8751aef86fb3b37f0ab1f992ad072bda69d857dfc6b957d->leave($__internal_b54c85b27b22dd5eb8751aef86fb3b37f0ab1f992ad072bda69d857dfc6b957d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.css.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
