<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_e50151a0b8ffb46319fad947601e2b472b6de3c5855a7063dc4f4714fe194a64 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f50a6a8e7c100accddc6aafee917262ed41bae105377a2a6905eb72279ed3523 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f50a6a8e7c100accddc6aafee917262ed41bae105377a2a6905eb72279ed3523->enter($__internal_f50a6a8e7c100accddc6aafee917262ed41bae105377a2a6905eb72279ed3523_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_94a0b91e05e846800387c06f546fdf6e3468c6b8bf06bf185fc3550ee4d622ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94a0b91e05e846800387c06f546fdf6e3468c6b8bf06bf185fc3550ee4d622ff->enter($__internal_94a0b91e05e846800387c06f546fdf6e3468c6b8bf06bf185fc3550ee4d622ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_f50a6a8e7c100accddc6aafee917262ed41bae105377a2a6905eb72279ed3523->leave($__internal_f50a6a8e7c100accddc6aafee917262ed41bae105377a2a6905eb72279ed3523_prof);

        
        $__internal_94a0b91e05e846800387c06f546fdf6e3468c6b8bf06bf185fc3550ee4d622ff->leave($__internal_94a0b91e05e846800387c06f546fdf6e3468c6b8bf06bf185fc3550ee4d622ff_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
