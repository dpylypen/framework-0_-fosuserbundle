<?php

/* SonataAdminBundle:CRUD:edit.html.twig */
class __TwigTemplate_e48c5fe22156d1d70fc1cc0e92d16a0bddbf4f08a37e93bc272777eb8bf30559 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_edit.html.twig", "SonataAdminBundle:CRUD:edit.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_edit.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5e85c96c21e501ab6af1510cc2617a7286d633cb410fe1d5a696d2f39fb857a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5e85c96c21e501ab6af1510cc2617a7286d633cb410fe1d5a696d2f39fb857a5->enter($__internal_5e85c96c21e501ab6af1510cc2617a7286d633cb410fe1d5a696d2f39fb857a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit.html.twig"));

        $__internal_733ad54f69270f436a82be5fc56e83dd9986ca21cb1ff823f55864d929ea0ba3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_733ad54f69270f436a82be5fc56e83dd9986ca21cb1ff823f55864d929ea0ba3->enter($__internal_733ad54f69270f436a82be5fc56e83dd9986ca21cb1ff823f55864d929ea0ba3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5e85c96c21e501ab6af1510cc2617a7286d633cb410fe1d5a696d2f39fb857a5->leave($__internal_5e85c96c21e501ab6af1510cc2617a7286d633cb410fe1d5a696d2f39fb857a5_prof);

        
        $__internal_733ad54f69270f436a82be5fc56e83dd9986ca21cb1ff823f55864d929ea0ba3->leave($__internal_733ad54f69270f436a82be5fc56e83dd9986ca21cb1ff823f55864d929ea0ba3_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_edit.html.twig' %}
", "SonataAdminBundle:CRUD:edit.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/edit.html.twig");
    }
}
