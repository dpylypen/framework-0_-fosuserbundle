<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_f7e92f177088ecc2508b5a91801de9836c54bbaa74ab89abdedef6745515ac3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eb1e2b33458258e1c3f7f7d89345cb74d26c9ec1ae7c2540cbcd65a7a9a12efb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb1e2b33458258e1c3f7f7d89345cb74d26c9ec1ae7c2540cbcd65a7a9a12efb->enter($__internal_eb1e2b33458258e1c3f7f7d89345cb74d26c9ec1ae7c2540cbcd65a7a9a12efb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_aa1765daf9a46535c7bdddb5be98c48ee256933de9506300adac450b13d76ce7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa1765daf9a46535c7bdddb5be98c48ee256933de9506300adac450b13d76ce7->enter($__internal_aa1765daf9a46535c7bdddb5be98c48ee256933de9506300adac450b13d76ce7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_eb1e2b33458258e1c3f7f7d89345cb74d26c9ec1ae7c2540cbcd65a7a9a12efb->leave($__internal_eb1e2b33458258e1c3f7f7d89345cb74d26c9ec1ae7c2540cbcd65a7a9a12efb_prof);

        
        $__internal_aa1765daf9a46535c7bdddb5be98c48ee256933de9506300adac450b13d76ce7->leave($__internal_aa1765daf9a46535c7bdddb5be98c48ee256933de9506300adac450b13d76ce7_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_f351815c8cea7ed23ecf51fd2840cc7256fe3f9d464101e09d6bcd314847b41a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f351815c8cea7ed23ecf51fd2840cc7256fe3f9d464101e09d6bcd314847b41a->enter($__internal_f351815c8cea7ed23ecf51fd2840cc7256fe3f9d464101e09d6bcd314847b41a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_ca697dac75bb4e7115345d8022b132c0de891ff493c99491937bdbad723e5b23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca697dac75bb4e7115345d8022b132c0de891ff493c99491937bdbad723e5b23->enter($__internal_ca697dac75bb4e7115345d8022b132c0de891ff493c99491937bdbad723e5b23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_ca697dac75bb4e7115345d8022b132c0de891ff493c99491937bdbad723e5b23->leave($__internal_ca697dac75bb4e7115345d8022b132c0de891ff493c99491937bdbad723e5b23_prof);

        
        $__internal_f351815c8cea7ed23ecf51fd2840cc7256fe3f9d464101e09d6bcd314847b41a->leave($__internal_f351815c8cea7ed23ecf51fd2840cc7256fe3f9d464101e09d6bcd314847b41a_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_aa9d8b582065e599f0f0deb417fc393bbe1c7824dfbc07c5e1db9315fd4516de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa9d8b582065e599f0f0deb417fc393bbe1c7824dfbc07c5e1db9315fd4516de->enter($__internal_aa9d8b582065e599f0f0deb417fc393bbe1c7824dfbc07c5e1db9315fd4516de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e72c4cdefa2d1c5bf93eb76d1f53a20ff8a9a488acfb9f208c00b60aa96871ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e72c4cdefa2d1c5bf93eb76d1f53a20ff8a9a488acfb9f208c00b60aa96871ab->enter($__internal_e72c4cdefa2d1c5bf93eb76d1f53a20ff8a9a488acfb9f208c00b60aa96871ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) || array_key_exists("location", $context) ? $context["location"] : (function () { throw new Twig_Error_Runtime('Variable "location" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) || array_key_exists("location", $context) ? $context["location"] : (function () { throw new Twig_Error_Runtime('Variable "location" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_e72c4cdefa2d1c5bf93eb76d1f53a20ff8a9a488acfb9f208c00b60aa96871ab->leave($__internal_e72c4cdefa2d1c5bf93eb76d1f53a20ff8a9a488acfb9f208c00b60aa96871ab_prof);

        
        $__internal_aa9d8b582065e599f0f0deb417fc393bbe1c7824dfbc07c5e1db9315fd4516de->leave($__internal_aa9d8b582065e599f0f0deb417fc393bbe1c7824dfbc07c5e1db9315fd4516de_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
