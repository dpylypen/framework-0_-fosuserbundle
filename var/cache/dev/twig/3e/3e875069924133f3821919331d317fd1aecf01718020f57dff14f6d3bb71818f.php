<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_fc9c41d16b8df2792c3f1e57fefdef54fe027b7d0a13fb1936056df6837bb5ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_19344332dc52feede437613944e456a4c441a49019a88f694e6039f7015d92a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19344332dc52feede437613944e456a4c441a49019a88f694e6039f7015d92a2->enter($__internal_19344332dc52feede437613944e456a4c441a49019a88f694e6039f7015d92a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $__internal_99e71ab21151b0d3e9321f7e6a4b76dc091ae37e6a068db927dc8e9a22ffa405 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_99e71ab21151b0d3e9321f7e6a4b76dc091ae37e6a068db927dc8e9a22ffa405->enter($__internal_99e71ab21151b0d3e9321f7e6a4b76dc091ae37e6a068db927dc8e9a22ffa405_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_19344332dc52feede437613944e456a4c441a49019a88f694e6039f7015d92a2->leave($__internal_19344332dc52feede437613944e456a4c441a49019a88f694e6039f7015d92a2_prof);

        
        $__internal_99e71ab21151b0d3e9321f7e6a4b76dc091ae37e6a068db927dc8e9a22ffa405->leave($__internal_99e71ab21151b0d3e9321f7e6a4b76dc091ae37e6a068db927dc8e9a22ffa405_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_d4306f67e96c35ecee2da3c861362426d4e4c79b22009d0571d8da20a777244d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d4306f67e96c35ecee2da3c861362426d4e4c79b22009d0571d8da20a777244d->enter($__internal_d4306f67e96c35ecee2da3c861362426d4e4c79b22009d0571d8da20a777244d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_f09aa379b3bb6478510b906cc15a5a6a6a8c7ae609d186b30d6c80f5f77c5f56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f09aa379b3bb6478510b906cc15a5a6a6a8c7ae609d186b30d6c80f5f77c5f56->enter($__internal_f09aa379b3bb6478510b906cc15a5a6a6a8c7ae609d186b30d6c80f5f77c5f56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_f09aa379b3bb6478510b906cc15a5a6a6a8c7ae609d186b30d6c80f5f77c5f56->leave($__internal_f09aa379b3bb6478510b906cc15a5a6a6a8c7ae609d186b30d6c80f5f77c5f56_prof);

        
        $__internal_d4306f67e96c35ecee2da3c861362426d4e4c79b22009d0571d8da20a777244d->leave($__internal_d4306f67e96c35ecee2da3c861362426d4e4c79b22009d0571d8da20a777244d_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_14c2dd40caba673673e5c13a79af0b1ad1f2f141edbf0761b71cf2c83885d634 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_14c2dd40caba673673e5c13a79af0b1ad1f2f141edbf0761b71cf2c83885d634->enter($__internal_14c2dd40caba673673e5c13a79af0b1ad1f2f141edbf0761b71cf2c83885d634_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_8d8c00a3d0a15a9e6418b1342830049c51b613839177f58336c7b4eaca012684 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d8c00a3d0a15a9e6418b1342830049c51b613839177f58336c7b4eaca012684->enter($__internal_8d8c00a3d0a15a9e6418b1342830049c51b613839177f58336c7b4eaca012684_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_8d8c00a3d0a15a9e6418b1342830049c51b613839177f58336c7b4eaca012684->leave($__internal_8d8c00a3d0a15a9e6418b1342830049c51b613839177f58336c7b4eaca012684_prof);

        
        $__internal_14c2dd40caba673673e5c13a79af0b1ad1f2f141edbf0761b71cf2c83885d634->leave($__internal_14c2dd40caba673673e5c13a79af0b1ad1f2f141edbf0761b71cf2c83885d634_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_bb3f3743494788769fd5219586360ec313712bfd6cfaea0a0d1ad41496d7908c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb3f3743494788769fd5219586360ec313712bfd6cfaea0a0d1ad41496d7908c->enter($__internal_bb3f3743494788769fd5219586360ec313712bfd6cfaea0a0d1ad41496d7908c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_742a4407c521a9e27060e5932816f86a2415a83f97cb50ffcc31e884472967fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_742a4407c521a9e27060e5932816f86a2415a83f97cb50ffcc31e884472967fb->enter($__internal_742a4407c521a9e27060e5932816f86a2415a83f97cb50ffcc31e884472967fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_742a4407c521a9e27060e5932816f86a2415a83f97cb50ffcc31e884472967fb->leave($__internal_742a4407c521a9e27060e5932816f86a2415a83f97cb50ffcc31e884472967fb_prof);

        
        $__internal_bb3f3743494788769fd5219586360ec313712bfd6cfaea0a0d1ad41496d7908c->leave($__internal_bb3f3743494788769fd5219586360ec313712bfd6cfaea0a0d1ad41496d7908c_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
