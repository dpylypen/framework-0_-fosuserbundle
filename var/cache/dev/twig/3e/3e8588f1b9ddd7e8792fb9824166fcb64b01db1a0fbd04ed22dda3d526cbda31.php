<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_0c55c523c0d2087a462fcf94685173e8a4452edeae2b0993bc7ea1ab8fdd0d77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9f4fd0c76b1dddefc6e8f2e2c726c41349bd2b21f3e85c6b304b856377d40d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9f4fd0c76b1dddefc6e8f2e2c726c41349bd2b21f3e85c6b304b856377d40d1->enter($__internal_c9f4fd0c76b1dddefc6e8f2e2c726c41349bd2b21f3e85c6b304b856377d40d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        $__internal_231b60af79412eebf172d44aa618d4594f6ad0740650b4eef46fa8d4f1dbcd80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_231b60af79412eebf172d44aa618d4594f6ad0740650b4eef46fa8d4f1dbcd80->enter($__internal_231b60af79412eebf172d44aa618d4594f6ad0740650b4eef46fa8d4f1dbcd80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_c9f4fd0c76b1dddefc6e8f2e2c726c41349bd2b21f3e85c6b304b856377d40d1->leave($__internal_c9f4fd0c76b1dddefc6e8f2e2c726c41349bd2b21f3e85c6b304b856377d40d1_prof);

        
        $__internal_231b60af79412eebf172d44aa618d4594f6ad0740650b4eef46fa8d4f1dbcd80->leave($__internal_231b60af79412eebf172d44aa618d4594f6ad0740650b4eef46fa8d4f1dbcd80_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
", "@Security/Collector/icon.svg", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/views/Collector/icon.svg");
    }
}
