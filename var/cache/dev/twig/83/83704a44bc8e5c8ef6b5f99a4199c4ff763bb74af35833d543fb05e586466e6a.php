<?php

/* SonataBlockBundle:Block:block_base.html.twig */
class __TwigTemplate_8e60c00f1f7f2ba24bf843b31c4b6153716ff8fd4444004da94ee5c2558dbd82 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f538c887515c1980e330cef298fc6bf77779641fdf1502e439ea7fda4442e1d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f538c887515c1980e330cef298fc6bf77779641fdf1502e439ea7fda4442e1d2->enter($__internal_f538c887515c1980e330cef298fc6bf77779641fdf1502e439ea7fda4442e1d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_base.html.twig"));

        $__internal_0d1c66e72598cba654c1d495a7678b8f2aa6fed51f195707f4a14a20cb86ac57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d1c66e72598cba654c1d495a7678b8f2aa6fed51f195707f4a14a20cb86ac57->enter($__internal_0d1c66e72598cba654c1d495a7678b8f2aa6fed51f195707f4a14a20cb86ac57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_base.html.twig"));

        // line 11
        echo "<div id=\"cms-block-";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["block"]) || array_key_exists("block", $context) ? $context["block"] : (function () { throw new Twig_Error_Runtime('Variable "block" does not exist.', 11, $this->getSourceContext()); })()), "id", array()), "html", null, true);
        echo "\" class=\"cms-block cms-block-element\">
    ";
        // line 12
        $this->displayBlock('block', $context, $blocks);
        // line 13
        echo "</div>
";
        
        $__internal_f538c887515c1980e330cef298fc6bf77779641fdf1502e439ea7fda4442e1d2->leave($__internal_f538c887515c1980e330cef298fc6bf77779641fdf1502e439ea7fda4442e1d2_prof);

        
        $__internal_0d1c66e72598cba654c1d495a7678b8f2aa6fed51f195707f4a14a20cb86ac57->leave($__internal_0d1c66e72598cba654c1d495a7678b8f2aa6fed51f195707f4a14a20cb86ac57_prof);

    }

    // line 12
    public function block_block($context, array $blocks = array())
    {
        $__internal_e7a5a89ee3b6a65c3197e2fb387c5aa0227dca1a9f69cf875a2af5156e664c33 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e7a5a89ee3b6a65c3197e2fb387c5aa0227dca1a9f69cf875a2af5156e664c33->enter($__internal_e7a5a89ee3b6a65c3197e2fb387c5aa0227dca1a9f69cf875a2af5156e664c33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_0cdd795f57405a75fbd8adb1c2135d75e31fe9e650df74e31e439b8422885016 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0cdd795f57405a75fbd8adb1c2135d75e31fe9e650df74e31e439b8422885016->enter($__internal_0cdd795f57405a75fbd8adb1c2135d75e31fe9e650df74e31e439b8422885016_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        echo "EMPTY CONTENT";
        
        $__internal_0cdd795f57405a75fbd8adb1c2135d75e31fe9e650df74e31e439b8422885016->leave($__internal_0cdd795f57405a75fbd8adb1c2135d75e31fe9e650df74e31e439b8422885016_prof);

        
        $__internal_e7a5a89ee3b6a65c3197e2fb387c5aa0227dca1a9f69cf875a2af5156e664c33->leave($__internal_e7a5a89ee3b6a65c3197e2fb387c5aa0227dca1a9f69cf875a2af5156e664c33_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 12,  33 => 13,  31 => 12,  26 => 11,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
<div id=\"cms-block-{{ block.id }}\" class=\"cms-block cms-block-element\">
    {% block block %}EMPTY CONTENT{% endblock %}
</div>
", "SonataBlockBundle:Block:block_base.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/block-bundle/Resources/views/Block/block_base.html.twig");
    }
}
