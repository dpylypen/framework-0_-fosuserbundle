<?php

/* @Twig/images/icon-minus-square-o.svg */
class __TwigTemplate_e212dc3599cc9129d5ed2f82f2a4e54dd90ef791229f9b3e3a2711eace5340f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04069d15f06271be1c019f004fdce5e02635d7f441381c1befdd5749ae2cf82f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_04069d15f06271be1c019f004fdce5e02635d7f441381c1befdd5749ae2cf82f->enter($__internal_04069d15f06271be1c019f004fdce5e02635d7f441381c1befdd5749ae2cf82f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square-o.svg"));

        $__internal_2658f87a0e42f3b01b39e3ab34207855f710d6f5b030385ec3c02b4f93461061 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2658f87a0e42f3b01b39e3ab34207855f710d6f5b030385ec3c02b4f93461061->enter($__internal_2658f87a0e42f3b01b39e3ab34207855f710d6f5b030385ec3c02b4f93461061_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square-o.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h832q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
";
        
        $__internal_04069d15f06271be1c019f004fdce5e02635d7f441381c1befdd5749ae2cf82f->leave($__internal_04069d15f06271be1c019f004fdce5e02635d7f441381c1befdd5749ae2cf82f_prof);

        
        $__internal_2658f87a0e42f3b01b39e3ab34207855f710d6f5b030385ec3c02b4f93461061->leave($__internal_2658f87a0e42f3b01b39e3ab34207855f710d6f5b030385ec3c02b4f93461061_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square-o.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h832q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
", "@Twig/images/icon-minus-square-o.svg", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-minus-square-o.svg");
    }
}
