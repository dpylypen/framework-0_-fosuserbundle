<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_c52349fc25f29a579b05e48c491adc9b6c0da293564bc81f865a2656a424b4ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e3654730889e44c525be62fcfdaf08b871078573d536b3f61c7c89d2ce9488a9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e3654730889e44c525be62fcfdaf08b871078573d536b3f61c7c89d2ce9488a9->enter($__internal_e3654730889e44c525be62fcfdaf08b871078573d536b3f61c7c89d2ce9488a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        $__internal_cb09a8a9c808b66018267a361f11366a2a47ec600967c7f7c6eabda2060da8cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb09a8a9c808b66018267a361f11366a2a47ec600967c7f7c6eabda2060da8cd->enter($__internal_cb09a8a9c808b66018267a361f11366a2a47ec600967c7f7c6eabda2060da8cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_e3654730889e44c525be62fcfdaf08b871078573d536b3f61c7c89d2ce9488a9->leave($__internal_e3654730889e44c525be62fcfdaf08b871078573d536b3f61c7c89d2ce9488a9_prof);

        
        $__internal_cb09a8a9c808b66018267a361f11366a2a47ec600967c7f7c6eabda2060da8cd->leave($__internal_cb09a8a9c808b66018267a361f11366a2a47ec600967c7f7c6eabda2060da8cd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
", "@Framework/Form/button_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_widget.html.php");
    }
}
