<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_c87c7361a28dbd2eeb595bb6387abb53ca2d5d4b02cd8ca46d8a9c62a6d29e7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e73565ce82b945580069ff7183475d38cc8024bb6b02a0e861de2be9a3e085ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e73565ce82b945580069ff7183475d38cc8024bb6b02a0e861de2be9a3e085ab->enter($__internal_e73565ce82b945580069ff7183475d38cc8024bb6b02a0e861de2be9a3e085ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_669a4e2cc54b834f321e38b579b170073b627ab5fd3d46413c1b9e7e5cedbdef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_669a4e2cc54b834f321e38b579b170073b627ab5fd3d46413c1b9e7e5cedbdef->enter($__internal_669a4e2cc54b834f321e38b579b170073b627ab5fd3d46413c1b9e7e5cedbdef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_e73565ce82b945580069ff7183475d38cc8024bb6b02a0e861de2be9a3e085ab->leave($__internal_e73565ce82b945580069ff7183475d38cc8024bb6b02a0e861de2be9a3e085ab_prof);

        
        $__internal_669a4e2cc54b834f321e38b579b170073b627ab5fd3d46413c1b9e7e5cedbdef->leave($__internal_669a4e2cc54b834f321e38b579b170073b627ab5fd3d46413c1b9e7e5cedbdef_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
