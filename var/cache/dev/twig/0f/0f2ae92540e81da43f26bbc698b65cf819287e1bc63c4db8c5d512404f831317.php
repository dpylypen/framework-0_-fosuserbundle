<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_dd9a441438f48acacab283ae24f63f5e6fb39ac965148ac867ce191964fce90e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ac694efdd28e43a8f24bd31f06b615b32755603b467d7d6e7fa2966b9ebe00f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ac694efdd28e43a8f24bd31f06b615b32755603b467d7d6e7fa2966b9ebe00f->enter($__internal_9ac694efdd28e43a8f24bd31f06b615b32755603b467d7d6e7fa2966b9ebe00f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $__internal_265a1d6216f6f3e0c545a0921176d03f5fbd08adc7289df81ce0e77fa5ca3fdf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_265a1d6216f6f3e0c545a0921176d03f5fbd08adc7289df81ce0e77fa5ca3fdf->enter($__internal_265a1d6216f6f3e0c545a0921176d03f5fbd08adc7289df81ce0e77fa5ca3fdf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9ac694efdd28e43a8f24bd31f06b615b32755603b467d7d6e7fa2966b9ebe00f->leave($__internal_9ac694efdd28e43a8f24bd31f06b615b32755603b467d7d6e7fa2966b9ebe00f_prof);

        
        $__internal_265a1d6216f6f3e0c545a0921176d03f5fbd08adc7289df81ce0e77fa5ca3fdf->leave($__internal_265a1d6216f6f3e0c545a0921176d03f5fbd08adc7289df81ce0e77fa5ca3fdf_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5cc237f28a694ee3e9b58511910cb92097554e981dea23a844ba26653b46817b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5cc237f28a694ee3e9b58511910cb92097554e981dea23a844ba26653b46817b->enter($__internal_5cc237f28a694ee3e9b58511910cb92097554e981dea23a844ba26653b46817b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_22265924d0affe15f61c30de8e7fe114e7399de559b234ce6f1131d3bb4275bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_22265924d0affe15f61c30de8e7fe114e7399de559b234ce6f1131d3bb4275bc->enter($__internal_22265924d0affe15f61c30de8e7fe114e7399de559b234ce6f1131d3bb4275bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_22265924d0affe15f61c30de8e7fe114e7399de559b234ce6f1131d3bb4275bc->leave($__internal_22265924d0affe15f61c30de8e7fe114e7399de559b234ce6f1131d3bb4275bc_prof);

        
        $__internal_5cc237f28a694ee3e9b58511910cb92097554e981dea23a844ba26653b46817b->leave($__internal_5cc237f28a694ee3e9b58511910cb92097554e981dea23a844ba26653b46817b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:edit.html.twig", "/Users/dp/Sites/frame-0/vendor/friendsofsymfony/user-bundle/Resources/views/Group/edit.html.twig");
    }
}
