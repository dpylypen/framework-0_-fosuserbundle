<?php

/* @WebProfiler/Icon/forward.svg */
class __TwigTemplate_2e73a67b16477c3f59a714987db455bc18cec63a1e8ef735c4b9e885abaf6a8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0f57eb309a9dfc93e3204aff96249360f03eebaacebf67f8a7b19733bc1f67b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0f57eb309a9dfc93e3204aff96249360f03eebaacebf67f8a7b19733bc1f67b->enter($__internal_e0f57eb309a9dfc93e3204aff96249360f03eebaacebf67f8a7b19733bc1f67b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        $__internal_24cf2c6ad24e34dbf16ea52b11e14d8c0149cf22a3f80575b62a1934d240aa34 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24cf2c6ad24e34dbf16ea52b11e14d8c0149cf22a3f80575b62a1934d240aa34->enter($__internal_24cf2c6ad24e34dbf16ea52b11e14d8c0149cf22a3f80575b62a1934d240aa34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
";
        
        $__internal_e0f57eb309a9dfc93e3204aff96249360f03eebaacebf67f8a7b19733bc1f67b->leave($__internal_e0f57eb309a9dfc93e3204aff96249360f03eebaacebf67f8a7b19733bc1f67b_prof);

        
        $__internal_24cf2c6ad24e34dbf16ea52b11e14d8c0149cf22a3f80575b62a1934d240aa34->leave($__internal_24cf2c6ad24e34dbf16ea52b11e14d8c0149cf22a3f80575b62a1934d240aa34_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/forward.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
", "@WebProfiler/Icon/forward.svg", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/forward.svg");
    }
}
