<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_5670d54e90ef24f76417a70ca69b2323b6d6964bfbb86db79a62be0247435313 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e324684762030289390d1610919dd259cc521547529328277ef20e72a6344263 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e324684762030289390d1610919dd259cc521547529328277ef20e72a6344263->enter($__internal_e324684762030289390d1610919dd259cc521547529328277ef20e72a6344263_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_46b43fbd01db3b702328a5888a34141c023fa6e98305b6c20c5223198940032e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46b43fbd01db3b702328a5888a34141c023fa6e98305b6c20c5223198940032e->enter($__internal_46b43fbd01db3b702328a5888a34141c023fa6e98305b6c20c5223198940032e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_e324684762030289390d1610919dd259cc521547529328277ef20e72a6344263->leave($__internal_e324684762030289390d1610919dd259cc521547529328277ef20e72a6344263_prof);

        
        $__internal_46b43fbd01db3b702328a5888a34141c023fa6e98305b6c20c5223198940032e->leave($__internal_46b43fbd01db3b702328a5888a34141c023fa6e98305b6c20c5223198940032e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
