<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_7d99e9b098d78202d9208f772449220de6e4386e9c76a0b7832b7de3425406ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_85a2d01e6ecfa0cd84d6ad7eaeb3409afa6bd10d353166a78f1f8ad5ac145960 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_85a2d01e6ecfa0cd84d6ad7eaeb3409afa6bd10d353166a78f1f8ad5ac145960->enter($__internal_85a2d01e6ecfa0cd84d6ad7eaeb3409afa6bd10d353166a78f1f8ad5ac145960_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_c669e19e4ce8475f5017703a5586ebca944eb32eaa518f15a63ac325e08fe0d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c669e19e4ce8475f5017703a5586ebca944eb32eaa518f15a63ac325e08fe0d4->enter($__internal_c669e19e4ce8475f5017703a5586ebca944eb32eaa518f15a63ac325e08fe0d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_85a2d01e6ecfa0cd84d6ad7eaeb3409afa6bd10d353166a78f1f8ad5ac145960->leave($__internal_85a2d01e6ecfa0cd84d6ad7eaeb3409afa6bd10d353166a78f1f8ad5ac145960_prof);

        
        $__internal_c669e19e4ce8475f5017703a5586ebca944eb32eaa518f15a63ac325e08fe0d4->leave($__internal_c669e19e4ce8475f5017703a5586ebca944eb32eaa518f15a63ac325e08fe0d4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}
