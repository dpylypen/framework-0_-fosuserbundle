<?php

/* @WebProfiler/Icon/cache.svg */
class __TwigTemplate_46bdf93a3b38288b764d04bc6a726111f030d1278f24c552e59fb69b3ce68703 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fba5ebcc050631c552ba799e4179bb36069a9715ee2fd41ff3ae4460ec7e503c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fba5ebcc050631c552ba799e4179bb36069a9715ee2fd41ff3ae4460ec7e503c->enter($__internal_fba5ebcc050631c552ba799e4179bb36069a9715ee2fd41ff3ae4460ec7e503c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/cache.svg"));

        $__internal_581da5fd0189c00fa4f837a9bae02906406e0941adf1acdb4b9b8bc75f7c5620 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_581da5fd0189c00fa4f837a9bae02906406e0941adf1acdb4b9b8bc75f7c5620->enter($__internal_581da5fd0189c00fa4f837a9bae02906406e0941adf1acdb4b9b8bc75f7c5620_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/cache.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAA\" d=\"M2.26 6.09l9.06-4.67a1.49 1.49 0 0 1 1.37 0l9.06 4.67a1.49 1.49 0 0 1 0 2.65l-9.06 4.67a1.49 1.49 0 0 1-1.37 0L2.26 8.74a1.49 1.49 0 0 1 0-2.65zM20.55 11L12 15.39 3.45 11a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73A1.36 1.36 0 0 0 20.55 11zm0 4.47L12 19.86l-8.55-4.41a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73a1.36 1.36 0 0 0-1.25-2.42z\"/>
</svg>
";
        
        $__internal_fba5ebcc050631c552ba799e4179bb36069a9715ee2fd41ff3ae4460ec7e503c->leave($__internal_fba5ebcc050631c552ba799e4179bb36069a9715ee2fd41ff3ae4460ec7e503c_prof);

        
        $__internal_581da5fd0189c00fa4f837a9bae02906406e0941adf1acdb4b9b8bc75f7c5620->leave($__internal_581da5fd0189c00fa4f837a9bae02906406e0941adf1acdb4b9b8bc75f7c5620_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/cache.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAA\" d=\"M2.26 6.09l9.06-4.67a1.49 1.49 0 0 1 1.37 0l9.06 4.67a1.49 1.49 0 0 1 0 2.65l-9.06 4.67a1.49 1.49 0 0 1-1.37 0L2.26 8.74a1.49 1.49 0 0 1 0-2.65zM20.55 11L12 15.39 3.45 11a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73A1.36 1.36 0 0 0 20.55 11zm0 4.47L12 19.86l-8.55-4.41a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73a1.36 1.36 0 0 0-1.25-2.42z\"/>
</svg>
", "@WebProfiler/Icon/cache.svg", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/cache.svg");
    }
}
