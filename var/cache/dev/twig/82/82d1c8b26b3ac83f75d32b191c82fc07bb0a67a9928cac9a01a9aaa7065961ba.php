<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_5d93fc426d4ef442e2a6a36d2e6c274e02e0006bbc37a72f6973b80e12430ed2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fb007d1723bd1b2c365fd4b145c5838596f19b67b5136e20d7e7b602ce7af77f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb007d1723bd1b2c365fd4b145c5838596f19b67b5136e20d7e7b602ce7af77f->enter($__internal_fb007d1723bd1b2c365fd4b145c5838596f19b67b5136e20d7e7b602ce7af77f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_93c6af6c144db7b23d83e4b6cf5b32985eaf703bda0af054dd951c9b95cf7dce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93c6af6c144db7b23d83e4b6cf5b32985eaf703bda0af054dd951c9b95cf7dce->enter($__internal_93c6af6c144db7b23d83e4b6cf5b32985eaf703bda0af054dd951c9b95cf7dce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })())));
        echo "
*/
";
        
        $__internal_fb007d1723bd1b2c365fd4b145c5838596f19b67b5136e20d7e7b602ce7af77f->leave($__internal_fb007d1723bd1b2c365fd4b145c5838596f19b67b5136e20d7e7b602ce7af77f_prof);

        
        $__internal_93c6af6c144db7b23d83e4b6cf5b32985eaf703bda0af054dd951c9b95cf7dce->leave($__internal_93c6af6c144db7b23d83e4b6cf5b32985eaf703bda0af054dd951c9b95cf7dce_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.js.twig", "/Users/dp/Sites/frame-0/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
