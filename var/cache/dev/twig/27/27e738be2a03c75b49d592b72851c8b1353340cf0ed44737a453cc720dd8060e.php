<?php

/* SonataAdminBundle:CRUD:list_email.html.twig */
class __TwigTemplate_99bba95c04365bc38dda76c781707d7dcad012224f7a115fa36a3d0764343bb8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_email.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d54a9b2bfb13b07bced9ed3dcf8cc7cda60fde53fa0448bfb25941ba04af1c6a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d54a9b2bfb13b07bced9ed3dcf8cc7cda60fde53fa0448bfb25941ba04af1c6a->enter($__internal_d54a9b2bfb13b07bced9ed3dcf8cc7cda60fde53fa0448bfb25941ba04af1c6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_email.html.twig"));

        $__internal_91373c1ea5f181baa6c39f1bcd0b463c0a6f4b384c951883dda0b1bc8ff1dc1a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91373c1ea5f181baa6c39f1bcd0b463c0a6f4b384c951883dda0b1bc8ff1dc1a->enter($__internal_91373c1ea5f181baa6c39f1bcd0b463c0a6f4b384c951883dda0b1bc8ff1dc1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_email.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d54a9b2bfb13b07bced9ed3dcf8cc7cda60fde53fa0448bfb25941ba04af1c6a->leave($__internal_d54a9b2bfb13b07bced9ed3dcf8cc7cda60fde53fa0448bfb25941ba04af1c6a_prof);

        
        $__internal_91373c1ea5f181baa6c39f1bcd0b463c0a6f4b384c951883dda0b1bc8ff1dc1a->leave($__internal_91373c1ea5f181baa6c39f1bcd0b463c0a6f4b384c951883dda0b1bc8ff1dc1a_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_597c740378fa10ee33d6fa947488ddee01e527f096aa6b8865b8660c675d1a6f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_597c740378fa10ee33d6fa947488ddee01e527f096aa6b8865b8660c675d1a6f->enter($__internal_597c740378fa10ee33d6fa947488ddee01e527f096aa6b8865b8660c675d1a6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_dfe484000fd58faf0b217bc7b567848f92f083333778cdaa28e39f1547a0392a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dfe484000fd58faf0b217bc7b567848f92f083333778cdaa28e39f1547a0392a->enter($__internal_dfe484000fd58faf0b217bc7b567848f92f083333778cdaa28e39f1547a0392a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $this->loadTemplate("SonataAdminBundle:CRUD:_email_link.html.twig", "SonataAdminBundle:CRUD:list_email.html.twig", 15)->display($context);
        
        $__internal_dfe484000fd58faf0b217bc7b567848f92f083333778cdaa28e39f1547a0392a->leave($__internal_dfe484000fd58faf0b217bc7b567848f92f083333778cdaa28e39f1547a0392a_prof);

        
        $__internal_597c740378fa10ee33d6fa947488ddee01e527f096aa6b8865b8660c675d1a6f->leave($__internal_597c740378fa10ee33d6fa947488ddee01e527f096aa6b8865b8660c675d1a6f_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {% include 'SonataAdminBundle:CRUD:_email_link.html.twig' %}
{% endblock %}
", "SonataAdminBundle:CRUD:list_email.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list_email.html.twig");
    }
}
