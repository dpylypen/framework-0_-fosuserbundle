<?php

/* SonataAdminBundle:CRUD:list_percent.html.twig */
class __TwigTemplate_53971db68b608fd115fdee664134c490e1fac12f5a6b1b411b37e11a85a0bb7c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new Twig_Error_Runtime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_percent.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_356ba541088e58772973376571c4244643d073b5abb173e4bd4af69d2c01b887 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_356ba541088e58772973376571c4244643d073b5abb173e4bd4af69d2c01b887->enter($__internal_356ba541088e58772973376571c4244643d073b5abb173e4bd4af69d2c01b887_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_percent.html.twig"));

        $__internal_dee8dde207911d6477d1fc96a5460711adc6157327b8b3ad85111eef3caba949 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dee8dde207911d6477d1fc96a5460711adc6157327b8b3ad85111eef3caba949->enter($__internal_dee8dde207911d6477d1fc96a5460711adc6157327b8b3ad85111eef3caba949_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_percent.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_356ba541088e58772973376571c4244643d073b5abb173e4bd4af69d2c01b887->leave($__internal_356ba541088e58772973376571c4244643d073b5abb173e4bd4af69d2c01b887_prof);

        
        $__internal_dee8dde207911d6477d1fc96a5460711adc6157327b8b3ad85111eef3caba949->leave($__internal_dee8dde207911d6477d1fc96a5460711adc6157327b8b3ad85111eef3caba949_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_e717625fd0390155d1764c440f36a838d1b2609d23d68c03a4832f328412fb49 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e717625fd0390155d1764c440f36a838d1b2609d23d68c03a4832f328412fb49->enter($__internal_e717625fd0390155d1764c440f36a838d1b2609d23d68c03a4832f328412fb49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_792bbe7f73afd16bd439446bc20ca473b56fc4ff70361905e067c43cb7e491fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_792bbe7f73afd16bd439446bc20ca473b56fc4ff70361905e067c43cb7e491fb->enter($__internal_792bbe7f73afd16bd439446bc20ca473b56fc4ff70361905e067c43cb7e491fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["value"] = ((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()) * 100);
        // line 16
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
        echo " %
";
        
        $__internal_792bbe7f73afd16bd439446bc20ca473b56fc4ff70361905e067c43cb7e491fb->leave($__internal_792bbe7f73afd16bd439446bc20ca473b56fc4ff70361905e067c43cb7e491fb_prof);

        
        $__internal_e717625fd0390155d1764c440f36a838d1b2609d23d68c03a4832f328412fb49->leave($__internal_e717625fd0390155d1764c440f36a838d1b2609d23d68c03a4832f328412fb49_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_percent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {% set value = value * 100 %}
    {{ value }} %
{% endblock %}
", "SonataAdminBundle:CRUD:list_percent.html.twig", "/Users/dp/Sites/frame-0/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list_percent.html.twig");
    }
}
